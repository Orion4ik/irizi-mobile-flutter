// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:irizi/db/flight_events_isar.dart';
import 'package:irizi/db/flight_missions_isar.dart';

import 'package:irizi/main.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    final dir = await getApplicationSupportDirectory();
    final Connectivity connectivity = Connectivity();
    final isar = await Isar.open(
    [GeoFlightMissionIsarSchema, GeoEventIsarSchema], 
    inspector: true,
    directory: dir.path
);
    await tester.pumpWidget( MyApp(
      isar: isar,
      connectivity: connectivity,
      ));

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });
}
