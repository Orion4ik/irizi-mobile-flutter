import 'package:flutter/material.dart';

//Main Colors
const PrimaryColor = Color(0x002f80ed);
const SecondColor = Color.fromARGB(255, 18, 18, 24);
const TabColor = Color(0x00141218);
const DisableColor = Color(0x002e2f30);
const CardColorDark = Color(0xFF333333);
final FabColorDark = Colors.grey.shade900;



ColorScheme defaultColorScheme = const ColorScheme(
    primary: Color(0x002f80ed),
    secondary: Color.fromARGB(100, 20, 18, 24),
    surface: Color(0xFF1F1D1D),
    background: Color(0xFF1F1D1D),
    error: Color(0xffCF6679),
    onPrimary: Color(0xffffffff),
    onSecondary: Color(0xffffffff),
    onSurface: Color(0xffffffff),
    onBackground: Color(0xffffffff),

    onError: Color(0xffffffff),
    brightness: Brightness.dark,
    onErrorContainer: Color(0xffffffff),
    );

//Secondary Colors
const ErrorColor = Color(0xFFEB5757);
const WarningColor = Color(0xFFF2C94C);
const SuccessColor = Color.fromARGB(248, 114, 192, 69);


const String otpGifImage = "assets/gif/otp.gif";


                  
        
    
//Text


/// Flutter code sample for [SearchAnchor.bar].

