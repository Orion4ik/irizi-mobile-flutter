import 'package:flutter/material.dart';
import 'package:irizi/repo/isar_db/isar_repo.dart';
import 'package:irizi/untils/theme.dart';

import '../../model/model_all.dart';
import 'info_screens_flight_missions/flight_mission_info_all.dart';
import 'info_screens_flight_missions/flight_mission_info_events.dart';
import 'info_screens_flight_missions/flight_mission_info_materials.dart';

class FlightMissionInfo extends StatelessWidget {
  const FlightMissionInfo({super.key,

    
    required this.ID,
    required this.num,
    required this.object,
    required this.date,
    required this.eventsState,
    required this.geoJson,
    required this.landGroupID,
    required this.geoOperator,
    required this.state,
    required this.route,
    required this.routeArea,
    required this.routeLength,
    required this.flightGroupID,
    required this.flightDuration,
    required this.flightMissionType,
    required this.latLngStartCombined,
    required this.latitude,
    required this.longitude,
    required this.nearestLocality,
    required this.responsiblePerson,
    required this.technicalSupport,
    required this.updateInfo,
    required this.examinationDateFrom,
    required this.examinationDateTo,
    required this.examinationDatesCombined,
    required this.executionDeadlineDate,
    required this.executionInfo,
    required this.executionPlanDatesCombined,
    required this.executionPlanEndDate,
    required this.executionPlanStartDate,
    required this.meteorologicalData,
    required this.geoEvent,
    required this.geoEventType,
    required this.geoObject,
    required this.ubmEnum,
    required this.dictTechnicalSupport,
    required this.geoAdjustmentStatus,
    required this.geoExecutionStatus,
    required this.mi_createDate,
    required this.mi_createUser,
    required this.mi_modifyDate,
    required this.mi_modifyUser,
    required this.mi_owner,
    required this.geoOperatorL,
    required this.geoRoute,
    required this.service

    
  }); 
  
  
  
  final int? ID;
  final String? num;
  final dynamic object;
  final int? flightMissionType;
  final dynamic route ;
  final dynamic geoOperator ;
  final double? latitude ;
  final double? longitude;
  final String? latLngStartCombined;
  final String? examinationDateFrom;
  final String? examinationDateTo ;
  final String? examinationDatesCombined;
  final String? date;
  final String? executionPlanStartDate;
  final String? executionPlanEndDate;
  final String? executionPlanDatesCombined;
  final String? executionDeadlineDate;
  final String? responsiblePerson;
  final String? geoJson;
  final int? executionInfo;
  final int? updateInfo;
  final dynamic technicalSupport;
  final String? meteorologicalData;
  final String? nearestLocality;
  final String? flightDuration;
  final double? routeLength;
  final int? routeArea;
  final String? state;
  final String? eventsState;
  final int? flightGroupID;
  final int? landGroupID;
  final List<GeoRoute>? geoRoute;
  final List<GeoEvent>? geoEvent;
  final List<GeoEventType>? geoEventType;
  final List<GeoObject>? geoObject;
  final List<UbmEnum>? ubmEnum;
  final List<GeoOperator>? geoOperatorL;
  final List<GeoExecutionStatus>? geoExecutionStatus;
  final List<GeoAdjustmentStatus>? geoAdjustmentStatus;
  final int? mi_owner;
  final String? mi_createDate;
  final int? mi_createUser;
  final String? mi_modifyDate;
  final int? mi_modifyUser;
   final IsarRepository service;
   final List<DictTechnicalSupport>? dictTechnicalSupport;







  
  
  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: SecondColor,
          appBar: AppBar(
            backgroundColor: SecondColor,
            
            bottom: const TabBar(
              isScrollable: true,
              indicatorColor: Colors.blue,
              labelColor: Colors.blue,
              tabs: [
                Tab(child: Text('Загальна інформація')),
                Tab(child: Text('Інші файли')),
                Tab(child: Text('Події')),
              ],
            ),
            title: const Text('Інформація про ПЗ'),
          ),
          body:  TabBarView(
            children: [
              FlightMissionInfoAll(
                  ID: ID!,
                  date: date,
                  num: num,
                  eventsState: eventsState,
                  landGroupID: landGroupID,
                  geoJson: geoJson,
                  flightDuration: flightDuration,
                  flightGroupID: flightGroupID,
                  flightMissionType: flightMissionType,
                  latLngStartCombined: latLngStartCombined,
                  latitude: latitude,
                  longitude: longitude,
                  meteorologicalData: meteorologicalData,
                  object: object,
                  geoOperator: geoOperator,
                  route: route,
                  routeArea: routeArea ,
                  routeLength: routeLength,
                  nearestLocality: nearestLocality,
                  state: state,
                  technicalSupport: technicalSupport,
                  updateInfo: updateInfo,
                  responsiblePerson: responsiblePerson,
                  examinationDateFrom: examinationDateFrom,
                  examinationDateTo: examinationDateTo,
                  examinationDatesCombined: examinationDatesCombined,
                  executionDeadlineDate: executionDeadlineDate,
                  executionInfo: executionInfo,
                  executionPlanDatesCombined: executionPlanDatesCombined,
                  executionPlanEndDate: executionPlanEndDate,
                  executionPlanStartDate: executionPlanStartDate,
                  mi_createDate: mi_createDate,
                   mi_createUser: mi_createUser,
                  mi_modifyDate: mi_modifyDate,
                  mi_modifyUser: mi_modifyUser,
                  mi_owner: mi_owner,
                  geoEvent: geoEvent,
                  geoEventType: geoEventType,
                  geoObject: geoObject,
                  geoOperatorL: geoOperatorL,
                  ubmEnum: ubmEnum,
                  service: service,
                  dictTechnicalSupport: dictTechnicalSupport,
                  geoAdjustmentStatus: geoAdjustmentStatus,
                  geoExecutionStatus: geoExecutionStatus,
                  geoRoute: geoRoute,


              ),
              FlightMissionInfoMaterials(service:service, flightMissionID: ID),
               FlightMissionInfoEvents(geoEvent: geoEvent, geoEventType: geoEventType, geoObject: geoObject, ubmEnum: ubmEnum , geoOperator: geoOperatorL, service: service,)
            ],
          ),
        ),
      );
  }
}





