

import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_svg/svg.dart';
import 'package:irizi/bloc/db_bloc/fight_missions_bloc/db_bloc.dart';
import 'package:irizi/bloc/db_bloc/fight_missions_bloc/db_event.dart';
import 'package:irizi/bloc/network_helper_bloc/network_bloc.dart';
import 'package:irizi/bloc/network_helper_bloc/network_state.dart';
import 'package:irizi/db/flight_missions_isar.dart';
import 'package:irizi/model/model_all.dart';
import 'package:irizi/screens/FlightGroup/flight_mission_info_screen.dart';
import 'package:irizi/untils/theme.dart';
import '../../bloc/db_bloc/fight_missions_bloc/db_state.dart';
import '../../bloc/network_helper_bloc/network_event.dart';
import '../../repo/isar_db/isar_repo.dart';
import '../../service/resolving_service.dart';
import 'flight_mission_map.dart';



class FlightsTaskScreen extends StatefulWidget {
   const FlightsTaskScreen({super.key, 
    required this.title, 
    this.dictTechnicalSupport, 
    this.geoEvent, 
    this.geoEventType,
    this.geoOperator,
    this.geoObject,
    this.geoRoute,
    this.ubmEnum,
    this.geoAdjustmentStatus,
    this.geoExecutionStatus
   


    
    });




 
  final String title;
 final List<GeoEvent>? geoEvent;
 final List<GeoEventType>? geoEventType;
 final List<GeoObject>? geoObject;
 final List<UbmEnum>? ubmEnum;
 final List<GeoOperator>? geoOperator;
 final List<GeoRoute>? geoRoute;
 final List<DictTechnicalSupport>? dictTechnicalSupport;
 final List<GeoExecutionStatus>? geoExecutionStatus;
final List<GeoAdjustmentStatus>? geoAdjustmentStatus;
  
  

  @override
  State<FlightsTaskScreen> createState() => _FlightsTaskScreenState();
}








class _FlightsTaskScreenState extends State<FlightsTaskScreen> {
//final NetworkBloc _bloc = NetworkBloc();
// final GeoFlightMissionBloc _fbloc = GeoFlightMissionBloc(isarRepo: IsarRepository());

final service = IsarRepository();
 
  late  List<GeoEvent>? geoEvent = widget.geoEvent;
  late  List<GeoEventType>? geoEventType = widget.geoEventType;
  late  List<GeoObject>? geoObject = widget.geoObject;
  late  List<UbmEnum>? ubmEnum = widget.ubmEnum;
  late  List<GeoOperator>? geoOperator = widget.geoOperator;
  late  List<GeoRoute>? geoRoute = widget.geoRoute;
  late  List<DictTechnicalSupport>? dictTechnicalSupport = widget.dictTechnicalSupport;
  late List<GeoExecutionStatus>? geoExecutionStatus =widget.geoExecutionStatus;
  late List<GeoAdjustmentStatus>? geoAdjustmentStatus = widget.geoAdjustmentStatus;
  



  
  @override
  void initState() {
    super.initState();


    getGeoObjects();
    getGeoOperators();
    getGeoRoute();
    getDictTechnicalSupport();
    getgeoEventType();
    getUbmEnum();
    getGeoEvents();
    getGeoAdjustmentStatus();
    getGeoExecutionStatus();

    fetchData();
   // service.openDB();
 // context.read<GeoFlightMissionBloc>().add(GetItems())
   

  }

 

  @override
  void dispose() {
   
    super.dispose();
  }


Future <void> fetchData()  async {
    getGeoObjects().then((objects) {
      setState(() {
        geoObject = objects;
      });
    });

    getGeoOperators().then((objects) {
      setState(() {
        geoOperator = objects;
      });
    });

    getGeoRoute().then((objects) {
      setState(() {
        geoRoute = objects;
      });
    });

    getDictTechnicalSupport().then((objects) {
      setState(() {
        dictTechnicalSupport = objects;
      });
    });
    getgeoEventType().then((objects) {
      setState(() {
        geoEventType = objects;
      });
    });

    getUbmEnum().then((objects) {
      setState(() {
        ubmEnum = objects;
      });
    });

    getGeoEvents().then((objects) {
      setState(() {
        geoEvent = objects;
      });
    });

     getGeoAdjustmentStatus().then((objects) {
      setState(() {
        geoAdjustmentStatus = objects;
      });
    });

    getGeoExecutionStatus().then((objects) {
      setState(() {
        geoExecutionStatus = objects;
      });
    });
  }


  

  Future<List<GeoObject>> getGeoObjects() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoObjectFromIsar();
  }
  Future<List<GeoOperator>> getGeoOperators() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoOperatorFromIsar();
  }
  Future<List<GeoRoute>> getGeoRoute() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoRouteFromIsar();
  }
  Future<List<DictTechnicalSupport>> getDictTechnicalSupport() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getdictTechnicalSupportFromIsar();
  }
  Future<List<GeoEventType>> getgeoEventType() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoEventTypeFromIsar();
  }

Future<List<UbmEnum>> getUbmEnum() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getubmEnumFromIsar();
  }

  Future<List<GeoEvent>> getGeoEvents() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoEventFromIsar();
  }

  Future<List<GeoAdjustmentStatus>> getGeoAdjustmentStatus() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoAdjustmentStatusFromIsar();
  }

   Future<List<GeoExecutionStatus>> getGeoExecutionStatus() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoExecutionStatusFromIsar();
  }

  


  
  @override
  Widget build(BuildContext context) {
   
  
    
    return  
    
    
     BlocConsumer<NetworkBloc, NetworkState>(
      listener: (context, state) {
        if (state.isConnected){
             ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Підключення до мережі відновлено',
          style: TextStyle(color: Colors.white)
          )));
        }

        else {
             ScaffoldMessenger.of(context)
               .showSnackBar(SnackBar(
          content: Text(
            'Додаток працює в оффлайн-режимі',
            style: const TextStyle(color: Colors.white),
            ),
          backgroundColor: Colors.blue ,
          ));
        }
      },
      builder: (context, state) {
        
        if (state.isConnected){
          return   
           Scaffold(
                  backgroundColor: SecondColor,
                  floatingActionButton: FloatingActionButton(
                      heroTag: "syncDataFlightMissions",         
                      backgroundColor: Colors.blue,
                      child: const Icon(Icons.sync),
                      onPressed: () {
                 
                       context.read<NetworkBloc>().add(CheckConnectivity());
                                   
                      
                      }),
                  
                  body:        CustomScrollView(
                                     
                            slivers:<Widget> [
                                     
                            
                                     
                              
                                     
                              
                                     
                              SliverAppBar(
                                     
                                  
                                     
                                //pinned: true,
                                     
                                //floating: false,
                                     
                                expandedHeight: 16,
                                     
                                backgroundColor: SecondColor,
                                     
                                title: const Text('Реєстр польотних завдань', style: TextStyle(fontSize: 16),),
                                     
                                actions: <Widget>[
                                     
                                  FloatingActionButton(
                                     
                                    heroTag: "btn2",         
                                     
                                    backgroundColor: FabColorDark,
                                     
                                    onPressed: () {
                                 
                                     
                                     
                                    },
                                     
                                    child:  const Icon(Icons.search)
                                     
                                               ),
                                     
                                               const SizedBox(width: 8,),
                                     
                                               FloatingActionButton(
                                     
                                 
                                     
                                    heroTag: "btn3",         
                                     
                                    backgroundColor: FabColorDark,
                                     
                                    onPressed: () {
                                     
                                     
                                     
                                    },
                                     
                                    child: const Icon(
                                     
                                      IconlyLight.filter2,
                                     
                                      color: Colors.white,
                                     
                                    ),
                                     
                                               ),
                                     
                                ],
                                     
                              
                                     
                                 
                                     
                              ),
                                     
                              SliverList(
                                     
                                
                                     
                                delegate: SliverChildListDelegate(
                                     
                                  
                                     
                                  <Widget>[
                                     
                                  
                                     
                                    
                                     
                                   
                                     
                                  StreamBuilder<List<GeoFlightMissionIsar>>(
                                     
                                      
                                     
                                      stream: service.geoFlightMisiionsStream(),
                                     
                                      builder: (context, snapshot) {
                                     
                                        if(snapshot.hasData){
                                     
                                          Resolver resolver = Resolver('');
                                     
                                          
                                     
                                         List<GeoFlightMissionIsar>  geoFlightMission = snapshot.data!;
                                     
                                          return SizedBox(
                                     
                                            height:  MediaQuery.of(context).size.height * 0.8,
                                     
                                            child: ListView.builder(
                                     
                                             
                                     
                                                              itemCount: geoFlightMission.length,
                                     
                                                           
                                     
                                                              itemBuilder: (context, index) {
                                     
                                                               
                                     
                                                               // flightMissionsIsar = all.geoFlightMission!.cast<GeoFlightMissionIsar>();
                                     
                                                               // geoFlightMissionService.addGeoFlightMissions(flightMissionsIsar!);
                                     
                                                                return GestureDetector(
                                     
                                                                  
                                     
                                                                  onTap: () {
                                     
                                                                    Navigator.of(context).push(
                                     
                                          MaterialPageRoute(
                                     
                                            builder: (context) => FlightMissionInfo(
                                     
                                                                         ID: geoFlightMission[index].ID,
                                     
                                    date: geoFlightMission[index].date ?? '-',
                                     
                                    num: geoFlightMission[index].num ?? '-',
                                     
                                    eventsState: geoFlightMission[index].eventsState,
                                     
                                    landGroupID: geoFlightMission[index].landGroupID,
                                     
                                    geoJson: geoFlightMission[index].geoJson,
                                     
                                    flightDuration: geoFlightMission[index].flightDuration ?? '0',
                                     
                                    flightGroupID: geoFlightMission[index].flightGroupID,
                                     
                                    flightMissionType: geoFlightMission[index].flightMissionType,
                                     
                                    latLngStartCombined:geoFlightMission[index]. latLngStartCombined ?? '-',
                                     
                                    latitude: geoFlightMission[index].latitude ?? 50.44073286148048,
                                     
                                    longitude: geoFlightMission[index].longitude ?? 30.575602096138812,
                                     
                                    meteorologicalData: geoFlightMission[index].meteorologicalData ?? '-',
                                     
                                    object:  resolver.resolveData(geoFlightMission[index].object, geoObject, displayKey: 'name'),
                                     
                                
                                     
                                    geoOperator: 
                                     
                                    resolver.resolveData(geoFlightMission[index].operator, geoOperator, displayKey: 'name') ,
                                     
                                  
                                     
                                    route:resolver.resolveData(geoFlightMission[index].route, geoRoute, displayKey: 'name') ,
                                     
                                  
                                     
                                    routeArea: geoFlightMission[index].routeArea ?? 0,
                                     
                                    routeLength: geoFlightMission[index].routeLength ?? 0,
                                     
                                    nearestLocality: geoFlightMission[index].nearestLocality ?? '-',
                                     
                                    state: resolver.resolveData(geoFlightMission[index].state, ubmEnum, displayKey: 'name', valueKey: 'code').toString(),
                                     
                                         
                                     
                                    technicalSupport: resolver.resolveData(geoFlightMission[index].technicalSupport, dictTechnicalSupport, displayKey: 'name', valueKey: 'ID'),
                                     
                                    updateInfo: geoFlightMission[index].updateInfo,
                                     
                                    responsiblePerson: geoFlightMission[index].responsiblePerson ?? '-',
                                     
                                    examinationDateFrom:geoFlightMission[index].examinationDateFrom ,
                                     
                                    examinationDateTo: geoFlightMission[index].examinationDateTo  ,
                                     
                                    examinationDatesCombined: geoFlightMission[index].examinationDatesCombined ,
                                     
                                    executionDeadlineDate: geoFlightMission[index].executionDeadlineDate ,
                                     
                                    executionInfo: geoFlightMission[index].executionInfo ,
                                     
                                    executionPlanDatesCombined: geoFlightMission[index].executionPlanDatesCombined  ,
                                     
                                    executionPlanEndDate: geoFlightMission[index].executionPlanEndDate ,
                                     
                                    executionPlanStartDate: geoFlightMission[index].executionPlanStartDate  ,
                                     
                                    geoEvent: resolver.getEventsByFlightTaskID(geoEvent, geoFlightMission[index].ID!) ,
                                     
                                    geoEventType: geoEventType,
                                     
                                    geoObject:geoObject,
                                     
                                    ubmEnum: ubmEnum,
                                     
                                    geoOperatorL: geoOperator,

                                    dictTechnicalSupport: dictTechnicalSupport,
                                     
                                   mi_createDate: geoFlightMission[index].miCreateDate,
                                     
                                  mi_createUser: geoFlightMission[index].miCreateUser,
                                     
                                   mi_modifyDate: geoFlightMission[index].miModifyDate,
                                     
                                   mi_modifyUser: geoFlightMission[index].miModifyUser,
                                     
                                  mi_owner: geoFlightMission[index].miOwner,
                                  service:  service,
                                  geoAdjustmentStatus: geoAdjustmentStatus,
                                  geoExecutionStatus: geoExecutionStatus,
                                  geoRoute: geoRoute,
                                     
                                                                )));
                                     
                                                                },
                                     
                                                                  
                                     
                                                                    
                                     
                                                                    child: 
                                                                    
                                                                    
                                                                    Container(
                                     
                                                                                                        margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                     
                                                                                                         height:  MediaQuery.of(context).size.height *0.53,
                                     
                                                                                                         //MediaQuery.sizeOf(context).height*0.3,
                                     
                                                                                                         //         width: MediaQuery.sizeOf(context).width,
                                     
                                                                                                        decoration: BoxDecoration(
                                     
                                                                                                            color: FabColorDark,
                                     
                                                                                                            borderRadius: const BorderRadius.all(Radius.circular(10)),
                                     
                                                                                                            
                                     
                                                                                                          ),
                                     
                                                                                                          child: 
                                                                                                        Column(
                                     
                                                                                                            children: [
                                     
                                                                                            
                                     
                                                                    Padding(
                                     
                                                                                            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                                     
                                                                                            child: Row(
                                     
                                                                                              
                                     
                                                                                             crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                     
                                                                                            
                                     
                                                                                                children: [
                                     
                                                                                                
                                     
                                                                                             
                                     
                                                                                              Row(
                                     
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                children: [  
                                     
                                                                                                Container(
                                     
                                                                                            margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                     
                                                                                             width: 12,
                                     
                                                                                             height: 12,
                                     
                                                                                             decoration: const BoxDecoration(
                                     
                                                                                               color: SuccessColor,
                                     
                                                                                               shape: BoxShape.circle
                                     
                                                                                             ),
                                     
                                                                                                ),
                                     
                                                                                                Padding(
                                     
                                                                                                  padding: const EdgeInsets.only(top: 12),
                                     
                                                                                                  child: Text(
                                     
                                                                                                     resolver.resolveData(geoFlightMission[index].state, ubmEnum, displayKey: 'name', valueKey: 'code')
                                     
                                                                                                    
                                     
                                                                                                    
                                     
                                                                                                    )),
                                     
                                  
                                     
                                                                                                ]),
                                     
                                                                                                
                                     
                                  
                                     
                                  
                                     
                                                                                              Container(
                                     
                                                                                               
                                     
                                                                                                padding: const EdgeInsets.only(right: 8),
                                     
                                                                                                child: FilledButton(
                                     
                                                                                             
                                     
                                                                                             onPressed: (){
                                     
                                                                                               Navigator.of(context).push(
                                     
                                                                                                       MaterialPageRoute(
                                     
                                                                                                        builder: (context) => FlightMissionMapScreen(
                                     
                                                                                                               ID: geoFlightMission[index].ID!,
                                     
                                    date: geoFlightMission[index].date,
                                     
                                    num: geoFlightMission[index].num ?? '-',
                                     
                                    eventsState: geoFlightMission[index].eventsState,
                                     
                                    landGroupID: geoFlightMission[index].landGroupID,
                                     
                                    geoJson: geoFlightMission[index].geoJson,
                                     
                                    flightDuration: geoFlightMission[index].flightDuration ?? '0',
                                     
                                    flightGroupID: geoFlightMission[index].flightGroupID,
                                     
                                    flightMissionType: geoFlightMission[index].flightMissionType,
                                     
                                    latLngStartCombined:geoFlightMission[index]. latLngStartCombined ?? '-',
                                     
                                    latitude: geoFlightMission[index].latitude ?? 0.0,
                                     
                                    longitude: geoFlightMission[index].longitude ?? 0.0,
                                     
                                    meteorologicalData: geoFlightMission[index].meteorologicalData ?? '-',
                                     
                                    object:  resolver.resolveData(geoFlightMission[index].object, geoObject, displayKey: 'name'),
                                     
                                
                                     
                                    geoOperator: 
                                     
                                    resolver.resolveData(geoFlightMission[index].operator, geoOperator, displayKey: 'name') ?? '-',
                                     
                                  
                                     
                                    route: resolver.resolveData(geoFlightMission[index].route, geoRoute, displayKey: 'name') ?? '-',
                                     
                                    routeArea: geoFlightMission[index].routeArea ?? 0,
                                     
                                    routeLength: geoFlightMission[index].routeLength ?? 0.0,
                                     
                                    nearestLocality: geoFlightMission[index].nearestLocality ?? '-',
                                     
                                    state: resolver.resolveData(geoFlightMission[index].state, ubmEnum, displayKey: 'name', valueKey: 'code').toString(),
                                     
                                         
                                     
                                    technicalSupport: resolver.resolveData(geoFlightMission[index].technicalSupport, dictTechnicalSupport, displayKey: 'name', valueKey: 'ID'),
                                     
                                    updateInfo: geoFlightMission[index].updateInfo,
                                     
                                    responsiblePerson: geoFlightMission[index].responsiblePerson ?? '-',
                                     
                                    examinationDateFrom:geoFlightMission[index].examinationDateFrom ,
                                     
                                    examinationDateTo: geoFlightMission[index].examinationDateTo ,
                                     
                                    examinationDatesCombined: geoFlightMission[index].examinationDatesCombined ,
                                     
                                    executionDeadlineDate: geoFlightMission[index].executionDeadlineDate ,
                                     
                                    executionInfo: geoFlightMission[index].executionInfo,
                                     
                                    executionPlanDatesCombined: geoFlightMission[index].executionPlanDatesCombined ,
                                     
                                    executionPlanEndDate: geoFlightMission[index].executionPlanEndDate,
                                     
                                    executionPlanStartDate: geoFlightMission[index].executionPlanStartDate ,
                                     
                                    geoEvent: resolver.getEventsByFlightTaskID(geoEvent, geoFlightMission[index].ID!),
                                     
                                    geoEventType:geoEventType,
                                     
                                    geoObject:geoObject,
                                     
                                    ubmEnum: ubmEnum,
                                     
                                    geoOperatorL: geoOperator,
                                     
                                  
                                     
                                   mi_createDate: geoFlightMission[index].miCreateDate,
                                     
                                   mi_createUser: geoFlightMission[index].miCreateUser,
                                     
                                   mi_modifyDate: geoFlightMission[index].miModifyDate,
                                     
                                    mi_modifyUser: geoFlightMission[index].miModifyUser,
                                     
                                    mi_owner: geoFlightMission[index].miOwner,
                                    service:  service,
                                  
                                     
                                                                                                         
                                     
                                                                                                        ),
                                     
                                                                                                       ));
                                     
                                                                                             },
                                     
                                                                                             style: ButtonStyle(
                                     
                                                                                               minimumSize: MaterialStateProperty.all(const Size(30, 40)) ,
                                     
                                                                                                   backgroundColor: MaterialStateProperty.all(
                                     
                                                                                                       Colors.grey.shade800),
                                     
                                                                                                  
                                     
                                                                                                      
                                     
                                                                                                       
                                     
                                                                                                   shape: MaterialStateProperty.all<
                                     
                                                                                                           RoundedRectangleBorder>(
                                     
                                                                                                       RoundedRectangleBorder(
                                     
                                                                                                           borderRadius:
                                     
                                                                                                               BorderRadius.circular(16.0)))),
                                     
                                                                                             child: const Center(child: Icon(IconlyLight.location)),
                                     
                                                                                            
                                     
                                                                                             )
                                     
                                                                                             
                                     
                                                                                                
                                     
                                                                                              ),
                                     
                                                                                            
                                     
                                                                                              ]),
                                     
                                                                    ),
                                     
                                                                    
                                     
                                                                    Container(
                                     
                                                                                            
                                     
                                                                                            padding: const EdgeInsets.only(left: 16),
                                     
                                                                                            child: 
                                     
                                                                                            Row(children: [
                                     
                                                                    
                                     
                                                                                              Container(
                                     
                                                                                                child: Column(
                                     
                                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                  children: [
                                     
                                                                      
                                     
                                                                                                  const AutoSizeText('Номер ПЗ',
                                     
                                                                                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                     
                                                                                                ),
                                     
                                                                        
                                     
                                                                       // SizedBox(height: 8,),
                                     
                                                                        Text(geoFlightMission[index].num.toString(),
                                     
                                                                        maxLines: 2,
                                     
                                                                                                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                                ),
                                     
                                                                                                ),
                                     
                                                                      
                                     
                                                                                                ],),
                                     
                                                                                              ),
                                     
                                                                                              
                                     
                                                                                              Container(
                                     
                                                                                            padding: const EdgeInsets.only(left: 16),
                                     
                                                                                            child: Row(children: [
                                     
                                                                    
                                     
                                                                                              Column(
                                     
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                children: [
                                     
                                                                    
                                     
                                                                                                const AutoSizeText('Дата створення',
                                     
                                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                     
                                                                                              ),
                                     
                                                                                            
                                     
                                                                     // SizedBox(height: 8,),
                                     
                                                                                            Text(
                                     
                                                                                              resolver.formatDateTime(geoFlightMission[index].executionPlanStartDate),
                                     
                                                                                             
                                     
                                                                                             // resolver.resolveData(all.geoFlightMission![index].geoOperator, all.geoOperator, displayKey: 'name').toString(),
                                     
                                                                                             
                                     
                                                                                            maxLines: 2,
                                     
                                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                              ),
                                     
                                                                                              ),
                                     
                                                                    
                                     
                                                                                              ],)
                                     
                                                                    
                                     
                                                                                            ],), 
                                     
                                                                    ),
                                     
                                                                                            ],),
                                     
                                                                                            
                                     
                                                                    ),
                                     
                                                                                            
                                     
                                                                    Container(
                                     
                                                                                            padding: const EdgeInsets.only(left: 16, top: 16),
                                     
                                                                                            child: 
                                     
                                                                                            Row(children: [
                                     
                                                                    
                                     
                                                                                              Container(
                                     
                                                                                                child: Column(
                                     
                                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                  children: [
                                     
                                                                      
                                     
                                                                                                  const AutoSizeText('Замовник',
                                     
                                                                                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                     
                                                                                                ),
                                     
                                                                        
                                     
                                                                       // SizedBox(height: 8,),
                                     
                                                                        AutoSizeText(
                                     
                                                                          
                                     
                                                                          resolver.resolveData(geoFlightMission[index].operator, geoOperator, displayKey: 'name'),
                                     
                                                                          //all.geoFlightMission[index].object.toString(),
                                     
                                                                        maxLines: 2,
                                     
                                                                                                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                                ),
                                     
                                                                                                ),
                                     
                                                                      
                                     
                                                                                                ],),
                                     
                                                                                              ),
                                     
                                                                    
                                     
                                                                                              
                                     
                                                                                            ],),
                                     
                                                                                            
                                     
                                                                    ),
                                     
                                                                    Container(
                                     
                                                                                            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                     
                                                                                            child: Row(children: [
                                     
                                                                    
                                     
                                                                                              Column(
                                     
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                children: [
                                     
                                                                    
                                     
                                                                                                const AutoSizeText("Об'єкт",
                                     
                                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                     
                                                                                              ),
                                     
                                                                                            
                                     
                                                                                                           
                                     
                                                                                            Text(
                                     
                                                                                              
                                     
                                                                                              resolver.resolveData(geoFlightMission[index].object, geoObject, displayKey: 'name').toString(),
                                     
                                                                                            maxLines: 2,
                                     
                                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                              ),
                                     
                                                                                              ),
                                     
                                                                    
                                     
                                                                                              ],)
                                     
                                                                    
                                     
                                                                                            ],), 
                                     
                                                                    ),
                                     
                                  
                                     
                                                                    Container(
                                     
                                                                                            padding: const EdgeInsets.only(left: 16),
                                     
                                                                                            child: Row(children: [
                                     
                                                                    
                                     
                                                                                              Column(
                                     
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                children: [
                                     
                                                                    
                                     
                                                                                                const AutoSizeText("Відповідальна особа",
                                     
                                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                     
                                                                                              ),
                                     
                                                                                            
                                     
                                                                                                           
                                     
                                                                                            Text(
                                     
                                                                                              
                                     
                                                                                              geoFlightMission[index].responsiblePerson ?? '-',
                                     
                                                                                            maxLines: 2,
                                     
                                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                              ),
                                     
                                                                                              ),
                                     
                                                                    
                                     
                                                                                              ],)
                                     
                                                                    
                                     
                                                                                            ],), 
                                     
                                                                    ),
                                     
                                                                     Container(
                                                                                          alignment: Alignment.centerLeft,
                                                                                            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                     
                                                                                            child: 
                                     
                                                                                          
                                     
                                                                    
                                     
                                                                                            
                                     
                                                                                                 Wrap(
                                     
                                                                                                  alignment: WrapAlignment.start,
                                     
                                                                                                  spacing:8.0,
                                     
                                                                                                  direction: Axis.horizontal,
                                     
                                                                                                  runSpacing: 8.0,
                                     
                                                                                                  crossAxisAlignment: WrapCrossAlignment.start,
                                     
                                                                                                  textDirection: TextDirection.ltr,
                                     
                                                                                                  verticalDirection: VerticalDirection.down,
                                     
                                                                                                  children: [
                                     
                                                                                                    SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.white),
                                     
                                                                                                    const SizedBox(width: 8,),
                                     
                                                                                                   Column(
                                     
                                                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                    children: [
                                     
                                                                       
                                     
                                                                                                    const AutoSizeText('Технічні засоби',
                                     
                                                                                                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                                     
                                                                                                  ),
                                     
                                                                          
                                     
                                                                         // SizedBox(height: 8,),
                                     
                                                                          Text(
                                     
                                                                            resolver.resolveData(geoFlightMission[index].technicalSupport, dictTechnicalSupport, displayKey: 'name').toString(),
                                     
                                                                            //all.geoFlightMission[index].technicalSupport.toString(),
                                     
                                                                          maxLines: 2,
                                     
                                                                                                  style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                                  ),
                                     
                                                                                                  ),
                                     
                                                                        
                                     
                                                                                                  ],),
                                     
                                                                                            ]),
                                     
                                                                                              ),
                                     
                                                                                             
                                     
                                                                                              
                                     
                                                                                            
                                     
                                                                                            
                                     
                                                                    
                                     
                                                                                            
                                     
                                                                                                            ],
                                     
                                                                                                          ),
                                                                                                        
                                                                                            
                                     
                                                                                                      ),
                                     
                                                                  
                                     
                                                                );
                                     
                                              
                                     
                                                              
                                     
                                        },),
                                     
                                          );
                                     
                                  
                                     
                                  
                                     
                                        } else {
                                     
                                          return const Center(child: CircularProgressIndicator());
                                     
                                        }
                                     
                                        
                                     
                                      },
                                     
                                  
                                     
                                    )
                                     
                                 
                                     
                                  ]
                                     
                            
                                     
                            
                                     
                                
                                     
                               
                                     
                               
                                     
                                  
                                     
                                )
                                     
                                  
                                     
                              
                                     
                                
                                     
                               
                                     
                               
                                     
                              )
                                     
                            ],
                                     
                                         
                                     
                                         )
                  
                  
                 );
          /*
          BlocConsumer<GeoFlightMissionBloc, GeoFlightMissionState>(
          
            listener: (context, state) {
               if (state is ItemLoading) {
                 ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Завантаження', style: TextStyle(color: Colors.white))));
            }
            if (state is ItemSaveToDB) {
                 ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Зберігання до локальної бази даних', style: TextStyle(color: Colors.white))));
            }
            if (state is ItemConvertation) {
                 ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Конвертація ', style: TextStyle(color: Colors.white))));
            }
          
             if (state is ItemError) {
          ScaffoldMessenger.of(context)
               .showSnackBar(SnackBar(content:
                 Text(state.error, style: const TextStyle(color: Colors.white)
               ),
               backgroundColor:  ErrorColor,
            ));
            }
              
            },
          
            builder: (context, state) {
              if (state is ItemLoading){
                  return Center (child:  CircularProgressIndicator(),);
              }
          
              if (state is ItemSaveToDB) {
                  return Center (child:  CircularProgressIndicator(),);
              }
          
              if (state is ItemConvertation){
                return Center(child:  CircularProgressIndicator(),);
              }
              
              if (state is ItemLoaded) {
                return   Scaffold(
                  backgroundColor: SecondColor,
                  floatingActionButton: FloatingActionButton(
                      heroTag: "syncDataFlightMissions",         
                      backgroundColor: Colors.blue,
                      child: const Icon(Icons.sync),
                      onPressed: () {
                 
                       context.read<NetworkBloc>().add(CheckConnectivity());
                                   
                      
                      }),
                  
                  body:        CustomScrollView(
                                     
                            slivers:<Widget> [
                                     
                            
                                     
                              
                                     
                              
                                     
                              SliverAppBar(
                                     
                                  
                                     
                                //pinned: true,
                                     
                                //floating: false,
                                     
                                expandedHeight: 16,
                                     
                                backgroundColor: SecondColor,
                                     
                                title: const Text('Реєстр польотних завдань', style: TextStyle(fontSize: 16),),
                                     
                                actions: <Widget>[
                                     
                                  FloatingActionButton(
                                     
                                    heroTag: "btn2",         
                                     
                                    backgroundColor: FabColorDark,
                                     
                                    onPressed: () {
                                 
                                     
                                     
                                    },
                                     
                                    child:  const Icon(Icons.search)
                                     
                                               ),
                                     
                                               const SizedBox(width: 8,),
                                     
                                               FloatingActionButton(
                                     
                                 
                                     
                                    heroTag: "btn3",         
                                     
                                    backgroundColor: FabColorDark,
                                     
                                    onPressed: () {
                                     
                                     
                                     
                                    },
                                     
                                    child: const Icon(
                                     
                                      IconlyLight.filter2,
                                     
                                      color: Colors.white,
                                     
                                    ),
                                     
                                               ),
                                     
                                ],
                                     
                              
                                     
                                 
                                     
                              ),
                                     
                              SliverList(
                                     
                                
                                     
                                delegate: SliverChildListDelegate(
                                     
                                  
                                     
                                  <Widget>[
                                     
                                  
                                     
                                    
                                     
                                   
                                     
                                  StreamBuilder<List<GeoFlightMissionIsar>>(
                                     
                                      
                                     
                                      stream: service.geoFlightMisiionsStream(),
                                     
                                      builder: (context, snapshot) {
                                     
                                        if(snapshot.hasData){
                                     
                                          Resolver resolver = Resolver('');
                                     
                                          
                                     
                                         List<GeoFlightMissionIsar>  geoFlightMission = snapshot.data!;
                                     
                                          return SizedBox(
                                     
                                            height:  MediaQuery.of(context).size.height * 0.8,
                                     
                                            child: ListView.builder(
                                     
                                             
                                     
                                                              itemCount: geoFlightMission.length,
                                     
                                                           
                                     
                                                              itemBuilder: (context, index) {
                                     
                                                               
                                     
                                                               // flightMissionsIsar = all.geoFlightMission!.cast<GeoFlightMissionIsar>();
                                     
                                                               // geoFlightMissionService.addGeoFlightMissions(flightMissionsIsar!);
                                     
                                                                return GestureDetector(
                                     
                                                                  
                                     
                                                                  onTap: () {
                                     
                                                                    Navigator.of(context).push(
                                     
                                          MaterialPageRoute(
                                     
                                            builder: (context) => FlightMissionInfo(
                                     
                                                                         ID: geoFlightMission[index].ID,
                                     
                                    date: geoFlightMission[index].date ?? '-',
                                     
                                    num: geoFlightMission[index].num ?? '-',
                                     
                                    eventsState: geoFlightMission[index].eventsState,
                                     
                                    landGroupID: geoFlightMission[index].landGroupID,
                                     
                                    geoJson: geoFlightMission[index].geoJson,
                                     
                                    flightDuration: geoFlightMission[index].flightDuration ?? '0',
                                     
                                    flightGroupID: geoFlightMission[index].flightGroupID,
                                     
                                    flightMissionType: geoFlightMission[index].flightMissionType,
                                     
                                    latLngStartCombined:geoFlightMission[index]. latLngStartCombined ?? '-',
                                     
                                    latitude: geoFlightMission[index].latitude ?? 0.0,
                                     
                                    longitude: geoFlightMission[index].longitude ?? 0.0,
                                     
                                    meteorologicalData: geoFlightMission[index].meteorologicalData ?? '-',
                                     
                                    object:  resolver.resolveData(geoFlightMission[index].object, geoObject, displayKey: 'name'),
                                     
                                
                                     
                                    geoOperator: 
                                     
                                    resolver.resolveData(geoFlightMission[index].operator, geoOperator, displayKey: 'name') ,
                                     
                                  
                                     
                                    route:resolver.resolveData(geoFlightMission[index].route, geoRoute, displayKey: 'name') ,
                                     
                                  
                                     
                                    routeArea: geoFlightMission[index].routeArea ?? 0,
                                     
                                    routeLength: geoFlightMission[index].routeLength ?? 0,
                                     
                                    nearestLocality: geoFlightMission[index].nearestLocality ?? '-',
                                     
                                    state: resolver.resolveData(geoFlightMission[index].state, ubmEnum, displayKey: 'name', valueKey: 'code').toString(),
                                     
                                         
                                     
                                    technicalSupport: resolver.resolveData(geoFlightMission[index].technicalSupport, dictTechnicalSupport, displayKey: 'name', valueKey: 'ID'),
                                     
                                    updateInfo: geoFlightMission[index].updateInfo,
                                     
                                    responsiblePerson: geoFlightMission[index].responsiblePerson ?? '-',
                                     
                                    examinationDateFrom:geoFlightMission[index].examinationDateFrom ,
                                     
                                    examinationDateTo: geoFlightMission[index].examinationDateTo  ,
                                     
                                    examinationDatesCombined: geoFlightMission[index].examinationDatesCombined ,
                                     
                                    executionDeadlineDate: geoFlightMission[index].executionDeadlineDate ,
                                     
                                    executionInfo: geoFlightMission[index].executionInfo ,
                                     
                                    executionPlanDatesCombined: geoFlightMission[index].executionPlanDatesCombined  ,
                                     
                                    executionPlanEndDate: geoFlightMission[index].executionPlanEndDate ,
                                     
                                    executionPlanStartDate: geoFlightMission[index].executionPlanStartDate  ,
                                     
                                    geoEvent: resolver.getEventsByFlightTaskID(geoEvent, geoFlightMission[index].ID!) ,
                                     
                                    geoEventType: geoEventType,
                                     
                                    geoObject:geoObject,
                                     
                                    ubmEnum: ubmEnum,
                                     
                                    geoOperatorL: geoOperator,
                                     
                                   mi_createDate: geoFlightMission[index].miCreateDate,
                                     
                                  mi_createUser: geoFlightMission[index].miCreateUser,
                                     
                                   mi_modifyDate: geoFlightMission[index].miModifyDate,
                                     
                                   mi_modifyUser: geoFlightMission[index].miModifyUser,
                                     
                                  mi_owner: geoFlightMission[index].miOwner,
                                     
                                                                )));
                                     
                                                                },
                                     
                                                                  child: Container(
                                     
                                                                    
                                     
                                                                    child: Container(
                                     
                                                                                                        margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                     
                                                                                                         height:  MediaQuery.of(context).size.height *0.55,
                                     
                                                                                                         //MediaQuery.sizeOf(context).height*0.3,
                                     
                                                                                                         //         width: MediaQuery.sizeOf(context).width,
                                     
                                                                                                        decoration: BoxDecoration(
                                     
                                                                                                            color: FabColorDark,
                                     
                                                                                                            borderRadius: const BorderRadius.all(Radius.circular(10)),
                                     
                                                                                                            
                                     
                                                                                                          ),
                                     
                                                                                                          child: Column(
                                     
                                                                                                            children: [
                                     
                                                                                            
                                     
                                                                    Padding(
                                     
                                                                                            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                                     
                                                                                            child: Row(
                                     
                                                                                              
                                     
                                                                                             crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                     
                                                                                            
                                     
                                                                                                children: [
                                     
                                                                                                
                                     
                                                                                             
                                     
                                                                                              Row(
                                     
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                children: [  
                                     
                                                                                                Container(
                                     
                                                                                            margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                     
                                                                                             width: 12,
                                     
                                                                                             height: 12,
                                     
                                                                                             decoration: const BoxDecoration(
                                     
                                                                                               color: SuccessColor,
                                     
                                                                                               shape: BoxShape.circle
                                     
                                                                                             ),
                                     
                                                                                                ),
                                     
                                                                                                Padding(
                                     
                                                                                                  padding: const EdgeInsets.only(top: 12),
                                     
                                                                                                  child: Text(
                                     
                                                                                                     resolver.resolveData(geoFlightMission[index].state, ubmEnum, displayKey: 'name', valueKey: 'code')
                                     
                                                                                                    
                                     
                                                                                                    
                                     
                                                                                                    )),
                                     
                                  
                                     
                                                                                                ]),
                                     
                                                                                                
                                     
                                  
                                     
                                  
                                     
                                                                                              Container(
                                     
                                                                                               
                                     
                                                                                                padding: const EdgeInsets.only(right: 8),
                                     
                                                                                                child: FilledButton(
                                     
                                                                                             
                                     
                                                                                             onPressed: (){
                                     
                                                                                               Navigator.of(context).push(
                                     
                                                                                                       MaterialPageRoute(
                                     
                                                                                                        builder: (context) => FlightMissionMapScreen(
                                     
                                                                                                               ID: geoFlightMission[index].ID!,
                                     
                                    date: geoFlightMission[index].date,
                                     
                                    num: geoFlightMission[index].num ?? '-',
                                     
                                    eventsState: geoFlightMission[index].eventsState,
                                     
                                    landGroupID: geoFlightMission[index].landGroupID,
                                     
                                    geoJson: geoFlightMission[index].geoJson,
                                     
                                    flightDuration: geoFlightMission[index].flightDuration ?? '0',
                                     
                                    flightGroupID: geoFlightMission[index].flightGroupID,
                                     
                                    flightMissionType: geoFlightMission[index].flightMissionType,
                                     
                                    latLngStartCombined:geoFlightMission[index]. latLngStartCombined ?? '-',
                                     
                                    latitude: geoFlightMission[index].latitude ?? 0.0,
                                     
                                    longitude: geoFlightMission[index].longitude ?? 0.0,
                                     
                                    meteorologicalData: geoFlightMission[index].meteorologicalData ?? '-',
                                     
                                    object:  resolver.resolveData(geoFlightMission[index].object, geoObject, displayKey: 'name'),
                                     
                                
                                     
                                    geoOperator: 
                                     
                                    resolver.resolveData(geoFlightMission[index].operator, geoOperator, displayKey: 'name') ?? '-',
                                     
                                  
                                     
                                    route: resolver.resolveData(geoFlightMission[index].route, geoRoute, displayKey: 'name') ?? '-',
                                     
                                    routeArea: geoFlightMission[index].routeArea ?? 0,
                                     
                                    routeLength: geoFlightMission[index].routeLength ?? 0.0,
                                     
                                    nearestLocality: geoFlightMission[index].nearestLocality ?? '-',
                                     
                                    state: resolver.resolveData(geoFlightMission[index].state, ubmEnum, displayKey: 'name', valueKey: 'code').toString(),
                                     
                                         
                                     
                                    technicalSupport: resolver.resolveData(geoFlightMission[index].technicalSupport, dictTechnicalSupport, displayKey: 'name', valueKey: 'ID'),
                                     
                                    updateInfo: geoFlightMission[index].updateInfo,
                                     
                                    responsiblePerson: geoFlightMission[index].responsiblePerson ?? '-',
                                     
                                    examinationDateFrom:geoFlightMission[index].examinationDateFrom ,
                                     
                                    examinationDateTo: geoFlightMission[index].examinationDateTo ,
                                     
                                    examinationDatesCombined: geoFlightMission[index].examinationDatesCombined ,
                                     
                                    executionDeadlineDate: geoFlightMission[index].executionDeadlineDate ,
                                     
                                    executionInfo: geoFlightMission[index].executionInfo,
                                     
                                    executionPlanDatesCombined: geoFlightMission[index].executionPlanDatesCombined ,
                                     
                                    executionPlanEndDate: geoFlightMission[index].executionPlanEndDate,
                                     
                                    executionPlanStartDate: geoFlightMission[index].executionPlanStartDate ,
                                     
                                    geoEvent: resolver.getEventsByFlightTaskID(geoEvent, geoFlightMission[index].ID!),
                                     
                                    geoEventType:geoEventType,
                                     
                                    geoObject:geoObject,
                                     
                                    ubmEnum: ubmEnum,
                                     
                                    geoOperatorL: geoOperator,
                                     
                                  
                                     
                                   mi_createDate: geoFlightMission[index].miCreateDate,
                                     
                                   mi_createUser: geoFlightMission[index].miCreateUser,
                                     
                                   mi_modifyDate: geoFlightMission[index].miModifyDate,
                                     
                                    mi_modifyUser: geoFlightMission[index].miModifyUser,
                                     
                                    mi_owner: geoFlightMission[index].miOwner,
                                     
                                                                                                         
                                     
                                                                                                        ),
                                     
                                                                                                       ));
                                     
                                                                                             },
                                     
                                                                                             style: ButtonStyle(
                                     
                                                                                               minimumSize: MaterialStateProperty.all(const Size(30, 40)) ,
                                     
                                                                                                   backgroundColor: MaterialStateProperty.all(
                                     
                                                                                                       Colors.grey.shade800),
                                     
                                                                                                  
                                     
                                                                                                      
                                     
                                                                                                       
                                     
                                                                                                   shape: MaterialStateProperty.all<
                                     
                                                                                                           RoundedRectangleBorder>(
                                     
                                                                                                       RoundedRectangleBorder(
                                     
                                                                                                           borderRadius:
                                     
                                                                                                               BorderRadius.circular(16.0)))),
                                     
                                                                                             child: const Center(child: Icon(IconlyLight.location)),
                                     
                                                                                            
                                     
                                                                                             )
                                     
                                                                                             
                                     
                                                                                                
                                     
                                                                                              ),
                                     
                                                                                            
                                     
                                                                                              ]),
                                     
                                                                    ),
                                     
                                                                    
                                     
                                                                    Container(
                                     
                                                                                            
                                     
                                                                                            padding: const EdgeInsets.only(left: 16),
                                     
                                                                                            child: 
                                     
                                                                                            Row(children: [
                                     
                                                                    
                                     
                                                                                              Container(
                                     
                                                                                                child: Column(
                                     
                                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                  children: [
                                     
                                                                      
                                     
                                                                                                  const AutoSizeText('Номер ПЗ',
                                     
                                                                                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                     
                                                                                                ),
                                     
                                                                        
                                     
                                                                       // SizedBox(height: 8,),
                                     
                                                                        Text(geoFlightMission[index].num.toString(),
                                     
                                                                        maxLines: 2,
                                     
                                                                                                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                                ),
                                     
                                                                                                ),
                                     
                                                                      
                                     
                                                                                                ],),
                                     
                                                                                              ),
                                     
                                                                                              
                                     
                                                                                              Container(
                                     
                                                                                            padding: const EdgeInsets.only(left: 16),
                                     
                                                                                            child: Row(children: [
                                     
                                                                    
                                     
                                                                                              Column(
                                     
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                children: [
                                     
                                                                    
                                     
                                                                                                const AutoSizeText('Дата створення',
                                     
                                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                     
                                                                                              ),
                                     
                                                                                            
                                     
                                                                     // SizedBox(height: 8,),
                                     
                                                                                            Text(
                                     
                                                                                              resolver.formatDateTime(geoFlightMission[index].executionPlanStartDate),
                                     
                                                                                             
                                     
                                                                                             // resolver.resolveData(all.geoFlightMission![index].geoOperator, all.geoOperator, displayKey: 'name').toString(),
                                     
                                                                                             
                                     
                                                                                            maxLines: 2,
                                     
                                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                              ),
                                     
                                                                                              ),
                                     
                                                                    
                                     
                                                                                              ],)
                                     
                                                                    
                                     
                                                                                            ],), 
                                     
                                                                    ),
                                     
                                                                                            ],),
                                     
                                                                                            
                                     
                                                                    ),
                                     
                                                                                            
                                     
                                                                    Container(
                                     
                                                                                            padding: const EdgeInsets.only(left: 16, top: 16),
                                     
                                                                                            child: 
                                     
                                                                                            Row(children: [
                                     
                                                                    
                                     
                                                                                              Container(
                                     
                                                                                                child: Column(
                                     
                                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                  children: [
                                     
                                                                      
                                     
                                                                                                  const AutoSizeText('Замовник',
                                     
                                                                                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                     
                                                                                                ),
                                     
                                                                        
                                     
                                                                       // SizedBox(height: 8,),
                                     
                                                                        AutoSizeText(
                                     
                                                                          
                                     
                                                                          resolver.resolveData(geoFlightMission[index].operator, geoOperator, displayKey: 'name'),
                                     
                                                                          //all.geoFlightMission[index].object.toString(),
                                     
                                                                        maxLines: 2,
                                     
                                                                                                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                                ),
                                     
                                                                                                ),
                                     
                                                                      
                                     
                                                                                                ],),
                                     
                                                                                              ),
                                     
                                                                    
                                     
                                                                                              
                                     
                                                                                            ],),
                                     
                                                                                            
                                     
                                                                    ),
                                     
                                                                    Container(
                                     
                                                                                            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                     
                                                                                            child: Row(children: [
                                     
                                                                    
                                     
                                                                                              Column(
                                     
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                children: [
                                     
                                                                    
                                     
                                                                                                const AutoSizeText("Об'єкт",
                                     
                                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                     
                                                                                              ),
                                     
                                                                                            
                                     
                                                                                                           
                                     
                                                                                            Text(
                                     
                                                                                              
                                     
                                                                                              resolver.resolveData(geoFlightMission[index].object, geoObject, displayKey: 'name').toString(),
                                     
                                                                                            maxLines: 2,
                                     
                                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                              ),
                                     
                                                                                              ),
                                     
                                                                    
                                     
                                                                                              ],)
                                     
                                                                    
                                     
                                                                                            ],), 
                                     
                                                                    ),
                                     
                                  
                                     
                                                                    Container(
                                     
                                                                                            padding: const EdgeInsets.only(left: 16),
                                     
                                                                                            child: Row(children: [
                                     
                                                                    
                                     
                                                                                              Column(
                                     
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                children: [
                                     
                                                                    
                                     
                                                                                                const AutoSizeText("Відповідальна особа",
                                     
                                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                     
                                                                                              ),
                                     
                                                                                            
                                     
                                                                                                           
                                     
                                                                                            Text(
                                     
                                                                                              
                                     
                                                                                              geoFlightMission[index].responsiblePerson.toString(),
                                     
                                                                                            maxLines: 2,
                                     
                                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                              ),
                                     
                                                                                              ),
                                     
                                                                    
                                     
                                                                                              ],)
                                     
                                                                    
                                     
                                                                                            ],), 
                                     
                                                                    ),
                                     
                                                                     Container(
                                     
                                                                                            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                     
                                                                                            child: 
                                     
                                                                                            Wrap(children: [
                                     
                                                                    
                                     
                                                                                              Container(
                                     
                                                                                                child: Wrap(
                                     
                                                                                                  alignment: WrapAlignment.start,
                                     
                                                                                                  spacing:8.0,
                                     
                                                                                                  direction: Axis.horizontal,
                                     
                                                                                                  runSpacing: 8.0,
                                     
                                                                                                  crossAxisAlignment: WrapCrossAlignment.start,
                                     
                                                                                                  textDirection: TextDirection.ltr,
                                     
                                                                                                  verticalDirection: VerticalDirection.down,
                                     
                                                                                                  children: [
                                     
                                                                                                    SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.white),
                                     
                                                                                                    const SizedBox(width: 8,),
                                     
                                                                                                   Column(
                                     
                                                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                     
                                                                                                    children: [
                                     
                                                                       
                                     
                                                                                                    const AutoSizeText('Технічні засоби',
                                     
                                                                                                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                                     
                                                                                                  ),
                                     
                                                                          
                                     
                                                                         // SizedBox(height: 8,),
                                     
                                                                          Text(
                                     
                                                                            resolver.resolveData(geoFlightMission[index].technicalSupport, dictTechnicalSupport, displayKey: 'name').toString(),
                                     
                                                                            //all.geoFlightMission[index].technicalSupport.toString(),
                                     
                                                                          maxLines: 2,
                                     
                                                                                                  style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                     
                                                                                                  ),
                                     
                                                                                                  ),
                                     
                                                                        
                                     
                                                                                                  ],),
                                     
                                                                                            ]),
                                     
                                                                                              ),
                                     
                                                                                             
                                     
                                                                                              
                                     
                                                                                            ],),
                                     
                                                                                            
                                     
                                                                    ),
                                     
                                                                                            
                                     
                                                                                                            ],
                                     
                                                                                                          ),
                                     
                                                                                            
                                     
                                                                                                      ),
                                     
                                                                  ),
                                     
                                                                );
                                     
                                              
                                     
                                                              
                                     
                                        },),
                                     
                                          );
                                     
                                  
                                     
                                  
                                     
                                        } else {
                                     
                                          return const Center(child: CircularProgressIndicator());
                                     
                                        }
                                     
                                        
                                     
                                      },
                                     
                                  
                                     
                                    )
                                     
                                 
                                     
                                  ]
                                     
                            
                                     
                            
                                     
                                
                                     
                               
                                     
                               
                                     
                                  
                                     
                                )
                                     
                                  
                                     
                              
                                     
                                
                                     
                               
                                     
                               
                                     
                              )
                                     
                            ],
                                     
                                         
                                     
                                         )
                  
                  
                 );
              }
            
            if (state is ItemSync) {
              return Center(child: CircularProgressIndicator(),);
            }
          
            if (state is ItemError) {
                      return Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                         
                          children:[
                          Image.asset('assets/images/content/error_404.png'),
                          const SizedBox(height: 16,),
                          const Text('Упс, помилка', style: TextStyle(fontSize: 16),)] ),
                  );
            }
          
            return Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                         
                          children:[
                          Image.asset('assets/images/content/error_404.png'),
                          const SizedBox(height: 16,),
                          const Text('Упс, помилка', style: TextStyle(fontSize: 16),)] ),
                  );
            },
          
          );
       */
        }

        else {
          return  Scaffold(
        backgroundColor: SecondColor,
        floatingActionButton: FloatingActionButton(
                    heroTag: "syncDataFlightMissions",         
                    backgroundColor: Colors.blue,
                    child: const Icon(Icons.sync),
                    onPressed: () {
       
                      context.read<NetworkBloc>().add(CheckConnectivity());
                                 
                    
                    }),
        
        body:        CustomScrollView(
                                   
                          slivers:<Widget> [
                                   
                          
                                   
                            
                                   
                            
                                   
                            SliverAppBar(
                                   
                                
                                   
                              //pinned: true,
                                   
                              //floating: false,
                                   
                              expandedHeight: 16,
                                   
                              backgroundColor: SecondColor,
                                   
                              title: const Text('Реєстр польотних завдань', style: TextStyle(fontSize: 16),),
                                   
                              actions: <Widget>[
                                   
                                FloatingActionButton(
                                   
                                  heroTag: "btn2",         
                                   
                                  backgroundColor: FabColorDark,
                                   
                                  onPressed: () {
                               
                                   
                                   
                                  },
                                   
                                  child:  const Icon(Icons.search)
                                   
                                             ),
                                   
                                             const SizedBox(width: 8,),
                                   
                                             FloatingActionButton(
                                   
                               
                                   
                                  heroTag: "btn3",         
                                   
                                  backgroundColor: FabColorDark,
                                   
                                  onPressed: () {
                                   
                                   
                                   
                                  },
                                   
                                  child: const Icon(
                                   
                                    IconlyLight.filter2,
                                   
                                    color: Colors.white,
                                   
                                  ),
                                   
                                             ),
                                   
                              ],
                                   
                            
                                   
                               
                                   
                            ),
                                   
                            SliverList(
                                   
                              
                                   
                              delegate: SliverChildListDelegate(
                                   
                                
                                   
                                <Widget>[
                                   
                                
                                   
                                  
                                   
                                 
                                   
                                StreamBuilder<List<GeoFlightMissionIsar>>(
                                   
                                    
                                   
                                    stream: service.geoFlightMisiionsStream(),
                                   
                                    builder: (context, snapshot) {
                                   
                                      if(snapshot.hasData){
                                   
                                        Resolver resolver = Resolver('');
                                   
                                        
                                   
                                       List<GeoFlightMissionIsar>  geoFlightMission = snapshot.data!;
                                   
                                        return SizedBox(
                                   
                                          height:  MediaQuery.of(context).size.height * 0.8,
                                   
                                          child: ListView.builder(
                                   
                                           
                                   
                                                            itemCount: geoFlightMission.length,
                                   
                                                         
                                   
                                                            itemBuilder: (context, index) {
                                   
                                                             
                                   
                                                             // flightMissionsIsar = all.geoFlightMission!.cast<GeoFlightMissionIsar>();
                                   
                                                             // geoFlightMissionService.addGeoFlightMissions(flightMissionsIsar!);
                                   
                                                              return GestureDetector(
                                   
                                                                
                                   
                                                                onTap: () {
                                   
                                                                  Navigator.of(context).push(
                                   
                                        MaterialPageRoute(
                                   
                                          builder: (context) => FlightMissionInfo(
                                   
                                                                       ID: geoFlightMission[index].ID,
                                   
                                  date: geoFlightMission[index].date ?? '-',
                                   
                                  num: geoFlightMission[index].num ?? '-',
                                   
                                  eventsState: geoFlightMission[index].eventsState,
                                   
                                  landGroupID: geoFlightMission[index].landGroupID,
                                   
                                  geoJson: geoFlightMission[index].geoJson,
                                   
                                  flightDuration: geoFlightMission[index].flightDuration ?? '0',
                                   
                                  flightGroupID: geoFlightMission[index].flightGroupID,
                                   
                                  flightMissionType: geoFlightMission[index].flightMissionType,
                                   
                                  latLngStartCombined:geoFlightMission[index]. latLngStartCombined ?? '-',
                                   
                                  latitude: geoFlightMission[index].latitude ?? 0.0,
                                   
                                  longitude: geoFlightMission[index].longitude ?? 0.0,
                                   
                                  meteorologicalData: geoFlightMission[index].meteorologicalData ?? '-',
                                   
                                  object:  resolver.resolveData(geoFlightMission[index].object, geoObject, displayKey: 'name'),
                                   
                              
                                   
                                  geoOperator: 
                                   
                                  resolver.resolveData(geoFlightMission[index].operator, geoOperator, displayKey: 'name') ,
                                   
                                
                                   
                                  route:resolver.resolveData(geoFlightMission[index].route, geoRoute, displayKey: 'name') ,
                                   
                                
                                   
                                  routeArea: geoFlightMission[index].routeArea ?? 0,
                                   
                                  routeLength: geoFlightMission[index].routeLength ?? 0,
                                   
                                  nearestLocality: geoFlightMission[index].nearestLocality ?? '-',
                                   
                                  state: resolver.resolveData(geoFlightMission[index].state, ubmEnum, displayKey: 'name', valueKey: 'code').toString(),
                                   
                                       
                                   
                                  technicalSupport: resolver.resolveData(geoFlightMission[index].technicalSupport, dictTechnicalSupport, displayKey: 'name', valueKey: 'ID'),
                                   
                                  updateInfo: geoFlightMission[index].updateInfo,
                                   
                                  responsiblePerson: geoFlightMission[index].responsiblePerson ?? '-',
                                   
                                  examinationDateFrom:geoFlightMission[index].examinationDateFrom ,
                                   
                                  examinationDateTo: geoFlightMission[index].examinationDateTo  ,
                                   
                                  examinationDatesCombined: geoFlightMission[index].examinationDatesCombined ,
                                   
                                  executionDeadlineDate: geoFlightMission[index].executionDeadlineDate ,
                                   
                                  executionInfo: geoFlightMission[index].executionInfo ,
                                   
                                  executionPlanDatesCombined: geoFlightMission[index].executionPlanDatesCombined  ,
                                   
                                  executionPlanEndDate: geoFlightMission[index].executionPlanEndDate ,
                                   
                                  executionPlanStartDate: geoFlightMission[index].executionPlanStartDate  ,
                                   
                                  geoEvent: resolver.getEventsByFlightTaskID(geoEvent, geoFlightMission[index].ID!) ,
                                   
                                  geoEventType: geoEventType,
                                   
                                  geoObject:geoObject,
                                   
                                  ubmEnum: ubmEnum,
                                   
                                  geoOperatorL: geoOperator,
                                   
                                 mi_createDate: geoFlightMission[index].miCreateDate,
                                   
                                mi_createUser: geoFlightMission[index].miCreateUser,
                                   
                                 mi_modifyDate: geoFlightMission[index].miModifyDate,
                                   
                                 mi_modifyUser: geoFlightMission[index].miModifyUser,
                                   
                                mi_owner: geoFlightMission[index].miOwner,

                                service:  service,

                                dictTechnicalSupport: dictTechnicalSupport,
                                geoAdjustmentStatus: geoAdjustmentStatus,
                                geoExecutionStatus: geoExecutionStatus,
                                geoRoute: geoRoute,
                                   
                                                              )));
                                   
                                                              },
                                   
                                                                child: Container(
                                   
                                                                  
                                   
                                                                  child: Container(
                                   
                                                                                                      margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                   
                                                                                                       height:  MediaQuery.of(context).size.height *0.55,
                                   
                                                                                                       //MediaQuery.sizeOf(context).height*0.3,
                                   
                                                                                                       //         width: MediaQuery.sizeOf(context).width,
                                   
                                                                                                      decoration: BoxDecoration(
                                   
                                                                                                          color: FabColorDark,
                                   
                                                                                                          borderRadius: const BorderRadius.all(Radius.circular(10)),
                                   
                                                                                                          
                                   
                                                                                                        ),
                                   
                                                                                                        child: Column(
                                   
                                                                                                          children: [
                                   
                                                                                          
                                   
                                                                  Padding(
                                   
                                                                                          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                                   
                                                                                          child: Row(
                                   
                                                                                            
                                   
                                                                                           crossAxisAlignment: CrossAxisAlignment.start,
                                   
                                                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                   
                                                                                          
                                   
                                                                                              children: [
                                   
                                                                                              
                                   
                                                                                           
                                   
                                                                                            Row(
                                   
                                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                   
                                                                                              children: [  
                                   
                                                                                              Container(
                                   
                                                                                          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                   
                                                                                           width: 12,
                                   
                                                                                           height: 12,
                                   
                                                                                           decoration: const BoxDecoration(
                                   
                                                                                             color: SuccessColor,
                                   
                                                                                             shape: BoxShape.circle
                                   
                                                                                           ),
                                   
                                                                                              ),
                                   
                                                                                              Padding(
                                   
                                                                                                padding: const EdgeInsets.only(top: 12),
                                   
                                                                                                child: Text(
                                   
                                                                                                   resolver.resolveData(geoFlightMission[index].state, ubmEnum, displayKey: 'name', valueKey: 'code')
                                   
                                                                                                  
                                   
                                                                                                  
                                   
                                                                                                  )),
                                   
                                
                                   
                                                                                              ]),
                                   
                                                                                              
                                   
                                
                                   
                                
                                   
                                                                                            Container(
                                   
                                                                                             
                                   
                                                                                              padding: const EdgeInsets.only(right: 8),
                                   
                                                                                              child: FilledButton(
                                   
                                                                                           
                                   
                                                                                           onPressed: (){
                                   
                                                                                             Navigator.of(context).push(
                                   
                                                                                                     MaterialPageRoute(
                                   
                                                                                                      builder: (context) => FlightMissionMapScreen(
                                   
                                                                                                             ID: geoFlightMission[index].ID!,
                                   
                                  date: geoFlightMission[index].date,
                                   
                                  num: geoFlightMission[index].num ?? '-',
                                   
                                  eventsState: geoFlightMission[index].eventsState,
                                   
                                  landGroupID: geoFlightMission[index].landGroupID,
                                   
                                  geoJson: geoFlightMission[index].geoJson,
                                   
                                  flightDuration: geoFlightMission[index].flightDuration ?? '0',
                                   
                                  flightGroupID: geoFlightMission[index].flightGroupID,
                                   
                                  flightMissionType: geoFlightMission[index].flightMissionType,
                                   
                                  latLngStartCombined:geoFlightMission[index]. latLngStartCombined ?? '-',
                                   
                                  latitude: geoFlightMission[index].latitude ?? 0.0,
                                   
                                  longitude: geoFlightMission[index].longitude ?? 0.0,
                                   
                                  meteorologicalData: geoFlightMission[index].meteorologicalData ?? '-',
                                   
                                  object:  resolver.resolveData(geoFlightMission[index].object, geoObject, displayKey: 'name'),
                                   
                              
                                   
                                  geoOperator: 
                                   
                                  resolver.resolveData(geoFlightMission[index].operator, geoOperator, displayKey: 'name') ?? '-',
                                   
                                
                                   
                                  route: resolver.resolveData(geoFlightMission[index].route, geoRoute, displayKey: 'name') ?? '-',
                                   
                                  routeArea: geoFlightMission[index].routeArea ?? 0,
                                   
                                  routeLength: geoFlightMission[index].routeLength ?? 0.0,
                                   
                                  nearestLocality: geoFlightMission[index].nearestLocality ?? '-',
                                   
                                  state: resolver.resolveData(geoFlightMission[index].state, ubmEnum, displayKey: 'name', valueKey: 'code').toString(),
                                   
                                       
                                   
                                  technicalSupport: resolver.resolveData(geoFlightMission[index].technicalSupport, dictTechnicalSupport, displayKey: 'name', valueKey: 'ID'),
                                   
                                  updateInfo: geoFlightMission[index].updateInfo,
                                   
                                  responsiblePerson: geoFlightMission[index].responsiblePerson ?? '-',
                                   
                                  examinationDateFrom:geoFlightMission[index].examinationDateFrom ,
                                   
                                  examinationDateTo: geoFlightMission[index].examinationDateTo ,
                                   
                                  examinationDatesCombined: geoFlightMission[index].examinationDatesCombined ,
                                   
                                  executionDeadlineDate: geoFlightMission[index].executionDeadlineDate ,
                                   
                                  executionInfo: geoFlightMission[index].executionInfo,
                                   
                                  executionPlanDatesCombined: geoFlightMission[index].executionPlanDatesCombined ,
                                   
                                  executionPlanEndDate: geoFlightMission[index].executionPlanEndDate,
                                   
                                  executionPlanStartDate: geoFlightMission[index].executionPlanStartDate ,
                                   
                                  geoEvent: resolver.getEventsByFlightTaskID(geoEvent, geoFlightMission[index].ID!),
                                   
                                  geoEventType:geoEventType,
                                   
                                  geoObject:geoObject,
                                   
                                  ubmEnum: ubmEnum,
                                   
                                  geoOperatorL: geoOperator,
                                   
                                
                                   
                                 mi_createDate: geoFlightMission[index].miCreateDate,
                                   
                                 mi_createUser: geoFlightMission[index].miCreateUser,
                                   
                                 mi_modifyDate: geoFlightMission[index].miModifyDate,
                                   
                                  mi_modifyUser: geoFlightMission[index].miModifyUser,
                                   
                                  mi_owner: geoFlightMission[index].miOwner,
                                  service:  service ,
                                   
                                                                                                       
                                   
                                                                                                      ),
                                   
                                                                                                     ));
                                   
                                                                                           },
                                   
                                                                                           style: ButtonStyle(
                                   
                                                                                             minimumSize: MaterialStateProperty.all(const Size(30, 40)) ,
                                   
                                                                                                 backgroundColor: MaterialStateProperty.all(
                                   
                                                                                                     Colors.grey.shade800),
                                   
                                                                                                
                                   
                                                                                                    
                                   
                                                                                                     
                                   
                                                                                                 shape: MaterialStateProperty.all<
                                   
                                                                                                         RoundedRectangleBorder>(
                                   
                                                                                                     RoundedRectangleBorder(
                                   
                                                                                                         borderRadius:
                                   
                                                                                                             BorderRadius.circular(16.0)))),
                                   
                                                                                           child: const Center(child: Icon(IconlyLight.location)),
                                   
                                                                                          
                                   
                                                                                           )
                                   
                                                                                           
                                   
                                                                                              
                                   
                                                                                            ),
                                   
                                                                                          
                                   
                                                                                            ]),
                                   
                                                                  ),
                                   
                                                                  
                                   
                                                                  Container(
                                   
                                                                                          
                                   
                                                                                          padding: const EdgeInsets.only(left: 16),
                                   
                                                                                          child: 
                                   
                                                                                          Row(children: [
                                   
                                                                  
                                   
                                                                                            Container(
                                   
                                                                                              child: Column(
                                   
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                   
                                                                                                children: [
                                   
                                                                    
                                   
                                                                                                const AutoSizeText('Номер ПЗ',
                                   
                                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                   
                                                                                              ),
                                   
                                                                      
                                   
                                                                     // SizedBox(height: 8,),
                                   
                                                                      Text(geoFlightMission[index].num.toString(),
                                   
                                                                      maxLines: 2,
                                   
                                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                   
                                                                                              ),
                                   
                                                                                              ),
                                   
                                                                    
                                   
                                                                                              ],),
                                   
                                                                                            ),
                                   
                                                                                            
                                   
                                                                                            Container(
                                   
                                                                                          padding: const EdgeInsets.only(left: 16),
                                   
                                                                                          child: Row(children: [
                                   
                                                                  
                                   
                                                                                            Column(
                                   
                                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                   
                                                                                              children: [
                                   
                                                                  
                                   
                                                                                              const AutoSizeText('Дата створення',
                                   
                                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                   
                                                                                            ),
                                   
                                                                                          
                                   
                                                                   // SizedBox(height: 8,),
                                   
                                                                                          Text(
                                   
                                                                                            resolver.formatDateTime(geoFlightMission[index].executionPlanStartDate),
                                   
                                                                                           
                                   
                                                                                           // resolver.resolveData(all.geoFlightMission![index].geoOperator, all.geoOperator, displayKey: 'name').toString(),
                                   
                                                                                           
                                   
                                                                                          maxLines: 2,
                                   
                                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                   
                                                                                            ),
                                   
                                                                                            ),
                                   
                                                                  
                                   
                                                                                            ],)
                                   
                                                                  
                                   
                                                                                          ],), 
                                   
                                                                  ),
                                   
                                                                                          ],),
                                   
                                                                                          
                                   
                                                                  ),
                                   
                                                                                          
                                   
                                                                  Container(
                                   
                                                                                          padding: const EdgeInsets.only(left: 16, top: 16),
                                   
                                                                                          child: 
                                   
                                                                                          Row(children: [
                                   
                                                                  
                                   
                                                                                            Container(
                                   
                                                                                              child: Column(
                                   
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                   
                                                                                                children: [
                                   
                                                                    
                                   
                                                                                                const AutoSizeText('Замовник',
                                   
                                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                   
                                                                                              ),
                                   
                                                                      
                                   
                                                                     // SizedBox(height: 8,),
                                   
                                                                      AutoSizeText(
                                   
                                                                        
                                   
                                                                        resolver.resolveData(geoFlightMission[index].operator, geoOperator, displayKey: 'name'),
                                   
                                                                        //all.geoFlightMission[index].object.toString(),
                                   
                                                                      maxLines: 2,
                                   
                                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                   
                                                                                              ),
                                   
                                                                                              ),
                                   
                                                                    
                                   
                                                                                              ],),
                                   
                                                                                            ),
                                   
                                                                  
                                   
                                                                                            
                                   
                                                                                          ],),
                                   
                                                                                          
                                   
                                                                  ),
                                   
                                                                  Container(
                                   
                                                                                          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                   
                                                                                          child: Row(children: [
                                   
                                                                  
                                   
                                                                                            Column(
                                   
                                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                   
                                                                                              children: [
                                   
                                                                  
                                   
                                                                                              const AutoSizeText("Об'єкт",
                                   
                                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                   
                                                                                            ),
                                   
                                                                                          
                                   
                                                                                                         
                                   
                                                                                          Text(
                                   
                                                                                            
                                   
                                                                                            resolver.resolveData(geoFlightMission[index].object, geoObject, displayKey: 'name').toString(),
                                   
                                                                                          maxLines: 2,
                                   
                                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                   
                                                                                            ),
                                   
                                                                                            ),
                                   
                                                                  
                                   
                                                                                            ],)
                                   
                                                                  
                                   
                                                                                          ],), 
                                   
                                                                  ),
                                   
                                
                                   
                                                                  Container(
                                   
                                                                                          padding: const EdgeInsets.only(left: 16),
                                   
                                                                                          child: Row(children: [
                                   
                                                                  
                                   
                                                                                            Column(
                                   
                                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                   
                                                                                              children: [
                                   
                                                                  
                                   
                                                                                              const AutoSizeText("Відповідальна особа",
                                   
                                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                   
                                                                                            ),
                                   
                                                                                          
                                   
                                                                                                         
                                   
                                                                                          Text(
                                   
                                                                                            
                                   
                                                                                            geoFlightMission[index].responsiblePerson.toString(),
                                   
                                                                                          maxLines: 2,
                                   
                                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                   
                                                                                            ),
                                   
                                                                                            ),
                                   
                                                                  
                                   
                                                                                            ],)
                                   
                                                                  
                                   
                                                                                          ],), 
                                   
                                                                  ),
                                   
                                                                   Container(
                                   
                                                                                          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                   
                                                                                          child: 
                                   
                                                                                          Wrap(children: [
                                   
                                                                  
                                   
                                                                                            Container(
                                   
                                                                                              child: Wrap(
                                   
                                                                                                alignment: WrapAlignment.start,
                                   
                                                                                                spacing:8.0,
                                   
                                                                                                direction: Axis.horizontal,
                                   
                                                                                                runSpacing: 8.0,
                                   
                                                                                                crossAxisAlignment: WrapCrossAlignment.start,
                                   
                                                                                                textDirection: TextDirection.ltr,
                                   
                                                                                                verticalDirection: VerticalDirection.down,
                                   
                                                                                                children: [
                                   
                                                                                                  SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.white),
                                   
                                                                                                  const SizedBox(width: 8,),
                                   
                                                                                                 Column(
                                   
                                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                   
                                                                                                  children: [
                                   
                                                                     
                                   
                                                                                                  const AutoSizeText('Технічні засоби',
                                   
                                                                                                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                                   
                                                                                                ),
                                   
                                                                        
                                   
                                                                       // SizedBox(height: 8,),
                                   
                                                                        Text(
                                   
                                                                          resolver.resolveData(geoFlightMission[index].technicalSupport, dictTechnicalSupport, displayKey: 'name').toString(),
                                   
                                                                          //all.geoFlightMission[index].technicalSupport.toString(),
                                   
                                                                        maxLines: 2,
                                   
                                                                                                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                   
                                                                                                ),
                                   
                                                                                                ),
                                   
                                                                      
                                   
                                                                                                ],),
                                   
                                                                                          ]),
                                   
                                                                                            ),
                                   
                                                                                           
                                   
                                                                                            
                                   
                                                                                          ],),
                                   
                                                                                          
                                   
                                                                  ),
                                   
                                                                                          
                                   
                                                                                                          ],
                                   
                                                                                                        ),
                                   
                                                                                          
                                   
                                                                                                    ),
                                   
                                                                ),
                                   
                                                              );
                                   
                                            
                                   
                                                            
                                   
                                      },),
                                   
                                        );
                                   
                                
                                   
                                
                                   
                                      } else {
                                   
                                        return const Center(child: CircularProgressIndicator());
                                   
                                      }
                                   
                                      
                                   
                                    },
                                   
                                
                                   
                                  )
                                   
                               
                                   
                                ]
                                   
                          
                                   
                          
                                   
                              
                                   
                             
                                   
                             
                                   
                                
                                   
                              )
                                   
                                
                                   
                            
                                   
                              
                                   
                             
                                   
                             
                                   
                            )
                                   
                          ],
                                   
                                       
                                   
                                       )
        
        
       );
 }

        
      },
      
     );
           
             } 
             
       
       
             
           
           
     
            
            }
            
           
      
          
   
          
   

    

    
  
  
   
 
             

  
  



    
  
  
   
 
             

  
  

