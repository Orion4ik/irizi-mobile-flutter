import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'package:irizi/bloc/layer_switch/layer_switch_bloc.dart';
import 'package:irizi/repo/isar_db/isar_repo.dart';
import 'package:irizi/screens/FlightGroup/info_screen_events/events_info_screen.dart';
import 'package:irizi/screens/FlightGroup/info_screen_events/events_materials.dart';
import 'package:irizi/ui/layer_switch_popup.dart';
import 'package:irizi/untils/theme.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';



class FlightEventMapScreen extends StatefulWidget {
   const FlightEventMapScreen({
    super.key, 
   required this.ID,
   required this.num,
   required this.geoOperator,
   required this.latitude,
   required this.longitude,
   required this.object,
   required this.state,
   required this.description,
   required this.eventDate,
   required this.eventTypeID,
   required this.flightMissionID,
   required this.verifyDate,
   required this.verifySummary,
   required this.miCreateDate,
   required this.miCreateUser,
   required this.miModifyDate,
   required this.miModifyUser,
   required this.miOwner,
   required this.service
  
   
   });


  
   final int? ID;
   final String? num;
   final dynamic eventTypeID;
   final String? eventDate;
  final double? latitude;
   final double? longitude;
   final String? description;
   final String? verifyDate;
   final String? verifySummary;
   final int? flightMissionID;
   final String? state;
   final dynamic object;
  final dynamic geoOperator;
   final int? miOwner;
   final String? miCreateDate;
   final int? miCreateUser;
   final String? miModifyDate;
   final int? miModifyUser;
   final  IsarRepository service;
 // final int? mi_owner;
 // final String? mi_createDate;
 // final int? mi_createUser;
 // final String? mi_modifyDate;
 // final int? mi_modifyUser;

  


  
   @override
  State<FlightEventMapScreen> createState() => _FlightEventMapScreenState();
}

class _FlightEventMapScreenState extends State<FlightEventMapScreen> {
  late final int? ID = widget.ID;
  late  final String? num = widget.num;
   late final dynamic eventTypeID = widget.eventTypeID;
   late final String? eventDate = widget.eventDate;
  late final double? latitude = widget.latitude;
  late  final double? longitude = widget.longitude;
   late final String? description = widget.description;
  late  final String? verifyDate = widget.verifyDate;
   late final String? verifySummary = widget.verifySummary;
   late final int? flightMissionID = widget.flightMissionID;
   late final String? state = widget.state;
   late final dynamic object = widget.object;
  late final dynamic geoOperator = widget.geoOperator;
   late final int? miOwner = widget.miOwner;
   late final String? miCreateDate = widget.miCreateDate;
   late final int? miCreateUser = widget.miCreateUser;
   late final String? miModifyDate = widget.miModifyDate;
   late final int? miModifyUser = widget.miModifyUser;
   late final  IsarRepository service = widget.service;


 //  late FollowOnLocationUpdate _followOnLocationUpdate;
 // late StreamController<double?> _followCurrentLocationStreamController;
  @override
  void initState() {
    super.initState();
   // _followOnLocationUpdate = FollowOnLocationUpdate.always;
   // _followCurrentLocationStreamController = StreamController<double?>();

  }

  @override
  void dispose() {
   // _followCurrentLocationStreamController.close();
    super.dispose();
  }



  final MapController mapController = MapController();
  
@override
  Widget build(BuildContext context) {
     
    
    return BlocProvider(
      
      create: (_) => LayerBloc(),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Подія:$num'),
          backgroundColor: SecondColor
        ),
        body: BlocBuilder<LayerBloc, Layer>(
          
          builder: (context, activeLayer) {
            return Stack(
              children: [
              SlidingUpPanel(
                  backdropEnabled: true,
                      parallaxEnabled: true,
                      parallaxOffset: .5,
                      borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(18.0),
                topRight: Radius.circular(18.0)),
                header:  Row(
              //crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 8, horizontal:180),
                  width: 30,
                  height: 5,
                  decoration: BoxDecoration(
                      color: Colors.grey.shade800,
                      borderRadius: const BorderRadius.all(Radius.circular(12.0))),
                ),
              ],
            ),
                panel: DefaultTabController(
        length: 2,
        child: TabBarView(
            children: [
              EventsInfo(
                  ID: ID,
                  eventDate: eventDate,
                  num: num,
                  description: description,
                  eventTypeID: eventTypeID,
                  flightMissionID: flightMissionID,
                  geoOperator: geoOperator,
                  latitude: latitude,
                  longitude: longitude,
                  object: object,
                  state: state,
                  verifyDate: verifyDate,
                  verifySummary: verifySummary,
                  miCreateDate: miCreateDate,
                  miCreateUser: miCreateUser,
                  miModifyDate:miModifyDate ,
                  miModifyUser: miModifyUser,
                  miOwner: miOwner,
                  service: service,
                  
                //  mi_creat eDate: mi_createDate,
                 // mi_createUser: mi_createUser,
                //  mi_modifyDate: mi_modifyDate,
                 // mi_modifyUser: mi_modifyUser,
                 // mi_owner: mi_owner,

            ),
              EventsInfoMaterials(ID: ID, service: service),
             ])
             ),
                body: FlutterMap(
                  
                  options: MapOptions(
                    center: LatLng(latitude!, longitude!),
                    zoom: 13,
                    minZoom: 2,
                    maxZoom: 18,
                    /*
                    onPositionChanged: (MapPosition position, bool hasGesture) {
                    if (hasGesture && _followOnLocationUpdate != FollowOnLocationUpdate.never) {
                      setState(
                        () => _followOnLocationUpdate = FollowOnLocationUpdate.never,
                      );
                    }
                  },
                  */
                  ),
                  /*
                  nonRotatedChildren: [
                    
                    Positioned(
                  
                    right: 20,
                    bottom: 20,
                    child: FloatingActionButton(
                      heroTag: 6,
                      backgroundColor: Colors.blue,
                      onPressed: () {
                        // Follow the location marker on the map when location updated until user interact with the map.
                        //setState( () => _followOnLocationUpdate = FollowOnLocationUpdate.always,);
                        // Follow the location marker on the map and zoom the map to level 18.
                        //_followCurrentLocationStreamController.add(18);
                      },
                      child: const Icon(
                        Icons.my_location,
                        color: Colors.white,
                      ),
                    ),
                  ),
                   
                          
                  /*
                  Positioned(
                    left: 20,
                   // right: 20,
                    bottom: 20,
                    child: FloatingActionButton(
                      
                      backgroundColor: Colors.blue,
                      onPressed: () {
                        
                        
                      },
                      child: const Icon(
                        Icons.layers,
                        color: Colors.white,
                      ),)),
                      */
                 
                          
                  
                  ],
                  */
                  children: [
                    TileLayer(
                      urlTemplate: getLayerUrl(activeLayer),
                      subdomains: const ['a', 'b', 'c'],
                      tileProvider: FMTC.instance('mapStore').getTileProvider(FMTCTileProviderSettings(
                      maxStoreLength: 0,
                      behavior: CacheBehavior.onlineFirst
                      ))
                    ),
                    MarkerLayer(markers: [
                      Marker(
                                          point: LatLng(latitude!, longitude!),
                                          builder: (ctx) => const Icon(
                                            Icons.location_on,
                                            size: 30,
                                            color: Colors.blue,))
                    ],)
                  /*
                    CurrentLocationLayer(
                    followCurrentLocationStream:
                        _followCurrentLocationStreamController.stream,
                    followOnLocationUpdate: _followOnLocationUpdate,
                  ),
                  */
                  ],
                ),
              ),
             const Positioned(
                  
                    //left: 20,
                    right: 16,
                    top: 40,
                    child: LayerSwitch(),
                          
                          
                  ),
            ]);
          },
        ),
      
      ),
    );
  }

   String getLayerUrl(Layer layer)  {
    switch (layer) {
      case Layer.normal:
        return 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
      case Layer.satellite:
        return 'http://mt0.google.com/vt/lyrs=s&x={x}&y={y}&z={z}';
      case Layer.terrain:
        return 'http://mt0.google.com/vt/lyrs=m&x={x}&y={y}&z={z}';
    }
  }
  
  
}
  

  



  
