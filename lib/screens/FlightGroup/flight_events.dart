

import 'package:auto_size_text/auto_size_text.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:irizi/model/model_all.dart';
import 'package:irizi/screens/FlightGroup/flight_mission_info_screen.dart';
import 'package:irizi/screens/FlightGroup/flight_mission_map.dart';
import 'package:irizi/untils/theme.dart';
import '../../bloc/db_bloc/fight_missions_bloc/db_bloc.dart';
import '../../bloc/db_bloc/fight_missions_bloc/db_event.dart';
import '../../bloc/db_bloc/fight_missions_bloc/db_state.dart';
import '../../bloc/network_helper_bloc/network_bloc.dart';
import '../../bloc/network_helper_bloc/network_state.dart';
import '../../db/dict_technical_support_isar.dart';
import '../../db/flight_events_isar.dart';
import '../../db/geo_event_type_isar.dart';
import '../../db/geo_objects_isar.dart';
import '../../db/geo_operator_isar.dart';
import '../../db/geo_route_isar.dart';
import '../../db/udb_enums_isar.dart';
import '../../repo/isar_db/isar_repo.dart';
import '../../service/resolving_service.dart';
import '../../service/retrofit/api_service.dart';
import 'flight_event_map.dart';
import 'info_screen_events/events_info_screen.dart';

class FlightsEventsScreen extends StatefulWidget {
   const FlightsEventsScreen({super.key,  required this.title});




 
  final String title;
  

  @override
  State<FlightsEventsScreen> createState() => _FlightsEventsScreenState();
}









class _FlightsEventsScreenState extends State<FlightsEventsScreen> {
      
      final service = IsarRepository();
      late  List<GeoFlightMission>? geoFlightMissions;
      late final  List<GeoEventType> geoEventType;
      late final List<GeoObject>? geoObject;
      late final List<UbmEnum>? ubmEnum ;
      late final List<GeoOperator>? geoOperator;
      late final List<GeoRoute>? geoRoute ;
      late final List<DictTechnicalSupport>? dictTechnicalSupport;
  

  @override
  void initState()  {
    super.initState();

    getGeoObjects();
    getGeoOperators();
    getGeoRoute();
    getDictTechnicalSupport();
    getgeoEventType();
    getUbmEnum();
    fetchData();
    
  

  }

 

  @override
  void dispose() {
    super.dispose();
  }

Future <void> fetchData()  async {
    getGeoObjects().then((objects) {
      setState(() {
        geoObject = objects;
      });
    });

    getGeoOperators().then((objects) {
      setState(() {
        geoOperator = objects;
      });
    });

    getGeoRoute().then((objects) {
      setState(() {
        geoRoute = objects;
      });
    });

    getDictTechnicalSupport().then((objects) {
      setState(() {
        dictTechnicalSupport = objects;
      });
    });
    getgeoEventType().then((objects) {
      setState(() {
        geoEventType = objects;
      });
    });

    getUbmEnum().then((objects) {
      setState(() {
        ubmEnum = objects;
      });
    });

    
  }

  

  Future<List<GeoObject>> getGeoObjects() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoObjectFromIsar();
  }
  Future<List<GeoOperator>> getGeoOperators() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoOperatorFromIsar();
  }
  Future<List<GeoRoute>> getGeoRoute() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoRouteFromIsar();
  }
  Future<List<DictTechnicalSupport>> getDictTechnicalSupport() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getdictTechnicalSupportFromIsar();
  }
  Future<List<GeoEventType>> getgeoEventType() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getgeoEventTypeFromIsar();
  }

Future<List<UbmEnum>> getUbmEnum() async {
    // Perform the API call or database query here and return the result as a List<GeoObject>.
    // For example, if you are using Retrofit, you might have a function like this:
    return service.getubmEnumFromIsar();
  }

 


  


  
  @override
  Widget build(BuildContext context) {
    
    // TODO: implement build
    return  Scaffold(
      backgroundColor: SecondColor,
      floatingActionButton: FloatingActionButton(
                  heroTag: "syncDataFlightMissions",         
                  backgroundColor: Colors.blue,
                  child: const Icon(Icons.sync),
                  onPressed: () {

                   
                  
                  }),
      
      body: CustomScrollView(
          slivers:<Widget> [

            SliverAppBar(

              //pinned: true,
              //floating: false,
              expandedHeight: 16,
              backgroundColor: SecondColor,
              title: const Text('Реєстр подій', style: TextStyle(fontSize: 16),),
              actions: <Widget>[
                FloatingActionButton(
                  heroTag: "btn2",         
                  backgroundColor: FabColorDark,
                  onPressed: () {
                   
                  },
                  child:  const Icon(Icons.search)
                             ),
                             const SizedBox(width: 8,),
                             FloatingActionButton(
               
                  heroTag: "btn3",         
                  backgroundColor: FabColorDark,
                  onPressed: () {
                   
                  },
                  child: const Icon(
                    IconlyLight.filter2,
                    color: Colors.white,
                  ),
                             ),
              ],
            
               
            ),
            SliverList(
              
              delegate: SliverChildListDelegate(

                <Widget>[

                  
                 
                FutureBuilder<List<GeoEvent>>(
                    
                    future: service.getgeoEventFromIsar(),
                    builder: (context, snapshot) {
                      if(snapshot.connectionState == ConnectionState.done){
                        Resolver resolver = Resolver('');
                       
                        List<GeoEvent>  geoEvent = snapshot.data!;
                        return SizedBox(
                          height:  MediaQuery.of(context).size.height*0.8,
                          child: ListView.builder(
                           
                                            itemCount: geoEvent.length,
                                            itemBuilder: (context, index) {
                                              return GestureDetector(
                                                onTap: (){
                                                  Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => EventsInfo(
                              ID: geoEvent![index].ID,
                              num: geoEvent![index].num,
                              eventDate: resolver.formatDateTime((geoEvent![index].eventDate)),
                              object: resolver.resolveData(geoEvent![index].object, geoObject, displayKey: 'name') ,
                              state: resolver.resolveData(geoEvent![index].state, ubmEnum, displayKey: 'name', valueKey: 'code'),
                              geoOperator: resolver.resolveData(geoEvent![index].geoOperator, geoOperator, displayKey: 'name'),
                              latitude: geoEvent![index].latitude,
                              longitude: geoEvent![index].longitude,
                              description: geoEvent[index].description,
                              eventTypeID: resolver.resolveData(geoEvent![index].eventTypeID, geoEventType, displayKey: 'name'),
                              flightMissionID: geoEvent![index].flightMissionID,
                              verifyDate: geoEvent![index].verifyDate,
                              verifySummary: geoEvent![index].verifySummary,
                              miCreateDate: geoEvent![index].miCreateDate,
                              miCreateUser: geoEvent![index].miCreateUser,
                              miModifyDate: geoEvent![index].miModifyDate,
                              miModifyUser: geoEvent![index].miModifyUser,
                              miOwner: geoEvent![index].miOwner,
                              service: service,

                          )));
                                                },
                                                child: Container(
                                                                                    margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                                                                     height:  MediaQuery.of(context).size.height /4,
                                                                                     //MediaQuery.sizeOf(context).height*0.3,
                                                                                     //         width: MediaQuery.sizeOf(context).width,
                                                                                    decoration: BoxDecoration(
                                                                                        color: FabColorDark,
                                                                                        borderRadius: const BorderRadius.all(Radius.circular(10)),
                                                                                        
                                                                                      ),
                                                                                      child: Column(
                                                                                        children: [
                                                                        
                                                Padding(
                                                                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                                                        child: Row(
                                                                          
                                                                         crossAxisAlignment: CrossAxisAlignment.start,
                                                                        
                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                            children: [
                                              
                                                                           Row(
                                                                             crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                                  Container(
                                                                        margin: const EdgeInsets.only(left: 16, top: 16),
                                                                         width: 12,
                                                                         height: 12,
                                                                         decoration: const BoxDecoration(
                                                                           color: SuccessColor,
                                                                           shape: BoxShape.circle
                                                                         ),
                                                                            ),
                                                                           
                                                                            Padding(
                                                                              padding: const EdgeInsets.only(top: 12, left:8),
                                                                              child: Text(resolver.resolveData(geoEvent![index].state, ubmEnum, displayKey: 'name', valueKey: 'code' ))),
                                                                          
                                                                           ],), 
                                                                            
                                                                          
                                                                          
                                                                          Container(
                                                                           
                                                                            padding: const EdgeInsets.only(right: 8),
                                                                            child: FilledButton(
                                                                         
                                                                         onPressed: (){
                                              
                                                                            Navigator.of(context).push(
                                                                                       MaterialPageRoute(
                                                                                        builder: (context) => FlightEventMapScreen(
                                              
                                                                                          ID: geoEvent![index].ID!, 
                                                                                          num: geoEvent![index].num ?? '-', 
                                                                                          state: resolver.resolveData(geoEvent![index].state, ubmEnum, displayKey: 'name', valueKey: 'code') ?? '-', 
                                                                                          description: geoEvent![index].description ?? '-', 
                                                                                          object: resolver.resolveData(geoEvent![index].object, geoObject, displayKey: 'name') ?? '-', 
                                                                                          latitude: geoEvent![index].latitude ?? 0.0, 
                                                                                          longitude: geoEvent![index].longitude ?? 0.0, 
                                                                                          geoOperator: resolver.resolveData(geoEvent![index].geoOperator, geoOperator, displayKey: 'name') ?? '-', 
                                                                                          eventDate: resolver.formatDateTime((geoEvent![index].eventDate)) ?? '-', 
                                                                                          eventTypeID: geoEvent![index].eventTypeID! ?? 0, 
                                                                                          flightMissionID: geoEvent![index].flightMissionID!, 
                                                                                          verifyDate: geoEvent![index].verifyDate ?? '-', 
                                                                                          verifySummary: geoEvent![index].verifySummary ?? '-', 
                                                                                          miCreateDate: geoEvent![index].miCreateDate ?? '-', 
                                                                                          miCreateUser: geoEvent![index].miCreateUser ?? 0, 
                                                                                          miModifyDate: geoEvent![index].miModifyDate ?? '-',  
                                                                                          miModifyUser: geoEvent![index].miModifyUser ?? 0, 
                                                                                          miOwner: geoEvent![index].miOwner ?? 0, 
                                                                                          service: service,
                                                                                           
                                                                                        )));
                                                                         },
                                                                         style: ButtonStyle(
                                                                           fixedSize: MaterialStateProperty.all(const Size(25, 25)) ,
                                                                               backgroundColor: MaterialStateProperty.all(
                                                                                   Colors.grey.shade800),
                                                                              
                                                                                  
                                                                                   
                                                                               shape: MaterialStateProperty.all<
                                                                                       RoundedRectangleBorder>(
                                                                                   RoundedRectangleBorder(
                                                                                       borderRadius:
                                                                                           BorderRadius.circular(16.0)))),
                                                                         child: const Center(child: Icon(IconlyLight.location)),
                                                                        
                                                                         )
                                                                         
                                                                            
                                                                          ),
                                                                        
                                                                          ]),
                                                ),
                                                
                                                Container(
                                                                        
                                                                        padding: const EdgeInsets.only(left: 16),
                                                                        child: 
                                                                        Row(children: [
                                                
                                                                          Container(
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Тип події',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                    
                                                   // SizedBox(height: 8,),
                                                    Text(resolver.resolveData(geoEvent![index].eventTypeID, geoEventType, displayKey: 'name'),
                                                    maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],),
                                                                          ),
                                                                          
                                                                          Container(
                                                                        padding: const EdgeInsets.symmetric(horizontal: 16),
                                                                        child: Row(children: [
                                                
                                                                          Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                
                                                                            const AutoSizeText("Об'єкт обстеження",
                                                                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                          ),
                                                                        
                                                
                                                                        Text(resolver.resolveData(geoEvent![index].object, geoObject, displayKey: 'name'),
                                                                        maxLines: 2,
                                                                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                          ),
                                                                          ),
                                                
                                                                          ],)
                                                
                                                                        ],), 
                                                ),
                                                                        ],),
                                                                        
                                                ),
                                                                        
                                                Container(
                                                                        padding: const EdgeInsets.only(left: 16, top: 16),
                                                                        child: 
                                                                        Row(children: [
                                                
                                                                          Container(
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Номер ПЗ',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                    
                                                   // SizedBox(height: 8,),
                                                    AutoSizeText(geoEvent![index].num.toString(),
                                                    maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],),
                                                                          ),
                                                
                                                                          Container(
                                                                        padding: const EdgeInsets.only(left: 16),
                                                                        child: Row(children: [
                                                
                                                                          Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                
                                                                            const AutoSizeText('Дата та час події',
                                                                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                          ),
                                                                        
                                                                                       
                                                                        Text(
                                                                          resolver.formatDateTime((geoEvent![index].eventDate)) ,
                                                                        maxLines: 2,
                                                                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                          ),
                                                                          ),
                                                
                                                                          ],)
                                                
                                                                        ],), 
                                                ),
                                                                        ],),
                                                                        
                                                ),
                                                
                                                                        
                                                                                        ],
                                                                                      ),
                                                                        
                                                                                  ),
                                              );
                            
                                            
                      },),
                        );

                      } else {
                        return const Center(child: CircularProgressIndicator());
                      }
                      
                    },

                  )
               
                ]
          
          
              
             
             
                
              )

            
              
             
             
            )
          ],
        
        )
      /*
      BlocConsumer<NetworkBloc, NetworkState>(
        listener: (context, state) {
      if (state is NetworkInitial) {
         ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Підключення до сервера',
          style: TextStyle(color: Colors.white)
          )));
      }
       if (state is NetworkFailure) {
          ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(
          content: Text(
            state.error,
            style: const TextStyle(color: Colors.white),
            ),
          backgroundColor: Colors.blue ,
          ));
      }
        },

        builder: (context, state) {
           if (state is NetworkInitial) {
        return const Center(
          child: CircularProgressIndicator(),
        );
        }
          if (state is NetworkFailure) {
        return  BlocConsumer<GeoFlightMissionBloc, GeoFlightMissionState>(
       
         listener: (context, state) {
      if (state is ItemLoading) {
         ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Завантаження', style: TextStyle(color: Colors.white))));
      }
      if (state is ItemConvertation) {
         ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Конвертація', style: TextStyle(color: Colors.white))));
      }
       if (state is ItemError) {
          ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content:
         Text(state.error, style: const TextStyle(color: Colors.white)
        ),
        backgroundColor:  ErrorColor,
      ));
      }
      
         },
      builder: (context, state){
        if (state is ItemLoading) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
       if (state is ItemError) {
        return Center(
          child: Text(state.error));
      }
      if (state is ItemLoaded) {
        List<GeoFlightMission> geoFlightMissions = state.geoFlightMission!;
        List<GeoEvent>? geoEvent = state.geoEvent!;
        List<GeoEventType>? geoEventType = state.geoEventType!;
        List<GeoObject>? geoObject= state.geoObject!;
        List<UbmEnum>? ubmEnum = state.ubmEnum!;
        List<GeoOperator>? geoOperator = state.geoOperator!;
        List<GeoRoute>? geoRoute = state.geoRoute!;
        List<DictTechnicalSupport>? dictTechnicalSupport = state.dictTechnicalSupport!;
          
        
        Resolver resolver = Resolver('');
         
        return CustomScrollView(
          slivers:<Widget> [

            SliverAppBar(

              //pinned: true,
              //floating: false,
              expandedHeight: 16,
              backgroundColor: SecondColor,
              title: const Text('Реєстр польотних завдань', style: TextStyle(fontSize: 16),),
              actions: <Widget>[
                FloatingActionButton(
                  heroTag: "btn2",         
                  backgroundColor: FabColorDark,
                  onPressed: () {
                   
                  },
                  child:  const Icon(Icons.search)
                             ),
                             const SizedBox(width: 8,),
                             FloatingActionButton(
               
                  heroTag: "btn3",         
                  backgroundColor: FabColorDark,
                  onPressed: () {
                   
                  },
                  child: const Icon(
                    IconlyLight.filter2,
                    color: Colors.white,
                  ),
                             ),
              ],
            
               
            ),
            SliverList(
              
              delegate: SliverChildListDelegate(

                <Widget>[

                  
                 
              
                        
                        //final List <GeoEvent> geoEvent = snapshot.data!;
                         SizedBox(
                          height:  MediaQuery.of(context).size.height*0.8,
                          child: ListView.builder(
                           
                                            itemCount: geoEvent.length,
                                            itemBuilder: (context, index) {
                                              Resolver resolver = Resolver('');
                                              return GestureDetector(
                                                onTap: (){
                                                  Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => EventsInfo(
                              ID: geoEvent[index].ID,
                              num: geoEvent[index].num,
                              eventDate: resolver.formatDateTime((geoEvent[index].eventDate)),
                              object: resolver.resolveData(geoEvent[index].object, geoObject, displayKey: 'name') ,
                              state: resolver.resolveData(geoEvent[index].state, ubmEnum, displayKey: 'name', valueKey: 'code'),
                              geoOperator: resolver.resolveData(geoEvent[index].geoOperator, geoOperator, displayKey: 'name'),
                              latitude: geoEvent[index].latitude,
                              longitude: geoEvent[index].longitude,
                              description: geoEvent[index].description,
                              eventTypeID: resolver.resolveData(geoEvent[index].eventTypeID, geoEventType, displayKey: 'name'),
                              flightMissionID: geoEvent[index].flightMissionID,
                              verifyDate: geoEvent[index].verifyDate,
                              verifySummary: geoEvent[index].verifySummary,
                              miCreateDate: geoEvent[index].miCreateDate,
                              miCreateUser: geoEvent[index].miCreateUser,
                              miModifyDate: geoEvent[index].miModifyDate,
                              miModifyUser: geoEvent[index].miModifyUser,
                              miOwner: geoEvent[index].miOwner,

                          )));
                                                },
                                                child: Container(
                                                                                    margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                                                                     height:  MediaQuery.of(context).size.height /4,
                                                                                     //MediaQuery.sizeOf(context).height*0.3,
                                                                                     //         width: MediaQuery.sizeOf(context).width,
                                                                                    decoration: BoxDecoration(
                                                                                        color: FabColorDark,
                                                                                        borderRadius: const BorderRadius.all(Radius.circular(10)),
                                                                                        
                                                                                      ),
                                                                                      child: Column(
                                                                                        children: [
                                                                        
                                                Padding(
                                                                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                                                        child: Row(
                                                                          
                                                                         crossAxisAlignment: CrossAxisAlignment.start,
                                                                        
                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                            children: [
                                              
                                                                           Row(
                                                                             crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                                  Container(
                                                                        margin: const EdgeInsets.only(left: 16, top: 16),
                                                                         width: 12,
                                                                         height: 12,
                                                                         decoration: const BoxDecoration(
                                                                           color: SuccessColor,
                                                                           shape: BoxShape.circle
                                                                         ),
                                                                            ),
                                                                           
                                                                            Padding(
                                                                              padding: const EdgeInsets.only(top: 12, left:8),
                                                                              child: Text(resolver.resolveData(geoEvent[index].state, ubmEnum, displayKey: 'name', valueKey: 'code' ))),
                                                                          
                                                                           ],), 
                                                                            
                                                                          
                                                                          
                                                                          Container(
                                                                           
                                                                            padding: const EdgeInsets.only(right: 8),
                                                                            child: FilledButton(
                                                                         
                                                                         onPressed: (){
                                              
                                                                            Navigator.of(context).push(
                                                                                       MaterialPageRoute(
                                                                                        builder: (context) => FlightEventMapScreen(
                                              
                                                                                          ID: geoEvent[index].ID!, 
                                                                                          num: geoEvent[index].num ?? '-', 
                                                                                          state: resolver.resolveData(geoEvent[index].state, ubmEnum, displayKey: 'name', valueKey: 'code') ?? '-', 
                                                                                          description: geoEvent[index].description ?? '-', 
                                                                                          object: resolver.resolveData(geoEvent[index].object, geoObject, displayKey: 'name') ?? '-', 
                                                                                          latitude: geoEvent[index].latitude ?? 0.0, 
                                                                                          longitude: geoEvent[index].longitude ?? 0.0, 
                                                                                          geoOperator: resolver.resolveData(geoEvent[index].geoOperator, geoOperator, displayKey: 'name') ?? '-', 
                                                                                          eventDate: resolver.formatDateTime((geoEvent[index].eventDate)) ?? '-', 
                                                                                          eventTypeID: geoEvent[index].eventTypeID! ?? 0, 
                                                                                          flightMissionID: geoEvent[index].flightMissionID!, 
                                                                                          verifyDate: geoEvent[index].verifyDate ?? '-', 
                                                                                          verifySummary: geoEvent[index].verifySummary ?? '-', 
                                                                                          miCreateDate: geoEvent[index].miCreateDate!, 
                                                                                          miCreateUser: geoEvent[index].miCreateUser!, 
                                                                                          miModifyDate: geoEvent[index].miModifyDate!, 
                                                                                          miModifyUser:geoEvent[index].miModifyUser!, 
                                                                                          miOwner: geoEvent[index].miOwner!, 
                                                                                           
                                                                                        )));
                                                                         },
                                                                         style: ButtonStyle(
                                                                           fixedSize: MaterialStateProperty.all(const Size(25, 25)) ,
                                                                               backgroundColor: MaterialStateProperty.all(
                                                                                   Colors.grey.shade800),
                                                                              
                                                                                  
                                                                                   
                                                                               shape: MaterialStateProperty.all<
                                                                                       RoundedRectangleBorder>(
                                                                                   RoundedRectangleBorder(
                                                                                       borderRadius:
                                                                                           BorderRadius.circular(16.0)))),
                                                                         child: const Center(child: Icon(IconlyLight.location)),
                                                                        
                                                                         )
                                                                         
                                                                            
                                                                          ),
                                                                        
                                                                          ]),
                                                ),
                                                
                                                Container(
                                                                        
                                                                        padding: const EdgeInsets.only(left: 16),
                                                                        child: 
                                                                        Row(children: [
                                                
                                                                          Container(
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Тип події',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                    
                                                   // SizedBox(height: 8,),
                                                    Text(resolver.resolveData(geoEvent[index].eventTypeID, geoEventType, displayKey: 'name'),
                                                    maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],),
                                                                          ),
                                                                          
                                                                          Container(
                                                                        padding: const EdgeInsets.symmetric(horizontal: 16),
                                                                        child: Row(children: [
                                                
                                                                          Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                
                                                                            const AutoSizeText("Об'єкт обстеження",
                                                                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                          ),
                                                                        
                                                
                                                                        Text(resolver.resolveData(geoEvent![index].object, geoObject, displayKey: 'name'),
                                                                        maxLines: 2,
                                                                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                          ),
                                                                          ),
                                                
                                                                          ],)
                                                
                                                                        ],), 
                                                ),
                                                                        ],),
                                                                        
                                                ),
                                                                        
                                                Container(
                                                                        padding: const EdgeInsets.only(left: 16, top: 16),
                                                                        child: 
                                                                        Row(children: [
                                                
                                                                          Container(
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Номер ПЗ',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                    
                                                   // SizedBox(height: 8,),
                                                    AutoSizeText(geoEvent[index].num.toString(),
                                                    maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],),
                                                                          ),
                                                
                                                                          Container(
                                                                        padding: const EdgeInsets.only(left: 16),
                                                                        child: Row(children: [
                                                
                                                                          Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                
                                                                            const AutoSizeText('Дата та час події',
                                                                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                          ),
                                                                        
                                                                                       
                                                                        Text(
                                                                          resolver.formatDateTime((geoEvent[index].eventDate)) ,
                                                                        maxLines: 2,
                                                                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                          ),
                                                                          ),
                                                
                                                                          ],)
                                                
                                                                        ],), 
                                                ),
                                                                        ],),
                                                                        
                                                ),
                                                
                                                                        
                                                                                        ],
                                                                                      ),
                                                                        
                                                                                  ),
                                              );
                            
                                            
                      },),
                )

                      
                      
                    

                  
               
                ]
          
          
              
             
             
                
              )

            
              
             
             
            )
          ],
        
        );

         
      }
          return const CircularProgressIndicator();
         
       }
         
         
         );
        }

        if (state is NetworkSuccess) {
        return  BlocProvider(
          create:(context) =>  GeoFlightMissionBloc(
          isarRepo: IsarRepository(),
             )..add(GetItemsFromDB()),
           child: BlocConsumer<GeoFlightMissionBloc, GeoFlightMissionState>(
       
            listener: (context, state) {
      if (state is ItemLoading) {
         ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Завантаження', style: TextStyle(color: Colors.white))));
      }
      if (state is ItemSaveToDB) {
         ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Зберігання до локальної бази даних', style: TextStyle(color: Colors.white))));
      }
      if (state is ItemConvertation) {
         ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Конвертація', style: TextStyle(color: Colors.white))));
      }
       if (state is ItemError) {
          ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content:
         Text(state.error, style: const TextStyle(color: Colors.white)
        ),
        backgroundColor:  ErrorColor,
      ));
      }
      
            },
      builder: (context, state){
        if (state is ItemLoading) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
       if (state is ItemError) {
        return Center(
          child: Text(state.error));
      }
      if (state is ItemLoaded) {
        List<GeoFlightMission> geoFlightMissions = state.geoFlightMission!;
        //List<GeoEvent>? geoEvent = state.geoEvent!;
        List<GeoEventType>? geoEventType = state.geoEventType!;
        List<GeoObject>? geoObject= state.geoObject!;
        List<UbmEnum>? ubmEnum = state.ubmEnum!;
        List<GeoOperator>? geoOperator = state.geoOperator!;
        List<GeoRoute>? geoRoute = state.geoRoute!;
        List<DictTechnicalSupport>? dictTechnicalSupport = state.dictTechnicalSupport!;
             
        
        Resolver resolver = Resolver('');
         
           return CustomScrollView(
          slivers:<Widget> [

            SliverAppBar(

              //pinned: true,
              //floating: false,
              expandedHeight: 16,
              backgroundColor: SecondColor,
              title: const Text('Реєстр подій', style: TextStyle(fontSize: 16),),
              actions: <Widget>[
                FloatingActionButton(
                  heroTag: "btn2",         
                  backgroundColor: FabColorDark,
                  onPressed: () {
                   
                  },
                  child:  const Icon(Icons.search)
                             ),
                             const SizedBox(width: 8,),
                             FloatingActionButton(
               
                  heroTag: "btn3",         
                  backgroundColor: FabColorDark,
                  onPressed: () {
                   
                  },
                  child: const Icon(
                    IconlyLight.filter2,
                    color: Colors.white,
                  ),
                             ),
              ],
            
               
            ),
            SliverList(
              
              delegate: SliverChildListDelegate(

                <Widget>[

                  
                 
                FutureBuilder(
                    
                    future: service.getgeoEventFromIsar(),
                    builder: (context, snapshot) {
                      if(snapshot.connectionState == ConnectionState.done){
                        Resolver resolver = Resolver('');
                       // final All all = snapshot.data!;
                       List<GeoEvent>  geoEvent = snapshot.data!;
                        return SizedBox(
                          height:  MediaQuery.of(context).size.height*0.8,
                          child: ListView.builder(
                           
                                            itemCount: geoEvent.length,
                                            itemBuilder: (context, index) {
                                              return GestureDetector(
                                                onTap: (){
                                                  Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => EventsInfo(
                              ID: geoEvent![index].ID,
                              num: geoEvent![index].num,
                              eventDate: resolver.formatDateTime((geoEvent![index].eventDate)),
                              object: resolver.resolveData(geoEvent![index].object, geoObject, displayKey: 'name') ,
                              state: resolver.resolveData(geoEvent![index].state, ubmEnum, displayKey: 'name', valueKey: 'code'),
                              geoOperator: resolver.resolveData(geoEvent![index].geoOperator, geoOperator, displayKey: 'name'),
                              latitude: geoEvent![index].latitude,
                              longitude: geoEvent![index].longitude,
                              description: geoEvent[index].description,
                              eventTypeID: resolver.resolveData(geoEvent![index].eventTypeID, geoEventType, displayKey: 'name'),
                              flightMissionID: geoEvent![index].flightMissionID,
                              verifyDate: geoEvent![index].verifyDate,
                              verifySummary: geoEvent![index].verifySummary,
                              miCreateDate: geoEvent![index].miCreateDate,
                              miCreateUser: geoEvent![index].miCreateUser,
                              miModifyDate: geoEvent![index].miModifyDate,
                              miModifyUser: geoEvent![index].miModifyUser,
                              miOwner: geoEvent![index].miOwner,

                          )));
                                                },
                                                child: Container(
                                                                                    margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                                                                     height:  MediaQuery.of(context).size.height /4,
                                                                                     //MediaQuery.sizeOf(context).height*0.3,
                                                                                     //         width: MediaQuery.sizeOf(context).width,
                                                                                    decoration: BoxDecoration(
                                                                                        color: FabColorDark,
                                                                                        borderRadius: const BorderRadius.all(Radius.circular(10)),
                                                                                        
                                                                                      ),
                                                                                      child: Column(
                                                                                        children: [
                                                                        
                                                Padding(
                                                                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                                                        child: Row(
                                                                          
                                                                         crossAxisAlignment: CrossAxisAlignment.start,
                                                                        
                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                            children: [
                                              
                                                                           Row(
                                                                             crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                                  Container(
                                                                        margin: const EdgeInsets.only(left: 16, top: 16),
                                                                         width: 12,
                                                                         height: 12,
                                                                         decoration: const BoxDecoration(
                                                                           color: SuccessColor,
                                                                           shape: BoxShape.circle
                                                                         ),
                                                                            ),
                                                                           
                                                                            Padding(
                                                                              padding: const EdgeInsets.only(top: 12, left:8),
                                                                              child: Text(resolver.resolveData(geoEvent![index].state, ubmEnum, displayKey: 'name', valueKey: 'code' ))),
                                                                          
                                                                           ],), 
                                                                            
                                                                          
                                                                          
                                                                          Container(
                                                                           
                                                                            padding: const EdgeInsets.only(right: 8),
                                                                            child: FilledButton(
                                                                         
                                                                         onPressed: (){
                                              
                                                                            Navigator.of(context).push(
                                                                                       MaterialPageRoute(
                                                                                        builder: (context) => FlightEventMapScreen(
                                              
                                                                                          ID: geoEvent![index].ID!, 
                                                                                          num: geoEvent![index].num ?? '-', 
                                                                                          state: resolver.resolveData(geoEvent![index].state, ubmEnum, displayKey: 'name', valueKey: 'code') ?? '-', 
                                                                                          description: geoEvent![index].description ?? '-', 
                                                                                          object: resolver.resolveData(geoEvent![index].object, geoObject, displayKey: 'name') ?? '-', 
                                                                                          latitude: geoEvent![index].latitude ?? 0.0, 
                                                                                          longitude: geoEvent![index].longitude ?? 0.0, 
                                                                                          geoOperator: resolver.resolveData(geoEvent![index].geoOperator, geoOperator, displayKey: 'name') ?? '-', 
                                                                                          eventDate: resolver.formatDateTime((geoEvent![index].eventDate)) ?? '-', 
                                                                                          eventTypeID: geoEvent![index].eventTypeID! ?? 0, 
                                                                                          flightMissionID: geoEvent![index].flightMissionID!, 
                                                                                          verifyDate: geoEvent![index].verifyDate ?? '-', 
                                                                                          verifySummary: geoEvent![index].verifySummary ?? '-', 
                                                                                          miCreateDate: geoEvent![index].miCreateDate!, 
                                                                                          miCreateUser: geoEvent![index].miCreateUser!, 
                                                                                          miModifyDate: geoEvent![index].miModifyDate!, 
                                                                                          miModifyUser: geoEvent![index].miModifyUser!, 
                                                                                          miOwner: geoEvent![index].miOwner!, 
                                                                                           
                                                                                        )));
                                                                         },
                                                                         style: ButtonStyle(
                                                                           fixedSize: MaterialStateProperty.all(const Size(25, 25)) ,
                                                                               backgroundColor: MaterialStateProperty.all(
                                                                                   Colors.grey.shade800),
                                                                              
                                                                                  
                                                                                   
                                                                               shape: MaterialStateProperty.all<
                                                                                       RoundedRectangleBorder>(
                                                                                   RoundedRectangleBorder(
                                                                                       borderRadius:
                                                                                           BorderRadius.circular(16.0)))),
                                                                         child: const Center(child: Icon(IconlyLight.location)),
                                                                        
                                                                         )
                                                                         
                                                                            
                                                                          ),
                                                                        
                                                                          ]),
                                                ),
                                                
                                                Container(
                                                                        
                                                                        padding: const EdgeInsets.only(left: 16),
                                                                        child: 
                                                                        Row(children: [
                                                
                                                                          Container(
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Тип події',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                    
                                                   // SizedBox(height: 8,),
                                                    Text(resolver.resolveData(geoEvent![index].eventTypeID, geoEventType, displayKey: 'name'),
                                                    maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],),
                                                                          ),
                                                                          
                                                                          Container(
                                                                        padding: const EdgeInsets.symmetric(horizontal: 16),
                                                                        child: Row(children: [
                                                
                                                                          Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                
                                                                            const AutoSizeText("Об'єкт обстеження",
                                                                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                          ),
                                                                        
                                                
                                                                        Text(resolver.resolveData(geoEvent![index].object, geoObject, displayKey: 'name'),
                                                                        maxLines: 2,
                                                                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                          ),
                                                                          ),
                                                
                                                                          ],)
                                                
                                                                        ],), 
                                                ),
                                                                        ],),
                                                                        
                                                ),
                                                                        
                                                Container(
                                                                        padding: const EdgeInsets.only(left: 16, top: 16),
                                                                        child: 
                                                                        Row(children: [
                                                
                                                                          Container(
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Номер ПЗ',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                    
                                                   // SizedBox(height: 8,),
                                                    AutoSizeText(geoEvent![index].num.toString(),
                                                    maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],),
                                                                          ),
                                                
                                                                          Container(
                                                                        padding: const EdgeInsets.only(left: 16),
                                                                        child: Row(children: [
                                                
                                                                          Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                
                                                                            const AutoSizeText('Дата та час події',
                                                                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                          ),
                                                                        
                                                                                       
                                                                        Text(
                                                                          resolver.formatDateTime((geoEvent![index].eventDate)) ,
                                                                        maxLines: 2,
                                                                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                          ),
                                                                          ),
                                                
                                                                          ],)
                                                
                                                                        ],), 
                                                ),
                                                                        ],),
                                                                        
                                                ),
                                                
                                                                        
                                                                                        ],
                                                                                      ),
                                                                        
                                                                                  ),
                                              );
                            
                                            
                      },),
                        );

                      } else {
                        return const Center(child: CircularProgressIndicator());
                      }
                      
                    },

                  )
               
                ]
          
          
              
             
             
                
              )

            
              
             
             
            )
          ],
        
        );

            
      }
             return const CircularProgressIndicator();
         
       }
            
            
            ));
        }


          return const CircularProgressIndicator();
        }
          
      )
      */
      
      );


           
             } 
             
       
 
                

}
