import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:irizi/db/material_flight_event_isar.dart';
import 'package:irizi/repo/isar_db/isar_repo.dart';
import 'package:irizi/service/resolving_service.dart';
import 'package:irizi/untils/theme.dart';
import 'package:open_file/open_file.dart';

class EventsInfoMaterials extends StatelessWidget {
  const EventsInfoMaterials({super.key,

    required this.service,
    required this.ID

    /*
    required this.ID,
    required this.num,
    required this.object,
    required this.date,
    required this.eventsState,
    required this.geoJson,
    required this.landGroupID,
    required this.operator,
    required this.state,
    required this.route,
    required this.routeArea,
    required this.routeLength,
    required this.flightGroupID,
    required this.flightDuration,
    required this.flightMissionType,
    required this.latLngStartCombined,
    required this.latitude,
    required this.longitude,
    required this.nearestLocality,
    required this.responsiblePerson,
    required this.technicalSupport,
    required this.updateInfo,
    required this.examinationDateFrom,
    required this.examinationDateTo,
    required this.examinationDatesCombined,
    required this.executionDeadlineDate,
    required this.executionInfo,
    required this.executionPlanDatesCombined,
    required this.executionPlanEndDate,
    required this.executionPlanStartDate,
    required this.meteorologicalData,
    required this.mi_createDate,
    required this.mi_createUser,
    required this.mi_modifyDate,
    required this.mi_modifyUser,
    required this.mi_owner
    */
  }); 
  
  
  /*
  final int ID;
  final String? num;
  final int? object;
  final String? flightMissionType;
  final int? route ;
  final int? operator ;
  final double? latitude ;
  final double? longitude;
  final String? latLngStartCombined;
  final String? examinationDateFrom;
  final String? examinationDateTo ;
  final String? examinationDatesCombined;
  final String? date;
  final String? executionPlanStartDate;
  final String? executionPlanEndDate;
  final String? executionPlanDatesCombined;
  final String? executionDeadlineDate;
  final String? responsiblePerson;
  final String? geoJson;
  final int? executionInfo;
  final int? updateInfo;
  final int? technicalSupport;
  final String? meteorologicalData;
  final String? nearestLocality;
  final String? flightDuration;
  final double? routeLength;
  final double? routeArea;
  final String? state;
  final String? eventsState;
  final int? flightGroupID;
  final int? landGroupID;
  final int? mi_owner;
  final String? mi_createDate;
  final int? mi_createUser;
  final String? mi_modifyDate;
  final int? mi_modifyUser;
  
*/
 final int? ID;
final  IsarRepository service;






  
  
  @override
  Widget build(BuildContext context) {

  
       Resolver resolver = new Resolver('');
    return 
      Scaffold(
          backgroundColor: SecondColor,

          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.blue,
            child: const Icon(Icons.publish_rounded),
            onPressed: () async{
                 service.pickMultipleFilesEventToDB(ID!);

            }
            ),
          body:  RefreshIndicator(

            onRefresh: () {
             return service.getGeoFlightMissionMaterial();
            },
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Center(child: 
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  children:[ 
          
                    SizedBox(
                      height:MediaQuery.of(context).size.height * 0.8  ,
                      child: 
          
                          /*
                          InkWell(
                           // onTap: () => OpenFile.open("/sdcard/example.txt"),
                            child: Container(
                              margin: EdgeInsets.only(top: 8),
                              padding: EdgeInsets.only(left: 16, right:8),
                              height: MediaQuery.of(context).size.height * 0.4,
                             
                              decoration: BoxDecoration(
                                              
                             color: FabColorDark,
                            borderRadius: const BorderRadius.all(Radius.circular(10)),),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                          
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                IconButton(onPressed: (){}, icon: Icon(Icons.more_vert_rounded))
                              ],),
                          
                              Container (
                                margin: EdgeInsets.only(bottom: 16),
                                height: 135,
                                width: 188,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                        
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage('assets/images/content/google_satelline.jpg',
          
                          
                                  ))
                                ),
                              ),
                          
                              Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                            direction: Axis.horizontal,
                                children:[ 
                                  SvgPicture.asset('assets/images/vector/image_placeholder.svg', width: 24, height: 24,),
                                   SizedBox(width: 8,),
                                  Container(
                                    width: 155,
                                    child: AutoSizeText(
                                      'marshrut88ffhfhfjfjfe6464sfsaafafafadadaaddaad.png',
                                     softWrap: true,
                                     maxLines: 2,
                                       overflow: TextOverflow.ellipsis,
                                    
                                      
                                        minFontSize: 12,
                                         maxFontSize: 16,),
                                  ),
                              
                                       SizedBox(width: 8,),
                              
                                       Chip(
                                        side: BorderSide(color: Colors.transparent),
                                        label: Text('1 MB' ),
                                        backgroundColor: Colors.blue,
                                        
                                       ),
                              ]),
                          
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                              Text('Створено 14.08.2023 о 14:14',
                              style: TextStyle(color: Colors.white38),
                              )
                            ],)
                          
                            ]),
                            
                            ),
                          ),
                        
                        
                         InkWell(
                           child: Container(
                            margin: EdgeInsets.only(top: 8),
                              padding: EdgeInsets.only(left: 16, right:8),
                              height: MediaQuery.of(context).size.height * 0.2,
                              width: MediaQuery.of(context).size.width * 0.8,
                              decoration: BoxDecoration(
                                             
                             color: FabColorDark,
                            borderRadius: const BorderRadius.all(Radius.circular(10)),),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                         
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                IconButton(onPressed: (){}, icon: Icon(Icons.more_vert_rounded))
                              ],),
                         
                            
                         
                              Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                            direction: Axis.horizontal,
                                children:[ 
                                  SvgPicture.asset('assets/images/vector/vscode-icons_file-type-word.svg'),
                                   SizedBox(width: 8,),
                                  AutoSizeText(
                                    'marshrut8888.docx',
                                    softWrap: true,
                                     overflow: TextOverflow.ellipsis,
                                  
                                     maxLines: 2,
                                      minFontSize: 12,
                                       maxFontSize: 16,),
                              
                                       SizedBox(width: 8,),
                              
                                       Chip(
                                        side: BorderSide(color: Colors.transparent),
                                        label: Text('1 MB' ),
                                        backgroundColor: Colors.blue,
                                        
                                       ),
                              ]),
                         
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                              Text('Створено 14.08.2023 о 14:14',
                              style: TextStyle(color: Colors.white38),
                              )
                            ],)
                         
                            ]),
                            
                            ),
                         ),
          
          
                          InkWell(
                            highlightColor: Colors.blue.withOpacity(0.4),
                            splashColor: Colors.blue.withOpacity(0.5),
                            
                            child: Container(
                            margin: EdgeInsets.only(top: 8),
                              padding: EdgeInsets.only(left: 16, right:8),
                              height: MediaQuery.of(context).size.height * 0.2,
                              width: MediaQuery.of(context).size.width * 0.8,
                              decoration: BoxDecoration(
                                              
                             color: FabColorDark,
                            borderRadius: const BorderRadius.all(Radius.circular(10)),),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                          
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                IconButton(onPressed: (){}, icon: Icon(Icons.more_vert_rounded))
                              ],),
                          
                            
                          
                              Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                            direction: Axis.horizontal,
                                children:[ 
                                  SvgPicture.asset('assets/images/vector/ph_file-bold.svg', width: 24, height: 24,),
                                   SizedBox(width: 8,),
                                  AutoSizeText(
                                    'marshrut8888.docx',
                                    softWrap: true,
                                     overflow: TextOverflow.ellipsis,
                                  
                                     maxLines: 2,
                                      minFontSize: 12,
                                       maxFontSize: 16,),
                              
                                       SizedBox(width: 8,),
                              
                                       Chip(
                                        side: BorderSide(color: Colors.transparent),
                                        label: Text('1 MB' ),
                                        backgroundColor: Colors.blue,
                                        
                                       ),
                              ]),
                          
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                              Text('Створено 14.08.2023 о 14:14',
                              style: TextStyle(color: Colors.white38),
                              )
                            ],)
                          
                            ]),
                            
                            ),
                          ),
                          */
                          FutureBuilder<List<FlightEventMaterialIsar>>(
                            future: service.getFlightEventMaterialIsar(),
                            builder: (context, snapshot) {
                              
                              if (snapshot.connectionState == ConnectionState.done){
                                final List<FlightEventMaterialIsar> get_materials = snapshot.data!;
                              final  materials = resolver.getMaterialsEventByFlightTaskID(get_materials, ID!);
                              
                               return  SizedBox(
                              height: MediaQuery.of(context).size.height  * 0.8 ,
                              child: ListView.builder(
                                itemCount: materials.length,
                                itemBuilder: (context, index) {
                                 final kb = materials[index].size! / 1024;
                                 final mb = kb / 1024;
                                 final gb = mb / 1024;
                                 final file_size = mb >= 1 ? '${mb.toStringAsFixed(2)} MB' : '${kb.toStringAsFixed(2)} KB';
                              
          
                                  
                       return   InkWell(
                        onTap: () {
                          OpenFile.open(materials[index].url);
                        },
                            highlightColor: Colors.blue.withOpacity(0.4),
                            splashColor: Colors.blue.withOpacity(0.5),
                            
                            child: Container(
                            margin: EdgeInsets.only(top: 8),
                              padding: EdgeInsets.only(left: 16, right:8),
                              height: MediaQuery.of(context).size.height * 0.2,
                              width: MediaQuery.of(context).size.width * 0.8,
                              decoration: BoxDecoration(
                                              
                             color: FabColorDark,
                            borderRadius: const BorderRadius.all(Radius.circular(10)),),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                          
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                IconButton(onPressed: (){}, icon: Icon(Icons.more_vert_rounded))
                              ],),
                          
                            
                          
                              Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                            direction: Axis.horizontal,
                                children:[ 
                                  SvgPicture.asset('assets/images/vector/ph_file-bold.svg', width: 24, height: 24,),
                                   SizedBox(width: 8,),
                                  AutoSizeText(
                                    materials[index].name!,
                                    softWrap: true,
                                     overflow: TextOverflow.ellipsis,
                                  
                                     maxLines: 2,
                                      minFontSize: 12,
                                       maxFontSize: 16,),
                              
                                       SizedBox(width: 8,),
                              
                                       Chip(
                                        side: BorderSide(color: Colors.transparent),
                                        label: Text(file_size ),
                                        backgroundColor: Colors.blue,
                                        
                                       ),
                              ]),
                          
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                              Text('Сворено ${materials[index].dateCreation.toString()}',
                              style: TextStyle(color: Colors.white38),
                              )
                            ],)
                          
                            ]),
                            
                            ),
                          );
          
          
                                },));
                            }
                           
                            
                       return  Center(child: Column(children: [
                                 Image.asset('assets/images/content/error_404.png'),
                            const SizedBox(height: 16,),
                                    Text('Упс, схоже матеріали не було додано'),
                                    SizedBox(height: 32,),
                                    FilledButton(
                                      style: ButtonStyle(
              minimumSize: MaterialStateProperty.all(const Size(200, 70)),
              backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16.0)))),
                                      onPressed: () async{
                                          service.pickMultipleFilesToDB(ID!);
                                      },
                                     child: Text ("Додати  матеріали")
                                    )
                              ]),);
                      
            
                  
            
            },
                           
                          )
                       
                        
                      
                    )
                    
                    
                  ]
          
              )
              
              )
                  
                  )),
          ));
     
  }
}





