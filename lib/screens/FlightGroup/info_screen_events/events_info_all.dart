import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:irizi/untils/theme.dart';


class EventsInfoAll extends StatelessWidget {
  const EventsInfoAll({super.key,

    
    required this.ID,
    required this.num,
    required this.object,
    required this.geoOperator,
    required this.state,
    required this.latitude,
    required this.longitude,
     required this.description,
      required this.eventDate,
       required this.eventTypeID,
        required this.flightMissionID,
         required this.verifyDate,
       required this.verifySummary,   
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
    required this.miOwner
    
  }); 
  
  
  
   final int? ID;
   final String? num;
   final dynamic eventTypeID;
   final String? eventDate;
   final double? latitude;
  final double? longitude;
   final String? description;
  final String? verifyDate;
   final String? verifySummary;
   final int? flightMissionID;
   final String? state;
   final dynamic object;
   final dynamic geoOperator;
   final int? miOwner;
  final String? miCreateDate;
  final int? miCreateUser;
   final String? miModifyDate;
   final int? miModifyUser;
  







  
  
  @override
  Widget build(BuildContext context) {
     
    return 
      Scaffold(
          backgroundColor: SecondColor,

          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.blue,
            child: const Icon(Icons.edit_outlined),
            onPressed: (){}
            ),
          body:  SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
             
              children: [

              Container(
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    const AutoSizeText('Номер події',
                    style: TextStyle(fontWeight: FontWeight.bold),
                    minFontSize: 16,
                    ),
              
                    
              
                    AutoSizeText('$num',
                    minFontSize: 20,
                    ),
              
              
                  ],),

                   Container(
                                                                           
                                                                            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                                                            child: FilledButton(
                                                                         
                                                                         onPressed: (){},
                                                                         style: ButtonStyle(
                                                                           minimumSize: MaterialStateProperty.all(const Size(30, 40)) ,
                                                                               backgroundColor: MaterialStateProperty.all(
                                                                                   Colors.grey.shade800),
                                                                              
                                                                                  
                                                                                   
                                                                               shape: MaterialStateProperty.all<
                                                                                       RoundedRectangleBorder>(
                                                                                   RoundedRectangleBorder(
                                                                                       borderRadius:
                                                                                           BorderRadius.circular(16.0)))),
                                                                         child: const Center(child: Icon(IconlyLight.location)),
                                                                        
                                                                         )
                                                                         
                                                                            
                                                                          ),

                 

                 
                ],),
              ),
              const Divider(color: Colors.white10),
 Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                    child: Row(
                      //crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                         Container(
                           
                             width: 12,
                             height: 12,
                             decoration: const BoxDecoration(
                               color: SuccessColor,
                               shape: BoxShape.circle
                             ),
                                ),
                                  const SizedBox(width: 12),
                       Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                         children:[ 

                          const AutoSizeText('Статус',
                                             minFontSize: 16,
                                             ),
                          AutoSizeText('$state',
                                             minFontSize: 14,
                                             ),
                      ]),
                     
                     
                    ],),
                  ),
                  const Divider(color: Colors.white10,),
               Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                 child: Row(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [
                 
                        const AutoSizeText('Замовник',
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 16,
                        ),
                               
                        
                               
                        AutoSizeText('$geoOperator',
                        minFontSize: 14,
                        ),
                               
                               
                      ],),
                             ]),
               ),
                const Divider(color: Colors.white10,),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                 child: Row(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [
                 
                        const AutoSizeText("Об'єкт",
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 16,
                        ),
                               
                        
                               
                        AutoSizeText('$object',
                        minFontSize: 14,
                        ),
                               
                               
                      ],),
                             ]),
               ),
                const Divider(color: Colors.white10,),
                
              

              
             

                      

               Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                        const AutoSizeText("Дата та час події",
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 12,
                        ),
                  
                  
                  
                        AutoSizeText('$eventDate',
                        minFontSize: 8,
                        ),
                  
                  
                      ],),
                ]),
              ),
              


               
             

                           


                           
            ],))
        
        );
     
  }
}





