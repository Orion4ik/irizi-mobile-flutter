import 'package:flutter/material.dart';
import 'package:irizi/repo/isar_db/isar_repo.dart';
import 'package:irizi/untils/theme.dart';

import 'events_info_all.dart';
import 'events_materials.dart';


class EventsInfo extends StatelessWidget {
  const EventsInfo({super.key,

    
    required this.ID,
    required this.num,
    required this.object,
    required this.geoOperator,
    required this.state,
    required this.description,
    required this.eventDate,
    required this.eventTypeID,
    required this.flightMissionID,
    required this.verifyDate,
    required this.verifySummary,
    required this.latitude,
    required this.longitude,
   
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
    required this.miOwner,
    required this.service
    
  }); 
  
  
  
   final int? ID;
   final String? num;
   final dynamic eventTypeID;
   final String? eventDate;
   final double? latitude;
  final double? longitude;
   final String? description;
  final String? verifyDate;
   final String? verifySummary;
   final int? flightMissionID;
   final String? state;
   final dynamic object;
   final dynamic geoOperator;
   final int? miOwner;
  final String? miCreateDate;
  final int? miCreateUser;
   final String? miModifyDate;
   final int? miModifyUser;
  final  IsarRepository service;




  
  
  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: SecondColor,
          appBar: AppBar(
            backgroundColor: SecondColor,
            title: const Text('Інформація про подію', style: TextStyle(fontSize: 16),),
            bottom: const TabBar(
              isScrollable: true,
              indicatorColor: Colors.blue,
              labelColor: Colors.blue,
              tabs: [
                Tab(child: Text('Загальна інформація')),
                Tab(child: Text('Інші файли')),
            
              ],
            ),
           
          ),
          body:  TabBarView(
            children: [
              EventsInfoAll(
                  ID: ID,
                  num: num,
                  latitude: latitude,
                  longitude: longitude,
                 description: description,
                  object: object,
                  geoOperator: geoOperator,
                 eventDate: eventDate,
                  state: state,
                  eventTypeID: eventTypeID,
                  flightMissionID: flightMissionID,
                 verifyDate: verifyDate,
                 verifySummary: verifySummary,
                 miCreateDate: miCreateDate,
                 miCreateUser: miCreateUser,
                 miModifyDate: miModifyDate,
                 miModifyUser: miModifyUser,
                 miOwner: miOwner,
                 

              ),
               EventsInfoMaterials(service: service, ID: ID),
            ],
          ),
        ),
      );
  }
}





