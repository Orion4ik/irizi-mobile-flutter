import 'package:auto_size_text/auto_size_text.dart';
import 'package:bottom_picker/bottom_picker.dart';
import 'package:bottom_picker/resources/arrays.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:irizi/db/flight_missions_isar.dart';
import 'package:irizi/model/model_all.dart';
import 'package:irizi/repo/isar_db/isar_repo.dart';
import 'package:irizi/service/resolving_service.dart';
import 'package:irizi/untils/theme.dart';




class FlightMissionInfoAllEdit extends StatefulWidget {
   FlightMissionInfoAllEdit({super.key,

    
    required this.ID,
    required this.num,
    required this.object,
    required this.date,
    required this.eventsState,
    required this.geoJson,
    required this.landGroupID,
    required this.geoOperator,
    required this.state,
    required this.route,
    required this.routeArea,
    required this.routeLength,
    required this.flightGroupID,
    required this.flightDuration,
    required this.flightMissionType,
    required this.latLngStartCombined,
    required this.latitude,
    required this.longitude,
    required this.nearestLocality,
    required this.responsiblePerson,
    required this.technicalSupport,
    required this.updateInfo,
    required this.examinationDateFrom,
    required this.examinationDateTo,
    required this.examinationDatesCombined,
    required this.executionDeadlineDate,
    required this.executionInfo,
    required this.executionPlanDatesCombined,
    required this.executionPlanEndDate,
    required this.executionPlanStartDate,
    required this.meteorologicalData,
    required this.mi_createDate,
    required this.mi_createUser,
    required this.mi_modifyDate,
    required this.mi_modifyUser,
    required this.mi_owner,
    required this.geoEvent,
    required this.geoEventType,
    required this.geoObject,
    required this.ubmEnum,
    required this.geoAdjustmentStatus,
    required this.geoExecutionStatus,
    required this.dictTechnicalSupport,
    required this.geoRoute,
    required this.operator,
    required this.service
    
  }); 
  
  
  
  final int ID;
   String? num;
  final dynamic object;
  final int? flightMissionType;
  final dynamic route ;
  final dynamic geoOperator ;
  final double? latitude ;
  final double? longitude;
  final String? latLngStartCombined;
  final String? examinationDateFrom;
  final String? examinationDateTo ;
  final String? examinationDatesCombined;
  final String? date;
  final String? executionPlanStartDate;
  final String? executionPlanEndDate;
  final String? executionPlanDatesCombined;
  final String? executionDeadlineDate;
  final String? responsiblePerson;
  final String? geoJson;
  final int? executionInfo;
  final int? updateInfo;
  final dynamic technicalSupport;
  final String? meteorologicalData;
  final String? nearestLocality;
  final String? flightDuration;
  final double? routeLength;
  final dynamic routeArea;
  final String? state;
  final String? eventsState;
  final int? flightGroupID;
  final int? landGroupID;
  final int? mi_owner;
  final String? mi_createDate;
  final int? mi_createUser;
  final String? mi_modifyDate;
  final int? mi_modifyUser;
  final List<GeoEvent>? geoEvent;
  final List<GeoEventType>? geoEventType;
  final List<GeoObject>? geoObject;
  final List<GeoOperator>? operator;
  final List<UbmEnum>? ubmEnum;
  final List<GeoRoute>? geoRoute;
  final List<DictTechnicalSupport>? dictTechnicalSupport;
  final List<GeoExecutionStatus>? geoExecutionStatus;
  final List<GeoAdjustmentStatus>? geoAdjustmentStatus;
  final IsarRepository service;
 






  
  

 @override
  State<FlightMissionInfoAllEdit> createState() => _FlightMissionInfoAllEditState();

  
}

class _FlightMissionInfoAllEditState extends State<FlightMissionInfoAllEdit>{
  late  int ID = widget.ID;
  late  String? num = widget.num;
  late  dynamic object = widget.object;
  late  int? flightMissionType = widget.flightMissionType;
  late dynamic route = widget.route;
  late dynamic geoOperator = widget.geoOperator;
  late double? latitude = widget.latitude;
  late double? longitude= widget.longitude;
  late String? latLngStartCombined= widget.latLngStartCombined;
  late String? examinationDateFrom= widget.examinationDateFrom;
  late String? examinationDateTo= widget.examinationDateTo;
  late String? examinationDatesCombined= widget.examinationDatesCombined;
  late String? date= widget.date;
  late String? executionPlanStartDate= widget.executionPlanStartDate;
  late String? executionPlanEndDate= widget.executionPlanEndDate;
  late String? executionPlanDatesCombined= widget.executionPlanDatesCombined;
  late String? executionDeadlineDate= widget.executionDeadlineDate;
  late String? responsiblePerson= widget.responsiblePerson;
  late  String? geoJson = widget.geoJson;
  late int? executionInfo= widget.executionInfo;
  late int? updateInfo= widget.updateInfo;
  late dynamic technicalSupport= widget.technicalSupport;
  late String? meteorologicalData= widget.meteorologicalData;
  late String? nearestLocality= widget.nearestLocality;
  late String? flightDuration= widget.flightDuration;
  late double? routeLength= widget.routeLength;
  late dynamic routeArea= widget.routeArea;
  late String? state= widget.state;
  late String? eventsState= widget.eventsState;
  late int? flightGroupID= widget.flightGroupID;
  late int? landGroupID= widget.landGroupID;
  late int? mi_owner= widget.mi_owner;
  late String? mi_createDate= widget.mi_createDate;
  late int? mi_createUser= widget.mi_createUser;
  late String? mi_modifyDate= widget.mi_modifyDate;
  late int? mi_modifyUser= widget.mi_modifyUser;
  late List<GeoRoute>? geoRoute = widget.geoRoute;
  late List<GeoEvent>? geoEvent= widget.geoEvent;
  late List<GeoEventType>? geoEventType= widget.geoEventType;
  late List<GeoObject>? geoObject= widget.geoObject;
  late List<UbmEnum>? ubmEnum= widget.ubmEnum;
  late List<DictTechnicalSupport>? dictTechnicalSupport = widget.dictTechnicalSupport;
  late List<GeoOperator>? operator = widget.operator;
  TextEditingController _numController = TextEditingController();
  TextEditingController _routeController = TextEditingController();
  TextEditingController _responsiblePersonController = TextEditingController();
  TextEditingController _nearestLocalityController = TextEditingController();
  TextEditingController _meteorologicalDataController = TextEditingController();
  TextEditingController _latitudeDataController = TextEditingController();
  TextEditingController _longitudeDataController = TextEditingController();
  TextEditingController _flightDurationController = TextEditingController();
  TextEditingController _routeAreaController = TextEditingController();
  TextEditingController _routeLengthController = TextEditingController();
  TextEditingController _objectController = TextEditingController();
  late String? displayObject = widget.object ?? '-';
  late String? displayOperator = widget.geoOperator ??'-';
  late String? displayDictTechnicalSupport =  widget.technicalSupport ??'-';
  late String? displayState = widget.state ?? '-';
  late String? displayRoute = widget.route ?? '-';
  String? displayGeoAdjustmentStatus = '-';
  String? displaygeoExecutionStatus = '-';
  UbmEnum? _selectedUbmEnum;
  DictTechnicalSupport? _selectDictTechnicalSupport;
  GeoRoute? _selectGeoRoute;
  GeoObject? _selectGeoObject;
  GeoOperator? _selectGeoOperator;
  late IsarRepository service  = widget.service;

    @override
  void dispose() {
    _numController.dispose();
    _latitudeDataController.dispose();
    _longitudeDataController.dispose();
    _meteorologicalDataController.dispose();
    _nearestLocalityController.dispose();
    _routeAreaController.dispose();
    _routeLengthController.dispose();
    _routeController.dispose();
    _flightDurationController.dispose();
    _responsiblePersonController.dispose();

    super.dispose();
  }

    @override
  Widget build(BuildContext context) {
      Resolver resolver = Resolver('');
      final ebmEnum = resolver.getEnumsByEgroup(ubmEnum, "GEO_FLIGHT_MISSION_STATE");
       

       
  
     
    return 
      Scaffold(
          backgroundColor: SecondColor,

          floatingActionButton: FloatingActionButton(
            heroTag: "editFlightMission",
            backgroundColor: Colors.blue,
            child: const Icon(Icons.save),
            onPressed: ()  async {
              
              final GeoFlightMissionIsar flightMisiionEdit = new GeoFlightMissionIsar  (
                ID: ID,
                num: num,
                date: date,
                state: state,
                geoJson: geoJson ?? '',
                eventsState: eventsState,
                latitude: latitude,
                longitude: longitude,
                object: _selectGeoObject?.ID,
                operator: _selectGeoOperator?.ID,
                landGroupID: landGroupID,
                technicalSupport: _selectDictTechnicalSupport?.ID,
                executionInfo: executionInfo,
                flightGroupID: flightGroupID,
                flightMissionType: flightMissionType,
                nearestLocality: nearestLocality,
                latLngStartCombined: latLngStartCombined,
                route: _selectGeoRoute?.ID,
                routeArea: routeArea,
                routeLength: routeLength,
                updateInfo: updateInfo,
                responsiblePerson: responsiblePerson,
                examinationDateFrom: examinationDateFrom,
                examinationDateTo: examinationDateTo,
                flightDuration: flightDuration,
                examinationDatesCombined: examinationDatesCombined,
                executionDeadlineDate: executionDeadlineDate,
                executionPlanDatesCombined: examinationDatesCombined,
                executionPlanEndDate: executionPlanEndDate,
                executionPlanStartDate: executionPlanStartDate,
                meteorologicalData: meteorologicalData,
                miCreateDate: mi_createDate,
                miCreateUser: mi_createUser,
                miModifyDate: mi_modifyDate,
                miModifyUser: mi_modifyUser,
                miOwner: mi_owner,
              
                


              );
                service.editGeoFlightMission( flightMisiionEdit);
            
              
            }
            ),
          body:  SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
             
              children: [
                    
              Container(
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    const AutoSizeText('Польотне завдання',
                    style: TextStyle(fontWeight: FontWeight.bold),
                    minFontSize: 16,
                    ),
              
                    
              
                    AutoSizeText('$num',
                    minFontSize: 20,
                    ),
              
              
                  ],),
                    
                  
                    
                 
                ],),
              ),
            
            //num
             Container(
              margin: EdgeInsets.all(16),
               child: TextField(
                controller: _numController,
                
                      
                onChanged: (value) {
                   
                   setState(() {
                     num = value;
                     //numNew = num;
                   });
                   
                },
              
            
                
                
                decoration: InputDecoration(
                  hintText: "$num",
                  labelText: 'Введіть нове значення',
                 labelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),

            floatingLabelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        hintStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        helperStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        
            floatingLabelBehavior: FloatingLabelBehavior.auto,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16)),
                             enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.white,
                            width: 1.0,
                          ),
                        ), 
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.blue,
                            width: 2.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: DisableColor,
                            width: 2.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: ErrorColor,
                            width: 2.0,
                          ),
                        ),
                
               

                 

               
               )),
             ),
             
              const Divider(color: Colors.white10),

              //status
                     Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                    child: Row(
                      //crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                         Container(
                           
                             width: 12,
                             height: 12,
                             decoration: const BoxDecoration(
                               color: SuccessColor,
                               shape: BoxShape.circle
                             ),
                                ),
                                  const SizedBox(width: 12),
                       Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                         children:[ 
                    
                          const AutoSizeText('Статус',
                                             minFontSize: 16,
                                             ),
                          AutoSizeText('$displayState',
                                             minFontSize: 14,
                                             ),
                      ]),
                     
                     
                    ],),


                  ),

                  Container(
                    margin: EdgeInsets.only(left:16),
                    alignment: AlignmentDirectional.centerStart,
                    child:  DropdownButton<UbmEnum>(
                    dropdownColor: Colors.blue,  
                 
                    borderRadius: BorderRadius.circular(10),
              value: _selectedUbmEnum,
              onChanged: (newValue) {
                setState(() {
                  _selectedUbmEnum = newValue;
                  displayState = _selectedUbmEnum?.name;
                 // state = newValue?.code;
                  
                });
              },
              items: ebmEnum.map((ubmEnum) {
                return DropdownMenuItem<UbmEnum>(

                  value: ubmEnum,

                  child: Text(
                    ubmEnum.name!
                    ),
                );
              }).toList(),
            ),
                  ),
                    

  


                  const Divider(color: Colors.white10,),
               Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                 child: Row(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [
                 
                        const AutoSizeText('Замовник',
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 16,
                        ),
                               
                        
                               
                        AutoSizeText('$displayOperator',
                        minFontSize: 14,
                        ),
                               
                               
                      ],),
                             ]),
               ),

                Container(
                    margin: EdgeInsets.only(left:16),
                    alignment: AlignmentDirectional.centerStart,
                    child:  DropdownButton<GeoOperator>(
                    dropdownColor: Colors.blue,  
                 
                    borderRadius: BorderRadius.circular(10),
              value: _selectGeoOperator,
              onChanged: (newValue) {
                setState(() {
                  _selectGeoOperator = newValue;
                  displayOperator = _selectGeoOperator?.name;
                 // geoOperator = newValue?.name;
                  
                });
              },
              items: operator?.map((geoOperator) {
                return DropdownMenuItem<GeoOperator>(

                  value: geoOperator,

                  child: Text(
                    geoOperator.name!
                    ),
                );
              }).toList(),
            ),
                  ),


                const Divider(color: Colors.white10,),

                //object
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                 child: Row(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [
                 
                        const AutoSizeText("Об'єкт",
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 16,
                        ),
                               
                        
                               
                        AutoSizeText('$displayObject',
                        minFontSize: 14,
                        ),
                               
                               
                      ],),
                             ]),
               ),

                Container(
                margin: EdgeInsets.all(16),
                 child: Row(
                  
                   children: [
                   DropdownButton<GeoObject>(
                    menuMaxHeight: 200,
                    
                   dropdownColor: Colors.blue,  
                     
                   borderRadius: BorderRadius.circular(10),
                               value: _selectGeoObject,
                               onChanged: (newValue) {
                    setState(() {
                      _selectGeoObject = newValue;
                      object =  _selectGeoObject;
                      displayObject = _selectGeoObject?.name;
                    });
                               },
                               items: geoObject?.map((geoObject) {
                    return DropdownMenuItem<GeoObject>(
                   
                      value: geoObject,
                
                      child: Container(
                        width: 300,
                       
                        child: AutoSizeText(
                                         geoObject.name ?? '-',
                                        maxFontSize: 14,
                                        maxLines: 3,
                                        softWrap: true,
                                        style: TextStyle(
                                        
                      
                                         overflow: TextOverflow.ellipsis
                                        ),
                                        ),
                      ),
                    );
                               }).toList(),
                             ),
                             ]),
               ),
               const Divider(color: Colors.white10,),

               //route
               Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                 child: Row(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [
                 
                        const AutoSizeText('Маршрут',
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 16,
                        ),
                               
                        
                               
                        AutoSizeText('$displayRoute',
                        minFontSize: 14,
                        ),
                               
                               
                      ],),
                             ]),
               ),

           Container(
                margin: EdgeInsets.all(16),
                 child: Row(
                  
                   children: [
                   DropdownButton<GeoRoute>(
                    menuMaxHeight: 200,
                    
                   dropdownColor: Colors.blue,  
                     
                   borderRadius: BorderRadius.circular(10),
                               value: _selectGeoRoute,
                               onChanged: (newValue) {
                    setState(() {
                      _selectGeoRoute = newValue;
                      displayRoute = newValue?.name;
                    });
                               },
                               items: geoRoute?.map((geoRoute) {
                    return DropdownMenuItem<GeoRoute>(
                   
                      value: geoRoute,
                  
                      child: Container(
                        width: 300,
                       
                        child: AutoSizeText(
                                        geoRoute.name!,
                                        maxFontSize: 14,
                                        maxLines: 3,
                                        softWrap: true,
                                        style: TextStyle(
                                        
                      
                                         overflow: TextOverflow.ellipsis
                                        ),
                                        ),
                      ),
                    );
                               }).toList(),
                             ),
                             ]),
               ),
                const Divider(color: Colors.white10,),

                //resposibility person
                Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                 child: Row(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [
                 
                        const AutoSizeText("Відповідальна особа",
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 16,
                        ),
                               
                        
                               
                        AutoSizeText('$responsiblePerson',
                        minFontSize: 14,
                        ),
                               
                               
                      ],),
                             ]),
               ),

                Container(
              margin: EdgeInsets.all(16),
               child: TextField(
                controller: _responsiblePersonController,
                
                      
                onChanged: (value) {
                   
                   setState(() {
                     responsiblePerson = value;
                     //numNew = num;
                   });
                   
                },
              
            
                
                
                decoration: InputDecoration(
                  hintText: "$responsiblePerson",
                  labelText: 'Введіть нове значення',
                 labelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),

            floatingLabelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        hintStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        helperStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        
            floatingLabelBehavior: FloatingLabelBehavior.auto,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16)),
                             enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.white,
                            width: 1.0,
                          ),
                        ), 
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.blue,
                            width: 2.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: DisableColor,
                            width: 2.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: ErrorColor,
                            width: 2.0,
                          ),
                        ),
                
               

                 

               
               )),
             ),
                
                
                
                const Divider(color: Colors.white10,),

              //technical support 
              Container(
                alignment:  AlignmentDirectional.centerStart,
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Wrap(
                   alignment: WrapAlignment.start,
              spacing:8.0,
              
              runSpacing: 8.0,
               crossAxisAlignment: WrapCrossAlignment.start,
               textDirection: TextDirection.ltr,
               verticalDirection: VerticalDirection.down,
                  
                  children: [
                    
                            SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.white),
                                const SizedBox(width: 12),
                    
                   Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    
                const AutoSizeText('Технічні засоби',
                style: TextStyle(fontWeight: FontWeight.bold),
                minFontSize: 16,
                ),
              
                 
              
                AutoSizeText('$technicalSupport',
                minFontSize: 14,
                ),
              
              
                ],),
                
                  
                ],),
              ),

               Container(
                margin: EdgeInsets.all(16),
                 child: Row(
                  
                   children: [
                   DropdownButton<DictTechnicalSupport>(
                    menuMaxHeight: 200,
                    
                   dropdownColor: Colors.blue,  
                     
                   borderRadius: BorderRadius.circular(10),
                               value: _selectDictTechnicalSupport,
                               onChanged: (newValue) {
                    setState(() {
                      _selectDictTechnicalSupport = newValue;
                      technicalSupport = newValue?.name;
                    });
                               },
                               items: dictTechnicalSupport?.map((dictTechnicalSupport) {
                    return DropdownMenuItem<DictTechnicalSupport>(
                   
                      value: dictTechnicalSupport,
                   /*
                    onTap: (){
                      setState(() {
                      _selectDictTechnicalSupport = dictTechnicalSupport;
                      technicalSupport = dictTechnicalSupport.name;
                      
                    });
                    },
                    */
                      child: Container(
                        width: 300,
                       
                        child: AutoSizeText(
                                        dictTechnicalSupport.name!,
                                        maxFontSize: 14,
                                        maxLines: 3,
                                        softWrap: true,
                                        style: TextStyle(
                                        
                      
                                         overflow: TextOverflow.ellipsis
                                        ),
                                        ),
                      ),
                    );
                               }).toList(),
                             ),
                             ]),
               ),
                    
              const Divider(color: Colors.white10,),
                    
               Container(
                
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: 
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    
                            SvgPicture.asset('assets/images/vector/ri_pin-distance-line.svg', color: Colors.white),
                                const SizedBox(width: 12),
                    
                   Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    
                const AutoSizeText('Довжина маршруту',
                style: TextStyle(fontWeight: FontWeight.bold),
                minFontSize: 12,
                ),
              
                 
              
                AutoSizeText(
                  
                  '$routeLength км',
                minFontSize: 16,
                ),
              
                
              
              
                ],),
                
                  
                ],),
              ),

               Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                 children: [ 
                  Container(
                  alignment: AlignmentDirectional.centerStart,
                  width: 200,
                  margin: EdgeInsets.all(16),
                   child: TextField(
                    keyboardType: TextInputType.number,
                    controller: _routeLengthController,
                    
                          
                    onChanged: (value) {
                       
                       setState(() {
                         routeLength = double.parse(value);
                         //numNew = num;
                       });
                       
                    },
                  
                            
                    
                    
                    decoration: InputDecoration(
                      hintText: "$routeLength",
                      labelText: 'Довжина маршруту',
                     labelStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                  
                            floatingLabelStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                        hintStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                        helperStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                        
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(16)),
                                 enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: Colors.white,
                                width: 1.0,
                              ),
                            ), 
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: Colors.blue,
                                width: 2.0,
                              ),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: DisableColor,
                                width: 2.0,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: ErrorColor,
                                width: 2.0,
                              ),
                            ),
                    
                   
                  
                     
                  
                   
                   )),
                             ),
              ]),     
                    
              const Divider(color: Colors.white10,),
            //routeArea        
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                     const Icon(Icons.square_foot_outlined),
                                const SizedBox(width: 12),
                    
                   Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    
                const AutoSizeText('Площа маршруту',
                style: TextStyle(fontWeight: FontWeight.bold),
                minFontSize: 12,
                ),
              
                 
              
                AutoSizeText('$routeArea км',
                minFontSize: 16,
                ),
              
              
                ],),
                
                  
                ],),
              ),
                Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                 children: [ 
                  Container(
                  alignment: AlignmentDirectional.centerStart,
                  width: 200,
                  margin: EdgeInsets.all(16),
                   child: TextField(

                    keyboardType: TextInputType.number,
                    controller: _routeAreaController,
                    
                          
                    onChanged: ( value) {
                       
                       setState(() {
                         routeArea = int.parse(value);
                         //numNew = num;
                       });
                       
                    },
                  
                            
                    
                    
                    decoration: InputDecoration(
                      hintText: "$routeArea",
                      labelText: 'Площа маршруту',
                     labelStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                  
                            floatingLabelStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                        hintStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                        helperStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                        
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(16)),
                                 enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: Colors.white,
                                width: 1.0,
                              ),
                            ), 
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: Colors.blue,
                                width: 2.0,
                              ),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: DisableColor,
                                width: 2.0,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: ErrorColor,
                                width: 2.0,
                              ),
                            ),
                    
                   
                  
                     
                  
                   
                   )),
                             ),
              ]),     
              const Divider(color: Colors.white10,),
                    
              //flight duration
               Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    
                           SvgPicture.asset('assets/images/vector/ri_pin-time.svg', color: Colors.white),
                                const SizedBox(width: 12),
                    
                   Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    
                const AutoSizeText('Тривалість польоту',
                style: TextStyle(fontWeight: FontWeight.bold),
                minFontSize: 12,
                ),
              
                 
              
                AutoSizeText('$flightDuration хв',
                minFontSize: 16,
                ),
              
              
                ],),
                
                  
                ],),
              ),

               Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                 children: [ 
                  Container(
                  alignment: AlignmentDirectional.centerStart,
                  width: 200,
                  margin: EdgeInsets.all(16),
                   child: TextField(
                    keyboardType: TextInputType.number,
                    controller: _flightDurationController,
                    
                          
                    onChanged: (value) {
                       
                       setState(() {
                         flightDuration = value;
                         //numNew = num;
                       });
                       
                    },
                  
                            
                    
                    
                    decoration: InputDecoration(
                      hintText: "$flightDuration",
                      labelText: 'Тривалість польоту',
                     labelStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                  
                            floatingLabelStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                        hintStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                        helperStyle: MaterialStateTextStyle.resolveWith(
                          (Set<MaterialState> states) {
                   if (states.contains(MaterialState.error)) {
                      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                    }
                    if (states.contains(MaterialState.focused)) {
                    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                    }
                  
                    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                  }
                            
                         
                  
                         
                        ),
                        
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(16)),
                                 enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: Colors.white,
                                width: 1.0,
                              ),
                            ), 
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: Colors.blue,
                                width: 2.0,
                              ),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: DisableColor,
                                width: 2.0,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16),
                              borderSide: const BorderSide(
                                color: ErrorColor,
                                width: 2.0,
                              ),
                            ),
                    
                   
                  
                     
                  
                   
                   )),
                             ),
              ]),
                    
              const Divider(color: Colors.white10,),
            
            //start point
               Container(
                alignment: AlignmentDirectional.centerStart,
                margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: Wrap(
                  
                  children: [
                    
                  const Icon(Icons.location_searching_outlined),
                  const SizedBox(width: 8,),
                     const AutoSizeText('Місце старту',
                     style: TextStyle(fontWeight: FontWeight.bold),
                     minFontSize: 12,
                     ),
              
                 
              
                     AutoSizeText('Широта :$latitude  Довгота:$longitude',
                     minFontSize: 16,
                     ),
                  
                   
                 
                    
                  
                ],),
              ),

              Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                  children: [
                     Container(
                      width: 150,
                margin: EdgeInsets.all(8),
                 child: TextField(
                  keyboardType: TextInputType.number,
                  controller: _latitudeDataController,
                  
                        
                  onChanged: (value) {
                     
                     setState(() {
                       latitude = double.parse(value);
                       //numNew = num;
                     });
                     
                  },
                
                          
                  
                  
                  decoration: InputDecoration(
                    hintText: "$latitude",
                    labelText: 'Широта',
                   labelStyle: MaterialStateTextStyle.resolveWith(
                        (Set<MaterialState> states) {
                 if (states.contains(MaterialState.error)) {
                    return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                  }
                  if (states.contains(MaterialState.focused)) {
                  return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                  }
                
                  return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                }
                          
                       
                
                       
                      ),
                
                          floatingLabelStyle: MaterialStateTextStyle.resolveWith(
                        (Set<MaterialState> states) {
                 if (states.contains(MaterialState.error)) {
                    return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                  }
                  if (states.contains(MaterialState.focused)) {
                  return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                  }
                
                  return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                }
                          
                       
                
                       
                      ),
                      hintStyle: MaterialStateTextStyle.resolveWith(
                        (Set<MaterialState> states) {
                 if (states.contains(MaterialState.error)) {
                    return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                  }
                  if (states.contains(MaterialState.focused)) {
                  return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                  }
                
                  return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                }
                          
                       
                
                       
                      ),
                      helperStyle: MaterialStateTextStyle.resolveWith(
                        (Set<MaterialState> states) {
                 if (states.contains(MaterialState.error)) {
                    return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                  }
                  if (states.contains(MaterialState.focused)) {
                  return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                  }
                
                  return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                }
                          
                       
                
                       
                      ),
                      
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(16)),
                               enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: const BorderSide(
                              color: Colors.white,
                              width: 1.0,
                            ),
                          ), 
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: const BorderSide(
                              color: Colors.blue,
                              width: 2.0,
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: const BorderSide(
                              color: DisableColor,
                              width: 2.0,
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: const BorderSide(
                              color: ErrorColor,
                              width: 2.0,
                            ),
                          ),
                  
                 
                
                   
                
                 
                 )),
                           ),
                
                Container(
                margin: EdgeInsets.all(8),
                width: 150,
                 child: TextField(
                  keyboardType: TextInputType.number,
                  controller: _longitudeDataController,
                  
                        
                  onChanged: (value) {
                     
                     setState(() {
                       longitude = double.parse(value);
                       //numNew = num;
                     });
                     
                  },
                
                          
                  
                  
                  decoration: InputDecoration(
                    hintText: "$longitude",
                    labelText: 'Довгота',
                   labelStyle: MaterialStateTextStyle.resolveWith(
                        (Set<MaterialState> states) {
                 if (states.contains(MaterialState.error)) {
                    return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                  }
                  if (states.contains(MaterialState.focused)) {
                  return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                  }
                
                  return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                }
                          
                       
                
                       
                      ),
                
                          floatingLabelStyle: MaterialStateTextStyle.resolveWith(
                        (Set<MaterialState> states) {
                 if (states.contains(MaterialState.error)) {
                    return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                  }
                  if (states.contains(MaterialState.focused)) {
                  return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                  }
                
                  return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                }
                          
                       
                
                       
                      ),
                      hintStyle: MaterialStateTextStyle.resolveWith(
                        (Set<MaterialState> states) {
                 if (states.contains(MaterialState.error)) {
                    return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                  }
                  if (states.contains(MaterialState.focused)) {
                  return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                  }
                
                  return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                }
                          
                       
                
                       
                      ),
                      helperStyle: MaterialStateTextStyle.resolveWith(
                        (Set<MaterialState> states) {
                 if (states.contains(MaterialState.error)) {
                    return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
                  }
                  if (states.contains(MaterialState.focused)) {
                  return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
                  }
                
                  return const TextStyle(color: Colors.white, letterSpacing: 1.3);
                }
                          
                       
                
                       
                      ),
                      
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(16)),
                               enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: const BorderSide(
                              color: Colors.white,
                              width: 1.0,
                            ),
                          ), 
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: const BorderSide(
                              color: Colors.blue,
                              width: 2.0,
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: const BorderSide(
                              color: DisableColor,
                              width: 2.0,
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: const BorderSide(
                              color: ErrorColor,
                              width: 2.0,
                            ),
                          ),
                  
                 
                
                   
                
                 
                 )),
                           ),
                ],),
              


              //nearestLocality
                    const Divider(color: Colors.white10,),
                    
                            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    const AutoSizeText('Найближчий населений пункт',
                    style: TextStyle(fontWeight: FontWeight.bold),
                    minFontSize: 16,
                    ),
              
                 
              
                    AutoSizeText('$nearestLocality',
                    minFontSize: 14,
                    ),
              
              
                  ],),
                    
                  
                    
                 
                    
                  
                ],),
              ),

              Container(
              margin: EdgeInsets.all(16),
               child: TextField(
                controller: _nearestLocalityController,
                
                      
                onChanged: (value) {
                   
                   setState(() {
                     nearestLocality = value;
                     //numNew = num;
                   });
                   
                },
              
            
                
                
                decoration: InputDecoration(
                  hintText: "$nearestLocality  ",
                  labelText: 'Введіть нове значення',
                 labelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),

            floatingLabelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        hintStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        helperStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        
            floatingLabelBehavior: FloatingLabelBehavior.auto,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16)),
                             enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.white,
                            width: 1.0,
                          ),
                        ), 
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.blue,
                            width: 2.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: DisableColor,
                            width: 2.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: ErrorColor,
                            width: 2.0,
                          ),
                        ),
                
               

                 

               
               )),
             ),
                    const Divider(color: Colors.white10,),
              
              //meteorologicalData
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                        const AutoSizeText('Метеорологічні дані',
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 12,
                        ),
                  
                  
                  
                        AutoSizeText('$meteorologicalData',
                        minFontSize: 8,
                        ),
                  
                  
                      ],),
                ]),
              ),

              Container(
              margin: EdgeInsets.all(16),
               child: TextField(
                controller: _meteorologicalDataController,
                
                      
                onChanged: (value) {
                   
                   setState(() {
                     meteorologicalData = value;
                     //numNew = num;
                   });
                   
                },
              
            
                
                
                decoration: InputDecoration(
                  hintText: "$meteorologicalData  ",
                  labelText: 'Введіть нове значення',
                 labelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),

            floatingLabelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        hintStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        helperStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        
            floatingLabelBehavior: FloatingLabelBehavior.auto,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16)),
                             enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.white,
                            width: 1.0,
                          ),
                        ), 
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.blue,
                            width: 2.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: DisableColor,
                            width: 2.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: ErrorColor,
                            width: 2.0,
                          ),
                        ),
                
               

                 

               
               )),
             ),
             const Divider(color: Colors.white10,),
              Container(
                margin: EdgeInsets.all(16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                  Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                          const AutoSizeText('Дата виконання (кінцева)',
                          style: TextStyle(fontWeight: FontWeight.bold),
                          minFontSize: 12,
                          maxLines: 3,
                          ),
                                    
                                       
                                    
                          AutoSizeText(
                            resolver.formatDate(executionDeadlineDate).toString(),
                           // '$executionPlanStartDate',
                          minFontSize: 8,
                          ),
                                    
                                    
                        ],),
                ]),
              ),

               Container(
                margin:  EdgeInsets.only(left:16, right:16, top:16, bottom: 16),
                alignment: AlignmentDirectional.centerStart,
                child: 
                  
                 MaterialButton(
  onPressed: () async {
    _openDateTimePickeEcutionDeadlineDate(context);
  },
  color: Colors.blue,
  textColor: Colors.white,
  child: Icon(
    Icons.calendar_today,
    size: 24,
  ),
  padding: EdgeInsets.all(16),
  shape: CircleBorder(),
)),

              
                
                  
              const Divider(color: Colors.white10,),
                     Container(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      const AutoSizeText('Плановий період виконання з',
                      style: TextStyle(fontWeight: FontWeight.bold),
                      minFontSize: 12,
                      maxLines: 3,
                      ),
                                
                                   
                                
                      AutoSizeText(
                        resolver.formatDate(executionPlanStartDate).toString(),
                       // '$executionPlanStartDate',
                      minFontSize: 8,
                      ),
                                
                                
                    ],),
                  ),
                 
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      const AutoSizeText('Плановий період виконання по',
                      style: TextStyle(fontWeight: FontWeight.bold),
                      minFontSize: 12,
                      maxLines: 2,
                      ),
                                
                             
                                
                      AutoSizeText( resolver.formatDate(executionPlanEndDate),
                      minFontSize: 8,
                      ),
                                
                                
                    ],),
                  ),
                    
                 
                    
                  
                ],),
              ),

              Container(
                margin:  EdgeInsets.only(left:32, right:48, top:16, bottom: 16),
                alignment: AlignmentDirectional.centerStart,
                child: Row (
                  
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                  
                 MaterialButton(
  onPressed: () async {
    _openDateTimePickerExecutionPlanStartDate(context);
  },
  color: Colors.blue,
  textColor: Colors.white,
  child: Icon(
    Icons.calendar_today,
    size: 24,
  ),
  padding: EdgeInsets.all(16),
  shape: CircleBorder(),
),

MaterialButton(
  onPressed: ()  async{
     _openDateTimePickExecutionPlanEndDate(context);
  },
  color: Colors.blue,
  textColor: Colors.white,
  child: Icon(
    Icons.calendar_today,
    size: 24,
  ),
  padding: EdgeInsets.all(16),
  shape: CircleBorder(),
)
                ],),
              ),
              const Divider(color: Colors.white10,),
                    
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      const AutoSizeText('Фактичний період виконання з',
                      style: TextStyle(fontWeight: FontWeight.bold),
                      minFontSize: 12,
                      maxLines: 3,
                      ),
                                
                                   
                                
                      AutoSizeText(
                        
                        resolver.formatDateTime(examinationDateFrom),
                      minFontSize: 8,
                      ),
                                
                                
                    ],),
                  ),
                 
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      const AutoSizeText('Фактичний період виконання по',
                      style: TextStyle(fontWeight: FontWeight.bold),
                      minFontSize: 12,
                      maxLines: 2,
                      ),
                                
                             
                                
                      AutoSizeText(resolver.formatDateTime(examinationDateTo),
                      minFontSize: 8,
                      ),
                                
                                
                    ],),
                  ),
                    
                 
                    
                  
                ],),
              ),

               Container(
                margin: EdgeInsets.only(left:32, right:48, top:16),
                alignment: AlignmentDirectional.centerStart,
                child: Row (
                  
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                  
                 MaterialButton(
  onPressed: () async{
    _openDateTimePickerExaminationDateFrom(context);
  },
  color: Colors.blue,
  textColor: Colors.white,
  child: Icon(
    Icons.calendar_today,
    size: 24,
  ), 
  padding: EdgeInsets.all(16),
  shape: CircleBorder(),
),

MaterialButton(
  onPressed: () async{
    _openDateTimePickerExaminationDateTo(context);
  },
  color: Colors.blue,
  textColor: Colors.white,
  child: Icon(
    Icons.calendar_today,
    size: 24,
  ),
  padding: EdgeInsets.all(16),
  shape: CircleBorder(),
)
                ],),
              ),
              const Divider(color: Colors.white10,),
                    
                    
               
             
                    
                           
                    
                    
                           
            ],))
      
        );

        
     
  }

  requiredValidatorPass(value) {
    RegExp regex = RegExp(r'^.{3,}$');
    if (value.isEmpty) {
      return "Це поле обов'язкове";
    }
    if (!RegExp("^[a-zA-Z0-9+_.-]").hasMatch(value)) {
      return ("Будь ласка введіть корректне значення");
    }
  }


  void _openDateTimePickerExaminationDateFrom(BuildContext context) {
    BottomPicker.dateTime(
      title: 'Оберіть дату та час ',
      titleStyle: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 15,
        color: Colors.black87,
      ),
      onSubmit: (date) {

        setState(() {
          examinationDateFrom = date.toString();
        });
        
        print(date);
      },
      onClose: () {
        print('Picker closed');
      },
      
      dateOrder:  DatePickerDateOrder.dmy,
      iconColor: Colors.white,
      minDateTime: DateTime(2021, 5, 1),
      maxDateTime:DateTime(2100, 12, 31),
      initialDateTime: DateTime.now(),
      buttonText: 'Встановити дату та час',
      use24hFormat: true,
     //backgroundColor: Color(0x151319), 
     
      buttonSingleColor: Colors.blue,
      //gradientColors: [Color(0x151319), Color(0x211E26)],
    ).show(context);
  }


  void _openDateTimePickerExaminationDateTo(BuildContext context) {
     BottomPicker.dateTime(
      title: 'Оберіть дату та час ',
      titleStyle: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 15,
        color: Colors.black87,
      ),
      onSubmit: (date) {

        setState(() {
          examinationDateTo= date.toString();
        });
        
        print(date);
      },
      onClose: () {
        print('Picker closed');
      },
      
      dateOrder:  DatePickerDateOrder.dmy,
      iconColor: Colors.white,
      minDateTime: DateTime(2021, 5, 1),
      maxDateTime:DateTime(2100, 12, 31),
      initialDateTime: DateTime.now(),
      buttonText: 'Встановити дату та час',
      use24hFormat: true,
     //backgroundColor: Color(0x151319), 
     
      buttonSingleColor: Colors.blue,
      //gradientColors: [Color(0x151319), Color(0x211E26)],
    ).show(context);
  }

  void _openDateTimePickerExecutionPlanStartDate(BuildContext context) {
    BottomPicker.dateTime(
      title: 'Оберіть дату та час ',
      titleStyle: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 15,
        color: Colors.black87,
      ),
      onSubmit: (date) {

        setState(() {
          executionPlanStartDate= date.toString();
        });
        
        print(date);
      },
      onClose: () {
        print('Picker closed');
      },
      
      dateOrder:  DatePickerDateOrder.dmy,
      iconColor: Colors.white,
      minDateTime: DateTime(2021, 5, 1),
      maxDateTime:DateTime(2100, 12, 31),
      initialDateTime: DateTime.now(),
      buttonText: 'Встановити дату та час',
      use24hFormat: true,
     //backgroundColor: Color(0x151319), 
     
      buttonSingleColor: Colors.blue,
      //gradientColors: [Color(0x151319), Color(0x211E26)],
    ).show(context);
  }

   void _openDateTimePickExecutionPlanEndDate(BuildContext context) {
    BottomPicker.dateTime(
      title: 'Оберіть дату та час ',
      titleStyle: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 15,
        color: Colors.black87,
      ),
      onSubmit: (date) {

        setState(() {
          executionPlanEndDate= date.toString();
        });
        
        print(date);
      },
      onClose: () {
        print('Picker closed');
      },
      
      dateOrder:  DatePickerDateOrder.dmy,
      iconColor: Colors.white,
      minDateTime: DateTime(2021, 5, 1),
      maxDateTime:DateTime(2100, 12, 31),
      initialDateTime: DateTime.now(),
      buttonText: 'Встановити дату та час',
      use24hFormat: true,
     //backgroundColor: Color(0x151319), 
     
      buttonSingleColor: Colors.blue,
      //gradientColors: [Color(0x151319), Color(0x211E26)],
    ).show(context);


   
 
}


 void _openDateTimePickeEcutionDeadlineDate(BuildContext context) {
    BottomPicker.dateTime(
      title: 'Оберіть дату та час ',
      titleStyle: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 15,
        color: Colors.black87,
      ),
      onSubmit: (date) {

        setState(() {
          executionDeadlineDate= date.toString();
        });
        
        print(date);
      },
      onClose: () {
        print('Picker closed');
      },
      
      dateOrder:  DatePickerDateOrder.dmy,
      iconColor: Colors.white,
      minDateTime: DateTime(2021, 5, 1),
      maxDateTime:DateTime(2100, 12, 31),
      initialDateTime: DateTime.now(),
      buttonText: 'Встановити дату та час',
      use24hFormat: true,
     //backgroundColor: Color(0x151319), 
     
      buttonSingleColor: Colors.blue,
      //gradientColors: [Color(0x151319), Color(0x211E26)],
    ).show(context);
  }



}
  
 









