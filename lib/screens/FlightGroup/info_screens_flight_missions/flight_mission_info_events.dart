import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:irizi/repo/isar_db/isar_repo.dart';
import 'package:irizi/screens/FlightGroup/flight_event_map.dart';
import 'package:irizi/untils/theme.dart';

import '../../../model/model_all.dart';
import '../../../service/resolving_service.dart';

class FlightMissionInfoEvents extends StatelessWidget {
  const FlightMissionInfoEvents({super.key,

    required this.geoEvent,
    required this.geoEventType,
    required this.geoObject,
    required this.ubmEnum,
    required this.geoOperator,
    required this.service


    /*
    required this.ID,
    required this.num,
    required this.object,
    required this.date,
    required this.eventsState,
    required this.geoJson,
    required this.landGroupID,
    required this.operator,
    required this.state,
    required this.route,
    required this.routeArea,
    required this.routeLength,
    required this.flightGroupID,
    required this.flightDuration,
    required this.flightMissionType,
    required this.latLngStartCombined,
    required this.latitude,
    required this.longitude,
    required this.nearestLocality,
    required this.responsiblePerson,
    required this.technicalSupport,
    required this.updateInfo,
    required this.examinationDateFrom,
    required this.examinationDateTo,
    required this.examinationDatesCombined,
    required this.executionDeadlineDate,
    required this.executionInfo,
    required this.executionPlanDatesCombined,
    required this.executionPlanEndDate,
    required this.executionPlanStartDate,
    required this.meteorologicalData,
    required this.mi_createDate,
    required this.mi_createUser,
    required this.mi_modifyDate,
    required this.mi_modifyUser,
    required this.mi_owner
    */
  }); 
  
  final List<GeoEvent>? geoEvent;
  final List<GeoEventType>? geoEventType;
  final List<GeoObject>? geoObject;
  final List<UbmEnum>? ubmEnum;
  final List<GeoOperator>? geoOperator;
  final  IsarRepository service;
  

  /*
  final int ID;
  final String? num;
  final int? object;
  final String? flightMissionType;
  final int? route ;
  final int? operator ;
  final double? latitude ;
  final double? longitude;
  final String? latLngStartCombined;
  final String? examinationDateFrom;
  final String? examinationDateTo ;
  final String? examinationDatesCombined;
  final String? date;
  final String? executionPlanStartDate;
  final String? executionPlanEndDate;
  final String? executionPlanDatesCombined;
  final String? executionDeadlineDate;
  final String? responsiblePerson;
  final String? geoJson;
  final int? executionInfo;
  final int? updateInfo;
  final int? technicalSupport;
  final String? meteorologicalData;
  final String? nearestLocality;
  final String? flightDuration;
  final double? routeLength;
  final double? routeArea;
  final String? state;
  final String? eventsState;
  final int? flightGroupID;
  final int? landGroupID;
  final int? mi_owner;
  final String? mi_createDate;
  final int? mi_createUser;
  final String? mi_modifyDate;
  final int? mi_modifyUser;
*/






  
  
  @override
  Widget build(BuildContext context) {
    Resolver resolver = Resolver('');
    
    if(geoEvent == null ) {
             return Scaffold(
          backgroundColor: SecondColor,

         
          body:  Center(child: 
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 100),
            child: Column(
              children:[ 
                
                const AutoSizeText('Додайте нову подію', minFontSize: 16,),
                Padding(
               padding: const EdgeInsets.only(top: 16),
                child: FilledButton(
                  style: ButtonStyle(
                minimumSize: MaterialStateProperty.all(const Size(200, 70)),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16.0)))),
                  child: const Text('Додати подію'),
                  
                  onPressed: () {
                  
                },),
              )],
            ),
          )
          )
        
        );
    } 

    return   Scaffold(
          backgroundColor: SecondColor,

         
          body:  SingleChildScrollView(
            scrollDirection: Axis.vertical,

            child: SizedBox(
              height: MediaQuery.of(context).size.height *0.8 ,
              child: ListView.builder(
            
                itemCount: geoEvent!.length ,
                itemBuilder: (context, index) {
                    return Container(
                                                                                    margin: const EdgeInsets.symmetric(horizontal: 7, vertical: 8),
                                                                                     height:  MediaQuery.of(context).size.height /4,
                                                                                     //MediaQuery.sizeOf(context).height*0.3,
                                                                                     //         width: MediaQuery.sizeOf(context).width,
                                                                                    decoration: BoxDecoration(
                                                                                        color: FabColorDark,
                                                                                        borderRadius: const BorderRadius.all(Radius.circular(10)),
                                                                                        
                                                                                      ),
                                                                                      child: Column(
                                                                                        children: [
                                                                        
                                                Padding(
                                                                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                                                        child: Row(
                                                                          
                                                                         crossAxisAlignment: CrossAxisAlignment.start,
                                                                        
                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                            children: [
            
                                                                           Row(
                                                                             crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                                  Container(
                                                                        margin: const EdgeInsets.only(left: 16, top: 16),
                                                                         width: 12,
                                                                         height: 12,
                                                                         decoration: const BoxDecoration(
                                                                           color: SuccessColor,
                                                                           shape: BoxShape.circle
                                                                         ),
                                                                            ),
                                                                           
                                                                            Padding(
                                                                              padding: const EdgeInsets.only(top: 12, left:8),
                                                                              child: Text(resolver.resolveData(geoEvent![index].state, ubmEnum, displayKey: 'name', valueKey: 'code' ))),
                                                                          
                                                                           ],), 
                                                                            
                                                                          
                                                                          
                                                                          Container(
                                                                           
                                                                            padding: const EdgeInsets.only(left: 85,right: 8),
                                                                            child: FilledButton(
                                                                         
                                                                         onPressed: (){

                                                                           Navigator.of(context).push(
                                                                                       MaterialPageRoute(
                                                                                        builder: (context) => FlightEventMapScreen(
                                              
                                                                                          ID: geoEvent![index].ID, 
                                                                                          num: geoEvent![index].num ?? '-', 
                                                                                          state: resolver.resolveData(geoEvent![index].state, ubmEnum, displayKey: 'name', valueKey: 'code') ?? '-', 
                                                                                          description: geoEvent![index].description ?? '-', 
                                                                                          object: resolver.resolveData(geoEvent![index].object, geoObject, displayKey: 'name') ?? '-', 
                                                                                          latitude: geoEvent![index].latitude ?? 0.0, 
                                                                                          longitude: geoEvent![index].longitude ?? 0.0, 
                                                                                          geoOperator: resolver.resolveData(geoEvent![index].geoOperator, geoOperator, displayKey: 'name') ?? '-', 
                                                                                          eventDate: resolver.formatDateTime((geoEvent![index].eventDate)) ?? '-', 
                                                                                          eventTypeID: geoEvent![index].eventTypeID! ?? 0, 
                                                                                          flightMissionID:geoEvent![index].flightMissionID!, 
                                                                                          verifyDate: geoEvent![index].verifyDate ?? '-', 
                                                                                          verifySummary: geoEvent![index].verifySummary ?? '-', 
                                                                                          miCreateDate: geoEvent![index].miCreateDate ?? '-', 
                                                                                          miCreateUser: geoEvent![index].miCreateUser ?? 0, 
                                                                                          miModifyDate: geoEvent![index].miModifyDate ?? '-', 
                                                                                          miModifyUser: geoEvent![index].miModifyUser ?? 0, 
                                                                                          miOwner: geoEvent![index].miOwner ?? 0, 
                                                                                          service: service,

                                                                                           
                                                                                        )));
                                                                         },
                                                                         style: ButtonStyle(
                                                                           fixedSize: MaterialStateProperty.all(const Size(25, 25)) ,
                                                                               backgroundColor: MaterialStateProperty.all(
                                                                                   Colors.grey.shade800),
                                                                              
                                                                                  
                                                                                   
                                                                               shape: MaterialStateProperty.all<
                                                                                       RoundedRectangleBorder>(
                                                                                   RoundedRectangleBorder(
                                                                                       borderRadius:
                                                                                           BorderRadius.circular(16.0)))),
                                                                         child: const Center(child: Icon(IconlyLight.location)),
                                                                        
                                                                         )
                                                                         
                                                                            
                                                                          ),
                                                                        
                                                                          ]),
                                                ),
                                                
                                                Container(
                                                                        
                                                                        padding: const EdgeInsets.only(left: 16),
                                                                        child: 
                                                                        Row(children: [
                                                
                                                                          Container(
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Тип події',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                    
                                                   // SizedBox(height: 8,),
                                                    Text(resolver.resolveData(geoEvent![index].eventTypeID, geoEventType, displayKey: 'name'),
                                                    maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],),
                                                                          ),
                                                                          
                                                                          Container(
                                                                        padding: const EdgeInsets.symmetric(horizontal: 16),
                                                                        child: Row(children: [
                                                
                                                                          Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                
                                                                            const AutoSizeText("Об'єкт обстеження",
                                                                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                          ),
                                                                        
                                                
                                                                        Text(resolver.resolveData(geoEvent![index].object, geoObject, displayKey: 'name'),
                                                                        maxLines: 2,
                                                                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                          ),
                                                                          ),
                                                
                                                                          ],)
                                                
                                                                        ],), 
                                                ),
                                                                        ],),
                                                                        
                                                ),
                                                                        
                                                Container(
                                                                        padding: const EdgeInsets.only(left: 16, top: 16),
                                                                        child: 
                                                                        Row(children: [
                                                
                                                                          Container(
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Номер події',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                    
                                                   // SizedBox(height: 8,),
                                                    AutoSizeText(geoEvent![index].num.toString(),
                                                    maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],),
                                                                          ),
                                                
                                                                          Container(
                                                                        padding: const EdgeInsets.only(left: 16),
                                                                        child: Row(children: [
                                                
                                                                          Column(
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                
                                                                            const AutoSizeText('Дата та час події',
                                                                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                          ),
                                                                        
                                                                                       
                                                                        Text(resolver.formatDateTime(geoEvent![index].eventDate) ,
                                                                        maxLines: 2,
                                                                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                          ),
                                                                          ),
                                                
                                                                          ],)
                                                
                                                                        ],), 
                                                ),
                                                                        ],),
                                                                        
                                                ),
                                                
                                                                        
                                                                                        ],
                                                                                      ),
                                                                        
                                                                                  );
            
                }
                ),
            )
            
            )
        
        );
    
  
     
     
  }
}





