import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:irizi/model/model_all.dart';
import 'package:irizi/service/resolving_service.dart';
import 'package:irizi/untils/theme.dart';




class FlightMissionInfoAllMap extends StatelessWidget {
  const FlightMissionInfoAllMap({super.key,

    
    required this.ID,
    required this.num,
    required this.object,
    required this.date,
    required this.eventsState,
    required this.geoJson,
    required this.landGroupID,
    required this.geoOperator,
    required this.state,
    required this.route,
    required this.routeArea,
    required this.routeLength,
    required this.flightGroupID,
    required this.flightDuration,
    required this.flightMissionType,
    required this.latLngStartCombined,
    required this.latitude,
    required this.longitude,
    required this.nearestLocality,
    required this.responsiblePerson,
    required this.technicalSupport,
    required this.updateInfo,
    required this.examinationDateFrom,
    required this.examinationDateTo,
    required this.examinationDatesCombined,
    required this.executionDeadlineDate,
    required this.executionInfo,
    required this.executionPlanDatesCombined,
    required this.executionPlanEndDate,
    required this.executionPlanStartDate,
    required this.meteorologicalData,
    required this.mi_createDate,
    required this.mi_createUser,
    required this.mi_modifyDate,
    required this.mi_modifyUser,
    required this.mi_owner,
    this.geoEvent,
    this.geoEventType,
    this.geoObject,
    this.ubmEnum
    
  }); 
  
  
  
  final int ID;
  final String? num;
  final dynamic object;
  final int? flightMissionType;
  final dynamic route ;
  final dynamic geoOperator ;
  final double? latitude ;
  final double? longitude;
  final String? latLngStartCombined;
  final String? examinationDateFrom;
  final String? examinationDateTo ;
  final String? examinationDatesCombined;
  final String? date;
  final String? executionPlanStartDate;
  final String? executionPlanEndDate;
  final String? executionPlanDatesCombined;
  final String? executionDeadlineDate;
  final String? responsiblePerson;
  final String? geoJson;
  final int? executionInfo;
  final int? updateInfo;
  final dynamic technicalSupport;
  final String? meteorologicalData;
  final String? nearestLocality;
  final String? flightDuration;
  final double? routeLength;
  final dynamic routeArea;
  final String? state;
  final String? eventsState;
  final int? flightGroupID;
  final int? landGroupID;
  final int? mi_owner;
  final String? mi_createDate;
  final int? mi_createUser;
  final String? mi_modifyDate;
  final int? mi_modifyUser;
  final List<GeoEvent>? geoEvent;
  final List<GeoEventType>? geoEventType;
  final List<GeoObject>? geoObject;
  final List<UbmEnum>? ubmEnum;
  






  
  
  @override
  Widget build(BuildContext context) {
      Resolver resolver = Resolver('');
      /*
       IsarService gfms = new IsarService();
        GeoFlightMissionIsar geoFlightMission = new GeoFlightMissionIsar(
        ID: ID,
        num: num,
        object: object,
        flightMissionType: flightMissionType,
        route: route,
        operator: geoOperator,
        latitude: latitude,
        longitude: longitude,
        latLngStartCombined: latLngStartCombined,
        state: state,
        date: date,
        examinationDateFrom: examinationDateFrom,
        examinationDateTo: examinationDateTo,
        examinationDatesCombined: examinationDatesCombined,
        executionPlanStartDate: executionPlanStartDate,
        executionPlanEndDate: executionPlanEndDate,
        routeArea: routeArea ?? 0,
        geoJson: geoJson,
        routeLength: routeLength,
        flightDuration: flightDuration,
        flightGroupID: flightGroupID,
        landGroupID: landGroupID,
        updateInfo: updateInfo,
        technicalSupport: technicalSupport,
        meteorologicalData: meteorologicalData,
        nearestLocality: nearestLocality,
        responsiblePerson: responsiblePerson,
        miCreateDate: mi_createDate ,
        miCreateUser: mi_createUser,
        miModifyDate: mi_modifyDate,
        miModifyUser: mi_modifyUser,
        miOwner: mi_owner,
        eventsState: eventsState,
        executionDeadlineDate: executionDeadlineDate,
        executionInfo: executionInfo,
        executionPlanDatesCombined: executionPlanDatesCombined
      );
      */
    return 
      Scaffold(
          backgroundColor: SecondColor,

          floatingActionButton: FloatingActionButton(
            heroTag: "editFlightMission",
            backgroundColor: Colors.blue,
            child: const Icon(Icons.edit_outlined),
            onPressed: ()  {
              

            
              
            }
            ),
          body:  SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
             
              children: [

              Container(
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    const AutoSizeText('Польотне завдання',
                    style: TextStyle(fontWeight: FontWeight.bold),
                    minFontSize: 16,
                    ),
              
                    
              
                    AutoSizeText('$num',
                    minFontSize: 20,
                    ),
              
              
                  ],),

                  

                 
                ],),
              ),
              const Divider(color: Colors.white10),
 Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                    child: Row(
                      //crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                         Container(
                           
                             width: 12,
                             height: 12,
                             decoration: const BoxDecoration(
                               color: SuccessColor,
                               shape: BoxShape.circle
                             ),
                                ),
                                  const SizedBox(width: 12),
                       Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                         children:[ 

                          const AutoSizeText('Статус',
                                             minFontSize: 16,
                                             ),
                          AutoSizeText('$state',
                                             minFontSize: 14,
                                             ),
                      ]),
                     
                     
                    ],),
                  ),
                  const Divider(color: Colors.white10,),
               Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                 child: Row(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [
                 
                        const AutoSizeText('Замовник',
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 16,
                        ),
                               
                        
                               
                        AutoSizeText('$geoOperator',
                        minFontSize: 14,
                        ),
                               
                               
                      ],),
                             ]),
               ),
                const Divider(color: Colors.white10,),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                 child: Row(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [
                 
                        const AutoSizeText("Об'єкт",
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 16,
                        ),
                               
                        
                               
                        AutoSizeText('$object',
                        minFontSize: 14,
                        ),
                               
                               
                      ],),
                             ]),
               ),
               const Divider(color: Colors.white10,),
               Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                 child: Row(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [
                 
                        const AutoSizeText('Маршрут',
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 16,
                        ),
                               
                        
                               
                        AutoSizeText('$route',
                        minFontSize: 14,
                        ),
                               
                               
                      ],),
                             ]),
               ),
                const Divider(color: Colors.white10,),
                Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                 child: Row(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        
                        children: [
                 
                        const AutoSizeText("Відповідальна особа",
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 16,
                        ),
                               
                        
                               
                        AutoSizeText('$responsiblePerson',
                        minFontSize: 14,
                        ),
                               
                               
                      ],),
                             ]),
               ),
                const Divider(color: Colors.white10,),
              Container(
                alignment: AlignmentDirectional.centerStart,
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Wrap(
                   alignment: WrapAlignment.start,
              spacing:8.0,
              
              runSpacing: 8.0,
               crossAxisAlignment: WrapCrossAlignment.start,
               textDirection: TextDirection.ltr,
               verticalDirection: VerticalDirection.down,
                  
                  children: [
                    
                            SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.white),
                                const SizedBox(width: 12),

                   Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                const AutoSizeText('Технічні засоби',
                style: TextStyle(fontWeight: FontWeight.bold),
                minFontSize: 16,
                ),
              
                 
              
                AutoSizeText('$technicalSupport',
                minFontSize: 14,
                ),
              
              
                ],),
                
                  
                ],),
              ),

              const Divider(color: Colors.white10,),

               Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    
                            SvgPicture.asset('assets/images/vector/ri_pin-distance-line.svg', color: Colors.white),
                                const SizedBox(width: 12),

                   Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                const AutoSizeText('Довжина маршруту',
                style: TextStyle(fontWeight: FontWeight.bold),
                minFontSize: 12,
                ),
              
                 
              
                AutoSizeText(
                  
                  '$routeLength км',
                minFontSize: 16,
                ),
              
              
                ],),
                
                  
                ],),
              ),

              const Divider(color: Colors.white10,),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                     const Icon(Icons.square_foot_outlined),
                                const SizedBox(width: 12),

                   Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                const AutoSizeText('Площа маршруту',
                style: TextStyle(fontWeight: FontWeight.bold),
                minFontSize: 12,
                ),
              
                 
              
                AutoSizeText('$routeArea км',
                minFontSize: 16,
                ),
              
              
                ],),
                
                  
                ],),
              ),

              const Divider(color: Colors.white10,),

              
               Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    
                           SvgPicture.asset('assets/images/vector/ri_pin-time.svg', color: Colors.white),
                                const SizedBox(width: 12),

                   Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                const AutoSizeText('Тривалість польоту',
                style: TextStyle(fontWeight: FontWeight.bold),
                minFontSize: 12,
                ),
              
                 
              
                AutoSizeText('$flightDuration хв',
                minFontSize: 16,
                ),
              
              
                ],),
                
                  
                ],),
              ),

              const Divider(color: Colors.white10,),

               Container(
                margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [

                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      
                              const Icon(Icons.location_searching_outlined),
                              const SizedBox(width: 8,),
                     Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                  const AutoSizeText('Місце старту',
                  style: TextStyle(fontWeight: FontWeight.bold),
                  minFontSize: 12,
                  ),
              
                 
              
                  AutoSizeText('$latLngStartCombined',
                  minFontSize: 16,
                  ),
              
              
                  ],),
                  
                    
                  ],),
                  
                   
                 

                  
                ],),
              ),
          const Divider(color: Colors.white10,),

                            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    const AutoSizeText('Найближчий населений пункт',
                    style: TextStyle(fontWeight: FontWeight.bold),
                    minFontSize: 16,
                    ),
              
                 
              
                    AutoSizeText('$nearestLocality',
                    minFontSize: 14,
                    ),
              
              
                  ],),

                  

                 

                  
                ],),
              ),
const Divider(color: Colors.white10,),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                        const AutoSizeText('Метеорологічні дані',
                        style: TextStyle(fontWeight: FontWeight.bold),
                        minFontSize: 12,
                        ),
                  
                  
                  
                        AutoSizeText('$meteorologicalData',
                        minFontSize: 8,
                        ),
                  
                  
                      ],),
                ]),
              ),
              const Divider(color: Colors.white10,),
 Container(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      const AutoSizeText('Плановий період виконання з',
                      style: TextStyle(fontWeight: FontWeight.bold),
                      minFontSize: 12,
                      maxLines: 3,
                      ),
                                
                                   
                                
                      AutoSizeText(
                        resolver.formatDate(executionPlanStartDate).toString(),
                       // '$executionPlanStartDate',
                      minFontSize: 8,
                      ),
                                
                                
                    ],),
                  ),
                 
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      const AutoSizeText('Плановий період виконання по',
                      style: TextStyle(fontWeight: FontWeight.bold),
                      minFontSize: 12,
                      maxLines: 2,
                      ),
                                
                             
                                
                      AutoSizeText( resolver.formatDate(executionPlanEndDate),
                      minFontSize: 8,
                      ),
                                
                                
                    ],),
                  ),

                 

                  
                ],),
              ),
              const Divider(color: Colors.white10,),

              Container(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      const AutoSizeText('Фактичний період виконання з',
                      style: TextStyle(fontWeight: FontWeight.bold),
                      minFontSize: 12,
                      maxLines: 3,
                      ),
                                
                                   
                                
                      AutoSizeText(
                        
                        resolver.formatDateTime(examinationDateFrom),
                      minFontSize: 8,
                      ),
                                
                                
                    ],),
                  ),
                 
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      const AutoSizeText('Фактичний період виконання по',
                      style: TextStyle(fontWeight: FontWeight.bold),
                      minFontSize: 12,
                      maxLines: 2,
                      ),
                                
                             
                                
                      AutoSizeText(resolver.formatDateTime(examinationDateTo),
                      minFontSize: 8,
                      ),
                                
                                
                    ],),
                  ),

                 

                  
                ],),
              ),
              const Divider(color: Colors.white10,),


               
             

                           


                           
            ],))
      
        );

        
     
  }

  
}





