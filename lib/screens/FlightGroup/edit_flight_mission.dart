import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:flutter_map_geojson/flutter_map_geojson.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'package:irizi/repo/isar_db/isar_repo.dart';
import 'package:irizi/screens/FlightGroup/info_screens_flight_missions/flight_mission_info_all_edit.dart';
import 'package:irizi/untils/theme.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';


import '../../bloc/layer_switch/layer_switch_bloc.dart';
import '../../model/model_all.dart';
import '../../ui/layer_switch_popup.dart';
import 'info_screens_flight_missions/flight_mission_info_all_map.dart';
import 'info_screens_flight_missions/flight_mission_info_events.dart';
import 'info_screens_flight_missions/flight_mission_info_materials.dart';


class FlightMissionEditScreen extends StatefulWidget {
    const FlightMissionEditScreen({
    super.key, 
   required this.ID,
   required this.date,
   required this.num,
   required this.eventsState,
   required this.geoJson,
   required this.flightGroupID,
   required this.flightDuration,
   required this.flightMissionType,
   required this.geoOperator,
   required this.landGroupID,
   required this.latitude,
   required this.longitude,
   required this.object,
   required this.latLngStartCombined,
   required this.nearestLocality,
   required this.responsiblePerson,
   required this.route,
   required this.state,
   required this.meteorologicalData,
   required this.routeArea,
   required this.routeLength,
   required this.technicalSupport,
   required this.updateInfo,
   required this.examinationDateFrom,
   required this.examinationDatesCombined,
   required this.examinationDateTo,
   required this.executionDeadlineDate,
   required this.executionInfo,
   required this.executionPlanDatesCombined,
   required this.executionPlanEndDate,
   required this.executionPlanStartDate,
   required this.geoEvent,
   required this.geoEventType,
   required this.geoObject,
   required this.ubmEnum,
   required this.geoOperatorL,
    required this.mi_createDate,
    required this.mi_createUser,
    required this.mi_modifyDate,
    required this.mi_modifyUser,
    required this.mi_owner,
    required this.service,
    required this.dictTechnicalSupport,
    required this.geoAdjustmentStatus,
    required this.geoExecutionStatus,
    required this.geoRoute,
    required this.operator
    
   
   });


  
  final int ID;
  final String? num;
  final dynamic object;
  final int? flightMissionType;
  final dynamic route ;
  final dynamic geoOperator ;
  final double? latitude ;
  final double? longitude;
  final String? latLngStartCombined;
  final String? examinationDateFrom;
  final String? examinationDateTo ;
  final String? examinationDatesCombined;
  final String? date;
  final String? executionPlanStartDate;
  final String? executionPlanEndDate;
  final String? executionPlanDatesCombined;
  final String? executionDeadlineDate;
  final String? responsiblePerson;
  final String? geoJson;
  final int? executionInfo;
  final int? updateInfo;
  final dynamic technicalSupport;
  final String? meteorologicalData;
  final String? nearestLocality;
  final String? flightDuration;
  final double? routeLength;
  final dynamic routeArea;
  final String? state;
  final String? eventsState;
  final int? flightGroupID;
  final int? landGroupID;
  final List<GeoRoute>? geoRoute;
  final List<GeoEvent>? geoEvent;
  final List<GeoEventType>? geoEventType;
  final List<GeoObject>? geoObject;
  final List<UbmEnum>? ubmEnum;
  final List<GeoOperator>? geoOperatorL;
  final List<DictTechnicalSupport>? dictTechnicalSupport;
  final List<GeoExecutionStatus>? geoExecutionStatus;
  final List<GeoAdjustmentStatus>? geoAdjustmentStatus;
  final List<GeoOperator>? operator;
  final int? mi_owner;
  final String? mi_createDate;
  final int? mi_createUser;
  final String? mi_modifyDate;
  final int? mi_modifyUser;
  final IsarRepository service;
 




  


  
   @override
  State<FlightMissionEditScreen> createState() => _FlightMissionEditScreenState();
}

 

class _FlightMissionEditScreenState extends State<FlightMissionEditScreen> {
  late final int ID = widget.ID;
  late final String? num = widget.num;
  late final dynamic object = widget.object;
  late final int? flightMissionType = widget.flightMissionType;
  late final dynamic route = widget.route;
  late final dynamic geoOperator = widget.geoOperator ;
  late final String? latLngStartCombined = widget.latLngStartCombined;
  late final String? examinationDateFrom = widget.examinationDateFrom;
  late final String? examinationDateTo = widget.examinationDateTo ;
  late final String? examinationDatesCombined = widget.examinationDatesCombined;
  late final String? date = widget.date ;
  late final String? executionPlanStartDate = widget.executionPlanStartDate;
  late final String? executionPlanEndDate = widget.executionPlanEndDate;
  late final String? executionPlanDatesCombined = widget.executionPlanDatesCombined ;
  late final String? executionDeadlineDate = widget.executionDeadlineDate;
  late final String? responsiblePerson = widget.responsiblePerson;
  late final int? executionInfo = widget.executionInfo;
  late final int? updateInfo = widget.updateInfo;
  late final dynamic technicalSupport = widget.technicalSupport;
  late final String? meteorologicalData = widget.meteorologicalData ;
  late final String? nearestLocality = widget.nearestLocality;
  late final String? flightDuration = widget.flightDuration;
  late final double? routeLength = widget.routeLength;
  late final dynamic routeArea = widget.routeArea;
  late final String? state = widget.state;
  late final String? eventsState = widget.eventsState;
  late final int? flightGroupID = widget.flightGroupID ;
  late final int? landGroupID = widget.landGroupID;
  late  final String? geoJson = widget.geoJson;
  late final int? mi_owner = widget.mi_owner;
  late final String? mi_createDate = widget.mi_createDate;
  late final int? mi_createUser = widget.mi_createUser;
  late final String? mi_modifyDate = widget.mi_modifyDate;
  late final int? mi_modifyUser = widget.mi_modifyUser;
  late LatLngBounds? _bounds;
  late List<GeoRoute>? geoRoute = widget.geoRoute;
  late final List<GeoEvent>? geoEvent = widget.geoEvent;
  late final List<GeoEventType>? geoEventType = widget.geoEventType;
  late final List<GeoObject>? geoObject = widget.geoObject;
  late final List<UbmEnum>? ubmEnum = widget.ubmEnum;
  late final List<GeoOperator>? geoOperatorL = widget.geoOperatorL;
  late List<GeoExecutionStatus>? geoExecutionStatus = widget.geoExecutionStatus;
  late List<GeoAdjustmentStatus>? geoAdjustmentStatus = widget.geoAdjustmentStatus;
 final MapController mapController = MapController();
 final PopupController _popupLayerController = PopupController();
  late final List<Marker > _markers;
 late  IsarRepository service = widget.service;
 late List<DictTechnicalSupport>? dictTechnicalSupport =  widget.dictTechnicalSupport;
 late List<GeoOperator>? operator = widget.operator;

   
 // LatLngBounds _bounds;
 // late FollowOnLocationUpdate _followOnLocationUpdate;
//  late StreamController<double?> _followCurrentLocationStreamController;
 
  @override
  void initState() {
    super.initState();
   // _followOnLocationUpdate = FollowOnLocationUpdate.once;
    //_followCurrentLocationStreamController = StreamController<double?>();
    
    loadGeoJson();
    buildMarkersFromEvents(geoEvent!);
    _markers = buildMarkersFromEvents(geoEvent!);
    //parseAndDrawAssetsOnMap();
  }

  @override
  void dispose() {
   // _followCurrentLocationStreamController.close();
    super.dispose();
  }

  Future <void> loadGeoJson() async  {
    String? geoJsonString = geoJson;
    if (geoJsonString == null) return;
    

    LatLngBounds bounds = getLatLngBoundsFromGeoJson(geoJsonString, geoEvent!);

    setState(() {
      _bounds = bounds;
    });

void fitMapToBounds(MapController mapController, LatLngBounds bounds) {
  mapController.fitBounds(
    bounds,
    options: const FitBoundsOptions(padding: EdgeInsets.all(20.0)),
  );
}

    
  }

void moveToMarker(LatLng markerPosition) {
  mapController.move(markerPosition, 16.0); // Change the zoom level (e.g., 16.0) as needed
}

// Example usage:
// Assuming you have a list of markers, and you want to move to the first marker in the list
void onMarkerTap(LatLng markerPosition) {
  moveToMarker(markerPosition);
}


LatLngBounds getLatLngBoundsFromGeoJson(String geoJsonString, List<GeoEvent> events) {
  Map<String, dynamic> geoJsonData = json.decode(geoJsonString);
  List<dynamic> features = geoJsonData['features'];
  List<LatLng> points = [];

  for (var feature in features) {
    Map<String, dynamic> geometry = feature['geometry'];
    String geometryType = geometry['type'];
    List<dynamic> coordinates = geometry['coordinates'];

    if (geometryType == 'Point') {
      double lat = coordinates[1];
      double lng = coordinates[0];
      points.add(LatLng(lat, lng));
    } else if (geometryType == 'LineString') {
      for (var coordinate in coordinates) {
        double lat = coordinate[1];
        double lng = coordinate[0];
        points.add(LatLng(lat, lng));
      }
    }
    
  }

  if (points.isEmpty) {
      for (var event in events) {
    double lat = event.latitude ?? 50.44073286148048;
    double lng = event.longitude ?? 30.575602096138812;

     points.add(LatLng(lat, lng));
  }
  }
  

  return LatLngBounds.fromPoints(points);

  
}


List<Marker> buildMarkersFromEvents(List<GeoEvent> events) {
  List<Marker> markers = [];

  for (var event in events) {
    double lat = event.latitude ?? 50.44073286148048;
    double lng = event.longitude ??  30.575602096138812;

    LatLng latLng = LatLng(lat, lng);

    Marker marker = Marker(
      point: latLng,
      builder: (context) => const Icon(Icons.location_on, color: Colors.blue),
    );

    markers.add(marker);
  }

  return markers;
}









@override
  Widget build(BuildContext context) {
    //GeoJsonParser? parserGeoJson = GeoJsonParser();
 
 // parse GeoJson data - GeoJson is stored as [String]
  
    final double? latitude = widget.latitude ;
    final double? longitude = widget.longitude;
   
   
   /*
    if (geoJson == null  ){
      /*
      ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar
              (backgroundColor: Colors.blue,
                content: Text('Відсутній GeoJSON. Неможливо побудувати маршрут')));
        */
        return  Scaffold(
      backgroundColor: SecondColor,
      appBar: AppBar(backgroundColor: SecondColor, ) ,
      body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
       
        children:[
        Image.asset('assets/images/content/error_404.png'),
        const SizedBox(height: 16,),
        const Text('Маршрут не прокладено', style: TextStyle(fontSize: 16),)] ));
        

   } 
   */

    
   
   
   
    //parserGeoJson.parseGeoJsonAsString(geoJson !);
    return BlocProvider(
     
      create: (_) => LayerBloc(),
      child: Scaffold(
        
        appBar: AppBar(title: Text('ПЗ:$num'), backgroundColor: SecondColor),
        body: BlocBuilder<LayerBloc, Layer>(
          
          builder: (context, activeLayer) {
            return Stack(
              children: [
              
             SlidingUpPanel(
                backdropEnabled: true,
                      parallaxEnabled: true,
                      parallaxOffset: .5,
                      borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(18.0),
                topRight: Radius.circular(18.0)),
              header:  Row(
              //crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 8, horizontal:180),
                  width: 30,
                  height: 5,
                  decoration: BoxDecoration(
                      color: Colors.grey.shade800,
                      borderRadius: const BorderRadius.all(Radius.circular(12.0))),
                ),
              ],
            ),
            
            
                      panel: DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: SecondColor,
          appBar: AppBar(
            backgroundColor: SecondColor,
            
            bottom: const TabBar(
              isScrollable: true,
              indicatorColor: Colors.blue,
              labelColor: Colors.blue,
              tabs: [
                Tab(child: Text('Загальна інформація')),
                Tab(child: Text('Матеріали')),
                Tab(child: Text('Події')),
              ],
            ),
            title: const Text('Інформація про ПЗ'),
          ),
          body:  TabBarView(
            children: [
              FlightMissionInfoAllEdit(
                  ID: ID,
                  date: date,
                  num: num,
                  eventsState: eventsState,
                  landGroupID: landGroupID,
                  geoJson: geoJson,
                  flightDuration: flightDuration,
                  flightGroupID: flightGroupID,
                  flightMissionType: flightMissionType,
                  latLngStartCombined: latLngStartCombined,
                  latitude: latitude,
                  longitude: longitude,
                  meteorologicalData: meteorologicalData,
                  object: object,
                  geoOperator: geoOperator,
                  route: route,
                  routeArea: routeArea ,
                  routeLength: routeLength,
                  nearestLocality: nearestLocality,
                  state: state,
                  technicalSupport: technicalSupport,
                  updateInfo: updateInfo,
                  responsiblePerson: responsiblePerson,
                  examinationDateFrom: examinationDateFrom,
                  examinationDateTo: examinationDateTo,
                  examinationDatesCombined: examinationDatesCombined,
                  executionDeadlineDate: executionDeadlineDate,
                  executionInfo: executionInfo,
                  executionPlanDatesCombined: executionPlanDatesCombined,
                  executionPlanEndDate: executionPlanEndDate,
                  executionPlanStartDate: executionPlanStartDate,
                  mi_createDate: mi_createDate,
                  mi_createUser: mi_createUser,
                  mi_modifyDate: mi_modifyDate,
                  mi_modifyUser: mi_modifyUser,
                  mi_owner: mi_owner,
                  geoEvent: geoEvent,
                  geoEventType:geoEventType,
                  geoObject: geoObject,
                  ubmEnum:ubmEnum,
                  dictTechnicalSupport: dictTechnicalSupport,
                  service: service,
                  geoAdjustmentStatus: geoAdjustmentStatus,
                  geoExecutionStatus:geoExecutionStatus ,
                  geoRoute: geoRoute,
                  operator: operator,

              ),
              FlightMissionInfoMaterials(service: service, flightMissionID: ID),
             FlightMissionInfoEvents(geoEvent: geoEvent, geoEventType: geoEventType, geoObject: geoObject, ubmEnum: ubmEnum, geoOperator:geoOperatorL, service: service,)
            ],
          ),
        ),
      ),
                     
                      color: SecondColor,
                      body:  FlutterMap(
                mapController:mapController,
                options: MapOptions(
                  center: LatLng(latitude! ??  50.44073286148048,longitude! ??  30.575602096138812),

                 
                  //bounds: _bounds,
                  //boundsOptions: const FitBoundsOptions(padding: EdgeInsets.all(20.0)),
                  zoom: 13,
                  minZoom: 2,
                  maxZoom: 18,
                  /*
                  onPositionChanged: (MapPosition position, bool hasGesture) {
                  if (hasGesture && _followOnLocationUpdate != FollowOnLocationUpdate.never) {
                    setState(
                      () => _followOnLocationUpdate = FollowOnLocationUpdate.once,
                    );
                  }
                },
                */
                ),
                nonRotatedChildren: const [
                  
                  /*
                  Positioned(
                
                  right: 20,
                  bottom: 20,
                  child: FloatingActionButton(
                    heroTag: 4,
                    backgroundColor: Colors.blue,
                    onPressed: () {
                      // Follow the location marker on the map when location updated until user interact with the map.
                      /*
                      setState(
                        () => _followOnLocationUpdate = FollowOnLocationUpdate.once,
                      );
                      // Follow the location marker on the map and zoom the map to level 18.
                      _followCurrentLocationStreamController.add(18);
                      */
                    },
                    child: const Icon(
                      Icons.my_location,
                      color: Colors.white,
                    ),
                  ),
                ),
                 */
            
                
            
                
                ],
                children: [
                  TileLayer(
                    urlTemplate: getLayerUrl(activeLayer),
                    subdomains: const ['a', 'b', 'c'],
                    tileProvider: FMTC.instance('mapStore').getTileProvider(FMTCTileProviderSettings(
                    maxStoreLength: 0,
                    behavior: CacheBehavior.onlineFirst
                    ))
                  ),
               //   PolygonLayer(polygons: parserGeoJson.polygons),
             // PolylineLayer(polylines: parserGeoJson.polylines),
             // MarkerLayer(markers: parserGeoJson.markers),
             // MarkerLayer(markers: _markers,)
            
            /*
            CurrentLocationLayer(
                  followCurrentLocationStream:
                      _followCurrentLocationStreamController.stream,
                  followOnLocationUpdate: _followOnLocationUpdate,
                ),
                */
                ],
              ),
            
                    ),

                      const Positioned(
                
                  //left: 20,
                  right: 16,
                  top: 40,
                  child: LayerSwitch(),
            
            
                ),
            
            
            
            ]);
          },
        ),
      
      ),
      
    );
   }
 
 
  }

   String getLayerUrl(Layer layer)  {
    switch (layer) {
      case Layer.normal:
        return 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
      case Layer.satellite:
        return 'http://mt0.google.com/vt/lyrs=s&x={x}&y={y}&z={z}';
      case Layer.terrain:
        return 'http://mt0.google.com/vt/lyrs=m&x={x}&y={y}&z={z}';
    }
  }
  
 
  
