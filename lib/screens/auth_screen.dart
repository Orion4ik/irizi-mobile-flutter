// ignore: file_names


import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:irizi/untils/theme.dart';
import 'package:go_router/go_router.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
 
  bool isHiddenPassword = true;

@override
void initState() {
  super.initState();
}
  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
   
    super.dispose();
  }

  void togglePasswordView() {
    setState(() {
      isHiddenPassword = !isHiddenPassword;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
    backgroundColor: SecondColor,
      body: SingleChildScrollView(
         child: Padding(
              padding: const EdgeInsets.all(20.0),
              child:  Form(
                key: _formKey,
                child: Column(children: <Widget>[

                 
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                      child: SvgPicture.asset( 'assets/images/vector/gis_drone.svg', height: 100, width: 100,)),
                    const SizedBox(
                      height: 30,
                    ),
                    
                     const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                       child: Text(
                          'Вхід через обліковий запис IRIZI',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold),
                        ),
                     ),
                   
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                      child: Text(
                          'Введіть логін та пароль для автентифікації у системі ',
                          style: TextStyle(
                              color: Colors.white24,
                              fontSize: 12,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.normal),
                              textAlign: TextAlign.left,
                        ),
                    ),
                    const SizedBox(height: 64,),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                      child: TextFormField(
                        controller: _emailController,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: (value) {
                          return requiredValidator(value);
                        },
                        autocorrect: false,
                        decoration: InputDecoration(
                          filled: false,
                          prefixIcon: const Icon(
                            Icons.email,
                          ),
                          labelText: 'Електронна пошта',
                          hintText: "Електронна пошта",
                          
                          labelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),

            floatingLabelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        hintStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        helperStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        
            floatingLabelBehavior: FloatingLabelBehavior.auto,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16)),
                             enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.white,
                            width: 1.0,
                          ),
                        ), 
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.blue,
                            width: 2.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: DisableColor,
                            width: 2.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: ErrorColor,
                            width: 2.0,
                          ),
                        ),
                    
                      ),
                        ),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                      child: TextFormField(
               
                        obscureText: isHiddenPassword,
                        controller: _passwordController,
                        validator: (value) {
                          return requiredValidatorPass(value);
                        },
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        decoration: InputDecoration(
                          filled: false,
                          prefixIcon: const Icon(Icons.password_outlined),
                          suffix: InkWell(
                            onTap: togglePasswordView,
                            child: Icon(
                              isHiddenPassword
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                              color: Colors.white,
                            ),
                          ),
                          labelText: 'Пароль',
                          hintText: "Пароль",
            labelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        
            floatingLabelStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        hintStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        helperStyle: MaterialStateTextStyle.resolveWith(
          (Set<MaterialState> states) {
 if (states.contains(MaterialState.error)) {
      return const TextStyle(color: ErrorColor, letterSpacing: 1.3);
    }
    if (states.contains(MaterialState.focused)) {
    return const TextStyle(color: Colors.blue, letterSpacing: 1.3);
    }

    return const TextStyle(color: Colors.white, letterSpacing: 1.3);
  }
            
         

         
        ),
        
            floatingLabelBehavior: FloatingLabelBehavior.auto,

            
           
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16)),
                             enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.white,
                            width: 1.0,
                          ),
                        ), 
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: Colors.blue,
                            width: 2.0,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: DisableColor,
                            width: 2.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            color: ErrorColor,
                            width: 2.0,
                          ),
                        ),
                    
                      )),
                    ),
  const SizedBox(height: 30,),
  Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                      child: Image.asset('assets/images/content/UB_logo.png',
                      
                     
                      height: 100, 
                       width: 50,
                     )),
  FilledButton(
        style: ButtonStyle(
            minimumSize: MaterialStateProperty.all(const Size(200, 70)),
            backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16.0)))),
        child: const Center(
            child: Text(
          'Увійти',
          style: TextStyle(fontFamily: 'Montserrat'),
        )),
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
            context.go('/nav');
          }
        }),
          
        
        

                ],))
                ,
      )
      
    ));
  }

  requiredValidator(value) {
    RegExp regex = RegExp(r'^.{3,}$');
    if (value.isEmpty) {
      return "Це поле обов'язкове";
    }
    if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]").hasMatch(value)) {
      return ("Будь ласка введіть правильний email");
    }
  }

  requiredValidatorPass(value) {
    RegExp regex = RegExp(r'^.{3,}$');
    if (value.isEmpty) {
      return "Це поле обов'язкове";
    }
    if (!RegExp("^[a-zA-Z0-9+_.-]").hasMatch(value)) {
      return ("Будь ласка введіть правильний пароль");
    }
  }

  void _authenticateWithEmailAndPassword(context) {
    if (_formKey.currentState!.validate()) {
      
       context.go("/nav");
    }
  }

  
}