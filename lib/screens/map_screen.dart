import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


import '../bloc/layer_switch/layer_switch_bloc.dart';

import '../ui/layer_switch_popup.dart';

class MapScreen extends StatefulWidget {
   const MapScreen({super.key, required this.title});



  final String title;


  
   @override
  State<MapScreen> createState() => _MapScreenScreenState();
}

class _MapScreenScreenState extends State<MapScreen> {
  

   late FollowOnLocationUpdate _followOnLocationUpdate;
  late StreamController<double?> _followCurrentLocationStreamController;
  @override
  void initState() {
    super.initState();
     
    _followOnLocationUpdate = FollowOnLocationUpdate.always;
    _followCurrentLocationStreamController = StreamController<double?>();

  }

 

  @override
  void dispose() {
    _followCurrentLocationStreamController.close();
  
    super.dispose();
  }



  final MapController mapController = MapController();
  
@override
  Widget build(BuildContext context) {
   
return BlocProvider(
      
      create: (_) => LayerBloc(),
      child: Scaffold(
        body: BlocBuilder<LayerBloc, Layer>(
          
          builder: (context, activeLayer) {
            return FlutterMap(
              
              options: MapOptions(
                center: LatLng(37.7749, -122.4194),
                zoom: 13,
                minZoom: 2,
                maxZoom: 18,
                onPositionChanged: (MapPosition position, bool hasGesture) {
                if (hasGesture && _followOnLocationUpdate != FollowOnLocationUpdate.never) {
                  setState(
                    () => _followOnLocationUpdate = FollowOnLocationUpdate.never,
                  );
                }
              },
              ),
              nonRotatedChildren: [
                Positioned(
              
                right: 20,
                bottom: 20,
                child: FloatingActionButton(
                  
                  backgroundColor: Colors.blue,
                  onPressed: () {
                    // Follow the location marker on the map when location updated until user interact with the map.
                    setState(
                      () => _followOnLocationUpdate = FollowOnLocationUpdate.always,
                    );
                    // Follow the location marker on the map and zoom the map to level 18.
                    _followCurrentLocationStreamController.add(18);
                  },
                  child: const Icon(
                    Icons.my_location,
                    color: Colors.white,
                  ),
                ),
              ),
               

              /*
              Positioned(
                left: 20,
               // right: 20,
                bottom: 20,
                child: FloatingActionButton(
                  
                  backgroundColor: Colors.blue,
                  onPressed: () {
                    
                    
                  },
                  child: const Icon(
                    Icons.layers,
                    color: Colors.white,
                  ),)),
                  */
              const Positioned(
              
                //left: 20,
                right: 16,
                top: 40,
                child: LayerSwitch(),


              ),

              
              ],
              children: [
                TileLayer(
                  urlTemplate: getLayerUrl(activeLayer),
                  subdomains: const ['a', 'b', 'c'],
                  tileProvider: FMTC.instance('mapStore').getTileProvider(FMTCTileProviderSettings(
                  maxStoreLength: 0,
                  behavior: CacheBehavior.onlineFirst
                  ))
                ),

                CurrentLocationLayer(
                followCurrentLocationStream:
                    _followCurrentLocationStreamController.stream,
                followOnLocationUpdate: _followOnLocationUpdate,
              ),
              
              ],
            );
          },
          
        ),
        
      
      ),
      
    );


    
  
}



  String getLayerUrl(Layer layer)  {
    switch (layer) {
      case Layer.normal:
        return 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
      case Layer.satellite:
        return 'http://mt0.google.com/vt/lyrs=s&x={x}&y={y}&z={z}';
      case Layer.terrain:
        return 'http://mt0.google.com/vt/lyrs=m&x={x}&y={y}&z={z}';
    }
  }



  
}