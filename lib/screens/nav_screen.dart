
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:irizi/screens/map_screen.dart';


import '../untils/theme.dart';
import 'FlightGroup/flight_events.dart';
import 'FlightGroup/reg_flight_missions_tasks.dart';

class NavigatorScreen extends StatefulWidget {
  const NavigatorScreen({Key? key}) : super(key: key);

  @override
  State<NavigatorScreen> createState() => _NavigationState();
}

class _NavigationState extends State<NavigatorScreen> {
  int currentTab = 0;
  bool isTap = false;
  final List<Widget> screens = [
      FlightsTaskScreen(title: '',),
     const FlightsEventsScreen(title: '',),
    const MapScreen(title: '',),
  
     
    
  ];

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen =  FlightsTaskScreen(title: '',);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: false,
      body: PageStorage(
        bucket: bucket,
        child: screens[currentTab],
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0.0,
        color: SecondColor,

        shape: const CircularNotchedRectangle(),
        notchMargin: 10,
        child: SizedBox(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                   

                    PopupMenuButton<int>(
                     icon: isTap
                          ? const Icon(
                              IconlyBold.category,
                              color: Colors.blue,
                            )
                          : const Icon(
                              IconlyLight.category,
                              color: Colors.white,
                            ), 
              itemBuilder: (context) => [

                PopupMenuItem( 
                  value: 1,
                  // row with 2 children
                  child: Row(
                    children: [
                      SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.blue),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text("Кабінет пілота")
                    ],
                  ),
                ),
                PopupMenuItem( 
                  value: 2,
                  // row with 2 children
                  child: Row(
                    children: [
                      SvgPicture.asset('assets/images/vector/fa6-solid_map.svg', color: Colors.blue, width: 24),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text("Кабінет наземної групи")
                    ],
                  ),
                ),
                

              ]),
                    const SizedBox(
                      width: 20,
                    ),
                    IconButton(
                        icon: currentTab == 0
                            ? 
                                SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.blue, width: 32,)
                              
                            : SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.white,width: 32, ),
                        onPressed: () {
                          setState(() {
                            currentScreen =  FlightsTaskScreen(title: '',);
                            currentTab = 0;
                          });
                        }),
                        const SizedBox(
                      width: 20,
                    ),
                    IconButton(
                        icon: currentTab == 1
                            ? 
                                SvgPicture.asset('assets/images/vector/grommet-icons_map.svg', color: Colors.blue, width: 24,)
                              
                            : SvgPicture.asset('assets/images/vector/grommet-icons_map.svg', color: Colors.white, width: 24,),
                        onPressed: () {
                          setState(() {
                            currentScreen =  const MapScreen(title: '',);
                            currentTab = 1;
                          });
                        }),
                        const SizedBox(
                      width: 20,
                    ),
                    IconButton(
                        icon: currentTab == 2
                            ? 
                                SvgPicture.asset('assets/images/vector/uiw_map.svg', color: Colors.blue, width: 24,)
                              
                            : SvgPicture.asset('assets/images/vector/uiw_map.svg', color: Colors.white,width: 24,),
                        onPressed: () {
                          setState(() {
                           // currentScreen =  FlightMissionInfo();
                            currentTab = 2;
                          });
                        }),
                  ],
                ),
              ),
              const SizedBox(
                      width: 20,),

               FloatingActionButton(
              heroTag: "btn1",
              backgroundColor: Colors.blue,
              onPressed: () {
                
                
              },
              child: const Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
              
            ],
          ),
        ),
      ),
    );

  }
}

