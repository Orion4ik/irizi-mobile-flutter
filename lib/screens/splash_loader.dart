import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:go_router/go_router.dart';
import 'package:irizi/bloc/network_helper_bloc/network_event.dart';
import 'package:irizi/screens/nav_screen.dart';
import 'package:irizi/untils/theme.dart';
import '../bloc/db_bloc/fight_missions_bloc/db_bloc.dart';
import '../bloc/db_bloc/fight_missions_bloc/db_event.dart';
import '../bloc/db_bloc/fight_missions_bloc/db_state.dart';
import '../bloc/network_helper_bloc/network_bloc.dart';
import '../bloc/network_helper_bloc/network_state.dart';
import '../repo/isar_db/isar_repo.dart';



class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);


  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {










  @override
  void initState() {
    super.initState();
   
    
}



   

  

 
  @override
  void dispose() {
  
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
    backgroundColor:  SecondColor,
        body:
        
        
         BlocConsumer<NetworkBloc, NetworkState>(
               listener: (context, state) {
            if (state.isConnected) {
         ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Підключення до сервера',
          style: TextStyle(color: Colors.white)
          )));
            }
            
           
            else  {
          
          
          ScaffoldMessenger.of(context)
               .showSnackBar(SnackBar(
          content: Text(
            'Додаток працює в оффлайн-режимі',
            style: const TextStyle(color: Colors.white),
            ),
          backgroundColor: Colors.blue ,
          ));
          
            }
            
               },
               
               //Builder
               builder: (context, state) {
         
         
           
     
               if (state.isConnected) {
               return  
               BlocProvider(
          create:(context) =>  GeoFlightMissionBloc(
          isarRepo: IsarRepository(),
             )..add(GetItems()),
           child: BlocConsumer<GeoFlightMissionBloc, GeoFlightMissionState>(
             
            listener: (context, state) {
            if (state is ItemLoading) {
         ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Завантаження', style: TextStyle(color: Colors.white))));
            }
            if (state is ItemSync) {
         ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Синхронізація', style: TextStyle(color: Colors.white))));
            }
            if (state is ItemSaveToDB) {
         ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Зберігання до локальної бази даних', style: TextStyle(color: Colors.white))));
            }
            if (state is ItemConvertation) {
         ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Конвертація ', style: TextStyle(color: Colors.white))));
            }
               
            if (state is ItemLoaded) {
         context.go("/nav");
            }
             if (state is ItemError) {
          ScaffoldMessenger.of(context)
               .showSnackBar(SnackBar(content:
         Text(state.error, style: const TextStyle(color: Colors.white)
               ),
               backgroundColor:  ErrorColor,
            ));
            }
            
            },
            builder: (context, state){
            
            if (state is ItemLoading) {
               return Center(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 64),
            child: Column(
              children: [
              Image.asset('assets/images/content/irizi_icon.png', ),
              SizedBox(height: 16,),
              Text('Завантаження даних'),
              SizedBox(height: 8,),
              CircularProgressIndicator ()
              ]),
          ),
               );
            }
               
            if (state is ItemSaveToDB){
               
         Center(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 64),
            child: Column(
              children: [
              Image.asset('assets/images/content/irizi_icon.png', ),
              SizedBox(height: 16,),
              Text('Збереження даних до локальної бази даних'),
              SizedBox(height: 8,),
              CircularProgressIndicator ()
              ]),
          ),
               );
            }
               
            if (state is ItemConvertation){
               Center(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 64),
            child: Column(
              children: [
              Image.asset('assets/images/content/irizi_icon.png', ),
              SizedBox(height: 16,),
              Text('Конвертація даних'),
              SizedBox(height: 8,),
              CircularProgressIndicator ()
              ]),
          ),
               );
            }
            if (state  is ItemLoaded) {
         Center(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 64),
            child: Column(
              children: [
              Image.asset('assets/images/content/irizi_icon.png', ),
              SizedBox(height: 16,),
              Text('Перенаправлення'),
              SizedBox(height: 8,),
              CircularProgressIndicator ()
              ]),
          ),
               );
             
            }
            if (state is ItemError) {}
               
               
               return  Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                         
                          children:[
                          Image.asset('assets/images/content/error_404.png'),
                          const SizedBox(height: 16,),
                          const Text('Упс, помилка', style: TextStyle(fontSize: 16),)] ),
         
                  );
             
            }
            
            
            ));
               
               }
               
               // <<Offline>>>
          else {
          
               return   
               
               BlocProvider(
          create:(context) =>  GeoFlightMissionDBBloc(
          isarRepo: IsarRepository(),
             )..add(GetItemsFromDB()),
           child: BlocConsumer<GeoFlightMissionDBBloc, GeoFlightMissionState>(
             
            listener: (context, state) {
            if (state is ItemLoading) {
         ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Завантаження', style: TextStyle(color: Colors.white))));
            }
            if (state is ItemSaveToDB) {
         ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Зберігання до локальної бази даних', style: TextStyle(color: Colors.white))));
            }
            if (state is ItemConvertation) {
         ScaffoldMessenger.of(context)
               .showSnackBar(const SnackBar(
          backgroundColor: Colors.green,
          content: Text('Конвертація ', style: TextStyle(color: Colors.white))));
            }
               
            if (state is ItemLoaded) {
         context.go("/nav");
            }
             if (state is ItemError) {
          ScaffoldMessenger.of(context)
               .showSnackBar(SnackBar(content:
         Text(state.error, style: const TextStyle(color: Colors.white)
               ),
               backgroundColor:  ErrorColor,
            ));
            }
            
            },
            builder: (context, state){
            
            if (state is ItemLoading) {
               return Center(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 64),
            child: Column(
              children: [
              Image.asset('assets/images/content/irizi_icon.png', ),
              SizedBox(height: 16,),
              Text('Завантаження даних'),
              SizedBox(height: 8,),
              CircularProgressIndicator ()
              ]),
          ),
               );
            }
               
            if (state is ItemSaveToDB){
               
         Center(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 64),
            child: Column(
              children: [
              Image.asset('assets/images/content/irizi_icon.png', ),
              SizedBox(height: 16,),
              Text('Збереження даних до локальної бази даних'),
              SizedBox(height: 8,),
              CircularProgressIndicator ()
              ]),
          ),
               );
            }
               
            if (state is ItemConvertation){
               Center(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 64),
            child: Column(
              children: [
              Image.asset('assets/images/content/irizi_icon.png', ),
              SizedBox(height: 16,),
              Text('Конвертація даних'),
              SizedBox(height: 8,),
              CircularProgressIndicator ()
              ]),
          ),
               );
            }
            if (state  is ItemLoaded) {
         Center(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 64),
            child: Column(
              children: [
              Image.asset('assets/images/content/irizi_icon.png', ),
              SizedBox(height: 16,),
              Text('Перенаправлення'),
              SizedBox(height: 8,),
              CircularProgressIndicator ()
              ]),
          ),
               );
             
            }
            if (state is ItemError) {}
               
               
               return  Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                         
                          children:[
                          Image.asset('assets/images/content/error_404.png'),
                          const SizedBox(height: 16,),
                          const Text('Упс, помилка', style: TextStyle(fontSize: 16),)] ),
                  );
             
            }
            
            
            ));
               
               /*
               Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                         
                          children:[
                          Image.asset('assets/images/content/error_404.png'),
                          const SizedBox(height: 16,),
                          const Text('Відсутнє підключення до інтернету. Підключіться до мережі щов відновити завантаження. Сторінка оновиться автоматично', style: TextStyle(fontSize: 16),),
                          const SizedBox(height: 16),
               
                          FilledButton(
                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      Colors.blue),
                                  minimumSize:
                                      MaterialStateProperty.all(const Size(100, 70)),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(16.0)))),
                onPressed: () {
                  context.go('/nav');
                },
                child: const Text('Перейти в оффлайн режим'),
              ),
                          ] 
                          
                          ),
                  );
          */
          }
               return CircularProgressIndicator();
               })
        
        );
  }

}