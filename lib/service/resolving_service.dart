 import 'package:intl/intl.dart';
import 'package:irizi/db/material_flight_event_isar.dart';
import 'package:irizi/db/material_flight_mission_isar.dart';
import 'package:irizi/model/model_all.dart';
 
 class Resolver {
   Resolver (this.title );

final String title;

  resolveData(objectId, data, {displayKey = 'name', valueKey = 'ID'}) {
    dynamic objectName = '-';
    if (objectId == null) return objectName;
    for (final object in data) {
      if (object[valueKey] == objectId) {
       
        objectName = object[displayKey];
        break;
      }
    }
    return objectName;
  }



  formatDate(dateString) {
  if (dateString == null) return '';
  
  final dateFormat = DateFormat('dd.MM.yyyy');
   final date = dateFormat.format(DateTime.parse(dateString));
  return date;
 }

 
  formatDateTime(dateString)  {
  if (dateString == null) return '-';
  
      final dateFormat = DateFormat('dd.MM.yyyy HH:mm:ss');
   final date = dateFormat.format(DateTime.parse(dateString));
  return date;
  }


List<GeoEvent> getEventsByFlightTaskID(List<GeoEvent>? allEvents, int flightTaskID) {
  return allEvents!.where((event) => event.flightMissionID == flightTaskID).toList();
}

List<FlightMissionMaterialIsar> getMaterialssByFlightTaskID(List<FlightMissionMaterialIsar>? allMaterials, int flightTaskID) {
  return allMaterials!.where((material) => material.ID == flightTaskID).toList();
}

List<FlightEventMaterialIsar> getMaterialsEventByFlightTaskID(List<FlightEventMaterialIsar>? allMaterials, int flightTaskID) {
  return allMaterials!.where((material) => material.ID == flightTaskID).toList();
}


List<UbmEnum> getEnumsByEgroup(List<UbmEnum>? ubmEnum, String eGroup) {
  return ubmEnum!.where((event) => event.eGroup == eGroup).toList();
}




  
  
  
  
   
 }




 

 
 