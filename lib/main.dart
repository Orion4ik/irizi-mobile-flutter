import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_map_tile_caching/flutter_map_tile_caching.dart';
import 'package:go_router/go_router.dart';
import 'package:irizi/bloc/db_bloc/fight_missions_bloc/db_event.dart';
import 'package:irizi/bloc/network_helper_bloc/network_event.dart';
import 'package:irizi/db/flight_events_isar.dart';
import 'package:irizi/db/flight_missions_isar.dart';
import 'package:irizi/db/material_flight_event_isar.dart';
import 'package:irizi/repo/isar_db/isar_repo.dart';
import 'package:irizi/screens/nav_screen.dart';
import 'package:irizi/screens/splash_loader.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';
import 'bloc/db_bloc/fight_missions_bloc/db_bloc.dart';
import 'bloc/network_helper_bloc/network_bloc.dart';
import 'db/dict_technical_support_isar.dart';
import 'db/geo_adjustment_status_isar.dart';
import 'db/geo_event_type_isar.dart';
import 'db/geo_execution_status_isar.dart';
import 'db/geo_flight_group_isar.dart';
import 'db/geo_flight_mission_type_isar.dart';
import 'db/geo_land_group_isar.dart';
import 'db/geo_objects_isar.dart';
import 'db/geo_operator_isar.dart';
import 'db/geo_route_isar.dart';
import 'db/material_flight_mission_isar.dart';
import 'db/uba_user_isar.dart';
import 'db/udb_enums_isar.dart';
import 'untils/theme.dart';

Future<void> main() async {
 final Connectivity connectivity = Connectivity();
   WidgetsFlutterBinding.ensureInitialized();   
  await FlutterMapTileCaching.initialise();
  await FMTC.instance('mapStore').manage.createAsync();
  
  final dir = await getApplicationSupportDirectory();
  final isar = await  Isar.open(
    [GeoFlightMissionIsarSchema, 
    GeoEventIsarSchema,
     DictTechnicalSupportIsarSchema,
     GeoAdjustmentStatusIsarSchema,
     GeoEventTypeIsarSchema,
     GeoExecutionStatusIsarSchema,
     GeoFlightGroupIsarSchema,
     GeoFlightMissionTypeIsarSchema,
     GeoLandGroupIsarSchema,
     GeoObjectIsarSchema,
     GeoOperatorIsarSchema,
     GeoRouteIsarSchema,
     UbaUserIsarSchema,
     UbmEnumIsarSchema,
     FlightMissionMaterialIsarSchema,
     FlightEventMaterialIsarSchema
     
     ], 
    inspector: true,
    directory: dir.path
); 

  runApp( MultiBlocProvider(
    providers: [
        RepositoryProvider(
          create: (context) => IsarRepository(),
          child: BlocProvider(
                create: (context) => GeoFlightMissionBloc(
                      isarRepo:
                          RepositoryProvider.of<IsarRepository>(context),
                    )..add(GetItems()),
                  ),
        ),

                 BlocProvider<NetworkBloc>(
              create: (context) => NetworkBloc(connectivity)..add(CheckConnectivity())),
    ],

    child: MyApp( 
      isar: isar,
      connectivity: connectivity
      ),
  ));
 
}



class MyApp extends StatelessWidget {
   const MyApp({super.key,
   
   required Isar isar, required Connectivity connectivity
   });

  

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => MaterialApp.router(
        routeInformationProvider: _router.routeInformationProvider,
        routeInformationParser: _router.routeInformationParser,
        routerDelegate: _router.routerDelegate,
        theme: ThemeClass.lightTheme,
        darkTheme: ThemeClass.darkTheme,
        localizationsDelegates: [
    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
  ],
  supportedLocales: [
    Locale('en'), // English
    Locale('uk'), // Ukrainian
  ],
locale: Locale('uk'),
        title: 'IRIZI',
        
      );
  }

  final GoRouter _router = GoRouter(
    routes: <GoRoute>[
      
      GoRoute(
        path: '/',
        builder: (BuildContext context, GoRouterState state) =>
              SplashScreen(),
      ),
      GoRoute(
        path: '/nav',
        builder: (BuildContext context, GoRouterState state) =>
               const NavigatorScreen(),
      ),
      
    ],
  );


class ThemeClass {
  static ThemeData lightTheme = ThemeData(
      useMaterial3: true,
      colorScheme: defaultColorScheme,
      scaffoldBackgroundColor: const Color.fromARGB(100, 20, 18, 24),
      primarySwatch: Colors.blue,
      inputDecorationTheme: const InputDecorationTheme(
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 3, color: PrimaryColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 3, color: PrimaryColor),
          )),
      appBarTheme: const AppBarTheme(
        backgroundColor: Colors.blue,
      ));

  static ThemeData darkTheme = ThemeData(
    useMaterial3: true,
    scaffoldBackgroundColor: SecondColor,
    appBarTheme: const AppBarTheme(
      backgroundColor: SecondColor,
    ),

    disabledColor: Colors.white30,
    primaryColorDark: PrimaryColor,
    fontFamily: 'Montserrat',
    inputDecorationTheme: const InputDecorationTheme(
      
      hintStyle: TextStyle(color: Colors.white),
            floatingLabelBehavior: FloatingLabelBehavior.auto,
      floatingLabelStyle: TextStyle(color: Colors.white),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: ErrorColor),
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: Colors.white30),
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: PrimaryColor),
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
    ), colorScheme: defaultColorScheme.copyWith(primary: Colors.blue, error: ErrorColor),
  );
}
