import 'package:isar/isar.dart';

part 'udb_enums_isar.g.dart';

@Collection()
class UbmEnumIsar {
  UbmEnumIsar({
    required this.ID,
    required this.code,
    required this.name,
    required this.eGroup,
  });

  Id? id = Isar.autoIncrement;
  
  @Index(unique: true, replace: true)
  late int? ID;

  @Index()
  late final String? code;

  @Index()
  late  String? name;

  @Index()
  late  String? eGroup;
}