import 'package:isar/isar.dart';

part 'uba_user_isar.g.dart';

@Collection()
class UbaUserIsar {
  UbaUserIsar({
    required this.ID,
    required this.name,
     this.fullName,
  });
  
  Id? id = Isar.autoIncrement;

   @Index(unique:true,replace:true)
  late int? ID;

  @Index()
  late  String? name;

  @Index()
  late  String? fullName;
}