import 'package:isar/isar.dart';

part 'geo_operator_isar.g.dart';

@Collection()
class GeoOperatorIsar {
  GeoOperatorIsar({
    required this.ID,
    required this.name,
    required this.code,
    required this.state,
    required this.phone,
    required this.email,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
   Id? id = Isar.autoIncrement;

   
  @Index(unique: true, replace: true)
  late  int? ID;

  @Index()
  late  String? name;

   @Index()
  late  String? code;

   @Index()
  late  String? state;

   @Index()
  late  String? phone;

   @Index()
  late  String? email;

   @Index()
  late  int? miOwner;

   @Index()
  late  String? miCreateDate;

   @Index()
  late  int? miCreateUser;

   @Index()
  late  String? miModifyDate;

   @Index()
  late  int? miModifyUser;



}