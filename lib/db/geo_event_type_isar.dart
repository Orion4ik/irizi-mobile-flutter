import 'package:isar/isar.dart';

part 'geo_event_type_isar.g.dart';

@Collection()
class GeoEventTypeIsar {
  GeoEventTypeIsar({
    required this.ID,
    required this.name,
    required this.color,
     this.icon,
     this.iconFile,
     this.iconDataUrl,
    required this.operatorID,
    required this.objectId,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });

  Id? id = Isar.autoIncrement;

  @Index(unique:  true, replace: true)
  late  int? ID;
  
  @Index()
  late  String? name;

  late  String? color;

  late  String? icon;

  
  late  String? iconFile;
  late  String? iconDataUrl;

  @Index()
  late final int? operatorID;

  @Index()
  late  int? objectId;
  
  @Index()
  late  int? miOwner;

  @Index()
  late  String? miCreateDate;

  @Index()
  late  int? miCreateUser;

  @Index()
  late  String? miModifyDate;

  @Index()
  late  int? miModifyUser;
  
  
}