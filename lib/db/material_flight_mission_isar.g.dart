// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'material_flight_mission_isar.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetFlightMissionMaterialIsarCollection on Isar {
  IsarCollection<FlightMissionMaterialIsar> get flightMissionMaterialIsars =>
      this.collection();
}

const FlightMissionMaterialIsarSchema = CollectionSchema(
  name: r'FlightMissionMaterialIsar',
  id: 8568917604785753595,
  properties: {
    r'ID': PropertySchema(
      id: 0,
      name: r'ID',
      type: IsarType.long,
    ),
    r'dateCreation': PropertySchema(
      id: 1,
      name: r'dateCreation',
      type: IsarType.dateTime,
    ),
    r'dateEdit': PropertySchema(
      id: 2,
      name: r'dateEdit',
      type: IsarType.dateTime,
    ),
    r'extension': PropertySchema(
      id: 3,
      name: r'extension',
      type: IsarType.string,
    ),
    r'name': PropertySchema(
      id: 4,
      name: r'name',
      type: IsarType.string,
    ),
    r'size': PropertySchema(
      id: 5,
      name: r'size',
      type: IsarType.long,
    ),
    r'url': PropertySchema(
      id: 6,
      name: r'url',
      type: IsarType.string,
    )
  },
  estimateSize: _flightMissionMaterialIsarEstimateSize,
  serialize: _flightMissionMaterialIsarSerialize,
  deserialize: _flightMissionMaterialIsarDeserialize,
  deserializeProp: _flightMissionMaterialIsarDeserializeProp,
  idName: r'id',
  indexes: {
    r'ID': IndexSchema(
      id: 5573724008404263404,
      name: r'ID',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'ID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'size': IndexSchema(
      id: -3849508999343250072,
      name: r'size',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'size',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'name': IndexSchema(
      id: 879695947855722453,
      name: r'name',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'name',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'url': IndexSchema(
      id: -5756857009679432345,
      name: r'url',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'url',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'extension': IndexSchema(
      id: 8733860368720318111,
      name: r'extension',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'extension',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'dateCreation': IndexSchema(
      id: 7825450381003139768,
      name: r'dateCreation',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'dateCreation',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'dateEdit': IndexSchema(
      id: 2309063707250300167,
      name: r'dateEdit',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'dateEdit',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _flightMissionMaterialIsarGetId,
  getLinks: _flightMissionMaterialIsarGetLinks,
  attach: _flightMissionMaterialIsarAttach,
  version: '3.1.0+1',
);

int _flightMissionMaterialIsarEstimateSize(
  FlightMissionMaterialIsar object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.extension;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.url;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _flightMissionMaterialIsarSerialize(
  FlightMissionMaterialIsar object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.ID);
  writer.writeDateTime(offsets[1], object.dateCreation);
  writer.writeDateTime(offsets[2], object.dateEdit);
  writer.writeString(offsets[3], object.extension);
  writer.writeString(offsets[4], object.name);
  writer.writeLong(offsets[5], object.size);
  writer.writeString(offsets[6], object.url);
}

FlightMissionMaterialIsar _flightMissionMaterialIsarDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = FlightMissionMaterialIsar(
    ID: reader.readLongOrNull(offsets[0]),
    dateCreation: reader.readDateTime(offsets[1]),
    dateEdit: reader.readDateTime(offsets[2]),
    extension: reader.readStringOrNull(offsets[3]),
    name: reader.readStringOrNull(offsets[4]),
    size: reader.readLongOrNull(offsets[5]),
    url: reader.readStringOrNull(offsets[6]),
  );
  object.id = id;
  return object;
}

P _flightMissionMaterialIsarDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readDateTime(offset)) as P;
    case 2:
      return (reader.readDateTime(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readLongOrNull(offset)) as P;
    case 6:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _flightMissionMaterialIsarGetId(FlightMissionMaterialIsar object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _flightMissionMaterialIsarGetLinks(
    FlightMissionMaterialIsar object) {
  return [];
}

void _flightMissionMaterialIsarAttach(
    IsarCollection<dynamic> col, Id id, FlightMissionMaterialIsar object) {
  object.id = id;
}

extension FlightMissionMaterialIsarQueryWhereSort on QueryBuilder<
    FlightMissionMaterialIsar, FlightMissionMaterialIsar, QWhere> {
  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhere> anyID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'ID'),
      );
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhere> anySize() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'size'),
      );
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhere> anyDateCreation() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'dateCreation'),
      );
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhere> anyDateEdit() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'dateEdit'),
      );
    });
  }
}

extension FlightMissionMaterialIsarQueryWhere on QueryBuilder<
    FlightMissionMaterialIsar, FlightMissionMaterialIsar, QWhereClause> {
  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [null],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> iDEqualTo(int? ID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [ID],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> iDNotEqualTo(int? ID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> iDGreaterThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [ID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> iDLessThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [],
        upper: [ID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> iDBetween(
    int? lowerID,
    int? upperID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [lowerID],
        includeLower: includeLower,
        upper: [upperID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> sizeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'size',
        value: [null],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> sizeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'size',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> sizeEqualTo(int? size) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'size',
        value: [size],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> sizeNotEqualTo(int? size) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'size',
              lower: [],
              upper: [size],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'size',
              lower: [size],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'size',
              lower: [size],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'size',
              lower: [],
              upper: [size],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> sizeGreaterThan(
    int? size, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'size',
        lower: [size],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> sizeLessThan(
    int? size, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'size',
        lower: [],
        upper: [size],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> sizeBetween(
    int? lowerSize,
    int? upperSize, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'size',
        lower: [lowerSize],
        includeLower: includeLower,
        upper: [upperSize],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [null],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'name',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> nameEqualTo(String? name) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [name],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> nameNotEqualTo(String? name) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> urlIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'url',
        value: [null],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> urlIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'url',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> urlEqualTo(String? url) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'url',
        value: [url],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> urlNotEqualTo(String? url) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'url',
              lower: [],
              upper: [url],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'url',
              lower: [url],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'url',
              lower: [url],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'url',
              lower: [],
              upper: [url],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> extensionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'extension',
        value: [null],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> extensionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'extension',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> extensionEqualTo(String? extension) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'extension',
        value: [extension],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> extensionNotEqualTo(String? extension) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'extension',
              lower: [],
              upper: [extension],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'extension',
              lower: [extension],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'extension',
              lower: [extension],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'extension',
              lower: [],
              upper: [extension],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> dateCreationEqualTo(DateTime dateCreation) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'dateCreation',
        value: [dateCreation],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> dateCreationNotEqualTo(DateTime dateCreation) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateCreation',
              lower: [],
              upper: [dateCreation],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateCreation',
              lower: [dateCreation],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateCreation',
              lower: [dateCreation],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateCreation',
              lower: [],
              upper: [dateCreation],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> dateCreationGreaterThan(
    DateTime dateCreation, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'dateCreation',
        lower: [dateCreation],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> dateCreationLessThan(
    DateTime dateCreation, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'dateCreation',
        lower: [],
        upper: [dateCreation],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> dateCreationBetween(
    DateTime lowerDateCreation,
    DateTime upperDateCreation, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'dateCreation',
        lower: [lowerDateCreation],
        includeLower: includeLower,
        upper: [upperDateCreation],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> dateEditEqualTo(DateTime dateEdit) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'dateEdit',
        value: [dateEdit],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> dateEditNotEqualTo(DateTime dateEdit) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateEdit',
              lower: [],
              upper: [dateEdit],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateEdit',
              lower: [dateEdit],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateEdit',
              lower: [dateEdit],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'dateEdit',
              lower: [],
              upper: [dateEdit],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> dateEditGreaterThan(
    DateTime dateEdit, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'dateEdit',
        lower: [dateEdit],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> dateEditLessThan(
    DateTime dateEdit, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'dateEdit',
        lower: [],
        upper: [dateEdit],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterWhereClause> dateEditBetween(
    DateTime lowerDateEdit,
    DateTime upperDateEdit, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'dateEdit',
        lower: [lowerDateEdit],
        includeLower: includeLower,
        upper: [upperDateEdit],
        includeUpper: includeUpper,
      ));
    });
  }
}

extension FlightMissionMaterialIsarQueryFilter on QueryBuilder<
    FlightMissionMaterialIsar, FlightMissionMaterialIsar, QFilterCondition> {
  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> iDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> iDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> iDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> iDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'ID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> dateCreationEqualTo(DateTime value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dateCreation',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> dateCreationGreaterThan(
    DateTime value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dateCreation',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> dateCreationLessThan(
    DateTime value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dateCreation',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> dateCreationBetween(
    DateTime lower,
    DateTime upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dateCreation',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> dateEditEqualTo(DateTime value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dateEdit',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> dateEditGreaterThan(
    DateTime value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dateEdit',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> dateEditLessThan(
    DateTime value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dateEdit',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> dateEditBetween(
    DateTime lower,
    DateTime upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dateEdit',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> extensionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'extension',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> extensionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'extension',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> extensionEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'extension',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> extensionGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'extension',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> extensionLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'extension',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> extensionBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'extension',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> extensionStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'extension',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> extensionEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'extension',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
          QAfterFilterCondition>
      extensionContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'extension',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
          QAfterFilterCondition>
      extensionMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'extension',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> extensionIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'extension',
        value: '',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> extensionIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'extension',
        value: '',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
          QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
          QAfterFilterCondition>
      nameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> sizeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'size',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> sizeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'size',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> sizeEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'size',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> sizeGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'size',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> sizeLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'size',
        value: value,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> sizeBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'size',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> urlIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'url',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> urlIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'url',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> urlEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> urlGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> urlLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> urlBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'url',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> urlStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> urlEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
          QAfterFilterCondition>
      urlContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
          QAfterFilterCondition>
      urlMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'url',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> urlIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'url',
        value: '',
      ));
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterFilterCondition> urlIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'url',
        value: '',
      ));
    });
  }
}

extension FlightMissionMaterialIsarQueryObject on QueryBuilder<
    FlightMissionMaterialIsar, FlightMissionMaterialIsar, QFilterCondition> {}

extension FlightMissionMaterialIsarQueryLinks on QueryBuilder<
    FlightMissionMaterialIsar, FlightMissionMaterialIsar, QFilterCondition> {}

extension FlightMissionMaterialIsarQuerySortBy on QueryBuilder<
    FlightMissionMaterialIsar, FlightMissionMaterialIsar, QSortBy> {
  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByDateCreation() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateCreation', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByDateCreationDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateCreation', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByDateEdit() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateEdit', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByDateEditDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateEdit', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByExtension() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'extension', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByExtensionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'extension', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortBySize() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'size', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortBySizeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'size', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> sortByUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.desc);
    });
  }
}

extension FlightMissionMaterialIsarQuerySortThenBy on QueryBuilder<
    FlightMissionMaterialIsar, FlightMissionMaterialIsar, QSortThenBy> {
  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByDateCreation() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateCreation', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByDateCreationDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateCreation', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByDateEdit() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateEdit', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByDateEditDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateEdit', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByExtension() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'extension', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByExtensionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'extension', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenBySize() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'size', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenBySizeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'size', Sort.desc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.asc);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar,
      QAfterSortBy> thenByUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.desc);
    });
  }
}

extension FlightMissionMaterialIsarQueryWhereDistinct on QueryBuilder<
    FlightMissionMaterialIsar, FlightMissionMaterialIsar, QDistinct> {
  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar, QDistinct>
      distinctByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'ID');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar, QDistinct>
      distinctByDateCreation() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dateCreation');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar, QDistinct>
      distinctByDateEdit() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dateEdit');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar, QDistinct>
      distinctByExtension({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'extension', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar, QDistinct>
      distinctByName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar, QDistinct>
      distinctBySize() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'size');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, FlightMissionMaterialIsar, QDistinct>
      distinctByUrl({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'url', caseSensitive: caseSensitive);
    });
  }
}

extension FlightMissionMaterialIsarQueryProperty on QueryBuilder<
    FlightMissionMaterialIsar, FlightMissionMaterialIsar, QQueryProperty> {
  QueryBuilder<FlightMissionMaterialIsar, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, int?, QQueryOperations> IDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'ID');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, DateTime, QQueryOperations>
      dateCreationProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dateCreation');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, DateTime, QQueryOperations>
      dateEditProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dateEdit');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, String?, QQueryOperations>
      extensionProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'extension');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, String?, QQueryOperations>
      nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, int?, QQueryOperations>
      sizeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'size');
    });
  }

  QueryBuilder<FlightMissionMaterialIsar, String?, QQueryOperations>
      urlProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'url');
    });
  }
}
