import 'package:isar/isar.dart';

part 'geo_route_isar.g.dart';

@Collection()
class GeoRouteIsar {
  GeoRouteIsar({
    required this.ID,
    required this.name,
    required this.objectID,
     this.geoJson,
    required this.color,
    required this.routeLength,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });

  Id id = Isar.autoIncrement;

  @Index(unique: true, replace: true)
  late  int? ID;

  @Index()
  late String? name;

  @Index()
  late  int? objectID;

  @Index()
  late  String? geoJson;

  @Index()
  late final String? color;

  @Index()
  late  String? routeLength;

  @Index()
  late  int? miOwner;

  @Index()
  late  String? miCreateDate;

  @Index()
  late  int? miCreateUser;

  @Index()
  late  String? miModifyDate;

  @Index()
  late  int? miModifyUser;
}