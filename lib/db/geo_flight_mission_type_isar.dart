import 'package:isar/isar.dart';

part 'geo_flight_mission_type_isar.g.dart';

@Collection()
class GeoFlightMissionTypeIsar {
  GeoFlightMissionTypeIsar({
    required this.ID,
    required this.name,
    required this.color,
    required this.operatorID,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  Id? id = Isar.autoIncrement;

  @Index(unique:  true, replace: true)
  late final int? ID;

  @Index()
  late final String? name;

  late final String? color;

  @Index()
  late final int? operatorID;

  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
  
 
  }