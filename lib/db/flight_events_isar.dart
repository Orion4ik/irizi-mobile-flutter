import 'package:irizi/db/flight_missions_isar.dart';
import 'package:isar/isar.dart';

part 'flight_events_isar.g.dart';

@Collection()
class GeoEventIsar {
  GeoEventIsar({
    required this.ID,
    required this.num,
    required this.eventTypeID,
    required this.eventDate,
    required this.latitude,
    required this.longitude,
    required this.description,
     this.verifyDate,
     this.verifySummary,
    required this.flightMissionID,
    required this.state,
    required this.object,
     this.operator,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  
  Id? id = Isar.autoIncrement;
  
  @Index(unique: true, replace: true)
  late int? ID;

  @Index()
  late  String? num;

   @Index()
  late  int? eventTypeID;

  @Index()
  late  String? eventDate;

  @Index()
  late  double? latitude;

  @Index()
  late  double? longitude;

  
  late  String? description;

  @Index()
  late  String? verifyDate;

  @Index()
  late  String? verifySummary;

  @Index()
  late  int? flightMissionID;

  @Index()
  late  String? state;

  @Index()
  late  int? object;

  @Index()
  late  int? operator;

  @Index()
  late  int? miOwner;

  @Index()
  late  String? miCreateDate;

  @Index()
  late  int? miCreateUser;

  @Index()
  late  String? miModifyDate;

  @Index()
  late  int? miModifyUser;

  final flightMission = IsarLink<GeoFlightMissionIsar>();
}