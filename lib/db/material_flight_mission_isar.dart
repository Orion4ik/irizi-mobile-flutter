import 'package:isar/isar.dart';

part 'material_flight_mission_isar.g.dart';

@Collection()
class FlightMissionMaterialIsar {

FlightMissionMaterialIsar({
  required this.url,
  required this.ID,
  required this.extension,
  required this.dateCreation,
  required this.dateEdit,
  required this.size,
  required this.name
});



  Id? id = Isar.autoIncrement;


  @Index()
  late int? ID;


  @Index()
  late int? size;

  @Index()
  late  String? name;

   @Index()
  late  String? url;

  @Index()
  late  String? extension;

   @Index()
  late DateTime dateCreation;

  @Index()
  late DateTime dateEdit;


    


}