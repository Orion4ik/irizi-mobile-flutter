import 'package:isar/isar.dart';

part 'geo_objects_isar.g.dart';

@Collection()
class GeoObjectIsar {
  GeoObjectIsar({
    required this.ID,
    required this.id,
    required this.name,
    required this.operatorID,
    required this.displayName,
    required this.attrValues,
    required this.geoJson,
    required this.color,
    required this.routeLength,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  

  Id id = Isar.autoIncrement;

  @Index(unique: true, replace: true)
  late int? ID;
  

  @Index()
  late  String? name;

  @Index()
  late  int? operatorID;

  @Index()
  late  String? displayName;

  @Index()
  late  String? attrValues;

  @Index()
  late  String? geoJson;

  @Index()
  late  String? color;

  @Index()
  late  String? routeLength;

  @Index()
  late  int? miOwner;

  @Index()
  late  String? miCreateDate;

  @Index()
  late  int? miCreateUser;

  @Index()
  late  String? miModifyDate;

  @Index()
  late  int? miModifyUser;
}