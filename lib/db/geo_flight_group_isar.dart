import 'package:isar/isar.dart';

part 'geo_flight_group_isar.g.dart';

@Collection()
class GeoFlightGroupIsar {
  GeoFlightGroupIsar({
    required this.ID,
    required this.name,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  Id? id = Isar.autoIncrement;

  @Index(unique:  true, replace: true)
  late  int? ID;

  @Index()
  late  String? name;

  @Index()
  late  int? miOwner;

  @Index()
  late  String? miCreateDate;

  @Index()
  late  int? miCreateUser;

  @Index()
  late  String? miModifyDate;

  @Index()
  late  int? miModifyUser;
}