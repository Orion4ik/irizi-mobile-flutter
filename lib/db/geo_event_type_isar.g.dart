// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geo_event_type_isar.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetGeoEventTypeIsarCollection on Isar {
  IsarCollection<GeoEventTypeIsar> get geoEventTypeIsars => this.collection();
}

const GeoEventTypeIsarSchema = CollectionSchema(
  name: r'GeoEventTypeIsar',
  id: -1706277108527231268,
  properties: {
    r'ID': PropertySchema(
      id: 0,
      name: r'ID',
      type: IsarType.long,
    ),
    r'color': PropertySchema(
      id: 1,
      name: r'color',
      type: IsarType.string,
    ),
    r'icon': PropertySchema(
      id: 2,
      name: r'icon',
      type: IsarType.string,
    ),
    r'iconDataUrl': PropertySchema(
      id: 3,
      name: r'iconDataUrl',
      type: IsarType.string,
    ),
    r'iconFile': PropertySchema(
      id: 4,
      name: r'iconFile',
      type: IsarType.string,
    ),
    r'miCreateDate': PropertySchema(
      id: 5,
      name: r'miCreateDate',
      type: IsarType.string,
    ),
    r'miCreateUser': PropertySchema(
      id: 6,
      name: r'miCreateUser',
      type: IsarType.long,
    ),
    r'miModifyDate': PropertySchema(
      id: 7,
      name: r'miModifyDate',
      type: IsarType.string,
    ),
    r'miModifyUser': PropertySchema(
      id: 8,
      name: r'miModifyUser',
      type: IsarType.long,
    ),
    r'miOwner': PropertySchema(
      id: 9,
      name: r'miOwner',
      type: IsarType.long,
    ),
    r'name': PropertySchema(
      id: 10,
      name: r'name',
      type: IsarType.string,
    ),
    r'objectId': PropertySchema(
      id: 11,
      name: r'objectId',
      type: IsarType.long,
    ),
    r'operatorID': PropertySchema(
      id: 12,
      name: r'operatorID',
      type: IsarType.long,
    )
  },
  estimateSize: _geoEventTypeIsarEstimateSize,
  serialize: _geoEventTypeIsarSerialize,
  deserialize: _geoEventTypeIsarDeserialize,
  deserializeProp: _geoEventTypeIsarDeserializeProp,
  idName: r'id',
  indexes: {
    r'ID': IndexSchema(
      id: 5573724008404263404,
      name: r'ID',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'ID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'name': IndexSchema(
      id: 879695947855722453,
      name: r'name',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'name',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'operatorID': IndexSchema(
      id: -3661453554629168571,
      name: r'operatorID',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'operatorID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'objectId': IndexSchema(
      id: -8330937420180998932,
      name: r'objectId',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'objectId',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miOwner': IndexSchema(
      id: 5009576193767868493,
      name: r'miOwner',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miOwner',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miCreateDate': IndexSchema(
      id: 7720202597010838026,
      name: r'miCreateDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miCreateUser': IndexSchema(
      id: -2425434464049914295,
      name: r'miCreateUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miModifyDate': IndexSchema(
      id: -6487646225374976868,
      name: r'miModifyDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miModifyUser': IndexSchema(
      id: -1454558942143342536,
      name: r'miModifyUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _geoEventTypeIsarGetId,
  getLinks: _geoEventTypeIsarGetLinks,
  attach: _geoEventTypeIsarAttach,
  version: '3.1.0+1',
);

int _geoEventTypeIsarEstimateSize(
  GeoEventTypeIsar object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.color;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.icon;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.iconDataUrl;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.iconFile;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miCreateDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miModifyDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _geoEventTypeIsarSerialize(
  GeoEventTypeIsar object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.ID);
  writer.writeString(offsets[1], object.color);
  writer.writeString(offsets[2], object.icon);
  writer.writeString(offsets[3], object.iconDataUrl);
  writer.writeString(offsets[4], object.iconFile);
  writer.writeString(offsets[5], object.miCreateDate);
  writer.writeLong(offsets[6], object.miCreateUser);
  writer.writeString(offsets[7], object.miModifyDate);
  writer.writeLong(offsets[8], object.miModifyUser);
  writer.writeLong(offsets[9], object.miOwner);
  writer.writeString(offsets[10], object.name);
  writer.writeLong(offsets[11], object.objectId);
  writer.writeLong(offsets[12], object.operatorID);
}

GeoEventTypeIsar _geoEventTypeIsarDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = GeoEventTypeIsar(
    ID: reader.readLongOrNull(offsets[0]),
    color: reader.readStringOrNull(offsets[1]),
    icon: reader.readStringOrNull(offsets[2]),
    iconDataUrl: reader.readStringOrNull(offsets[3]),
    iconFile: reader.readStringOrNull(offsets[4]),
    miCreateDate: reader.readStringOrNull(offsets[5]),
    miCreateUser: reader.readLongOrNull(offsets[6]),
    miModifyDate: reader.readStringOrNull(offsets[7]),
    miModifyUser: reader.readLongOrNull(offsets[8]),
    miOwner: reader.readLongOrNull(offsets[9]),
    name: reader.readStringOrNull(offsets[10]),
    objectId: reader.readLongOrNull(offsets[11]),
    operatorID: reader.readLongOrNull(offsets[12]),
  );
  object.id = id;
  return object;
}

P _geoEventTypeIsarDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readStringOrNull(offset)) as P;
    case 6:
      return (reader.readLongOrNull(offset)) as P;
    case 7:
      return (reader.readStringOrNull(offset)) as P;
    case 8:
      return (reader.readLongOrNull(offset)) as P;
    case 9:
      return (reader.readLongOrNull(offset)) as P;
    case 10:
      return (reader.readStringOrNull(offset)) as P;
    case 11:
      return (reader.readLongOrNull(offset)) as P;
    case 12:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _geoEventTypeIsarGetId(GeoEventTypeIsar object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _geoEventTypeIsarGetLinks(GeoEventTypeIsar object) {
  return [];
}

void _geoEventTypeIsarAttach(
    IsarCollection<dynamic> col, Id id, GeoEventTypeIsar object) {
  object.id = id;
}

extension GeoEventTypeIsarByIndex on IsarCollection<GeoEventTypeIsar> {
  Future<GeoEventTypeIsar?> getByID(int? ID) {
    return getByIndex(r'ID', [ID]);
  }

  GeoEventTypeIsar? getByIDSync(int? ID) {
    return getByIndexSync(r'ID', [ID]);
  }

  Future<bool> deleteByID(int? ID) {
    return deleteByIndex(r'ID', [ID]);
  }

  bool deleteByIDSync(int? ID) {
    return deleteByIndexSync(r'ID', [ID]);
  }

  Future<List<GeoEventTypeIsar?>> getAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndex(r'ID', values);
  }

  List<GeoEventTypeIsar?> getAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'ID', values);
  }

  Future<int> deleteAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'ID', values);
  }

  int deleteAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'ID', values);
  }

  Future<Id> putByID(GeoEventTypeIsar object) {
    return putByIndex(r'ID', object);
  }

  Id putByIDSync(GeoEventTypeIsar object, {bool saveLinks = true}) {
    return putByIndexSync(r'ID', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByID(List<GeoEventTypeIsar> objects) {
    return putAllByIndex(r'ID', objects);
  }

  List<Id> putAllByIDSync(List<GeoEventTypeIsar> objects,
      {bool saveLinks = true}) {
    return putAllByIndexSync(r'ID', objects, saveLinks: saveLinks);
  }
}

extension GeoEventTypeIsarQueryWhereSort
    on QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QWhere> {
  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhere> anyID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'ID'),
      );
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhere>
      anyOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'operatorID'),
      );
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhere> anyObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'objectId'),
      );
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhere> anyMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miOwner'),
      );
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhere>
      anyMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miCreateUser'),
      );
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhere>
      anyMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miModifyUser'),
      );
    });
  }
}

extension GeoEventTypeIsarQueryWhere
    on QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QWhereClause> {
  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause> iDEqualTo(
      int? ID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [ID],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      iDNotEqualTo(int? ID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      iDGreaterThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [ID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      iDLessThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [],
        upper: [ID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause> iDBetween(
    int? lowerID,
    int? upperID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [lowerID],
        includeLower: includeLower,
        upper: [upperID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'name',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      nameEqualTo(String? name) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [name],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      nameNotEqualTo(String? name) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      operatorIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'operatorID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      operatorIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      operatorIDEqualTo(int? operatorID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'operatorID',
        value: [operatorID],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      operatorIDNotEqualTo(int? operatorID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [],
              upper: [operatorID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [operatorID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [operatorID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [],
              upper: [operatorID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      operatorIDGreaterThan(
    int? operatorID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [operatorID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      operatorIDLessThan(
    int? operatorID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [],
        upper: [operatorID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      operatorIDBetween(
    int? lowerOperatorID,
    int? upperOperatorID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [lowerOperatorID],
        includeLower: includeLower,
        upper: [upperOperatorID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      objectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'objectId',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      objectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'objectId',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      objectIdEqualTo(int? objectId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'objectId',
        value: [objectId],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      objectIdNotEqualTo(int? objectId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'objectId',
              lower: [],
              upper: [objectId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'objectId',
              lower: [objectId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'objectId',
              lower: [objectId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'objectId',
              lower: [],
              upper: [objectId],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      objectIdGreaterThan(
    int? objectId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'objectId',
        lower: [objectId],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      objectIdLessThan(
    int? objectId, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'objectId',
        lower: [],
        upper: [objectId],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      objectIdBetween(
    int? lowerObjectId,
    int? upperObjectId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'objectId',
        lower: [lowerObjectId],
        includeLower: includeLower,
        upper: [upperObjectId],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miOwnerEqualTo(int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [miOwner],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miOwnerNotEqualTo(int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miOwnerGreaterThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [miOwner],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miOwnerLessThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [],
        upper: [miOwner],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miOwnerBetween(
    int? lowerMiOwner,
    int? upperMiOwner, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [lowerMiOwner],
        includeLower: includeLower,
        upper: [upperMiOwner],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateDateEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [miCreateDate],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateDateNotEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateUserEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [miCreateUser],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateUserNotEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateUserGreaterThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [miCreateUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateUserLessThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [],
        upper: [miCreateUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miCreateUserBetween(
    int? lowerMiCreateUser,
    int? upperMiCreateUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [lowerMiCreateUser],
        includeLower: includeLower,
        upper: [upperMiCreateUser],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyDateEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [miModifyDate],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyDateNotEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyUserEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [miModifyUser],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyUserNotEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyUserGreaterThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [miModifyUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyUserLessThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [],
        upper: [miModifyUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterWhereClause>
      miModifyUserBetween(
    int? lowerMiModifyUser,
    int? upperMiModifyUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [lowerMiModifyUser],
        includeLower: includeLower,
        upper: [upperMiModifyUser],
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GeoEventTypeIsarQueryFilter
    on QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QFilterCondition> {
  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'ID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'color',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'color',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'color',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'color',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      colorIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'icon',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'icon',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'icon',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'icon',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'icon',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'icon',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'icon',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'icon',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'icon',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'icon',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'icon',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'icon',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'iconDataUrl',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'iconDataUrl',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'iconDataUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'iconDataUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'iconDataUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'iconDataUrl',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'iconDataUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'iconDataUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'iconDataUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'iconDataUrl',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'iconDataUrl',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconDataUrlIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'iconDataUrl',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'iconFile',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'iconFile',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'iconFile',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'iconFile',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'iconFile',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'iconFile',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'iconFile',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'iconFile',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'iconFile',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'iconFile',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'iconFile',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      iconFileIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'iconFile',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miCreateDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miCreateUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miModifyDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miModifyUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miOwnerEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miOwnerGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miOwnerLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      miOwnerBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miOwner',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      objectIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'objectId',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      objectIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'objectId',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      objectIdEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      objectIdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      objectIdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'objectId',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      objectIdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'objectId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      operatorIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'operatorID',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      operatorIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'operatorID',
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      operatorIDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'operatorID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      operatorIDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'operatorID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      operatorIDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'operatorID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterFilterCondition>
      operatorIDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'operatorID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GeoEventTypeIsarQueryObject
    on QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QFilterCondition> {}

extension GeoEventTypeIsarQueryLinks
    on QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QFilterCondition> {}

extension GeoEventTypeIsarQuerySortBy
    on QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QSortBy> {
  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy> sortByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy> sortByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy> sortByIcon() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'icon', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByIconDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'icon', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByIconDataUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'iconDataUrl', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByIconDataUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'iconDataUrl', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByIconFile() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'iconFile', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByIconFileDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'iconFile', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy> sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByObjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      sortByOperatorIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.desc);
    });
  }
}

extension GeoEventTypeIsarQuerySortThenBy
    on QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QSortThenBy> {
  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy> thenByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy> thenByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy> thenByIcon() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'icon', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByIconDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'icon', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByIconDataUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'iconDataUrl', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByIconDataUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'iconDataUrl', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByIconFile() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'iconFile', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByIconFileDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'iconFile', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy> thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByObjectIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectId', Sort.desc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.asc);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QAfterSortBy>
      thenByOperatorIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.desc);
    });
  }
}

extension GeoEventTypeIsarQueryWhereDistinct
    on QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct> {
  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct> distinctByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'ID');
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct> distinctByColor(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'color', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct> distinctByIcon(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'icon', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct>
      distinctByIconDataUrl({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'iconDataUrl', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct>
      distinctByIconFile({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'iconFile', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct>
      distinctByMiCreateDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct>
      distinctByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateUser');
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct>
      distinctByMiModifyDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct>
      distinctByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyUser');
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct>
      distinctByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miOwner');
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct> distinctByName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct>
      distinctByObjectId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'objectId');
    });
  }

  QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QDistinct>
      distinctByOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'operatorID');
    });
  }
}

extension GeoEventTypeIsarQueryProperty
    on QueryBuilder<GeoEventTypeIsar, GeoEventTypeIsar, QQueryProperty> {
  QueryBuilder<GeoEventTypeIsar, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<GeoEventTypeIsar, int?, QQueryOperations> IDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'ID');
    });
  }

  QueryBuilder<GeoEventTypeIsar, String?, QQueryOperations> colorProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'color');
    });
  }

  QueryBuilder<GeoEventTypeIsar, String?, QQueryOperations> iconProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'icon');
    });
  }

  QueryBuilder<GeoEventTypeIsar, String?, QQueryOperations>
      iconDataUrlProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'iconDataUrl');
    });
  }

  QueryBuilder<GeoEventTypeIsar, String?, QQueryOperations> iconFileProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'iconFile');
    });
  }

  QueryBuilder<GeoEventTypeIsar, String?, QQueryOperations>
      miCreateDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateDate');
    });
  }

  QueryBuilder<GeoEventTypeIsar, int?, QQueryOperations>
      miCreateUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateUser');
    });
  }

  QueryBuilder<GeoEventTypeIsar, String?, QQueryOperations>
      miModifyDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyDate');
    });
  }

  QueryBuilder<GeoEventTypeIsar, int?, QQueryOperations>
      miModifyUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyUser');
    });
  }

  QueryBuilder<GeoEventTypeIsar, int?, QQueryOperations> miOwnerProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miOwner');
    });
  }

  QueryBuilder<GeoEventTypeIsar, String?, QQueryOperations> nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<GeoEventTypeIsar, int?, QQueryOperations> objectIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'objectId');
    });
  }

  QueryBuilder<GeoEventTypeIsar, int?, QQueryOperations> operatorIDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'operatorID');
    });
  }
}
