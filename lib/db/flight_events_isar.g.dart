// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flight_events_isar.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetGeoEventIsarCollection on Isar {
  IsarCollection<GeoEventIsar> get geoEventIsars => this.collection();
}

const GeoEventIsarSchema = CollectionSchema(
  name: r'GeoEventIsar',
  id: -3620492593959382510,
  properties: {
    r'ID': PropertySchema(
      id: 0,
      name: r'ID',
      type: IsarType.long,
    ),
    r'description': PropertySchema(
      id: 1,
      name: r'description',
      type: IsarType.string,
    ),
    r'eventDate': PropertySchema(
      id: 2,
      name: r'eventDate',
      type: IsarType.string,
    ),
    r'eventTypeID': PropertySchema(
      id: 3,
      name: r'eventTypeID',
      type: IsarType.long,
    ),
    r'flightMissionID': PropertySchema(
      id: 4,
      name: r'flightMissionID',
      type: IsarType.long,
    ),
    r'latitude': PropertySchema(
      id: 5,
      name: r'latitude',
      type: IsarType.double,
    ),
    r'longitude': PropertySchema(
      id: 6,
      name: r'longitude',
      type: IsarType.double,
    ),
    r'miCreateDate': PropertySchema(
      id: 7,
      name: r'miCreateDate',
      type: IsarType.string,
    ),
    r'miCreateUser': PropertySchema(
      id: 8,
      name: r'miCreateUser',
      type: IsarType.long,
    ),
    r'miModifyDate': PropertySchema(
      id: 9,
      name: r'miModifyDate',
      type: IsarType.string,
    ),
    r'miModifyUser': PropertySchema(
      id: 10,
      name: r'miModifyUser',
      type: IsarType.long,
    ),
    r'miOwner': PropertySchema(
      id: 11,
      name: r'miOwner',
      type: IsarType.long,
    ),
    r'num': PropertySchema(
      id: 12,
      name: r'num',
      type: IsarType.string,
    ),
    r'object': PropertySchema(
      id: 13,
      name: r'object',
      type: IsarType.long,
    ),
    r'operator': PropertySchema(
      id: 14,
      name: r'operator',
      type: IsarType.long,
    ),
    r'state': PropertySchema(
      id: 15,
      name: r'state',
      type: IsarType.string,
    ),
    r'verifyDate': PropertySchema(
      id: 16,
      name: r'verifyDate',
      type: IsarType.string,
    ),
    r'verifySummary': PropertySchema(
      id: 17,
      name: r'verifySummary',
      type: IsarType.string,
    )
  },
  estimateSize: _geoEventIsarEstimateSize,
  serialize: _geoEventIsarSerialize,
  deserialize: _geoEventIsarDeserialize,
  deserializeProp: _geoEventIsarDeserializeProp,
  idName: r'id',
  indexes: {
    r'ID': IndexSchema(
      id: 5573724008404263404,
      name: r'ID',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'ID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'num': IndexSchema(
      id: -1976589900541946553,
      name: r'num',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'num',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'eventTypeID': IndexSchema(
      id: -8525858362513585027,
      name: r'eventTypeID',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'eventTypeID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'eventDate': IndexSchema(
      id: -2827469816326842607,
      name: r'eventDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'eventDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'latitude': IndexSchema(
      id: 2839588665230214757,
      name: r'latitude',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'latitude',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'longitude': IndexSchema(
      id: -7076447437327017580,
      name: r'longitude',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'longitude',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'verifyDate': IndexSchema(
      id: 329432909319794176,
      name: r'verifyDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'verifyDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'verifySummary': IndexSchema(
      id: 2133843561395672900,
      name: r'verifySummary',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'verifySummary',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'flightMissionID': IndexSchema(
      id: -756953147024238084,
      name: r'flightMissionID',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'flightMissionID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'state': IndexSchema(
      id: 7917036384617311412,
      name: r'state',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'state',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'object': IndexSchema(
      id: 9217563911824436619,
      name: r'object',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'object',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'operator': IndexSchema(
      id: -6223719989933430860,
      name: r'operator',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'operator',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miOwner': IndexSchema(
      id: 5009576193767868493,
      name: r'miOwner',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miOwner',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miCreateDate': IndexSchema(
      id: 7720202597010838026,
      name: r'miCreateDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miCreateUser': IndexSchema(
      id: -2425434464049914295,
      name: r'miCreateUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miModifyDate': IndexSchema(
      id: -6487646225374976868,
      name: r'miModifyDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miModifyUser': IndexSchema(
      id: -1454558942143342536,
      name: r'miModifyUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {
    r'flightMission': LinkSchema(
      id: -8309527843489339294,
      name: r'flightMission',
      target: r'GeoFlightMissionIsar',
      single: true,
    )
  },
  embeddedSchemas: {},
  getId: _geoEventIsarGetId,
  getLinks: _geoEventIsarGetLinks,
  attach: _geoEventIsarAttach,
  version: '3.1.0+1',
);

int _geoEventIsarEstimateSize(
  GeoEventIsar object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.description;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.eventDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miCreateDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miModifyDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.num;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.state;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.verifyDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.verifySummary;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _geoEventIsarSerialize(
  GeoEventIsar object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.ID);
  writer.writeString(offsets[1], object.description);
  writer.writeString(offsets[2], object.eventDate);
  writer.writeLong(offsets[3], object.eventTypeID);
  writer.writeLong(offsets[4], object.flightMissionID);
  writer.writeDouble(offsets[5], object.latitude);
  writer.writeDouble(offsets[6], object.longitude);
  writer.writeString(offsets[7], object.miCreateDate);
  writer.writeLong(offsets[8], object.miCreateUser);
  writer.writeString(offsets[9], object.miModifyDate);
  writer.writeLong(offsets[10], object.miModifyUser);
  writer.writeLong(offsets[11], object.miOwner);
  writer.writeString(offsets[12], object.num);
  writer.writeLong(offsets[13], object.object);
  writer.writeLong(offsets[14], object.operator);
  writer.writeString(offsets[15], object.state);
  writer.writeString(offsets[16], object.verifyDate);
  writer.writeString(offsets[17], object.verifySummary);
}

GeoEventIsar _geoEventIsarDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = GeoEventIsar(
    ID: reader.readLongOrNull(offsets[0]),
    description: reader.readStringOrNull(offsets[1]),
    eventDate: reader.readStringOrNull(offsets[2]),
    eventTypeID: reader.readLongOrNull(offsets[3]),
    flightMissionID: reader.readLongOrNull(offsets[4]),
    latitude: reader.readDoubleOrNull(offsets[5]),
    longitude: reader.readDoubleOrNull(offsets[6]),
    miCreateDate: reader.readStringOrNull(offsets[7]),
    miCreateUser: reader.readLongOrNull(offsets[8]),
    miModifyDate: reader.readStringOrNull(offsets[9]),
    miModifyUser: reader.readLongOrNull(offsets[10]),
    miOwner: reader.readLongOrNull(offsets[11]),
    num: reader.readStringOrNull(offsets[12]),
    object: reader.readLongOrNull(offsets[13]),
    operator: reader.readLongOrNull(offsets[14]),
    state: reader.readStringOrNull(offsets[15]),
    verifyDate: reader.readStringOrNull(offsets[16]),
    verifySummary: reader.readStringOrNull(offsets[17]),
  );
  object.id = id;
  return object;
}

P _geoEventIsarDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readLongOrNull(offset)) as P;
    case 4:
      return (reader.readLongOrNull(offset)) as P;
    case 5:
      return (reader.readDoubleOrNull(offset)) as P;
    case 6:
      return (reader.readDoubleOrNull(offset)) as P;
    case 7:
      return (reader.readStringOrNull(offset)) as P;
    case 8:
      return (reader.readLongOrNull(offset)) as P;
    case 9:
      return (reader.readStringOrNull(offset)) as P;
    case 10:
      return (reader.readLongOrNull(offset)) as P;
    case 11:
      return (reader.readLongOrNull(offset)) as P;
    case 12:
      return (reader.readStringOrNull(offset)) as P;
    case 13:
      return (reader.readLongOrNull(offset)) as P;
    case 14:
      return (reader.readLongOrNull(offset)) as P;
    case 15:
      return (reader.readStringOrNull(offset)) as P;
    case 16:
      return (reader.readStringOrNull(offset)) as P;
    case 17:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _geoEventIsarGetId(GeoEventIsar object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _geoEventIsarGetLinks(GeoEventIsar object) {
  return [object.flightMission];
}

void _geoEventIsarAttach(
    IsarCollection<dynamic> col, Id id, GeoEventIsar object) {
  object.id = id;
  object.flightMission.attach(
      col, col.isar.collection<GeoFlightMissionIsar>(), r'flightMission', id);
}

extension GeoEventIsarByIndex on IsarCollection<GeoEventIsar> {
  Future<GeoEventIsar?> getByID(int? ID) {
    return getByIndex(r'ID', [ID]);
  }

  GeoEventIsar? getByIDSync(int? ID) {
    return getByIndexSync(r'ID', [ID]);
  }

  Future<bool> deleteByID(int? ID) {
    return deleteByIndex(r'ID', [ID]);
  }

  bool deleteByIDSync(int? ID) {
    return deleteByIndexSync(r'ID', [ID]);
  }

  Future<List<GeoEventIsar?>> getAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndex(r'ID', values);
  }

  List<GeoEventIsar?> getAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'ID', values);
  }

  Future<int> deleteAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'ID', values);
  }

  int deleteAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'ID', values);
  }

  Future<Id> putByID(GeoEventIsar object) {
    return putByIndex(r'ID', object);
  }

  Id putByIDSync(GeoEventIsar object, {bool saveLinks = true}) {
    return putByIndexSync(r'ID', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByID(List<GeoEventIsar> objects) {
    return putAllByIndex(r'ID', objects);
  }

  List<Id> putAllByIDSync(List<GeoEventIsar> objects, {bool saveLinks = true}) {
    return putAllByIndexSync(r'ID', objects, saveLinks: saveLinks);
  }
}

extension GeoEventIsarQueryWhereSort
    on QueryBuilder<GeoEventIsar, GeoEventIsar, QWhere> {
  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'ID'),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyEventTypeID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'eventTypeID'),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyLatitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'latitude'),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyLongitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'longitude'),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyFlightMissionID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'flightMissionID'),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyObject() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'object'),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyOperator() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'operator'),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miOwner'),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miCreateUser'),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhere> anyMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miModifyUser'),
      );
    });
  }
}

extension GeoEventIsarQueryWhere
    on QueryBuilder<GeoEventIsar, GeoEventIsar, QWhereClause> {
  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> idGreaterThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> iDEqualTo(
      int? ID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [ID],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> iDNotEqualTo(
      int? ID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> iDGreaterThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [ID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> iDLessThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [],
        upper: [ID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> iDBetween(
    int? lowerID,
    int? upperID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [lowerID],
        includeLower: includeLower,
        upper: [upperID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> numIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'num',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> numIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'num',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> numEqualTo(
      String? num) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'num',
        value: [num],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> numNotEqualTo(
      String? num) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'num',
              lower: [],
              upper: [num],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'num',
              lower: [num],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'num',
              lower: [num],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'num',
              lower: [],
              upper: [num],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      eventTypeIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'eventTypeID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      eventTypeIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'eventTypeID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      eventTypeIDEqualTo(int? eventTypeID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'eventTypeID',
        value: [eventTypeID],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      eventTypeIDNotEqualTo(int? eventTypeID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventTypeID',
              lower: [],
              upper: [eventTypeID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventTypeID',
              lower: [eventTypeID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventTypeID',
              lower: [eventTypeID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventTypeID',
              lower: [],
              upper: [eventTypeID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      eventTypeIDGreaterThan(
    int? eventTypeID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'eventTypeID',
        lower: [eventTypeID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      eventTypeIDLessThan(
    int? eventTypeID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'eventTypeID',
        lower: [],
        upper: [eventTypeID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      eventTypeIDBetween(
    int? lowerEventTypeID,
    int? upperEventTypeID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'eventTypeID',
        lower: [lowerEventTypeID],
        includeLower: includeLower,
        upper: [upperEventTypeID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      eventDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'eventDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      eventDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'eventDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> eventDateEqualTo(
      String? eventDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'eventDate',
        value: [eventDate],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      eventDateNotEqualTo(String? eventDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventDate',
              lower: [],
              upper: [eventDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventDate',
              lower: [eventDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventDate',
              lower: [eventDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventDate',
              lower: [],
              upper: [eventDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> latitudeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'latitude',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      latitudeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'latitude',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> latitudeEqualTo(
      double? latitude) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'latitude',
        value: [latitude],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      latitudeNotEqualTo(double? latitude) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latitude',
              lower: [],
              upper: [latitude],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latitude',
              lower: [latitude],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latitude',
              lower: [latitude],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latitude',
              lower: [],
              upper: [latitude],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      latitudeGreaterThan(
    double? latitude, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'latitude',
        lower: [latitude],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> latitudeLessThan(
    double? latitude, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'latitude',
        lower: [],
        upper: [latitude],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> latitudeBetween(
    double? lowerLatitude,
    double? upperLatitude, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'latitude',
        lower: [lowerLatitude],
        includeLower: includeLower,
        upper: [upperLatitude],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      longitudeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'longitude',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      longitudeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'longitude',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> longitudeEqualTo(
      double? longitude) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'longitude',
        value: [longitude],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      longitudeNotEqualTo(double? longitude) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'longitude',
              lower: [],
              upper: [longitude],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'longitude',
              lower: [longitude],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'longitude',
              lower: [longitude],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'longitude',
              lower: [],
              upper: [longitude],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      longitudeGreaterThan(
    double? longitude, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'longitude',
        lower: [longitude],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> longitudeLessThan(
    double? longitude, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'longitude',
        lower: [],
        upper: [longitude],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> longitudeBetween(
    double? lowerLongitude,
    double? upperLongitude, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'longitude',
        lower: [lowerLongitude],
        includeLower: includeLower,
        upper: [upperLongitude],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      verifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'verifyDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      verifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'verifyDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> verifyDateEqualTo(
      String? verifyDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'verifyDate',
        value: [verifyDate],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      verifyDateNotEqualTo(String? verifyDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'verifyDate',
              lower: [],
              upper: [verifyDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'verifyDate',
              lower: [verifyDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'verifyDate',
              lower: [verifyDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'verifyDate',
              lower: [],
              upper: [verifyDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      verifySummaryIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'verifySummary',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      verifySummaryIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'verifySummary',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      verifySummaryEqualTo(String? verifySummary) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'verifySummary',
        value: [verifySummary],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      verifySummaryNotEqualTo(String? verifySummary) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'verifySummary',
              lower: [],
              upper: [verifySummary],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'verifySummary',
              lower: [verifySummary],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'verifySummary',
              lower: [verifySummary],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'verifySummary',
              lower: [],
              upper: [verifySummary],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      flightMissionIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'flightMissionID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      flightMissionIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightMissionID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      flightMissionIDEqualTo(int? flightMissionID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'flightMissionID',
        value: [flightMissionID],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      flightMissionIDNotEqualTo(int? flightMissionID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightMissionID',
              lower: [],
              upper: [flightMissionID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightMissionID',
              lower: [flightMissionID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightMissionID',
              lower: [flightMissionID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightMissionID',
              lower: [],
              upper: [flightMissionID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      flightMissionIDGreaterThan(
    int? flightMissionID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightMissionID',
        lower: [flightMissionID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      flightMissionIDLessThan(
    int? flightMissionID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightMissionID',
        lower: [],
        upper: [flightMissionID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      flightMissionIDBetween(
    int? lowerFlightMissionID,
    int? upperFlightMissionID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightMissionID',
        lower: [lowerFlightMissionID],
        includeLower: includeLower,
        upper: [upperFlightMissionID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> stateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'state',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> stateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'state',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> stateEqualTo(
      String? state) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'state',
        value: [state],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> stateNotEqualTo(
      String? state) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'state',
              lower: [],
              upper: [state],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'state',
              lower: [state],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'state',
              lower: [state],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'state',
              lower: [],
              upper: [state],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> objectIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'object',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      objectIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'object',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> objectEqualTo(
      int? object) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'object',
        value: [object],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> objectNotEqualTo(
      int? object) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'object',
              lower: [],
              upper: [object],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'object',
              lower: [object],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'object',
              lower: [object],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'object',
              lower: [],
              upper: [object],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> objectGreaterThan(
    int? object, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'object',
        lower: [object],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> objectLessThan(
    int? object, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'object',
        lower: [],
        upper: [object],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> objectBetween(
    int? lowerObject,
    int? upperObject, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'object',
        lower: [lowerObject],
        includeLower: includeLower,
        upper: [upperObject],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> operatorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'operator',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      operatorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operator',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> operatorEqualTo(
      int? operator) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'operator',
        value: [operator],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      operatorNotEqualTo(int? operator) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operator',
              lower: [],
              upper: [operator],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operator',
              lower: [operator],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operator',
              lower: [operator],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operator',
              lower: [],
              upper: [operator],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      operatorGreaterThan(
    int? operator, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operator',
        lower: [operator],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> operatorLessThan(
    int? operator, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operator',
        lower: [],
        upper: [operator],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> operatorBetween(
    int? lowerOperator,
    int? upperOperator, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operator',
        lower: [lowerOperator],
        includeLower: includeLower,
        upper: [upperOperator],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> miOwnerEqualTo(
      int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [miOwner],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> miOwnerNotEqualTo(
      int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miOwnerGreaterThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [miOwner],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> miOwnerLessThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [],
        upper: [miOwner],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause> miOwnerBetween(
    int? lowerMiOwner,
    int? upperMiOwner, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [lowerMiOwner],
        includeLower: includeLower,
        upper: [upperMiOwner],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateDateEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [miCreateDate],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateDateNotEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateUserEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [miCreateUser],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateUserNotEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateUserGreaterThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [miCreateUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateUserLessThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [],
        upper: [miCreateUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miCreateUserBetween(
    int? lowerMiCreateUser,
    int? upperMiCreateUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [lowerMiCreateUser],
        includeLower: includeLower,
        upper: [upperMiCreateUser],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyDateEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [miModifyDate],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyDateNotEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyUserEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [miModifyUser],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyUserNotEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyUserGreaterThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [miModifyUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyUserLessThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [],
        upper: [miModifyUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterWhereClause>
      miModifyUserBetween(
    int? lowerMiModifyUser,
    int? upperMiModifyUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [lowerMiModifyUser],
        includeLower: includeLower,
        upper: [upperMiModifyUser],
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GeoEventIsarQueryFilter
    on QueryBuilder<GeoEventIsar, GeoEventIsar, QFilterCondition> {
  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> iDEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> iDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> iDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> iDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'ID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'description',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'description',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'description',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'description',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      descriptionIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'description',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'eventDate',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'eventDate',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'eventDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'eventDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'eventDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'eventDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'eventDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'eventDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'eventDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'eventDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'eventDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'eventDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventTypeIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'eventTypeID',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventTypeIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'eventTypeID',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventTypeIDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'eventTypeID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventTypeIDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'eventTypeID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventTypeIDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'eventTypeID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      eventTypeIDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'eventTypeID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      flightMissionIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'flightMissionID',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      flightMissionIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'flightMissionID',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      flightMissionIDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'flightMissionID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      flightMissionIDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'flightMissionID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      flightMissionIDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'flightMissionID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      flightMissionIDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'flightMissionID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> idEqualTo(
      Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      latitudeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'latitude',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      latitudeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'latitude',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      latitudeEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'latitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      latitudeGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'latitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      latitudeLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'latitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      latitudeBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'latitude',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      longitudeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'longitude',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      longitudeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'longitude',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      longitudeEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'longitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      longitudeGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'longitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      longitudeLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'longitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      longitudeBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'longitude',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miCreateDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miCreateUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miModifyDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miModifyUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miOwnerEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miOwnerGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miOwnerLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      miOwnerBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miOwner',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> numIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'num',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      numIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'num',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> numEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      numGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> numLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> numBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'num',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> numStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> numEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> numContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> numMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'num',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> numIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'num',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      numIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'num',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      objectIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'object',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      objectIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'object',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> objectEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'object',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      objectGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'object',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      objectLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'object',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> objectBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'object',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      operatorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'operator',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      operatorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'operator',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      operatorEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'operator',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      operatorGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'operator',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      operatorLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'operator',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      operatorBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'operator',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      stateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'state',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      stateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'state',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> stateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      stateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> stateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> stateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'state',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      stateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> stateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> stateContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> stateMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'state',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      stateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'state',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      stateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'state',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'verifyDate',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'verifyDate',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'verifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'verifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'verifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'verifyDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'verifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'verifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'verifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'verifyDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'verifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifyDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'verifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'verifySummary',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'verifySummary',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'verifySummary',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'verifySummary',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'verifySummary',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'verifySummary',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'verifySummary',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'verifySummary',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'verifySummary',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'verifySummary',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'verifySummary',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      verifySummaryIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'verifySummary',
        value: '',
      ));
    });
  }
}

extension GeoEventIsarQueryObject
    on QueryBuilder<GeoEventIsar, GeoEventIsar, QFilterCondition> {}

extension GeoEventIsarQueryLinks
    on QueryBuilder<GeoEventIsar, GeoEventIsar, QFilterCondition> {
  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition> flightMission(
      FilterQuery<GeoFlightMissionIsar> q) {
    return QueryBuilder.apply(this, (query) {
      return query.link(q, r'flightMission');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterFilterCondition>
      flightMissionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'flightMission', 0, true, 0, true);
    });
  }
}

extension GeoEventIsarQuerySortBy
    on QueryBuilder<GeoEventIsar, GeoEventIsar, QSortBy> {
  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      sortByDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByEventDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByEventDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByEventTypeID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventTypeID', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      sortByEventTypeIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventTypeID', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      sortByFlightMissionID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightMissionID', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      sortByFlightMissionIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightMissionID', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByLatitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latitude', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByLatitudeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latitude', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByLongitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'longitude', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByLongitudeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'longitude', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      sortByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      sortByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      sortByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      sortByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByNum() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'num', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByNumDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'num', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByObject() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'object', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByObjectDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'object', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByOperator() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operator', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByOperatorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operator', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByState() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'state', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByStateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'state', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByVerifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'verifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      sortByVerifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'verifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> sortByVerifySummary() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'verifySummary', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      sortByVerifySummaryDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'verifySummary', Sort.desc);
    });
  }
}

extension GeoEventIsarQuerySortThenBy
    on QueryBuilder<GeoEventIsar, GeoEventIsar, QSortThenBy> {
  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByDescription() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      thenByDescriptionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'description', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByEventDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByEventDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByEventTypeID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventTypeID', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      thenByEventTypeIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventTypeID', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      thenByFlightMissionID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightMissionID', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      thenByFlightMissionIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightMissionID', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByLatitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latitude', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByLatitudeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latitude', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByLongitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'longitude', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByLongitudeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'longitude', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      thenByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      thenByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      thenByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      thenByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByNum() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'num', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByNumDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'num', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByObject() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'object', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByObjectDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'object', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByOperator() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operator', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByOperatorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operator', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByState() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'state', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByStateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'state', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByVerifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'verifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      thenByVerifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'verifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy> thenByVerifySummary() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'verifySummary', Sort.asc);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QAfterSortBy>
      thenByVerifySummaryDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'verifySummary', Sort.desc);
    });
  }
}

extension GeoEventIsarQueryWhereDistinct
    on QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> {
  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'ID');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByDescription(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'description', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByEventDate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'eventDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByEventTypeID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'eventTypeID');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct>
      distinctByFlightMissionID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'flightMissionID');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByLatitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'latitude');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByLongitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'longitude');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByMiCreateDate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateUser');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByMiModifyDate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyUser');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miOwner');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByNum(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'num', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByObject() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'object');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByOperator() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'operator');
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByState(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'state', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByVerifyDate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'verifyDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoEventIsar, GeoEventIsar, QDistinct> distinctByVerifySummary(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'verifySummary',
          caseSensitive: caseSensitive);
    });
  }
}

extension GeoEventIsarQueryProperty
    on QueryBuilder<GeoEventIsar, GeoEventIsar, QQueryProperty> {
  QueryBuilder<GeoEventIsar, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<GeoEventIsar, int?, QQueryOperations> IDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'ID');
    });
  }

  QueryBuilder<GeoEventIsar, String?, QQueryOperations> descriptionProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'description');
    });
  }

  QueryBuilder<GeoEventIsar, String?, QQueryOperations> eventDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'eventDate');
    });
  }

  QueryBuilder<GeoEventIsar, int?, QQueryOperations> eventTypeIDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'eventTypeID');
    });
  }

  QueryBuilder<GeoEventIsar, int?, QQueryOperations> flightMissionIDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'flightMissionID');
    });
  }

  QueryBuilder<GeoEventIsar, double?, QQueryOperations> latitudeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'latitude');
    });
  }

  QueryBuilder<GeoEventIsar, double?, QQueryOperations> longitudeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'longitude');
    });
  }

  QueryBuilder<GeoEventIsar, String?, QQueryOperations> miCreateDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateDate');
    });
  }

  QueryBuilder<GeoEventIsar, int?, QQueryOperations> miCreateUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateUser');
    });
  }

  QueryBuilder<GeoEventIsar, String?, QQueryOperations> miModifyDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyDate');
    });
  }

  QueryBuilder<GeoEventIsar, int?, QQueryOperations> miModifyUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyUser');
    });
  }

  QueryBuilder<GeoEventIsar, int?, QQueryOperations> miOwnerProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miOwner');
    });
  }

  QueryBuilder<GeoEventIsar, String?, QQueryOperations> numProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'num');
    });
  }

  QueryBuilder<GeoEventIsar, int?, QQueryOperations> objectProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'object');
    });
  }

  QueryBuilder<GeoEventIsar, int?, QQueryOperations> operatorProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'operator');
    });
  }

  QueryBuilder<GeoEventIsar, String?, QQueryOperations> stateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'state');
    });
  }

  QueryBuilder<GeoEventIsar, String?, QQueryOperations> verifyDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'verifyDate');
    });
  }

  QueryBuilder<GeoEventIsar, String?, QQueryOperations>
      verifySummaryProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'verifySummary');
    });
  }
}
