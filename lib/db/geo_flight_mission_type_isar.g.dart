// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geo_flight_mission_type_isar.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetGeoFlightMissionTypeIsarCollection on Isar {
  IsarCollection<GeoFlightMissionTypeIsar> get geoFlightMissionTypeIsars =>
      this.collection();
}

const GeoFlightMissionTypeIsarSchema = CollectionSchema(
  name: r'GeoFlightMissionTypeIsar',
  id: -1194666753170246687,
  properties: {
    r'ID': PropertySchema(
      id: 0,
      name: r'ID',
      type: IsarType.long,
    ),
    r'color': PropertySchema(
      id: 1,
      name: r'color',
      type: IsarType.string,
    ),
    r'miCreateDate': PropertySchema(
      id: 2,
      name: r'miCreateDate',
      type: IsarType.string,
    ),
    r'miCreateUser': PropertySchema(
      id: 3,
      name: r'miCreateUser',
      type: IsarType.long,
    ),
    r'miModifyDate': PropertySchema(
      id: 4,
      name: r'miModifyDate',
      type: IsarType.string,
    ),
    r'miModifyUser': PropertySchema(
      id: 5,
      name: r'miModifyUser',
      type: IsarType.long,
    ),
    r'miOwner': PropertySchema(
      id: 6,
      name: r'miOwner',
      type: IsarType.long,
    ),
    r'name': PropertySchema(
      id: 7,
      name: r'name',
      type: IsarType.string,
    ),
    r'operatorID': PropertySchema(
      id: 8,
      name: r'operatorID',
      type: IsarType.long,
    )
  },
  estimateSize: _geoFlightMissionTypeIsarEstimateSize,
  serialize: _geoFlightMissionTypeIsarSerialize,
  deserialize: _geoFlightMissionTypeIsarDeserialize,
  deserializeProp: _geoFlightMissionTypeIsarDeserializeProp,
  idName: r'id',
  indexes: {
    r'ID': IndexSchema(
      id: 5573724008404263404,
      name: r'ID',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'ID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'name': IndexSchema(
      id: 879695947855722453,
      name: r'name',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'name',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'operatorID': IndexSchema(
      id: -3661453554629168571,
      name: r'operatorID',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'operatorID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _geoFlightMissionTypeIsarGetId,
  getLinks: _geoFlightMissionTypeIsarGetLinks,
  attach: _geoFlightMissionTypeIsarAttach,
  version: '3.1.0+1',
);

int _geoFlightMissionTypeIsarEstimateSize(
  GeoFlightMissionTypeIsar object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.color;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miCreateDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miModifyDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _geoFlightMissionTypeIsarSerialize(
  GeoFlightMissionTypeIsar object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.ID);
  writer.writeString(offsets[1], object.color);
  writer.writeString(offsets[2], object.miCreateDate);
  writer.writeLong(offsets[3], object.miCreateUser);
  writer.writeString(offsets[4], object.miModifyDate);
  writer.writeLong(offsets[5], object.miModifyUser);
  writer.writeLong(offsets[6], object.miOwner);
  writer.writeString(offsets[7], object.name);
  writer.writeLong(offsets[8], object.operatorID);
}

GeoFlightMissionTypeIsar _geoFlightMissionTypeIsarDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = GeoFlightMissionTypeIsar(
    ID: reader.readLongOrNull(offsets[0]),
    color: reader.readStringOrNull(offsets[1]),
    miCreateDate: reader.readStringOrNull(offsets[2]),
    miCreateUser: reader.readLongOrNull(offsets[3]),
    miModifyDate: reader.readStringOrNull(offsets[4]),
    miModifyUser: reader.readLongOrNull(offsets[5]),
    miOwner: reader.readLongOrNull(offsets[6]),
    name: reader.readStringOrNull(offsets[7]),
    operatorID: reader.readLongOrNull(offsets[8]),
  );
  object.id = id;
  return object;
}

P _geoFlightMissionTypeIsarDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readLongOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readLongOrNull(offset)) as P;
    case 6:
      return (reader.readLongOrNull(offset)) as P;
    case 7:
      return (reader.readStringOrNull(offset)) as P;
    case 8:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _geoFlightMissionTypeIsarGetId(GeoFlightMissionTypeIsar object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _geoFlightMissionTypeIsarGetLinks(
    GeoFlightMissionTypeIsar object) {
  return [];
}

void _geoFlightMissionTypeIsarAttach(
    IsarCollection<dynamic> col, Id id, GeoFlightMissionTypeIsar object) {
  object.id = id;
}

extension GeoFlightMissionTypeIsarByIndex
    on IsarCollection<GeoFlightMissionTypeIsar> {
  Future<GeoFlightMissionTypeIsar?> getByID(int? ID) {
    return getByIndex(r'ID', [ID]);
  }

  GeoFlightMissionTypeIsar? getByIDSync(int? ID) {
    return getByIndexSync(r'ID', [ID]);
  }

  Future<bool> deleteByID(int? ID) {
    return deleteByIndex(r'ID', [ID]);
  }

  bool deleteByIDSync(int? ID) {
    return deleteByIndexSync(r'ID', [ID]);
  }

  Future<List<GeoFlightMissionTypeIsar?>> getAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndex(r'ID', values);
  }

  List<GeoFlightMissionTypeIsar?> getAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'ID', values);
  }

  Future<int> deleteAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'ID', values);
  }

  int deleteAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'ID', values);
  }

  Future<Id> putByID(GeoFlightMissionTypeIsar object) {
    return putByIndex(r'ID', object);
  }

  Id putByIDSync(GeoFlightMissionTypeIsar object, {bool saveLinks = true}) {
    return putByIndexSync(r'ID', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByID(List<GeoFlightMissionTypeIsar> objects) {
    return putAllByIndex(r'ID', objects);
  }

  List<Id> putAllByIDSync(List<GeoFlightMissionTypeIsar> objects,
      {bool saveLinks = true}) {
    return putAllByIndexSync(r'ID', objects, saveLinks: saveLinks);
  }
}

extension GeoFlightMissionTypeIsarQueryWhereSort on QueryBuilder<
    GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QWhere> {
  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterWhere>
      anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterWhere>
      anyID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'ID'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterWhere>
      anyOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'operatorID'),
      );
    });
  }
}

extension GeoFlightMissionTypeIsarQueryWhere on QueryBuilder<
    GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QWhereClause> {
  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> iDEqualTo(int? ID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [ID],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> iDNotEqualTo(int? ID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> iDGreaterThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [ID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> iDLessThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [],
        upper: [ID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> iDBetween(
    int? lowerID,
    int? upperID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [lowerID],
        includeLower: includeLower,
        upper: [upperID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'name',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> nameEqualTo(String? name) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [name],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> nameNotEqualTo(String? name) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> operatorIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'operatorID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> operatorIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> operatorIDEqualTo(int? operatorID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'operatorID',
        value: [operatorID],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> operatorIDNotEqualTo(int? operatorID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [],
              upper: [operatorID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [operatorID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [operatorID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [],
              upper: [operatorID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> operatorIDGreaterThan(
    int? operatorID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [operatorID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> operatorIDLessThan(
    int? operatorID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [],
        upper: [operatorID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterWhereClause> operatorIDBetween(
    int? lowerOperatorID,
    int? upperOperatorID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [lowerOperatorID],
        includeLower: includeLower,
        upper: [upperOperatorID],
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GeoFlightMissionTypeIsarQueryFilter on QueryBuilder<
    GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QFilterCondition> {
  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> iDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> iDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> iDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> iDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'ID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> colorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'color',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> colorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'color',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> colorEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> colorGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> colorLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> colorBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'color',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> colorStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> colorEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
          QAfterFilterCondition>
      colorContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
          QAfterFilterCondition>
      colorMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'color',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> colorIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> colorIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
          QAfterFilterCondition>
      miCreateDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
          QAfterFilterCondition>
      miCreateDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miCreateDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miCreateUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
          QAfterFilterCondition>
      miModifyDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
          QAfterFilterCondition>
      miModifyDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miModifyDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miModifyUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miOwnerEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miOwnerGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miOwnerLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> miOwnerBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miOwner',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
          QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
          QAfterFilterCondition>
      nameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> operatorIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'operatorID',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> operatorIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'operatorID',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> operatorIDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'operatorID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> operatorIDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'operatorID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> operatorIDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'operatorID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar,
      QAfterFilterCondition> operatorIDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'operatorID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GeoFlightMissionTypeIsarQueryObject on QueryBuilder<
    GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QFilterCondition> {}

extension GeoFlightMissionTypeIsarQueryLinks on QueryBuilder<
    GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QFilterCondition> {}

extension GeoFlightMissionTypeIsarQuerySortBy on QueryBuilder<
    GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QSortBy> {
  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      sortByOperatorIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.desc);
    });
  }
}

extension GeoFlightMissionTypeIsarQuerySortThenBy on QueryBuilder<
    GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QSortThenBy> {
  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QAfterSortBy>
      thenByOperatorIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.desc);
    });
  }
}

extension GeoFlightMissionTypeIsarQueryWhereDistinct on QueryBuilder<
    GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QDistinct> {
  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QDistinct>
      distinctByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'ID');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QDistinct>
      distinctByColor({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'color', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QDistinct>
      distinctByMiCreateDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QDistinct>
      distinctByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateUser');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QDistinct>
      distinctByMiModifyDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QDistinct>
      distinctByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyUser');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QDistinct>
      distinctByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miOwner');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QDistinct>
      distinctByName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QDistinct>
      distinctByOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'operatorID');
    });
  }
}

extension GeoFlightMissionTypeIsarQueryProperty on QueryBuilder<
    GeoFlightMissionTypeIsar, GeoFlightMissionTypeIsar, QQueryProperty> {
  QueryBuilder<GeoFlightMissionTypeIsar, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, int?, QQueryOperations> IDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'ID');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, String?, QQueryOperations>
      colorProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'color');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, String?, QQueryOperations>
      miCreateDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateDate');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, int?, QQueryOperations>
      miCreateUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateUser');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, String?, QQueryOperations>
      miModifyDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyDate');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, int?, QQueryOperations>
      miModifyUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyUser');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, int?, QQueryOperations>
      miOwnerProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miOwner');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, String?, QQueryOperations>
      nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<GeoFlightMissionTypeIsar, int?, QQueryOperations>
      operatorIDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'operatorID');
    });
  }
}
