import 'package:isar/isar.dart';

part 'geo_execution_status_isar.g.dart';

@Collection()
class GeoExecutionStatusIsar {
  GeoExecutionStatusIsar({
    required this.ID,
    required this.name,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
   Id? id = Isar.autoIncrement;
   
  @Index(unique: true, replace: true)
  late final int? ID;

  @Index()
  late final String? name;

  @Index()
  late final int? miOwner;

  @Index()
  late final String? miCreateDate;

  @Index()
  late final int? miCreateUser;

  @Index()
  late final String? miModifyDate;

  @Index()
  late final int? miModifyUser;
}