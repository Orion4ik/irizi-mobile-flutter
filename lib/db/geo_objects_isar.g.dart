// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geo_objects_isar.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetGeoObjectIsarCollection on Isar {
  IsarCollection<GeoObjectIsar> get geoObjectIsars => this.collection();
}

const GeoObjectIsarSchema = CollectionSchema(
  name: r'GeoObjectIsar',
  id: 4505852486226036281,
  properties: {
    r'ID': PropertySchema(
      id: 0,
      name: r'ID',
      type: IsarType.long,
    ),
    r'attrValues': PropertySchema(
      id: 1,
      name: r'attrValues',
      type: IsarType.string,
    ),
    r'color': PropertySchema(
      id: 2,
      name: r'color',
      type: IsarType.string,
    ),
    r'displayName': PropertySchema(
      id: 3,
      name: r'displayName',
      type: IsarType.string,
    ),
    r'geoJson': PropertySchema(
      id: 4,
      name: r'geoJson',
      type: IsarType.string,
    ),
    r'miCreateDate': PropertySchema(
      id: 5,
      name: r'miCreateDate',
      type: IsarType.string,
    ),
    r'miCreateUser': PropertySchema(
      id: 6,
      name: r'miCreateUser',
      type: IsarType.long,
    ),
    r'miModifyDate': PropertySchema(
      id: 7,
      name: r'miModifyDate',
      type: IsarType.string,
    ),
    r'miModifyUser': PropertySchema(
      id: 8,
      name: r'miModifyUser',
      type: IsarType.long,
    ),
    r'miOwner': PropertySchema(
      id: 9,
      name: r'miOwner',
      type: IsarType.long,
    ),
    r'name': PropertySchema(
      id: 10,
      name: r'name',
      type: IsarType.string,
    ),
    r'operatorID': PropertySchema(
      id: 11,
      name: r'operatorID',
      type: IsarType.long,
    ),
    r'routeLength': PropertySchema(
      id: 12,
      name: r'routeLength',
      type: IsarType.string,
    )
  },
  estimateSize: _geoObjectIsarEstimateSize,
  serialize: _geoObjectIsarSerialize,
  deserialize: _geoObjectIsarDeserialize,
  deserializeProp: _geoObjectIsarDeserializeProp,
  idName: r'id',
  indexes: {
    r'ID': IndexSchema(
      id: 5573724008404263404,
      name: r'ID',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'ID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'name': IndexSchema(
      id: 879695947855722453,
      name: r'name',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'name',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'operatorID': IndexSchema(
      id: -3661453554629168571,
      name: r'operatorID',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'operatorID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'displayName': IndexSchema(
      id: -825365117524145674,
      name: r'displayName',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'displayName',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'attrValues': IndexSchema(
      id: 3862169497400472261,
      name: r'attrValues',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'attrValues',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'geoJson': IndexSchema(
      id: -5063599521477353396,
      name: r'geoJson',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'geoJson',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'color': IndexSchema(
      id: 880366885425937065,
      name: r'color',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'color',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'routeLength': IndexSchema(
      id: 3291277472231861796,
      name: r'routeLength',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'routeLength',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miOwner': IndexSchema(
      id: 5009576193767868493,
      name: r'miOwner',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miOwner',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miCreateDate': IndexSchema(
      id: 7720202597010838026,
      name: r'miCreateDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miCreateUser': IndexSchema(
      id: -2425434464049914295,
      name: r'miCreateUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miModifyDate': IndexSchema(
      id: -6487646225374976868,
      name: r'miModifyDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miModifyUser': IndexSchema(
      id: -1454558942143342536,
      name: r'miModifyUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _geoObjectIsarGetId,
  getLinks: _geoObjectIsarGetLinks,
  attach: _geoObjectIsarAttach,
  version: '3.1.0+1',
);

int _geoObjectIsarEstimateSize(
  GeoObjectIsar object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.attrValues;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.color;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.displayName;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.geoJson;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miCreateDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miModifyDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.routeLength;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _geoObjectIsarSerialize(
  GeoObjectIsar object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.ID);
  writer.writeString(offsets[1], object.attrValues);
  writer.writeString(offsets[2], object.color);
  writer.writeString(offsets[3], object.displayName);
  writer.writeString(offsets[4], object.geoJson);
  writer.writeString(offsets[5], object.miCreateDate);
  writer.writeLong(offsets[6], object.miCreateUser);
  writer.writeString(offsets[7], object.miModifyDate);
  writer.writeLong(offsets[8], object.miModifyUser);
  writer.writeLong(offsets[9], object.miOwner);
  writer.writeString(offsets[10], object.name);
  writer.writeLong(offsets[11], object.operatorID);
  writer.writeString(offsets[12], object.routeLength);
}

GeoObjectIsar _geoObjectIsarDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = GeoObjectIsar(
    ID: reader.readLongOrNull(offsets[0]),
    attrValues: reader.readStringOrNull(offsets[1]),
    color: reader.readStringOrNull(offsets[2]),
    displayName: reader.readStringOrNull(offsets[3]),
    geoJson: reader.readStringOrNull(offsets[4]),
    id: id,
    miCreateDate: reader.readStringOrNull(offsets[5]),
    miCreateUser: reader.readLongOrNull(offsets[6]),
    miModifyDate: reader.readStringOrNull(offsets[7]),
    miModifyUser: reader.readLongOrNull(offsets[8]),
    miOwner: reader.readLongOrNull(offsets[9]),
    name: reader.readStringOrNull(offsets[10]),
    operatorID: reader.readLongOrNull(offsets[11]),
    routeLength: reader.readStringOrNull(offsets[12]),
  );
  return object;
}

P _geoObjectIsarDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readStringOrNull(offset)) as P;
    case 6:
      return (reader.readLongOrNull(offset)) as P;
    case 7:
      return (reader.readStringOrNull(offset)) as P;
    case 8:
      return (reader.readLongOrNull(offset)) as P;
    case 9:
      return (reader.readLongOrNull(offset)) as P;
    case 10:
      return (reader.readStringOrNull(offset)) as P;
    case 11:
      return (reader.readLongOrNull(offset)) as P;
    case 12:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _geoObjectIsarGetId(GeoObjectIsar object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _geoObjectIsarGetLinks(GeoObjectIsar object) {
  return [];
}

void _geoObjectIsarAttach(
    IsarCollection<dynamic> col, Id id, GeoObjectIsar object) {
  object.id = id;
}

extension GeoObjectIsarByIndex on IsarCollection<GeoObjectIsar> {
  Future<GeoObjectIsar?> getByID(int? ID) {
    return getByIndex(r'ID', [ID]);
  }

  GeoObjectIsar? getByIDSync(int? ID) {
    return getByIndexSync(r'ID', [ID]);
  }

  Future<bool> deleteByID(int? ID) {
    return deleteByIndex(r'ID', [ID]);
  }

  bool deleteByIDSync(int? ID) {
    return deleteByIndexSync(r'ID', [ID]);
  }

  Future<List<GeoObjectIsar?>> getAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndex(r'ID', values);
  }

  List<GeoObjectIsar?> getAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'ID', values);
  }

  Future<int> deleteAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'ID', values);
  }

  int deleteAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'ID', values);
  }

  Future<Id> putByID(GeoObjectIsar object) {
    return putByIndex(r'ID', object);
  }

  Id putByIDSync(GeoObjectIsar object, {bool saveLinks = true}) {
    return putByIndexSync(r'ID', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByID(List<GeoObjectIsar> objects) {
    return putAllByIndex(r'ID', objects);
  }

  List<Id> putAllByIDSync(List<GeoObjectIsar> objects,
      {bool saveLinks = true}) {
    return putAllByIndexSync(r'ID', objects, saveLinks: saveLinks);
  }
}

extension GeoObjectIsarQueryWhereSort
    on QueryBuilder<GeoObjectIsar, GeoObjectIsar, QWhere> {
  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhere> anyID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'ID'),
      );
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhere> anyOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'operatorID'),
      );
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhere> anyMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miOwner'),
      );
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhere> anyMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miCreateUser'),
      );
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhere> anyMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miModifyUser'),
      );
    });
  }
}

extension GeoObjectIsarQueryWhere
    on QueryBuilder<GeoObjectIsar, GeoObjectIsar, QWhereClause> {
  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> idGreaterThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> idLessThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> iDEqualTo(
      int? ID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [ID],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> iDNotEqualTo(
      int? ID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> iDGreaterThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [ID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> iDLessThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [],
        upper: [ID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> iDBetween(
    int? lowerID,
    int? upperID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [lowerID],
        includeLower: includeLower,
        upper: [upperID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'name',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> nameEqualTo(
      String? name) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [name],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> nameNotEqualTo(
      String? name) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      operatorIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'operatorID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      operatorIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      operatorIDEqualTo(int? operatorID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'operatorID',
        value: [operatorID],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      operatorIDNotEqualTo(int? operatorID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [],
              upper: [operatorID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [operatorID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [operatorID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operatorID',
              lower: [],
              upper: [operatorID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      operatorIDGreaterThan(
    int? operatorID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [operatorID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      operatorIDLessThan(
    int? operatorID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [],
        upper: [operatorID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      operatorIDBetween(
    int? lowerOperatorID,
    int? upperOperatorID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operatorID',
        lower: [lowerOperatorID],
        includeLower: includeLower,
        upper: [upperOperatorID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      displayNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'displayName',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      displayNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'displayName',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      displayNameEqualTo(String? displayName) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'displayName',
        value: [displayName],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      displayNameNotEqualTo(String? displayName) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'displayName',
              lower: [],
              upper: [displayName],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'displayName',
              lower: [displayName],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'displayName',
              lower: [displayName],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'displayName',
              lower: [],
              upper: [displayName],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      attrValuesIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'attrValues',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      attrValuesIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'attrValues',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      attrValuesEqualTo(String? attrValues) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'attrValues',
        value: [attrValues],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      attrValuesNotEqualTo(String? attrValues) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'attrValues',
              lower: [],
              upper: [attrValues],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'attrValues',
              lower: [attrValues],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'attrValues',
              lower: [attrValues],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'attrValues',
              lower: [],
              upper: [attrValues],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      geoJsonIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'geoJson',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      geoJsonIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'geoJson',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> geoJsonEqualTo(
      String? geoJson) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'geoJson',
        value: [geoJson],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      geoJsonNotEqualTo(String? geoJson) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [],
              upper: [geoJson],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [geoJson],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [geoJson],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [],
              upper: [geoJson],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> colorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'color',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      colorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'color',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> colorEqualTo(
      String? color) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'color',
        value: [color],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> colorNotEqualTo(
      String? color) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'color',
              lower: [],
              upper: [color],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'color',
              lower: [color],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'color',
              lower: [color],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'color',
              lower: [],
              upper: [color],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      routeLengthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'routeLength',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      routeLengthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'routeLength',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      routeLengthEqualTo(String? routeLength) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'routeLength',
        value: [routeLength],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      routeLengthNotEqualTo(String? routeLength) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [],
              upper: [routeLength],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [routeLength],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [routeLength],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [],
              upper: [routeLength],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> miOwnerEqualTo(
      int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [miOwner],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miOwnerNotEqualTo(int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miOwnerGreaterThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [miOwner],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> miOwnerLessThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [],
        upper: [miOwner],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause> miOwnerBetween(
    int? lowerMiOwner,
    int? upperMiOwner, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [lowerMiOwner],
        includeLower: includeLower,
        upper: [upperMiOwner],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateDateEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [miCreateDate],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateDateNotEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateUserEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [miCreateUser],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateUserNotEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateUserGreaterThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [miCreateUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateUserLessThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [],
        upper: [miCreateUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miCreateUserBetween(
    int? lowerMiCreateUser,
    int? upperMiCreateUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [lowerMiCreateUser],
        includeLower: includeLower,
        upper: [upperMiCreateUser],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyDateEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [miModifyDate],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyDateNotEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyUserEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [miModifyUser],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyUserNotEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyUserGreaterThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [miModifyUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyUserLessThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [],
        upper: [miModifyUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterWhereClause>
      miModifyUserBetween(
    int? lowerMiModifyUser,
    int? upperMiModifyUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [lowerMiModifyUser],
        includeLower: includeLower,
        upper: [upperMiModifyUser],
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GeoObjectIsarQueryFilter
    on QueryBuilder<GeoObjectIsar, GeoObjectIsar, QFilterCondition> {
  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition> iDEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      iDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition> iDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition> iDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'ID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'attrValues',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'attrValues',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'attrValues',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'attrValues',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'attrValues',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'attrValues',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'attrValues',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'attrValues',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'attrValues',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'attrValues',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'attrValues',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      attrValuesIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'attrValues',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'color',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'color',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'color',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'color',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      colorIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'displayName',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'displayName',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'displayName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'displayName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'displayName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'displayName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'displayName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'displayName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'displayName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'displayName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'displayName',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      displayNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'displayName',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'geoJson',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'geoJson',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'geoJson',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'geoJson',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'geoJson',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      geoJsonIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'geoJson',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition> idEqualTo(
      Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miCreateDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miCreateUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miModifyDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miModifyUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miOwnerEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miOwnerGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miOwnerLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      miOwnerBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miOwner',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition> nameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      operatorIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'operatorID',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      operatorIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'operatorID',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      operatorIDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'operatorID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      operatorIDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'operatorID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      operatorIDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'operatorID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      operatorIDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'operatorID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'routeLength',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'routeLength',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'routeLength',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'routeLength',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'routeLength',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterFilterCondition>
      routeLengthIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'routeLength',
        value: '',
      ));
    });
  }
}

extension GeoObjectIsarQueryObject
    on QueryBuilder<GeoObjectIsar, GeoObjectIsar, QFilterCondition> {}

extension GeoObjectIsarQueryLinks
    on QueryBuilder<GeoObjectIsar, GeoObjectIsar, QFilterCondition> {}

extension GeoObjectIsarQuerySortBy
    on QueryBuilder<GeoObjectIsar, GeoObjectIsar, QSortBy> {
  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByAttrValues() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'attrValues', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByAttrValuesDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'attrValues', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByDisplayName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayName', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByDisplayNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayName', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByGeoJson() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByGeoJsonDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByOperatorIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> sortByRouteLength() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      sortByRouteLengthDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.desc);
    });
  }
}

extension GeoObjectIsarQuerySortThenBy
    on QueryBuilder<GeoObjectIsar, GeoObjectIsar, QSortThenBy> {
  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByAttrValues() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'attrValues', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByAttrValuesDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'attrValues', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByDisplayName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayName', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByDisplayNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'displayName', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByGeoJson() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByGeoJsonDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByOperatorIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operatorID', Sort.desc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy> thenByRouteLength() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.asc);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QAfterSortBy>
      thenByRouteLengthDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.desc);
    });
  }
}

extension GeoObjectIsarQueryWhereDistinct
    on QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> {
  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'ID');
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByAttrValues(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'attrValues', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByColor(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'color', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByDisplayName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'displayName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByGeoJson(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'geoJson', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByMiCreateDate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct>
      distinctByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateUser');
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByMiModifyDate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct>
      distinctByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyUser');
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miOwner');
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByOperatorID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'operatorID');
    });
  }

  QueryBuilder<GeoObjectIsar, GeoObjectIsar, QDistinct> distinctByRouteLength(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'routeLength', caseSensitive: caseSensitive);
    });
  }
}

extension GeoObjectIsarQueryProperty
    on QueryBuilder<GeoObjectIsar, GeoObjectIsar, QQueryProperty> {
  QueryBuilder<GeoObjectIsar, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<GeoObjectIsar, int?, QQueryOperations> IDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'ID');
    });
  }

  QueryBuilder<GeoObjectIsar, String?, QQueryOperations> attrValuesProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'attrValues');
    });
  }

  QueryBuilder<GeoObjectIsar, String?, QQueryOperations> colorProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'color');
    });
  }

  QueryBuilder<GeoObjectIsar, String?, QQueryOperations> displayNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'displayName');
    });
  }

  QueryBuilder<GeoObjectIsar, String?, QQueryOperations> geoJsonProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'geoJson');
    });
  }

  QueryBuilder<GeoObjectIsar, String?, QQueryOperations>
      miCreateDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateDate');
    });
  }

  QueryBuilder<GeoObjectIsar, int?, QQueryOperations> miCreateUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateUser');
    });
  }

  QueryBuilder<GeoObjectIsar, String?, QQueryOperations>
      miModifyDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyDate');
    });
  }

  QueryBuilder<GeoObjectIsar, int?, QQueryOperations> miModifyUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyUser');
    });
  }

  QueryBuilder<GeoObjectIsar, int?, QQueryOperations> miOwnerProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miOwner');
    });
  }

  QueryBuilder<GeoObjectIsar, String?, QQueryOperations> nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<GeoObjectIsar, int?, QQueryOperations> operatorIDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'operatorID');
    });
  }

  QueryBuilder<GeoObjectIsar, String?, QQueryOperations> routeLengthProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'routeLength');
    });
  }
}
