// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dict_technical_support_isar.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetDictTechnicalSupportIsarCollection on Isar {
  IsarCollection<DictTechnicalSupportIsar> get dictTechnicalSupportIsars =>
      this.collection();
}

const DictTechnicalSupportIsarSchema = CollectionSchema(
  name: r'DictTechnicalSupportIsar',
  id: 2566772672437672678,
  properties: {
    r'ID': PropertySchema(
      id: 0,
      name: r'ID',
      type: IsarType.long,
    ),
    r'miCreateDate': PropertySchema(
      id: 1,
      name: r'miCreateDate',
      type: IsarType.string,
    ),
    r'miCreateUser': PropertySchema(
      id: 2,
      name: r'miCreateUser',
      type: IsarType.long,
    ),
    r'miModifyDate': PropertySchema(
      id: 3,
      name: r'miModifyDate',
      type: IsarType.string,
    ),
    r'miModifyUser': PropertySchema(
      id: 4,
      name: r'miModifyUser',
      type: IsarType.long,
    ),
    r'miOwner': PropertySchema(
      id: 5,
      name: r'miOwner',
      type: IsarType.long,
    ),
    r'name': PropertySchema(
      id: 6,
      name: r'name',
      type: IsarType.string,
    )
  },
  estimateSize: _dictTechnicalSupportIsarEstimateSize,
  serialize: _dictTechnicalSupportIsarSerialize,
  deserialize: _dictTechnicalSupportIsarDeserialize,
  deserializeProp: _dictTechnicalSupportIsarDeserializeProp,
  idName: r'id',
  indexes: {
    r'ID': IndexSchema(
      id: 5573724008404263404,
      name: r'ID',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'ID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'name': IndexSchema(
      id: 879695947855722453,
      name: r'name',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'name',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miOwner': IndexSchema(
      id: 5009576193767868493,
      name: r'miOwner',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miOwner',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miCreateDate': IndexSchema(
      id: 7720202597010838026,
      name: r'miCreateDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miCreateUser': IndexSchema(
      id: -2425434464049914295,
      name: r'miCreateUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miModifyDate': IndexSchema(
      id: -6487646225374976868,
      name: r'miModifyDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miModifyUser': IndexSchema(
      id: -1454558942143342536,
      name: r'miModifyUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _dictTechnicalSupportIsarGetId,
  getLinks: _dictTechnicalSupportIsarGetLinks,
  attach: _dictTechnicalSupportIsarAttach,
  version: '3.1.0+1',
);

int _dictTechnicalSupportIsarEstimateSize(
  DictTechnicalSupportIsar object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.miCreateDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miModifyDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _dictTechnicalSupportIsarSerialize(
  DictTechnicalSupportIsar object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.ID);
  writer.writeString(offsets[1], object.miCreateDate);
  writer.writeLong(offsets[2], object.miCreateUser);
  writer.writeString(offsets[3], object.miModifyDate);
  writer.writeLong(offsets[4], object.miModifyUser);
  writer.writeLong(offsets[5], object.miOwner);
  writer.writeString(offsets[6], object.name);
}

DictTechnicalSupportIsar _dictTechnicalSupportIsarDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = DictTechnicalSupportIsar(
    ID: reader.readLongOrNull(offsets[0]),
    miCreateDate: reader.readStringOrNull(offsets[1]),
    miCreateUser: reader.readLongOrNull(offsets[2]),
    miModifyDate: reader.readStringOrNull(offsets[3]),
    miModifyUser: reader.readLongOrNull(offsets[4]),
    miOwner: reader.readLongOrNull(offsets[5]),
    name: reader.readStringOrNull(offsets[6]),
  );
  object.id = id;
  return object;
}

P _dictTechnicalSupportIsarDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readLongOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readLongOrNull(offset)) as P;
    case 5:
      return (reader.readLongOrNull(offset)) as P;
    case 6:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _dictTechnicalSupportIsarGetId(DictTechnicalSupportIsar object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _dictTechnicalSupportIsarGetLinks(
    DictTechnicalSupportIsar object) {
  return [];
}

void _dictTechnicalSupportIsarAttach(
    IsarCollection<dynamic> col, Id id, DictTechnicalSupportIsar object) {
  object.id = id;
}

extension DictTechnicalSupportIsarByIndex
    on IsarCollection<DictTechnicalSupportIsar> {
  Future<DictTechnicalSupportIsar?> getByID(int? ID) {
    return getByIndex(r'ID', [ID]);
  }

  DictTechnicalSupportIsar? getByIDSync(int? ID) {
    return getByIndexSync(r'ID', [ID]);
  }

  Future<bool> deleteByID(int? ID) {
    return deleteByIndex(r'ID', [ID]);
  }

  bool deleteByIDSync(int? ID) {
    return deleteByIndexSync(r'ID', [ID]);
  }

  Future<List<DictTechnicalSupportIsar?>> getAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndex(r'ID', values);
  }

  List<DictTechnicalSupportIsar?> getAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'ID', values);
  }

  Future<int> deleteAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'ID', values);
  }

  int deleteAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'ID', values);
  }

  Future<Id> putByID(DictTechnicalSupportIsar object) {
    return putByIndex(r'ID', object);
  }

  Id putByIDSync(DictTechnicalSupportIsar object, {bool saveLinks = true}) {
    return putByIndexSync(r'ID', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByID(List<DictTechnicalSupportIsar> objects) {
    return putAllByIndex(r'ID', objects);
  }

  List<Id> putAllByIDSync(List<DictTechnicalSupportIsar> objects,
      {bool saveLinks = true}) {
    return putAllByIndexSync(r'ID', objects, saveLinks: saveLinks);
  }
}

extension DictTechnicalSupportIsarQueryWhereSort on QueryBuilder<
    DictTechnicalSupportIsar, DictTechnicalSupportIsar, QWhere> {
  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterWhere>
      anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterWhere>
      anyID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'ID'),
      );
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterWhere>
      anyMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miOwner'),
      );
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterWhere>
      anyMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miCreateUser'),
      );
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterWhere>
      anyMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miModifyUser'),
      );
    });
  }
}

extension DictTechnicalSupportIsarQueryWhere on QueryBuilder<
    DictTechnicalSupportIsar, DictTechnicalSupportIsar, QWhereClause> {
  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [null],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> iDEqualTo(int? ID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [ID],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> iDNotEqualTo(int? ID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> iDGreaterThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [ID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> iDLessThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [],
        upper: [ID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> iDBetween(
    int? lowerID,
    int? upperID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [lowerID],
        includeLower: includeLower,
        upper: [upperID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [null],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'name',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> nameEqualTo(String? name) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [name],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> nameNotEqualTo(String? name) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [null],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miOwnerEqualTo(int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [miOwner],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miOwnerNotEqualTo(int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miOwnerGreaterThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [miOwner],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miOwnerLessThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [],
        upper: [miOwner],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miOwnerBetween(
    int? lowerMiOwner,
    int? upperMiOwner, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [lowerMiOwner],
        includeLower: includeLower,
        upper: [upperMiOwner],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateDateEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [miCreateDate],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateDateNotEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateUserEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [miCreateUser],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateUserNotEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateUserGreaterThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [miCreateUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateUserLessThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [],
        upper: [miCreateUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miCreateUserBetween(
    int? lowerMiCreateUser,
    int? upperMiCreateUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [lowerMiCreateUser],
        includeLower: includeLower,
        upper: [upperMiCreateUser],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyDateEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [miModifyDate],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyDateNotEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyUserEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [miModifyUser],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyUserNotEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyUserGreaterThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [miModifyUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyUserLessThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [],
        upper: [miModifyUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterWhereClause> miModifyUserBetween(
    int? lowerMiModifyUser,
    int? upperMiModifyUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [lowerMiModifyUser],
        includeLower: includeLower,
        upper: [upperMiModifyUser],
        includeUpper: includeUpper,
      ));
    });
  }
}

extension DictTechnicalSupportIsarQueryFilter on QueryBuilder<
    DictTechnicalSupportIsar, DictTechnicalSupportIsar, QFilterCondition> {
  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> iDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> iDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> iDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> iDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'ID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
          QAfterFilterCondition>
      miCreateDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
          QAfterFilterCondition>
      miCreateDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miCreateDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miCreateUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
          QAfterFilterCondition>
      miModifyDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
          QAfterFilterCondition>
      miModifyDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miModifyDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miModifyUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miOwnerEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miOwnerGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miOwnerLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> miOwnerBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miOwner',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
          QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
          QAfterFilterCondition>
      nameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar,
      QAfterFilterCondition> nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }
}

extension DictTechnicalSupportIsarQueryObject on QueryBuilder<
    DictTechnicalSupportIsar, DictTechnicalSupportIsar, QFilterCondition> {}

extension DictTechnicalSupportIsarQueryLinks on QueryBuilder<
    DictTechnicalSupportIsar, DictTechnicalSupportIsar, QFilterCondition> {}

extension DictTechnicalSupportIsarQuerySortBy on QueryBuilder<
    DictTechnicalSupportIsar, DictTechnicalSupportIsar, QSortBy> {
  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }
}

extension DictTechnicalSupportIsarQuerySortThenBy on QueryBuilder<
    DictTechnicalSupportIsar, DictTechnicalSupportIsar, QSortThenBy> {
  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QAfterSortBy>
      thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }
}

extension DictTechnicalSupportIsarQueryWhereDistinct on QueryBuilder<
    DictTechnicalSupportIsar, DictTechnicalSupportIsar, QDistinct> {
  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QDistinct>
      distinctByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'ID');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QDistinct>
      distinctByMiCreateDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QDistinct>
      distinctByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateUser');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QDistinct>
      distinctByMiModifyDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QDistinct>
      distinctByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyUser');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QDistinct>
      distinctByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miOwner');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, DictTechnicalSupportIsar, QDistinct>
      distinctByName({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }
}

extension DictTechnicalSupportIsarQueryProperty on QueryBuilder<
    DictTechnicalSupportIsar, DictTechnicalSupportIsar, QQueryProperty> {
  QueryBuilder<DictTechnicalSupportIsar, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, int?, QQueryOperations> IDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'ID');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, String?, QQueryOperations>
      miCreateDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateDate');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, int?, QQueryOperations>
      miCreateUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateUser');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, String?, QQueryOperations>
      miModifyDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyDate');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, int?, QQueryOperations>
      miModifyUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyUser');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, int?, QQueryOperations>
      miOwnerProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miOwner');
    });
  }

  QueryBuilder<DictTechnicalSupportIsar, String?, QQueryOperations>
      nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }
}
