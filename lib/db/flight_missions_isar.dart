import 'package:irizi/db/flight_events_isar.dart';
import 'package:isar/isar.dart';

import 'material_flight_mission_isar.dart';

part 'flight_missions_isar.g.dart';


@Collection()
class GeoFlightMissionIsar {
  GeoFlightMissionIsar({
    required this.ID,
    required this.num,
    required this.object,
    required this.flightMissionType,
    required this.route,
    required this.operator,
    required this.latitude,
    required this.longitude,
    required this.latLngStartCombined,
    required this.examinationDateFrom,
    required this.examinationDateTo,
    required this.examinationDatesCombined,
    required this.date,
    required this.executionPlanStartDate,
    required this.executionPlanEndDate,
    required this.executionPlanDatesCombined,
    required this.executionDeadlineDate,
    required this.responsiblePerson,
    required this.geoJson,
    required this.executionInfo,
    required this.updateInfo,
    required this.technicalSupport,
    required this.meteorologicalData,
    required this.nearestLocality,
    required this.flightDuration,
    required this.routeLength,
    required this.routeArea,
    required this.state,
    required this.eventsState,
    required this.flightGroupID,
    required this.landGroupID,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
   // required this.flightGroupID_name,
   // required this.landGroupID_name,
   // required this.object_name,
   // required this.operator_name,
    //required this.route_name,
   // required this.state_name,
   // required this.technicalSupport_name
  });

  Id? id = Isar.autoIncrement;
  
  @Index(unique: true,  replace: true)
    
  late int? ID;

  @Index()
  late  String? num;

  @Index()
  late  int? object;

 // @Index()
 // late  String? object_name;

  @Index()
  late int? flightMissionType;

  @Index()
  late  int? route;

 // @Index()
  //late  String? route_name;

  @Index()
  late  int? operator;

 // @Index()
 // late  String? operator_name;

  @Index()
  late  double? latitude;

  @Index()
  late  double? longitude;

  @Index()
  late  String? latLngStartCombined;

  @Index()
  late  String? examinationDateFrom;

  @Index()
  late  String? examinationDateTo;
  @Index()
  late  String? examinationDatesCombined;

  @Index()
  late  String? date;

  @Index()
  late  String? executionPlanStartDate;

  @Index()

  late  String? executionPlanEndDate;

  @Index()
  late  String? executionPlanDatesCombined;

  @Index()
  late  String? executionDeadlineDate;

  @Index()
  late  String? responsiblePerson;

  @Index()
  late  String? geoJson;

  @Index()
  late  int? executionInfo;

  @Index()
  late  int? updateInfo;

  @Index()
  late  int? technicalSupport;

//  @Index()
//  late  String? technicalSupport_name;

  @Index()
  late  String? meteorologicalData;

  @Index()
  late  String? nearestLocality;

  @Index()
  late  String? flightDuration;

  @Index()
  late  double? routeLength;

  @Index()
  late  int? routeArea;

  @Index()
  late  String? state;

 // @Index()
 // late  String? state_name;

  @Index()
  late  String? eventsState;

  @Index()
  late  int? flightGroupID;

 // @Index()
//  late  String? flightGroupID_name;

  @Index()
  late  int? landGroupID;
  
 // @Index()
 // late  String? landGroupID_name;

  @Index()
  late  int? miOwner;

  @Index()
  late  String? miCreateDate;

  @Index()
  late  int? miCreateUser;

  @Index()
  late  String? miModifyDate;

  @Index()
  late  int? miModifyUser;


  final flightEvents = IsarLinks<GeoEventIsar>();

  final flightMaterials = IsarLinks<FlightMissionMaterialIsar>();

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['num'] = num;
    data['object'] = object;
    data['flightMissionType'] = flightMissionType;
    data['route'] = route;
    data['operator'] = operator;
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['latLngStartCombined'] = latLngStartCombined;
    data['examinationDateFrom'] = examinationDateFrom;
    data['examinationDateTo'] = examinationDateTo;
    data['examinationDatesCombined'] = examinationDatesCombined;
    data['date'] = date;
    data['executionPlanStartDate'] = executionPlanStartDate;
    data['executionPlanEndDate'] = executionPlanEndDate;
    data['executionPlanDatesCombined'] = executionPlanDatesCombined;
    data['executionDeadlineDate'] = executionDeadlineDate;
    data['responsiblePerson'] = responsiblePerson;
    data['geoJson'] = geoJson;
    data['executionInfo'] = executionInfo;
    data['updateInfo'] = updateInfo;
    data['technicalSupport'] = technicalSupport;
    data['meteorologicalData'] = meteorologicalData;
    data['nearestLocality'] = nearestLocality;
    data['flightDuration'] = flightDuration;
    data['routeLength'] = routeLength;
    data['routeArea'] = routeArea;
    data['state'] = state;
    data['eventsState'] = eventsState;
    data['flightGroupID'] = flightGroupID;
    data['landGroupID'] = landGroupID;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }

  
  

}

