// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geo_route_isar.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetGeoRouteIsarCollection on Isar {
  IsarCollection<GeoRouteIsar> get geoRouteIsars => this.collection();
}

const GeoRouteIsarSchema = CollectionSchema(
  name: r'GeoRouteIsar',
  id: -3607923458392388387,
  properties: {
    r'ID': PropertySchema(
      id: 0,
      name: r'ID',
      type: IsarType.long,
    ),
    r'color': PropertySchema(
      id: 1,
      name: r'color',
      type: IsarType.string,
    ),
    r'geoJson': PropertySchema(
      id: 2,
      name: r'geoJson',
      type: IsarType.string,
    ),
    r'miCreateDate': PropertySchema(
      id: 3,
      name: r'miCreateDate',
      type: IsarType.string,
    ),
    r'miCreateUser': PropertySchema(
      id: 4,
      name: r'miCreateUser',
      type: IsarType.long,
    ),
    r'miModifyDate': PropertySchema(
      id: 5,
      name: r'miModifyDate',
      type: IsarType.string,
    ),
    r'miModifyUser': PropertySchema(
      id: 6,
      name: r'miModifyUser',
      type: IsarType.long,
    ),
    r'miOwner': PropertySchema(
      id: 7,
      name: r'miOwner',
      type: IsarType.long,
    ),
    r'name': PropertySchema(
      id: 8,
      name: r'name',
      type: IsarType.string,
    ),
    r'objectID': PropertySchema(
      id: 9,
      name: r'objectID',
      type: IsarType.long,
    ),
    r'routeLength': PropertySchema(
      id: 10,
      name: r'routeLength',
      type: IsarType.string,
    )
  },
  estimateSize: _geoRouteIsarEstimateSize,
  serialize: _geoRouteIsarSerialize,
  deserialize: _geoRouteIsarDeserialize,
  deserializeProp: _geoRouteIsarDeserializeProp,
  idName: r'id',
  indexes: {
    r'ID': IndexSchema(
      id: 5573724008404263404,
      name: r'ID',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'ID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'name': IndexSchema(
      id: 879695947855722453,
      name: r'name',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'name',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'objectID': IndexSchema(
      id: 6778081443600357311,
      name: r'objectID',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'objectID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'geoJson': IndexSchema(
      id: -5063599521477353396,
      name: r'geoJson',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'geoJson',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'color': IndexSchema(
      id: 880366885425937065,
      name: r'color',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'color',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'routeLength': IndexSchema(
      id: 3291277472231861796,
      name: r'routeLength',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'routeLength',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miOwner': IndexSchema(
      id: 5009576193767868493,
      name: r'miOwner',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miOwner',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miCreateDate': IndexSchema(
      id: 7720202597010838026,
      name: r'miCreateDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miCreateUser': IndexSchema(
      id: -2425434464049914295,
      name: r'miCreateUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miModifyDate': IndexSchema(
      id: -6487646225374976868,
      name: r'miModifyDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miModifyUser': IndexSchema(
      id: -1454558942143342536,
      name: r'miModifyUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _geoRouteIsarGetId,
  getLinks: _geoRouteIsarGetLinks,
  attach: _geoRouteIsarAttach,
  version: '3.1.0+1',
);

int _geoRouteIsarEstimateSize(
  GeoRouteIsar object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.color;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.geoJson;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miCreateDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miModifyDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.routeLength;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _geoRouteIsarSerialize(
  GeoRouteIsar object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.ID);
  writer.writeString(offsets[1], object.color);
  writer.writeString(offsets[2], object.geoJson);
  writer.writeString(offsets[3], object.miCreateDate);
  writer.writeLong(offsets[4], object.miCreateUser);
  writer.writeString(offsets[5], object.miModifyDate);
  writer.writeLong(offsets[6], object.miModifyUser);
  writer.writeLong(offsets[7], object.miOwner);
  writer.writeString(offsets[8], object.name);
  writer.writeLong(offsets[9], object.objectID);
  writer.writeString(offsets[10], object.routeLength);
}

GeoRouteIsar _geoRouteIsarDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = GeoRouteIsar(
    ID: reader.readLongOrNull(offsets[0]),
    color: reader.readStringOrNull(offsets[1]),
    geoJson: reader.readStringOrNull(offsets[2]),
    miCreateDate: reader.readStringOrNull(offsets[3]),
    miCreateUser: reader.readLongOrNull(offsets[4]),
    miModifyDate: reader.readStringOrNull(offsets[5]),
    miModifyUser: reader.readLongOrNull(offsets[6]),
    miOwner: reader.readLongOrNull(offsets[7]),
    name: reader.readStringOrNull(offsets[8]),
    objectID: reader.readLongOrNull(offsets[9]),
    routeLength: reader.readStringOrNull(offsets[10]),
  );
  object.id = id;
  return object;
}

P _geoRouteIsarDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readLongOrNull(offset)) as P;
    case 5:
      return (reader.readStringOrNull(offset)) as P;
    case 6:
      return (reader.readLongOrNull(offset)) as P;
    case 7:
      return (reader.readLongOrNull(offset)) as P;
    case 8:
      return (reader.readStringOrNull(offset)) as P;
    case 9:
      return (reader.readLongOrNull(offset)) as P;
    case 10:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _geoRouteIsarGetId(GeoRouteIsar object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _geoRouteIsarGetLinks(GeoRouteIsar object) {
  return [];
}

void _geoRouteIsarAttach(
    IsarCollection<dynamic> col, Id id, GeoRouteIsar object) {
  object.id = id;
}

extension GeoRouteIsarByIndex on IsarCollection<GeoRouteIsar> {
  Future<GeoRouteIsar?> getByID(int? ID) {
    return getByIndex(r'ID', [ID]);
  }

  GeoRouteIsar? getByIDSync(int? ID) {
    return getByIndexSync(r'ID', [ID]);
  }

  Future<bool> deleteByID(int? ID) {
    return deleteByIndex(r'ID', [ID]);
  }

  bool deleteByIDSync(int? ID) {
    return deleteByIndexSync(r'ID', [ID]);
  }

  Future<List<GeoRouteIsar?>> getAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndex(r'ID', values);
  }

  List<GeoRouteIsar?> getAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'ID', values);
  }

  Future<int> deleteAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'ID', values);
  }

  int deleteAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'ID', values);
  }

  Future<Id> putByID(GeoRouteIsar object) {
    return putByIndex(r'ID', object);
  }

  Id putByIDSync(GeoRouteIsar object, {bool saveLinks = true}) {
    return putByIndexSync(r'ID', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByID(List<GeoRouteIsar> objects) {
    return putAllByIndex(r'ID', objects);
  }

  List<Id> putAllByIDSync(List<GeoRouteIsar> objects, {bool saveLinks = true}) {
    return putAllByIndexSync(r'ID', objects, saveLinks: saveLinks);
  }
}

extension GeoRouteIsarQueryWhereSort
    on QueryBuilder<GeoRouteIsar, GeoRouteIsar, QWhere> {
  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhere> anyID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'ID'),
      );
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhere> anyObjectID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'objectID'),
      );
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhere> anyMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miOwner'),
      );
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhere> anyMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miCreateUser'),
      );
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhere> anyMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miModifyUser'),
      );
    });
  }
}

extension GeoRouteIsarQueryWhere
    on QueryBuilder<GeoRouteIsar, GeoRouteIsar, QWhereClause> {
  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> idGreaterThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> iDEqualTo(
      int? ID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [ID],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> iDNotEqualTo(
      int? ID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> iDGreaterThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [ID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> iDLessThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [],
        upper: [ID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> iDBetween(
    int? lowerID,
    int? upperID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [lowerID],
        includeLower: includeLower,
        upper: [upperID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'name',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> nameEqualTo(
      String? name) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'name',
        value: [name],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> nameNotEqualTo(
      String? name) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [name],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'name',
              lower: [],
              upper: [name],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> objectIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'objectID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      objectIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'objectID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> objectIDEqualTo(
      int? objectID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'objectID',
        value: [objectID],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      objectIDNotEqualTo(int? objectID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'objectID',
              lower: [],
              upper: [objectID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'objectID',
              lower: [objectID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'objectID',
              lower: [objectID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'objectID',
              lower: [],
              upper: [objectID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      objectIDGreaterThan(
    int? objectID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'objectID',
        lower: [objectID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> objectIDLessThan(
    int? objectID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'objectID',
        lower: [],
        upper: [objectID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> objectIDBetween(
    int? lowerObjectID,
    int? upperObjectID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'objectID',
        lower: [lowerObjectID],
        includeLower: includeLower,
        upper: [upperObjectID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> geoJsonIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'geoJson',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      geoJsonIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'geoJson',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> geoJsonEqualTo(
      String? geoJson) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'geoJson',
        value: [geoJson],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> geoJsonNotEqualTo(
      String? geoJson) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [],
              upper: [geoJson],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [geoJson],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [geoJson],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [],
              upper: [geoJson],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> colorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'color',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> colorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'color',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> colorEqualTo(
      String? color) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'color',
        value: [color],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> colorNotEqualTo(
      String? color) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'color',
              lower: [],
              upper: [color],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'color',
              lower: [color],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'color',
              lower: [color],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'color',
              lower: [],
              upper: [color],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      routeLengthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'routeLength',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      routeLengthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'routeLength',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      routeLengthEqualTo(String? routeLength) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'routeLength',
        value: [routeLength],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      routeLengthNotEqualTo(String? routeLength) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [],
              upper: [routeLength],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [routeLength],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [routeLength],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [],
              upper: [routeLength],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> miOwnerEqualTo(
      int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [miOwner],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> miOwnerNotEqualTo(
      int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miOwnerGreaterThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [miOwner],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> miOwnerLessThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [],
        upper: [miOwner],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause> miOwnerBetween(
    int? lowerMiOwner,
    int? upperMiOwner, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [lowerMiOwner],
        includeLower: includeLower,
        upper: [upperMiOwner],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateDateEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [miCreateDate],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateDateNotEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateUserEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [miCreateUser],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateUserNotEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateUserGreaterThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [miCreateUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateUserLessThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [],
        upper: [miCreateUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miCreateUserBetween(
    int? lowerMiCreateUser,
    int? upperMiCreateUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [lowerMiCreateUser],
        includeLower: includeLower,
        upper: [upperMiCreateUser],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyDateEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [miModifyDate],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyDateNotEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyUserEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [miModifyUser],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyUserNotEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyUserGreaterThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [miModifyUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyUserLessThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [],
        upper: [miModifyUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterWhereClause>
      miModifyUserBetween(
    int? lowerMiModifyUser,
    int? upperMiModifyUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [lowerMiModifyUser],
        includeLower: includeLower,
        upper: [upperMiModifyUser],
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GeoRouteIsarQueryFilter
    on QueryBuilder<GeoRouteIsar, GeoRouteIsar, QFilterCondition> {
  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> iDEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> iDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> iDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> iDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'ID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      colorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'color',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      colorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'color',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> colorEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      colorGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> colorLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> colorBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'color',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      colorStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> colorEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> colorContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'color',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> colorMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'color',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      colorIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      colorIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'color',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'geoJson',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'geoJson',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'geoJson',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'geoJson',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'geoJson',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      geoJsonIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'geoJson',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> idEqualTo(
      Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miCreateDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miCreateUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miModifyDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miModifyUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miOwnerEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miOwnerGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miOwnerLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      miOwnerBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miOwner',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> nameContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition> nameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      objectIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'objectID',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      objectIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'objectID',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      objectIDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'objectID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      objectIDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'objectID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      objectIDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'objectID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      objectIDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'objectID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'routeLength',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'routeLength',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'routeLength',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'routeLength',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'routeLength',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'routeLength',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterFilterCondition>
      routeLengthIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'routeLength',
        value: '',
      ));
    });
  }
}

extension GeoRouteIsarQueryObject
    on QueryBuilder<GeoRouteIsar, GeoRouteIsar, QFilterCondition> {}

extension GeoRouteIsarQueryLinks
    on QueryBuilder<GeoRouteIsar, GeoRouteIsar, QFilterCondition> {}

extension GeoRouteIsarQuerySortBy
    on QueryBuilder<GeoRouteIsar, GeoRouteIsar, QSortBy> {
  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByGeoJson() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByGeoJsonDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy>
      sortByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy>
      sortByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy>
      sortByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy>
      sortByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByObjectID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectID', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByObjectIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectID', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> sortByRouteLength() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy>
      sortByRouteLengthDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.desc);
    });
  }
}

extension GeoRouteIsarQuerySortThenBy
    on QueryBuilder<GeoRouteIsar, GeoRouteIsar, QSortThenBy> {
  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByColor() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByColorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'color', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByGeoJson() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByGeoJsonDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy>
      thenByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy>
      thenByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy>
      thenByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy>
      thenByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByObjectID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectID', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByObjectIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'objectID', Sort.desc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy> thenByRouteLength() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.asc);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QAfterSortBy>
      thenByRouteLengthDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.desc);
    });
  }
}

extension GeoRouteIsarQueryWhereDistinct
    on QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> {
  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'ID');
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByColor(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'color', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByGeoJson(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'geoJson', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByMiCreateDate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateUser');
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByMiModifyDate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyUser');
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miOwner');
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByObjectID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'objectID');
    });
  }

  QueryBuilder<GeoRouteIsar, GeoRouteIsar, QDistinct> distinctByRouteLength(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'routeLength', caseSensitive: caseSensitive);
    });
  }
}

extension GeoRouteIsarQueryProperty
    on QueryBuilder<GeoRouteIsar, GeoRouteIsar, QQueryProperty> {
  QueryBuilder<GeoRouteIsar, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<GeoRouteIsar, int?, QQueryOperations> IDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'ID');
    });
  }

  QueryBuilder<GeoRouteIsar, String?, QQueryOperations> colorProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'color');
    });
  }

  QueryBuilder<GeoRouteIsar, String?, QQueryOperations> geoJsonProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'geoJson');
    });
  }

  QueryBuilder<GeoRouteIsar, String?, QQueryOperations> miCreateDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateDate');
    });
  }

  QueryBuilder<GeoRouteIsar, int?, QQueryOperations> miCreateUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateUser');
    });
  }

  QueryBuilder<GeoRouteIsar, String?, QQueryOperations> miModifyDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyDate');
    });
  }

  QueryBuilder<GeoRouteIsar, int?, QQueryOperations> miModifyUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyUser');
    });
  }

  QueryBuilder<GeoRouteIsar, int?, QQueryOperations> miOwnerProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miOwner');
    });
  }

  QueryBuilder<GeoRouteIsar, String?, QQueryOperations> nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<GeoRouteIsar, int?, QQueryOperations> objectIDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'objectID');
    });
  }

  QueryBuilder<GeoRouteIsar, String?, QQueryOperations> routeLengthProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'routeLength');
    });
  }
}
