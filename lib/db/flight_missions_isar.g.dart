// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flight_missions_isar.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetGeoFlightMissionIsarCollection on Isar {
  IsarCollection<GeoFlightMissionIsar> get geoFlightMissionIsars =>
      this.collection();
}

const GeoFlightMissionIsarSchema = CollectionSchema(
  name: r'GeoFlightMissionIsar',
  id: 8053776971321773176,
  properties: {
    r'ID': PropertySchema(
      id: 0,
      name: r'ID',
      type: IsarType.long,
    ),
    r'date': PropertySchema(
      id: 1,
      name: r'date',
      type: IsarType.string,
    ),
    r'eventsState': PropertySchema(
      id: 2,
      name: r'eventsState',
      type: IsarType.string,
    ),
    r'examinationDateFrom': PropertySchema(
      id: 3,
      name: r'examinationDateFrom',
      type: IsarType.string,
    ),
    r'examinationDateTo': PropertySchema(
      id: 4,
      name: r'examinationDateTo',
      type: IsarType.string,
    ),
    r'examinationDatesCombined': PropertySchema(
      id: 5,
      name: r'examinationDatesCombined',
      type: IsarType.string,
    ),
    r'executionDeadlineDate': PropertySchema(
      id: 6,
      name: r'executionDeadlineDate',
      type: IsarType.string,
    ),
    r'executionInfo': PropertySchema(
      id: 7,
      name: r'executionInfo',
      type: IsarType.long,
    ),
    r'executionPlanDatesCombined': PropertySchema(
      id: 8,
      name: r'executionPlanDatesCombined',
      type: IsarType.string,
    ),
    r'executionPlanEndDate': PropertySchema(
      id: 9,
      name: r'executionPlanEndDate',
      type: IsarType.string,
    ),
    r'executionPlanStartDate': PropertySchema(
      id: 10,
      name: r'executionPlanStartDate',
      type: IsarType.string,
    ),
    r'flightDuration': PropertySchema(
      id: 11,
      name: r'flightDuration',
      type: IsarType.string,
    ),
    r'flightGroupID': PropertySchema(
      id: 12,
      name: r'flightGroupID',
      type: IsarType.long,
    ),
    r'flightMissionType': PropertySchema(
      id: 13,
      name: r'flightMissionType',
      type: IsarType.long,
    ),
    r'geoJson': PropertySchema(
      id: 14,
      name: r'geoJson',
      type: IsarType.string,
    ),
    r'landGroupID': PropertySchema(
      id: 15,
      name: r'landGroupID',
      type: IsarType.long,
    ),
    r'latLngStartCombined': PropertySchema(
      id: 16,
      name: r'latLngStartCombined',
      type: IsarType.string,
    ),
    r'latitude': PropertySchema(
      id: 17,
      name: r'latitude',
      type: IsarType.double,
    ),
    r'longitude': PropertySchema(
      id: 18,
      name: r'longitude',
      type: IsarType.double,
    ),
    r'meteorologicalData': PropertySchema(
      id: 19,
      name: r'meteorologicalData',
      type: IsarType.string,
    ),
    r'miCreateDate': PropertySchema(
      id: 20,
      name: r'miCreateDate',
      type: IsarType.string,
    ),
    r'miCreateUser': PropertySchema(
      id: 21,
      name: r'miCreateUser',
      type: IsarType.long,
    ),
    r'miModifyDate': PropertySchema(
      id: 22,
      name: r'miModifyDate',
      type: IsarType.string,
    ),
    r'miModifyUser': PropertySchema(
      id: 23,
      name: r'miModifyUser',
      type: IsarType.long,
    ),
    r'miOwner': PropertySchema(
      id: 24,
      name: r'miOwner',
      type: IsarType.long,
    ),
    r'nearestLocality': PropertySchema(
      id: 25,
      name: r'nearestLocality',
      type: IsarType.string,
    ),
    r'num': PropertySchema(
      id: 26,
      name: r'num',
      type: IsarType.string,
    ),
    r'object': PropertySchema(
      id: 27,
      name: r'object',
      type: IsarType.long,
    ),
    r'operator': PropertySchema(
      id: 28,
      name: r'operator',
      type: IsarType.long,
    ),
    r'responsiblePerson': PropertySchema(
      id: 29,
      name: r'responsiblePerson',
      type: IsarType.string,
    ),
    r'route': PropertySchema(
      id: 30,
      name: r'route',
      type: IsarType.long,
    ),
    r'routeArea': PropertySchema(
      id: 31,
      name: r'routeArea',
      type: IsarType.long,
    ),
    r'routeLength': PropertySchema(
      id: 32,
      name: r'routeLength',
      type: IsarType.double,
    ),
    r'state': PropertySchema(
      id: 33,
      name: r'state',
      type: IsarType.string,
    ),
    r'technicalSupport': PropertySchema(
      id: 34,
      name: r'technicalSupport',
      type: IsarType.long,
    ),
    r'updateInfo': PropertySchema(
      id: 35,
      name: r'updateInfo',
      type: IsarType.long,
    )
  },
  estimateSize: _geoFlightMissionIsarEstimateSize,
  serialize: _geoFlightMissionIsarSerialize,
  deserialize: _geoFlightMissionIsarDeserialize,
  deserializeProp: _geoFlightMissionIsarDeserializeProp,
  idName: r'id',
  indexes: {
    r'ID': IndexSchema(
      id: 5573724008404263404,
      name: r'ID',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'ID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'num': IndexSchema(
      id: -1976589900541946553,
      name: r'num',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'num',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'object': IndexSchema(
      id: 9217563911824436619,
      name: r'object',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'object',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'flightMissionType': IndexSchema(
      id: 9172516787912864034,
      name: r'flightMissionType',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'flightMissionType',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'route': IndexSchema(
      id: 8686557153332962867,
      name: r'route',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'route',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'operator': IndexSchema(
      id: -6223719989933430860,
      name: r'operator',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'operator',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'latitude': IndexSchema(
      id: 2839588665230214757,
      name: r'latitude',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'latitude',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'longitude': IndexSchema(
      id: -7076447437327017580,
      name: r'longitude',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'longitude',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'latLngStartCombined': IndexSchema(
      id: -9121271298071349656,
      name: r'latLngStartCombined',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'latLngStartCombined',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'examinationDateFrom': IndexSchema(
      id: 7108296191965022302,
      name: r'examinationDateFrom',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'examinationDateFrom',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'examinationDateTo': IndexSchema(
      id: 2014273921717232260,
      name: r'examinationDateTo',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'examinationDateTo',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'examinationDatesCombined': IndexSchema(
      id: -569821495534550722,
      name: r'examinationDatesCombined',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'examinationDatesCombined',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'date': IndexSchema(
      id: -7552997827385218417,
      name: r'date',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'date',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'executionPlanStartDate': IndexSchema(
      id: -8641610776686470202,
      name: r'executionPlanStartDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'executionPlanStartDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'executionPlanEndDate': IndexSchema(
      id: -4510034652314949982,
      name: r'executionPlanEndDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'executionPlanEndDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'executionPlanDatesCombined': IndexSchema(
      id: 2683530055864412427,
      name: r'executionPlanDatesCombined',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'executionPlanDatesCombined',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'executionDeadlineDate': IndexSchema(
      id: -7013738509653368662,
      name: r'executionDeadlineDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'executionDeadlineDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'responsiblePerson': IndexSchema(
      id: 7792800716778845282,
      name: r'responsiblePerson',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'responsiblePerson',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'geoJson': IndexSchema(
      id: -5063599521477353396,
      name: r'geoJson',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'geoJson',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'executionInfo': IndexSchema(
      id: 8334884868947125398,
      name: r'executionInfo',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'executionInfo',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'updateInfo': IndexSchema(
      id: -3829585817773757679,
      name: r'updateInfo',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'updateInfo',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'technicalSupport': IndexSchema(
      id: -3032909699596222815,
      name: r'technicalSupport',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'technicalSupport',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'meteorologicalData': IndexSchema(
      id: 3558461677970360590,
      name: r'meteorologicalData',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'meteorologicalData',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'nearestLocality': IndexSchema(
      id: -132182171373551492,
      name: r'nearestLocality',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'nearestLocality',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'flightDuration': IndexSchema(
      id: -4479081242285392543,
      name: r'flightDuration',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'flightDuration',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'routeLength': IndexSchema(
      id: 3291277472231861796,
      name: r'routeLength',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'routeLength',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'routeArea': IndexSchema(
      id: 3988303286024644872,
      name: r'routeArea',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'routeArea',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'state': IndexSchema(
      id: 7917036384617311412,
      name: r'state',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'state',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'eventsState': IndexSchema(
      id: 952163493315883368,
      name: r'eventsState',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'eventsState',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'flightGroupID': IndexSchema(
      id: 497229043274415720,
      name: r'flightGroupID',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'flightGroupID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'landGroupID': IndexSchema(
      id: -7396875543706643878,
      name: r'landGroupID',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'landGroupID',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miOwner': IndexSchema(
      id: 5009576193767868493,
      name: r'miOwner',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miOwner',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miCreateDate': IndexSchema(
      id: 7720202597010838026,
      name: r'miCreateDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miCreateUser': IndexSchema(
      id: -2425434464049914295,
      name: r'miCreateUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miCreateUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    ),
    r'miModifyDate': IndexSchema(
      id: -6487646225374976868,
      name: r'miModifyDate',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyDate',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'miModifyUser': IndexSchema(
      id: -1454558942143342536,
      name: r'miModifyUser',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'miModifyUser',
          type: IndexType.value,
          caseSensitive: false,
        )
      ],
    )
  },
  links: {
    r'flightEvents': LinkSchema(
      id: 4000721816235002370,
      name: r'flightEvents',
      target: r'GeoEventIsar',
      single: false,
    ),
    r'flightMaterials': LinkSchema(
      id: 3901621590317846610,
      name: r'flightMaterials',
      target: r'FlightMissionMaterialIsar',
      single: false,
    )
  },
  embeddedSchemas: {},
  getId: _geoFlightMissionIsarGetId,
  getLinks: _geoFlightMissionIsarGetLinks,
  attach: _geoFlightMissionIsarAttach,
  version: '3.1.0+1',
);

int _geoFlightMissionIsarEstimateSize(
  GeoFlightMissionIsar object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.date;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.eventsState;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.examinationDateFrom;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.examinationDateTo;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.examinationDatesCombined;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.executionDeadlineDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.executionPlanDatesCombined;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.executionPlanEndDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.executionPlanStartDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.flightDuration;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.geoJson;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.latLngStartCombined;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.meteorologicalData;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miCreateDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.miModifyDate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.nearestLocality;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.num;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.responsiblePerson;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.state;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _geoFlightMissionIsarSerialize(
  GeoFlightMissionIsar object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.ID);
  writer.writeString(offsets[1], object.date);
  writer.writeString(offsets[2], object.eventsState);
  writer.writeString(offsets[3], object.examinationDateFrom);
  writer.writeString(offsets[4], object.examinationDateTo);
  writer.writeString(offsets[5], object.examinationDatesCombined);
  writer.writeString(offsets[6], object.executionDeadlineDate);
  writer.writeLong(offsets[7], object.executionInfo);
  writer.writeString(offsets[8], object.executionPlanDatesCombined);
  writer.writeString(offsets[9], object.executionPlanEndDate);
  writer.writeString(offsets[10], object.executionPlanStartDate);
  writer.writeString(offsets[11], object.flightDuration);
  writer.writeLong(offsets[12], object.flightGroupID);
  writer.writeLong(offsets[13], object.flightMissionType);
  writer.writeString(offsets[14], object.geoJson);
  writer.writeLong(offsets[15], object.landGroupID);
  writer.writeString(offsets[16], object.latLngStartCombined);
  writer.writeDouble(offsets[17], object.latitude);
  writer.writeDouble(offsets[18], object.longitude);
  writer.writeString(offsets[19], object.meteorologicalData);
  writer.writeString(offsets[20], object.miCreateDate);
  writer.writeLong(offsets[21], object.miCreateUser);
  writer.writeString(offsets[22], object.miModifyDate);
  writer.writeLong(offsets[23], object.miModifyUser);
  writer.writeLong(offsets[24], object.miOwner);
  writer.writeString(offsets[25], object.nearestLocality);
  writer.writeString(offsets[26], object.num);
  writer.writeLong(offsets[27], object.object);
  writer.writeLong(offsets[28], object.operator);
  writer.writeString(offsets[29], object.responsiblePerson);
  writer.writeLong(offsets[30], object.route);
  writer.writeLong(offsets[31], object.routeArea);
  writer.writeDouble(offsets[32], object.routeLength);
  writer.writeString(offsets[33], object.state);
  writer.writeLong(offsets[34], object.technicalSupport);
  writer.writeLong(offsets[35], object.updateInfo);
}

GeoFlightMissionIsar _geoFlightMissionIsarDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = GeoFlightMissionIsar(
    ID: reader.readLongOrNull(offsets[0]),
    date: reader.readStringOrNull(offsets[1]),
    eventsState: reader.readStringOrNull(offsets[2]),
    examinationDateFrom: reader.readStringOrNull(offsets[3]),
    examinationDateTo: reader.readStringOrNull(offsets[4]),
    examinationDatesCombined: reader.readStringOrNull(offsets[5]),
    executionDeadlineDate: reader.readStringOrNull(offsets[6]),
    executionInfo: reader.readLongOrNull(offsets[7]),
    executionPlanDatesCombined: reader.readStringOrNull(offsets[8]),
    executionPlanEndDate: reader.readStringOrNull(offsets[9]),
    executionPlanStartDate: reader.readStringOrNull(offsets[10]),
    flightDuration: reader.readStringOrNull(offsets[11]),
    flightGroupID: reader.readLongOrNull(offsets[12]),
    flightMissionType: reader.readLongOrNull(offsets[13]),
    geoJson: reader.readStringOrNull(offsets[14]),
    landGroupID: reader.readLongOrNull(offsets[15]),
    latLngStartCombined: reader.readStringOrNull(offsets[16]),
    latitude: reader.readDoubleOrNull(offsets[17]),
    longitude: reader.readDoubleOrNull(offsets[18]),
    meteorologicalData: reader.readStringOrNull(offsets[19]),
    miCreateDate: reader.readStringOrNull(offsets[20]),
    miCreateUser: reader.readLongOrNull(offsets[21]),
    miModifyDate: reader.readStringOrNull(offsets[22]),
    miModifyUser: reader.readLongOrNull(offsets[23]),
    miOwner: reader.readLongOrNull(offsets[24]),
    nearestLocality: reader.readStringOrNull(offsets[25]),
    num: reader.readStringOrNull(offsets[26]),
    object: reader.readLongOrNull(offsets[27]),
    operator: reader.readLongOrNull(offsets[28]),
    responsiblePerson: reader.readStringOrNull(offsets[29]),
    route: reader.readLongOrNull(offsets[30]),
    routeArea: reader.readLongOrNull(offsets[31]),
    routeLength: reader.readDoubleOrNull(offsets[32]),
    state: reader.readStringOrNull(offsets[33]),
    technicalSupport: reader.readLongOrNull(offsets[34]),
    updateInfo: reader.readLongOrNull(offsets[35]),
  );
  object.id = id;
  return object;
}

P _geoFlightMissionIsarDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readStringOrNull(offset)) as P;
    case 6:
      return (reader.readStringOrNull(offset)) as P;
    case 7:
      return (reader.readLongOrNull(offset)) as P;
    case 8:
      return (reader.readStringOrNull(offset)) as P;
    case 9:
      return (reader.readStringOrNull(offset)) as P;
    case 10:
      return (reader.readStringOrNull(offset)) as P;
    case 11:
      return (reader.readStringOrNull(offset)) as P;
    case 12:
      return (reader.readLongOrNull(offset)) as P;
    case 13:
      return (reader.readLongOrNull(offset)) as P;
    case 14:
      return (reader.readStringOrNull(offset)) as P;
    case 15:
      return (reader.readLongOrNull(offset)) as P;
    case 16:
      return (reader.readStringOrNull(offset)) as P;
    case 17:
      return (reader.readDoubleOrNull(offset)) as P;
    case 18:
      return (reader.readDoubleOrNull(offset)) as P;
    case 19:
      return (reader.readStringOrNull(offset)) as P;
    case 20:
      return (reader.readStringOrNull(offset)) as P;
    case 21:
      return (reader.readLongOrNull(offset)) as P;
    case 22:
      return (reader.readStringOrNull(offset)) as P;
    case 23:
      return (reader.readLongOrNull(offset)) as P;
    case 24:
      return (reader.readLongOrNull(offset)) as P;
    case 25:
      return (reader.readStringOrNull(offset)) as P;
    case 26:
      return (reader.readStringOrNull(offset)) as P;
    case 27:
      return (reader.readLongOrNull(offset)) as P;
    case 28:
      return (reader.readLongOrNull(offset)) as P;
    case 29:
      return (reader.readStringOrNull(offset)) as P;
    case 30:
      return (reader.readLongOrNull(offset)) as P;
    case 31:
      return (reader.readLongOrNull(offset)) as P;
    case 32:
      return (reader.readDoubleOrNull(offset)) as P;
    case 33:
      return (reader.readStringOrNull(offset)) as P;
    case 34:
      return (reader.readLongOrNull(offset)) as P;
    case 35:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _geoFlightMissionIsarGetId(GeoFlightMissionIsar object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _geoFlightMissionIsarGetLinks(
    GeoFlightMissionIsar object) {
  return [object.flightEvents, object.flightMaterials];
}

void _geoFlightMissionIsarAttach(
    IsarCollection<dynamic> col, Id id, GeoFlightMissionIsar object) {
  object.id = id;
  object.flightEvents
      .attach(col, col.isar.collection<GeoEventIsar>(), r'flightEvents', id);
  object.flightMaterials.attach(col,
      col.isar.collection<FlightMissionMaterialIsar>(), r'flightMaterials', id);
}

extension GeoFlightMissionIsarByIndex on IsarCollection<GeoFlightMissionIsar> {
  Future<GeoFlightMissionIsar?> getByID(int? ID) {
    return getByIndex(r'ID', [ID]);
  }

  GeoFlightMissionIsar? getByIDSync(int? ID) {
    return getByIndexSync(r'ID', [ID]);
  }

  Future<bool> deleteByID(int? ID) {
    return deleteByIndex(r'ID', [ID]);
  }

  bool deleteByIDSync(int? ID) {
    return deleteByIndexSync(r'ID', [ID]);
  }

  Future<List<GeoFlightMissionIsar?>> getAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndex(r'ID', values);
  }

  List<GeoFlightMissionIsar?> getAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'ID', values);
  }

  Future<int> deleteAllByID(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'ID', values);
  }

  int deleteAllByIDSync(List<int?> IDValues) {
    final values = IDValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'ID', values);
  }

  Future<Id> putByID(GeoFlightMissionIsar object) {
    return putByIndex(r'ID', object);
  }

  Id putByIDSync(GeoFlightMissionIsar object, {bool saveLinks = true}) {
    return putByIndexSync(r'ID', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByID(List<GeoFlightMissionIsar> objects) {
    return putAllByIndex(r'ID', objects);
  }

  List<Id> putAllByIDSync(List<GeoFlightMissionIsar> objects,
      {bool saveLinks = true}) {
    return putAllByIndexSync(r'ID', objects, saveLinks: saveLinks);
  }
}

extension GeoFlightMissionIsarQueryWhereSort
    on QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QWhere> {
  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'ID'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyObject() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'object'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyFlightMissionType() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'flightMissionType'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyRoute() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'route'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyOperator() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'operator'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyLatitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'latitude'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyLongitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'longitude'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyExecutionInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'executionInfo'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyUpdateInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'updateInfo'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyTechnicalSupport() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'technicalSupport'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyRouteLength() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'routeLength'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyRouteArea() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'routeArea'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyFlightGroupID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'flightGroupID'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyLandGroupID() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'landGroupID'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miOwner'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miCreateUser'),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhere>
      anyMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        const IndexWhereClause.any(indexName: r'miModifyUser'),
      );
    });
  }
}

extension GeoFlightMissionIsarQueryWhere
    on QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QWhereClause> {
  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      iDEqualTo(int? ID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'ID',
        value: [ID],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      iDNotEqualTo(int? ID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [ID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'ID',
              lower: [],
              upper: [ID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      iDGreaterThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [ID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      iDLessThan(
    int? ID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [],
        upper: [ID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      iDBetween(
    int? lowerID,
    int? upperID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'ID',
        lower: [lowerID],
        includeLower: includeLower,
        upper: [upperID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      numIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'num',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      numIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'num',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      numEqualTo(String? num) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'num',
        value: [num],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      numNotEqualTo(String? num) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'num',
              lower: [],
              upper: [num],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'num',
              lower: [num],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'num',
              lower: [num],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'num',
              lower: [],
              upper: [num],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      objectIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'object',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      objectIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'object',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      objectEqualTo(int? object) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'object',
        value: [object],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      objectNotEqualTo(int? object) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'object',
              lower: [],
              upper: [object],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'object',
              lower: [object],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'object',
              lower: [object],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'object',
              lower: [],
              upper: [object],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      objectGreaterThan(
    int? object, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'object',
        lower: [object],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      objectLessThan(
    int? object, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'object',
        lower: [],
        upper: [object],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      objectBetween(
    int? lowerObject,
    int? upperObject, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'object',
        lower: [lowerObject],
        includeLower: includeLower,
        upper: [upperObject],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightMissionTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'flightMissionType',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightMissionTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightMissionType',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightMissionTypeEqualTo(int? flightMissionType) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'flightMissionType',
        value: [flightMissionType],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightMissionTypeNotEqualTo(int? flightMissionType) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightMissionType',
              lower: [],
              upper: [flightMissionType],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightMissionType',
              lower: [flightMissionType],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightMissionType',
              lower: [flightMissionType],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightMissionType',
              lower: [],
              upper: [flightMissionType],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightMissionTypeGreaterThan(
    int? flightMissionType, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightMissionType',
        lower: [flightMissionType],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightMissionTypeLessThan(
    int? flightMissionType, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightMissionType',
        lower: [],
        upper: [flightMissionType],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightMissionTypeBetween(
    int? lowerFlightMissionType,
    int? upperFlightMissionType, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightMissionType',
        lower: [lowerFlightMissionType],
        includeLower: includeLower,
        upper: [upperFlightMissionType],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'route',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'route',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeEqualTo(int? route) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'route',
        value: [route],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeNotEqualTo(int? route) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'route',
              lower: [],
              upper: [route],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'route',
              lower: [route],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'route',
              lower: [route],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'route',
              lower: [],
              upper: [route],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeGreaterThan(
    int? route, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'route',
        lower: [route],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeLessThan(
    int? route, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'route',
        lower: [],
        upper: [route],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeBetween(
    int? lowerRoute,
    int? upperRoute, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'route',
        lower: [lowerRoute],
        includeLower: includeLower,
        upper: [upperRoute],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      operatorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'operator',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      operatorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operator',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      operatorEqualTo(int? operator) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'operator',
        value: [operator],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      operatorNotEqualTo(int? operator) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operator',
              lower: [],
              upper: [operator],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operator',
              lower: [operator],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operator',
              lower: [operator],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'operator',
              lower: [],
              upper: [operator],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      operatorGreaterThan(
    int? operator, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operator',
        lower: [operator],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      operatorLessThan(
    int? operator, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operator',
        lower: [],
        upper: [operator],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      operatorBetween(
    int? lowerOperator,
    int? upperOperator, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'operator',
        lower: [lowerOperator],
        includeLower: includeLower,
        upper: [upperOperator],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latitudeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'latitude',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latitudeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'latitude',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latitudeEqualTo(double? latitude) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'latitude',
        value: [latitude],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latitudeNotEqualTo(double? latitude) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latitude',
              lower: [],
              upper: [latitude],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latitude',
              lower: [latitude],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latitude',
              lower: [latitude],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latitude',
              lower: [],
              upper: [latitude],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latitudeGreaterThan(
    double? latitude, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'latitude',
        lower: [latitude],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latitudeLessThan(
    double? latitude, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'latitude',
        lower: [],
        upper: [latitude],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latitudeBetween(
    double? lowerLatitude,
    double? upperLatitude, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'latitude',
        lower: [lowerLatitude],
        includeLower: includeLower,
        upper: [upperLatitude],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      longitudeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'longitude',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      longitudeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'longitude',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      longitudeEqualTo(double? longitude) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'longitude',
        value: [longitude],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      longitudeNotEqualTo(double? longitude) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'longitude',
              lower: [],
              upper: [longitude],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'longitude',
              lower: [longitude],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'longitude',
              lower: [longitude],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'longitude',
              lower: [],
              upper: [longitude],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      longitudeGreaterThan(
    double? longitude, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'longitude',
        lower: [longitude],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      longitudeLessThan(
    double? longitude, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'longitude',
        lower: [],
        upper: [longitude],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      longitudeBetween(
    double? lowerLongitude,
    double? upperLongitude, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'longitude',
        lower: [lowerLongitude],
        includeLower: includeLower,
        upper: [upperLongitude],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latLngStartCombinedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'latLngStartCombined',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latLngStartCombinedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'latLngStartCombined',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latLngStartCombinedEqualTo(String? latLngStartCombined) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'latLngStartCombined',
        value: [latLngStartCombined],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      latLngStartCombinedNotEqualTo(String? latLngStartCombined) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latLngStartCombined',
              lower: [],
              upper: [latLngStartCombined],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latLngStartCombined',
              lower: [latLngStartCombined],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latLngStartCombined',
              lower: [latLngStartCombined],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'latLngStartCombined',
              lower: [],
              upper: [latLngStartCombined],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDateFromIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'examinationDateFrom',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDateFromIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'examinationDateFrom',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDateFromEqualTo(String? examinationDateFrom) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'examinationDateFrom',
        value: [examinationDateFrom],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDateFromNotEqualTo(String? examinationDateFrom) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDateFrom',
              lower: [],
              upper: [examinationDateFrom],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDateFrom',
              lower: [examinationDateFrom],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDateFrom',
              lower: [examinationDateFrom],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDateFrom',
              lower: [],
              upper: [examinationDateFrom],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDateToIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'examinationDateTo',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDateToIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'examinationDateTo',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDateToEqualTo(String? examinationDateTo) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'examinationDateTo',
        value: [examinationDateTo],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDateToNotEqualTo(String? examinationDateTo) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDateTo',
              lower: [],
              upper: [examinationDateTo],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDateTo',
              lower: [examinationDateTo],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDateTo',
              lower: [examinationDateTo],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDateTo',
              lower: [],
              upper: [examinationDateTo],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDatesCombinedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'examinationDatesCombined',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDatesCombinedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'examinationDatesCombined',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDatesCombinedEqualTo(String? examinationDatesCombined) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'examinationDatesCombined',
        value: [examinationDatesCombined],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      examinationDatesCombinedNotEqualTo(String? examinationDatesCombined) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDatesCombined',
              lower: [],
              upper: [examinationDatesCombined],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDatesCombined',
              lower: [examinationDatesCombined],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDatesCombined',
              lower: [examinationDatesCombined],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'examinationDatesCombined',
              lower: [],
              upper: [examinationDatesCombined],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      dateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'date',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      dateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'date',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      dateEqualTo(String? date) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'date',
        value: [date],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      dateNotEqualTo(String? date) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'date',
              lower: [],
              upper: [date],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'date',
              lower: [date],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'date',
              lower: [date],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'date',
              lower: [],
              upper: [date],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanStartDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'executionPlanStartDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanStartDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'executionPlanStartDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanStartDateEqualTo(String? executionPlanStartDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'executionPlanStartDate',
        value: [executionPlanStartDate],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanStartDateNotEqualTo(String? executionPlanStartDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanStartDate',
              lower: [],
              upper: [executionPlanStartDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanStartDate',
              lower: [executionPlanStartDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanStartDate',
              lower: [executionPlanStartDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanStartDate',
              lower: [],
              upper: [executionPlanStartDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanEndDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'executionPlanEndDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanEndDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'executionPlanEndDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanEndDateEqualTo(String? executionPlanEndDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'executionPlanEndDate',
        value: [executionPlanEndDate],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanEndDateNotEqualTo(String? executionPlanEndDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanEndDate',
              lower: [],
              upper: [executionPlanEndDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanEndDate',
              lower: [executionPlanEndDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanEndDate',
              lower: [executionPlanEndDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanEndDate',
              lower: [],
              upper: [executionPlanEndDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanDatesCombinedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'executionPlanDatesCombined',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanDatesCombinedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'executionPlanDatesCombined',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanDatesCombinedEqualTo(String? executionPlanDatesCombined) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'executionPlanDatesCombined',
        value: [executionPlanDatesCombined],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionPlanDatesCombinedNotEqualTo(String? executionPlanDatesCombined) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanDatesCombined',
              lower: [],
              upper: [executionPlanDatesCombined],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanDatesCombined',
              lower: [executionPlanDatesCombined],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanDatesCombined',
              lower: [executionPlanDatesCombined],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionPlanDatesCombined',
              lower: [],
              upper: [executionPlanDatesCombined],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionDeadlineDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'executionDeadlineDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionDeadlineDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'executionDeadlineDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionDeadlineDateEqualTo(String? executionDeadlineDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'executionDeadlineDate',
        value: [executionDeadlineDate],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionDeadlineDateNotEqualTo(String? executionDeadlineDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionDeadlineDate',
              lower: [],
              upper: [executionDeadlineDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionDeadlineDate',
              lower: [executionDeadlineDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionDeadlineDate',
              lower: [executionDeadlineDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionDeadlineDate',
              lower: [],
              upper: [executionDeadlineDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      responsiblePersonIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'responsiblePerson',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      responsiblePersonIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'responsiblePerson',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      responsiblePersonEqualTo(String? responsiblePerson) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'responsiblePerson',
        value: [responsiblePerson],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      responsiblePersonNotEqualTo(String? responsiblePerson) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'responsiblePerson',
              lower: [],
              upper: [responsiblePerson],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'responsiblePerson',
              lower: [responsiblePerson],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'responsiblePerson',
              lower: [responsiblePerson],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'responsiblePerson',
              lower: [],
              upper: [responsiblePerson],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      geoJsonIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'geoJson',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      geoJsonIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'geoJson',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      geoJsonEqualTo(String? geoJson) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'geoJson',
        value: [geoJson],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      geoJsonNotEqualTo(String? geoJson) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [],
              upper: [geoJson],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [geoJson],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [geoJson],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'geoJson',
              lower: [],
              upper: [geoJson],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionInfoIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'executionInfo',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionInfoIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'executionInfo',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionInfoEqualTo(int? executionInfo) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'executionInfo',
        value: [executionInfo],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionInfoNotEqualTo(int? executionInfo) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionInfo',
              lower: [],
              upper: [executionInfo],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionInfo',
              lower: [executionInfo],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionInfo',
              lower: [executionInfo],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'executionInfo',
              lower: [],
              upper: [executionInfo],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionInfoGreaterThan(
    int? executionInfo, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'executionInfo',
        lower: [executionInfo],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionInfoLessThan(
    int? executionInfo, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'executionInfo',
        lower: [],
        upper: [executionInfo],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      executionInfoBetween(
    int? lowerExecutionInfo,
    int? upperExecutionInfo, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'executionInfo',
        lower: [lowerExecutionInfo],
        includeLower: includeLower,
        upper: [upperExecutionInfo],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      updateInfoIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'updateInfo',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      updateInfoIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'updateInfo',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      updateInfoEqualTo(int? updateInfo) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'updateInfo',
        value: [updateInfo],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      updateInfoNotEqualTo(int? updateInfo) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'updateInfo',
              lower: [],
              upper: [updateInfo],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'updateInfo',
              lower: [updateInfo],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'updateInfo',
              lower: [updateInfo],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'updateInfo',
              lower: [],
              upper: [updateInfo],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      updateInfoGreaterThan(
    int? updateInfo, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'updateInfo',
        lower: [updateInfo],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      updateInfoLessThan(
    int? updateInfo, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'updateInfo',
        lower: [],
        upper: [updateInfo],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      updateInfoBetween(
    int? lowerUpdateInfo,
    int? upperUpdateInfo, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'updateInfo',
        lower: [lowerUpdateInfo],
        includeLower: includeLower,
        upper: [upperUpdateInfo],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      technicalSupportIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'technicalSupport',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      technicalSupportIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'technicalSupport',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      technicalSupportEqualTo(int? technicalSupport) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'technicalSupport',
        value: [technicalSupport],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      technicalSupportNotEqualTo(int? technicalSupport) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'technicalSupport',
              lower: [],
              upper: [technicalSupport],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'technicalSupport',
              lower: [technicalSupport],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'technicalSupport',
              lower: [technicalSupport],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'technicalSupport',
              lower: [],
              upper: [technicalSupport],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      technicalSupportGreaterThan(
    int? technicalSupport, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'technicalSupport',
        lower: [technicalSupport],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      technicalSupportLessThan(
    int? technicalSupport, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'technicalSupport',
        lower: [],
        upper: [technicalSupport],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      technicalSupportBetween(
    int? lowerTechnicalSupport,
    int? upperTechnicalSupport, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'technicalSupport',
        lower: [lowerTechnicalSupport],
        includeLower: includeLower,
        upper: [upperTechnicalSupport],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      meteorologicalDataIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'meteorologicalData',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      meteorologicalDataIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'meteorologicalData',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      meteorologicalDataEqualTo(String? meteorologicalData) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'meteorologicalData',
        value: [meteorologicalData],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      meteorologicalDataNotEqualTo(String? meteorologicalData) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'meteorologicalData',
              lower: [],
              upper: [meteorologicalData],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'meteorologicalData',
              lower: [meteorologicalData],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'meteorologicalData',
              lower: [meteorologicalData],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'meteorologicalData',
              lower: [],
              upper: [meteorologicalData],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      nearestLocalityIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'nearestLocality',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      nearestLocalityIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'nearestLocality',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      nearestLocalityEqualTo(String? nearestLocality) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'nearestLocality',
        value: [nearestLocality],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      nearestLocalityNotEqualTo(String? nearestLocality) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'nearestLocality',
              lower: [],
              upper: [nearestLocality],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'nearestLocality',
              lower: [nearestLocality],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'nearestLocality',
              lower: [nearestLocality],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'nearestLocality',
              lower: [],
              upper: [nearestLocality],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightDurationIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'flightDuration',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightDurationIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightDuration',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightDurationEqualTo(String? flightDuration) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'flightDuration',
        value: [flightDuration],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightDurationNotEqualTo(String? flightDuration) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightDuration',
              lower: [],
              upper: [flightDuration],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightDuration',
              lower: [flightDuration],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightDuration',
              lower: [flightDuration],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightDuration',
              lower: [],
              upper: [flightDuration],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeLengthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'routeLength',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeLengthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'routeLength',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeLengthEqualTo(double? routeLength) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'routeLength',
        value: [routeLength],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeLengthNotEqualTo(double? routeLength) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [],
              upper: [routeLength],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [routeLength],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [routeLength],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeLength',
              lower: [],
              upper: [routeLength],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeLengthGreaterThan(
    double? routeLength, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'routeLength',
        lower: [routeLength],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeLengthLessThan(
    double? routeLength, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'routeLength',
        lower: [],
        upper: [routeLength],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeLengthBetween(
    double? lowerRouteLength,
    double? upperRouteLength, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'routeLength',
        lower: [lowerRouteLength],
        includeLower: includeLower,
        upper: [upperRouteLength],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeAreaIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'routeArea',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeAreaIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'routeArea',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeAreaEqualTo(int? routeArea) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'routeArea',
        value: [routeArea],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeAreaNotEqualTo(int? routeArea) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeArea',
              lower: [],
              upper: [routeArea],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeArea',
              lower: [routeArea],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeArea',
              lower: [routeArea],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'routeArea',
              lower: [],
              upper: [routeArea],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeAreaGreaterThan(
    int? routeArea, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'routeArea',
        lower: [routeArea],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeAreaLessThan(
    int? routeArea, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'routeArea',
        lower: [],
        upper: [routeArea],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      routeAreaBetween(
    int? lowerRouteArea,
    int? upperRouteArea, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'routeArea',
        lower: [lowerRouteArea],
        includeLower: includeLower,
        upper: [upperRouteArea],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      stateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'state',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      stateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'state',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      stateEqualTo(String? state) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'state',
        value: [state],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      stateNotEqualTo(String? state) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'state',
              lower: [],
              upper: [state],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'state',
              lower: [state],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'state',
              lower: [state],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'state',
              lower: [],
              upper: [state],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      eventsStateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'eventsState',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      eventsStateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'eventsState',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      eventsStateEqualTo(String? eventsState) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'eventsState',
        value: [eventsState],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      eventsStateNotEqualTo(String? eventsState) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventsState',
              lower: [],
              upper: [eventsState],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventsState',
              lower: [eventsState],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventsState',
              lower: [eventsState],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'eventsState',
              lower: [],
              upper: [eventsState],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightGroupIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'flightGroupID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightGroupIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightGroupID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightGroupIDEqualTo(int? flightGroupID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'flightGroupID',
        value: [flightGroupID],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightGroupIDNotEqualTo(int? flightGroupID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightGroupID',
              lower: [],
              upper: [flightGroupID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightGroupID',
              lower: [flightGroupID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightGroupID',
              lower: [flightGroupID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'flightGroupID',
              lower: [],
              upper: [flightGroupID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightGroupIDGreaterThan(
    int? flightGroupID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightGroupID',
        lower: [flightGroupID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightGroupIDLessThan(
    int? flightGroupID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightGroupID',
        lower: [],
        upper: [flightGroupID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      flightGroupIDBetween(
    int? lowerFlightGroupID,
    int? upperFlightGroupID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'flightGroupID',
        lower: [lowerFlightGroupID],
        includeLower: includeLower,
        upper: [upperFlightGroupID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      landGroupIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'landGroupID',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      landGroupIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'landGroupID',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      landGroupIDEqualTo(int? landGroupID) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'landGroupID',
        value: [landGroupID],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      landGroupIDNotEqualTo(int? landGroupID) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'landGroupID',
              lower: [],
              upper: [landGroupID],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'landGroupID',
              lower: [landGroupID],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'landGroupID',
              lower: [landGroupID],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'landGroupID',
              lower: [],
              upper: [landGroupID],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      landGroupIDGreaterThan(
    int? landGroupID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'landGroupID',
        lower: [landGroupID],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      landGroupIDLessThan(
    int? landGroupID, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'landGroupID',
        lower: [],
        upper: [landGroupID],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      landGroupIDBetween(
    int? lowerLandGroupID,
    int? upperLandGroupID, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'landGroupID',
        lower: [lowerLandGroupID],
        includeLower: includeLower,
        upper: [upperLandGroupID],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miOwnerEqualTo(int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miOwner',
        value: [miOwner],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miOwnerNotEqualTo(int? miOwner) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [miOwner],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miOwner',
              lower: [],
              upper: [miOwner],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miOwnerGreaterThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [miOwner],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miOwnerLessThan(
    int? miOwner, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [],
        upper: [miOwner],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miOwnerBetween(
    int? lowerMiOwner,
    int? upperMiOwner, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miOwner',
        lower: [lowerMiOwner],
        includeLower: includeLower,
        upper: [upperMiOwner],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateDateEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateDate',
        value: [miCreateDate],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateDateNotEqualTo(String? miCreateDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [miCreateDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateDate',
              lower: [],
              upper: [miCreateDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateUserEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miCreateUser',
        value: [miCreateUser],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateUserNotEqualTo(int? miCreateUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [miCreateUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miCreateUser',
              lower: [],
              upper: [miCreateUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateUserGreaterThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [miCreateUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateUserLessThan(
    int? miCreateUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [],
        upper: [miCreateUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miCreateUserBetween(
    int? lowerMiCreateUser,
    int? upperMiCreateUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miCreateUser',
        lower: [lowerMiCreateUser],
        includeLower: includeLower,
        upper: [upperMiCreateUser],
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyDate',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyDateEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyDate',
        value: [miModifyDate],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyDateNotEqualTo(String? miModifyDate) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [miModifyDate],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyDate',
              lower: [],
              upper: [miModifyDate],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [null],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyUserEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'miModifyUser',
        value: [miModifyUser],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyUserNotEqualTo(int? miModifyUser) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [miModifyUser],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'miModifyUser',
              lower: [],
              upper: [miModifyUser],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyUserGreaterThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [miModifyUser],
        includeLower: include,
        upper: [],
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyUserLessThan(
    int? miModifyUser, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [],
        upper: [miModifyUser],
        includeUpper: include,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterWhereClause>
      miModifyUserBetween(
    int? lowerMiModifyUser,
    int? upperMiModifyUser, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'miModifyUser',
        lower: [lowerMiModifyUser],
        includeLower: includeLower,
        upper: [upperMiModifyUser],
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GeoFlightMissionIsarQueryFilter on QueryBuilder<GeoFlightMissionIsar,
    GeoFlightMissionIsar, QFilterCondition> {
  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> iDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> iDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'ID',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> iDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> iDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> iDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'ID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> iDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'ID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> dateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'date',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> dateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'date',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> dateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'date',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> dateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'date',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> dateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'date',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> dateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'date',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> dateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'date',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> dateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'date',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      dateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'date',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      dateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'date',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> dateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'date',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> dateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'date',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> eventsStateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'eventsState',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> eventsStateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'eventsState',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> eventsStateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'eventsState',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> eventsStateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'eventsState',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> eventsStateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'eventsState',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> eventsStateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'eventsState',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> eventsStateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'eventsState',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> eventsStateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'eventsState',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      eventsStateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'eventsState',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      eventsStateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'eventsState',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> eventsStateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'eventsState',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> eventsStateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'eventsState',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateFromIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'examinationDateFrom',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateFromIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'examinationDateFrom',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateFromEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'examinationDateFrom',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateFromGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'examinationDateFrom',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateFromLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'examinationDateFrom',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateFromBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'examinationDateFrom',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateFromStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'examinationDateFrom',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateFromEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'examinationDateFrom',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      examinationDateFromContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'examinationDateFrom',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      examinationDateFromMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'examinationDateFrom',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateFromIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'examinationDateFrom',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateFromIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'examinationDateFrom',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateToIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'examinationDateTo',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateToIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'examinationDateTo',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateToEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'examinationDateTo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateToGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'examinationDateTo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateToLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'examinationDateTo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateToBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'examinationDateTo',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateToStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'examinationDateTo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateToEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'examinationDateTo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      examinationDateToContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'examinationDateTo',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      examinationDateToMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'examinationDateTo',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateToIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'examinationDateTo',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDateToIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'examinationDateTo',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDatesCombinedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'examinationDatesCombined',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDatesCombinedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'examinationDatesCombined',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDatesCombinedEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'examinationDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDatesCombinedGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'examinationDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDatesCombinedLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'examinationDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDatesCombinedBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'examinationDatesCombined',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDatesCombinedStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'examinationDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDatesCombinedEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'examinationDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      examinationDatesCombinedContains(String value,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'examinationDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      examinationDatesCombinedMatches(String pattern,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'examinationDatesCombined',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDatesCombinedIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'examinationDatesCombined',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> examinationDatesCombinedIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'examinationDatesCombined',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionDeadlineDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'executionDeadlineDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionDeadlineDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'executionDeadlineDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionDeadlineDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'executionDeadlineDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionDeadlineDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'executionDeadlineDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionDeadlineDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'executionDeadlineDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionDeadlineDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'executionDeadlineDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionDeadlineDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'executionDeadlineDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionDeadlineDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'executionDeadlineDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      executionDeadlineDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'executionDeadlineDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      executionDeadlineDateMatches(String pattern,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'executionDeadlineDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionDeadlineDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'executionDeadlineDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionDeadlineDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'executionDeadlineDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionInfoIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'executionInfo',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionInfoIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'executionInfo',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionInfoEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'executionInfo',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionInfoGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'executionInfo',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionInfoLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'executionInfo',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionInfoBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'executionInfo',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanDatesCombinedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'executionPlanDatesCombined',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanDatesCombinedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'executionPlanDatesCombined',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanDatesCombinedEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'executionPlanDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanDatesCombinedGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'executionPlanDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanDatesCombinedLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'executionPlanDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanDatesCombinedBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'executionPlanDatesCombined',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanDatesCombinedStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'executionPlanDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanDatesCombinedEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'executionPlanDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      executionPlanDatesCombinedContains(String value,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'executionPlanDatesCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      executionPlanDatesCombinedMatches(String pattern,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'executionPlanDatesCombined',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanDatesCombinedIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'executionPlanDatesCombined',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanDatesCombinedIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'executionPlanDatesCombined',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanEndDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'executionPlanEndDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanEndDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'executionPlanEndDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanEndDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'executionPlanEndDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanEndDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'executionPlanEndDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanEndDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'executionPlanEndDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanEndDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'executionPlanEndDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanEndDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'executionPlanEndDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanEndDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'executionPlanEndDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      executionPlanEndDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'executionPlanEndDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      executionPlanEndDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'executionPlanEndDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanEndDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'executionPlanEndDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanEndDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'executionPlanEndDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanStartDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'executionPlanStartDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanStartDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'executionPlanStartDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanStartDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'executionPlanStartDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanStartDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'executionPlanStartDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanStartDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'executionPlanStartDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanStartDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'executionPlanStartDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanStartDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'executionPlanStartDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanStartDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'executionPlanStartDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      executionPlanStartDateContains(String value,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'executionPlanStartDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      executionPlanStartDateMatches(String pattern,
          {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'executionPlanStartDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanStartDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'executionPlanStartDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> executionPlanStartDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'executionPlanStartDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightDurationIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'flightDuration',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightDurationIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'flightDuration',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightDurationEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'flightDuration',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightDurationGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'flightDuration',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightDurationLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'flightDuration',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightDurationBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'flightDuration',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightDurationStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'flightDuration',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightDurationEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'flightDuration',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      flightDurationContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'flightDuration',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      flightDurationMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'flightDuration',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightDurationIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'flightDuration',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightDurationIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'flightDuration',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightGroupIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'flightGroupID',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightGroupIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'flightGroupID',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightGroupIDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'flightGroupID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightGroupIDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'flightGroupID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightGroupIDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'flightGroupID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightGroupIDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'flightGroupID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMissionTypeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'flightMissionType',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMissionTypeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'flightMissionType',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMissionTypeEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'flightMissionType',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMissionTypeGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'flightMissionType',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMissionTypeLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'flightMissionType',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMissionTypeBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'flightMissionType',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> geoJsonIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'geoJson',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> geoJsonIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'geoJson',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> geoJsonEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> geoJsonGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> geoJsonLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> geoJsonBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'geoJson',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> geoJsonStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> geoJsonEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      geoJsonContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'geoJson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      geoJsonMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'geoJson',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> geoJsonIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'geoJson',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> geoJsonIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'geoJson',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> landGroupIDIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'landGroupID',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> landGroupIDIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'landGroupID',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> landGroupIDEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'landGroupID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> landGroupIDGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'landGroupID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> landGroupIDLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'landGroupID',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> landGroupIDBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'landGroupID',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latLngStartCombinedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'latLngStartCombined',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latLngStartCombinedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'latLngStartCombined',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latLngStartCombinedEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'latLngStartCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latLngStartCombinedGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'latLngStartCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latLngStartCombinedLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'latLngStartCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latLngStartCombinedBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'latLngStartCombined',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latLngStartCombinedStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'latLngStartCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latLngStartCombinedEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'latLngStartCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      latLngStartCombinedContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'latLngStartCombined',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      latLngStartCombinedMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'latLngStartCombined',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latLngStartCombinedIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'latLngStartCombined',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latLngStartCombinedIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'latLngStartCombined',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latitudeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'latitude',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latitudeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'latitude',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latitudeEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'latitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latitudeGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'latitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latitudeLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'latitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> latitudeBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'latitude',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> longitudeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'longitude',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> longitudeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'longitude',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> longitudeEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'longitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> longitudeGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'longitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> longitudeLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'longitude',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> longitudeBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'longitude',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> meteorologicalDataIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'meteorologicalData',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> meteorologicalDataIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'meteorologicalData',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> meteorologicalDataEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'meteorologicalData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> meteorologicalDataGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'meteorologicalData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> meteorologicalDataLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'meteorologicalData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> meteorologicalDataBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'meteorologicalData',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> meteorologicalDataStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'meteorologicalData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> meteorologicalDataEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'meteorologicalData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      meteorologicalDataContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'meteorologicalData',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      meteorologicalDataMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'meteorologicalData',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> meteorologicalDataIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'meteorologicalData',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> meteorologicalDataIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'meteorologicalData',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      miCreateDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miCreateDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      miCreateDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miCreateDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miCreateDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miCreateUser',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miCreateUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miCreateUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miCreateUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyDateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyDateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyDate',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyDateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyDateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyDateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyDateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyDate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyDateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyDateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      miModifyDateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'miModifyDate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      miModifyDateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'miModifyDate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyDateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyDateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'miModifyDate',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyUserIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyUserIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miModifyUser',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyUserEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyUserGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyUserLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miModifyUser',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miModifyUserBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miModifyUser',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miOwnerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miOwnerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'miOwner',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miOwnerEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miOwnerGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miOwnerLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'miOwner',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> miOwnerBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'miOwner',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> nearestLocalityIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'nearestLocality',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> nearestLocalityIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'nearestLocality',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> nearestLocalityEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nearestLocality',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> nearestLocalityGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'nearestLocality',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> nearestLocalityLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'nearestLocality',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> nearestLocalityBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'nearestLocality',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> nearestLocalityStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'nearestLocality',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> nearestLocalityEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'nearestLocality',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      nearestLocalityContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'nearestLocality',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      nearestLocalityMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'nearestLocality',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> nearestLocalityIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'nearestLocality',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> nearestLocalityIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'nearestLocality',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> numIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'num',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> numIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'num',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> numEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> numGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> numLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> numBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'num',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> numStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> numEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      numContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'num',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      numMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'num',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> numIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'num',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> numIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'num',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> objectIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'object',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> objectIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'object',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> objectEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'object',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> objectGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'object',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> objectLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'object',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> objectBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'object',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> operatorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'operator',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> operatorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'operator',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> operatorEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'operator',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> operatorGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'operator',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> operatorLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'operator',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> operatorBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'operator',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> responsiblePersonIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'responsiblePerson',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> responsiblePersonIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'responsiblePerson',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> responsiblePersonEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'responsiblePerson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> responsiblePersonGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'responsiblePerson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> responsiblePersonLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'responsiblePerson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> responsiblePersonBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'responsiblePerson',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> responsiblePersonStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'responsiblePerson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> responsiblePersonEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'responsiblePerson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      responsiblePersonContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'responsiblePerson',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      responsiblePersonMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'responsiblePerson',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> responsiblePersonIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'responsiblePerson',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> responsiblePersonIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'responsiblePerson',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'route',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'route',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'route',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'route',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'route',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'route',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeAreaIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'routeArea',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeAreaIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'routeArea',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeAreaEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'routeArea',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeAreaGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'routeArea',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeAreaLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'routeArea',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeAreaBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'routeArea',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeLengthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'routeLength',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeLengthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'routeLength',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeLengthEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'routeLength',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeLengthGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'routeLength',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeLengthLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'routeLength',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> routeLengthBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'routeLength',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> stateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'state',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> stateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'state',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> stateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> stateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> stateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> stateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'state',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> stateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> stateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      stateContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'state',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      stateMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'state',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> stateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'state',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> stateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'state',
        value: '',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> technicalSupportIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'technicalSupport',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> technicalSupportIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'technicalSupport',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> technicalSupportEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'technicalSupport',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> technicalSupportGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'technicalSupport',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> technicalSupportLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'technicalSupport',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> technicalSupportBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'technicalSupport',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> updateInfoIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'updateInfo',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> updateInfoIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'updateInfo',
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> updateInfoEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'updateInfo',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> updateInfoGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'updateInfo',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> updateInfoLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'updateInfo',
        value: value,
      ));
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> updateInfoBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'updateInfo',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GeoFlightMissionIsarQueryObject on QueryBuilder<GeoFlightMissionIsar,
    GeoFlightMissionIsar, QFilterCondition> {}

extension GeoFlightMissionIsarQueryLinks on QueryBuilder<GeoFlightMissionIsar,
    GeoFlightMissionIsar, QFilterCondition> {
  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightEvents(FilterQuery<GeoEventIsar> q) {
    return QueryBuilder.apply(this, (query) {
      return query.link(q, r'flightEvents');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightEventsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'flightEvents', length, true, length, true);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightEventsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'flightEvents', 0, true, 0, true);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightEventsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'flightEvents', 0, false, 999999, true);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightEventsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'flightEvents', 0, true, length, include);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightEventsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'flightEvents', length, include, 999999, true);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightEventsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(
          r'flightEvents', lower, includeLower, upper, includeUpper);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
          QAfterFilterCondition>
      flightMaterials(FilterQuery<FlightMissionMaterialIsar> q) {
    return QueryBuilder.apply(this, (query) {
      return query.link(q, r'flightMaterials');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMaterialsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'flightMaterials', length, true, length, true);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMaterialsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'flightMaterials', 0, true, 0, true);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMaterialsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'flightMaterials', 0, false, 999999, true);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMaterialsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'flightMaterials', 0, true, length, include);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMaterialsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(
          r'flightMaterials', length, include, 999999, true);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar,
      QAfterFilterCondition> flightMaterialsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(
          r'flightMaterials', lower, includeLower, upper, includeUpper);
    });
  }
}

extension GeoFlightMissionIsarQuerySortBy
    on QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QSortBy> {
  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'date', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'date', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByEventsState() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventsState', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByEventsStateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventsState', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExaminationDateFrom() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDateFrom', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExaminationDateFromDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDateFrom', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExaminationDateTo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDateTo', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExaminationDateToDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDateTo', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExaminationDatesCombined() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDatesCombined', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExaminationDatesCombinedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDatesCombined', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExecutionDeadlineDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionDeadlineDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExecutionDeadlineDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionDeadlineDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExecutionInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionInfo', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExecutionInfoDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionInfo', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExecutionPlanDatesCombined() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanDatesCombined', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExecutionPlanDatesCombinedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanDatesCombined', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExecutionPlanEndDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanEndDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExecutionPlanEndDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanEndDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExecutionPlanStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanStartDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByExecutionPlanStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanStartDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByFlightDuration() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightDuration', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByFlightDurationDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightDuration', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByFlightGroupID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightGroupID', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByFlightGroupIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightGroupID', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByFlightMissionType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightMissionType', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByFlightMissionTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightMissionType', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByGeoJson() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByGeoJsonDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByLandGroupID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'landGroupID', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByLandGroupIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'landGroupID', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByLatLngStartCombined() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latLngStartCombined', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByLatLngStartCombinedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latLngStartCombined', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByLatitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latitude', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByLatitudeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latitude', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByLongitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'longitude', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByLongitudeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'longitude', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMeteorologicalData() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'meteorologicalData', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMeteorologicalDataDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'meteorologicalData', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByNearestLocality() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nearestLocality', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByNearestLocalityDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nearestLocality', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByNum() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'num', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByNumDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'num', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByObject() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'object', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByObjectDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'object', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByOperator() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operator', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByOperatorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operator', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByResponsiblePerson() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'responsiblePerson', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByResponsiblePersonDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'responsiblePerson', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByRoute() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'route', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByRouteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'route', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByRouteArea() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeArea', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByRouteAreaDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeArea', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByRouteLength() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByRouteLengthDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByState() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'state', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByStateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'state', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByTechnicalSupport() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'technicalSupport', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByTechnicalSupportDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'technicalSupport', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByUpdateInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'updateInfo', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      sortByUpdateInfoDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'updateInfo', Sort.desc);
    });
  }
}

extension GeoFlightMissionIsarQuerySortThenBy
    on QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QSortThenBy> {
  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'ID', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'date', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'date', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByEventsState() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventsState', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByEventsStateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'eventsState', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExaminationDateFrom() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDateFrom', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExaminationDateFromDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDateFrom', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExaminationDateTo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDateTo', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExaminationDateToDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDateTo', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExaminationDatesCombined() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDatesCombined', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExaminationDatesCombinedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'examinationDatesCombined', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExecutionDeadlineDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionDeadlineDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExecutionDeadlineDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionDeadlineDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExecutionInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionInfo', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExecutionInfoDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionInfo', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExecutionPlanDatesCombined() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanDatesCombined', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExecutionPlanDatesCombinedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanDatesCombined', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExecutionPlanEndDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanEndDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExecutionPlanEndDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanEndDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExecutionPlanStartDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanStartDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByExecutionPlanStartDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'executionPlanStartDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByFlightDuration() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightDuration', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByFlightDurationDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightDuration', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByFlightGroupID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightGroupID', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByFlightGroupIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightGroupID', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByFlightMissionType() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightMissionType', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByFlightMissionTypeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'flightMissionType', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByGeoJson() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByGeoJsonDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'geoJson', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByLandGroupID() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'landGroupID', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByLandGroupIDDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'landGroupID', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByLatLngStartCombined() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latLngStartCombined', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByLatLngStartCombinedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latLngStartCombined', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByLatitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latitude', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByLatitudeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'latitude', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByLongitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'longitude', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByLongitudeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'longitude', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMeteorologicalData() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'meteorologicalData', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMeteorologicalDataDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'meteorologicalData', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMiCreateDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMiCreateDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMiCreateUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miCreateUser', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMiModifyDate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMiModifyDateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyDate', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMiModifyUserDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miModifyUser', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByMiOwnerDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'miOwner', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByNearestLocality() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nearestLocality', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByNearestLocalityDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'nearestLocality', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByNum() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'num', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByNumDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'num', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByObject() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'object', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByObjectDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'object', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByOperator() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operator', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByOperatorDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'operator', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByResponsiblePerson() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'responsiblePerson', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByResponsiblePersonDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'responsiblePerson', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByRoute() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'route', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByRouteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'route', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByRouteArea() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeArea', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByRouteAreaDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeArea', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByRouteLength() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByRouteLengthDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'routeLength', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByState() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'state', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByStateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'state', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByTechnicalSupport() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'technicalSupport', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByTechnicalSupportDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'technicalSupport', Sort.desc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByUpdateInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'updateInfo', Sort.asc);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QAfterSortBy>
      thenByUpdateInfoDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'updateInfo', Sort.desc);
    });
  }
}

extension GeoFlightMissionIsarQueryWhereDistinct
    on QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct> {
  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'ID');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'date', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByEventsState({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'eventsState', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByExaminationDateFrom({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'examinationDateFrom',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByExaminationDateTo({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'examinationDateTo',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByExaminationDatesCombined({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'examinationDatesCombined',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByExecutionDeadlineDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'executionDeadlineDate',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByExecutionInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'executionInfo');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByExecutionPlanDatesCombined({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'executionPlanDatesCombined',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByExecutionPlanEndDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'executionPlanEndDate',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByExecutionPlanStartDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'executionPlanStartDate',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByFlightDuration({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'flightDuration',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByFlightGroupID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'flightGroupID');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByFlightMissionType() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'flightMissionType');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByGeoJson({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'geoJson', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByLandGroupID() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'landGroupID');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByLatLngStartCombined({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'latLngStartCombined',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByLatitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'latitude');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByLongitude() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'longitude');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByMeteorologicalData({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'meteorologicalData',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByMiCreateDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByMiCreateUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miCreateUser');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByMiModifyDate({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyDate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByMiModifyUser() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miModifyUser');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByMiOwner() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'miOwner');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByNearestLocality({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'nearestLocality',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByNum({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'num', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByObject() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'object');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByOperator() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'operator');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByResponsiblePerson({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'responsiblePerson',
          caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByRoute() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'route');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByRouteArea() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'routeArea');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByRouteLength() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'routeLength');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByState({bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'state', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByTechnicalSupport() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'technicalSupport');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, GeoFlightMissionIsar, QDistinct>
      distinctByUpdateInfo() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'updateInfo');
    });
  }
}

extension GeoFlightMissionIsarQueryProperty on QueryBuilder<
    GeoFlightMissionIsar, GeoFlightMissionIsar, QQueryProperty> {
  QueryBuilder<GeoFlightMissionIsar, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations> IDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'ID');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations> dateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'date');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      eventsStateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'eventsState');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      examinationDateFromProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'examinationDateFrom');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      examinationDateToProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'examinationDateTo');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      examinationDatesCombinedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'examinationDatesCombined');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      executionDeadlineDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'executionDeadlineDate');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations>
      executionInfoProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'executionInfo');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      executionPlanDatesCombinedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'executionPlanDatesCombined');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      executionPlanEndDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'executionPlanEndDate');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      executionPlanStartDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'executionPlanStartDate');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      flightDurationProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'flightDuration');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations>
      flightGroupIDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'flightGroupID');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations>
      flightMissionTypeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'flightMissionType');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      geoJsonProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'geoJson');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations>
      landGroupIDProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'landGroupID');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      latLngStartCombinedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'latLngStartCombined');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, double?, QQueryOperations>
      latitudeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'latitude');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, double?, QQueryOperations>
      longitudeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'longitude');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      meteorologicalDataProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'meteorologicalData');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      miCreateDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateDate');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations>
      miCreateUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miCreateUser');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      miModifyDateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyDate');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations>
      miModifyUserProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miModifyUser');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations> miOwnerProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'miOwner');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      nearestLocalityProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'nearestLocality');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations> numProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'num');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations> objectProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'object');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations>
      operatorProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'operator');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      responsiblePersonProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'responsiblePerson');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations> routeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'route');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations>
      routeAreaProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'routeArea');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, double?, QQueryOperations>
      routeLengthProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'routeLength');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, String?, QQueryOperations>
      stateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'state');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations>
      technicalSupportProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'technicalSupport');
    });
  }

  QueryBuilder<GeoFlightMissionIsar, int?, QQueryOperations>
      updateInfoProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'updateInfo');
    });
  }
}
