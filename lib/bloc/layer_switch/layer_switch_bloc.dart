import 'package:flutter_bloc/flutter_bloc.dart';

enum Layer { normal, satellite, terrain }

class LayerBloc extends Cubit<Layer> {
  LayerBloc() : super(Layer.normal);

  void switchLayer(Layer layer) {
    emit(layer);
  }
}