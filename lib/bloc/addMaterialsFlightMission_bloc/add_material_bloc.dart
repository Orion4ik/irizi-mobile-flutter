



import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:irizi/bloc/addMaterialsFlightMission_bloc/add_material_event.dart';
import 'package:irizi/bloc/addMaterialsFlightMission_bloc/add_material_state.dart';

import '../../repo/isar_db/isar_repo.dart';

class GeoFlightMissionMaterialsBloc extends Bloc<AddMaterialEvent, GeoFlightMissionMaterialState> {
  final IsarRepository isarRepo;

  GeoFlightMissionMaterialsBloc( {required this.isarRepo}) : super(ItemEmpty()) {
    on<AddMaterial>((event, emit) async {
      
      try {
        
      } catch (e) {

      }

      emit(ItemSaveToDB());
    }
  );}

}