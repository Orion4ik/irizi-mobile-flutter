

import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:irizi/model/model_all.dart';


import '../../../repo/isar_db/isar_repo.dart';
import '../../../service/retrofit/api_service.dart';
import 'db_state.dart';
import 'db_event.dart';

class GeoFlightMissionBloc extends Bloc<GeoFlightMissionEvent, GeoFlightMissionState> {
  final IsarRepository isarRepo;

  GeoFlightMissionBloc( {required this.isarRepo}) : super(ItemLoading()) {
    on<GetItems>((event, emit) async {
      emit(ItemLoading());
      try {
      
      final apiService = RestClient(Dio(BaseOptions(contentType: "application/json")));
      // Fetch items from the API
      final apiItems = await apiService.getAll();
      
      final List<GeoFlightMission>? flightMissions = apiItems.geoFlightMission;
      final List<GeoEvent>? geoEvent = apiItems.geoEvent;
      final List<GeoEventType>? geoEventType = apiItems.geoEventType;
      final List<GeoObject>? geoObject = apiItems.geoObject ;
      final List<GeoFlightMissionType>? geoFlightMissionType = apiItems.geoFlightMissionType;
      final List<GeoRoute>? geoRoute = apiItems.geoRoute;
      final List<GeoOperator>? geoOperator = apiItems.geoOperator; 
      final List<GeoExecutionStatus>? geoExecutionStatus = apiItems.geoExecutionStatus;
      final List<GeoAdjustmentStatus>? geoAdjustmentStatus = apiItems.geoAdjustmentStatus;
      final List<DictTechnicalSupport>? dictTechnicalSupport = apiItems.dictTechnicalSupport;
      final List<GeoFlightGroup>? geoFlightGroup = apiItems.geoFlightGroup;
      final List<GeoLandGroup>? geoLandGroup = apiItems.geoLandGroup;
      final List<UbmEnum>? ubmEnum = apiItems.ubmEnum;
      final List<UbaUser>? ubaUser = apiItems.ubaUser;

 emit(ItemConvertation());
      //Convert JSON to API to List
      List<Map<String, dynamic>> flightMissionMaps = flightMissions!.map((flightMission) {
      return flightMission.toJson();
      }).toList();
       List<Map<String, dynamic>> geoEventMaps = geoEvent!.map((geoEvent) {
      return geoEvent.toJson();
      }).toList();

 List<Map<String, dynamic>> geoEventTypeMaps = geoEventType!.map((geoEventType) {
      return geoEventType.toJson();
      }).toList();

 List<Map<String, dynamic>> geoObjectMaps = geoObject!.map((geoObject) {
      return geoObject.toJson();
      }).toList();

 List<Map<String, dynamic>> geoFlightMissionTypeMaps = geoFlightMissionType!.map((geoFlightMissionType) {
      return geoFlightMissionType.toJson();
      }).toList();

 List<Map<String, dynamic>> geoRouteMaps = geoRoute!.map((geoRoute) {
      return geoRoute.toJson();
      }).toList();

 List<Map<String, dynamic>> geoOperatorMaps = geoOperator!.map((geoOperator) {
      return geoOperator.toJson();
      }).toList();

 List<Map<String, dynamic>> geoAdjustmentStatusMaps = geoAdjustmentStatus!.map((geoAdjustmentStatus) {
      return geoAdjustmentStatus.toJson();
      }).toList();

       List<Map<String, dynamic>> geoExecutionStatusMaps = geoExecutionStatus!.map((geoExecutionStatus) {
      return geoExecutionStatus.toJson();
      }).toList();

 List<Map<String, dynamic>> dictTechnicalSupportMaps = dictTechnicalSupport!.map((dictTechnicalSupport) {
      return dictTechnicalSupport.toJson();
      }).toList();

 List<Map<String, dynamic>> geoFlightGroupMaps = geoFlightGroup!.map((geoFlightGroup) {
      return geoFlightGroup.toJson();
      }).toList();

 List<Map<String, dynamic>> geoLandGroupMaps = geoLandGroup!.map((geoLandGroup) {
      return geoLandGroup.toJson();
      }).toList();

 List<Map<String, dynamic>> ubmEnumMaps = ubmEnum!.map((ubmEnum) {
      return ubmEnum.toJson();
      }).toList();

 List<Map<String, dynamic>> ubaUserMaps = ubaUser!.map((ubaUser) {
      return ubaUser.toJson();
      }).toList();

 


      
/*     
emit(ItemSync());
final flightMissionsCheckFromIsar = await isarRepo.getAllGeoFlightMissions ();
      for (final apiRecord in flightMissions) {
  final matchingRecord = flightMissionsCheckFromIsar.firstWhere(
    (isarRecord) => isarRecord. ID == apiRecord.ID,

  );

  if (matchingRecord != null) {
    // Update existing record in Isar Database with data from the API
    await isarRepo.updateGeoFlightMission(matchingRecord.ID!, apiRecord);
  } else {
    // Insert new record into Isar Database
    await isarRepo.addGeoFlightMissionsfromJson(flightMissionMaps);
     emit(ItemSaveToDB());
  }
}
*/

emit(ItemSaveToDB());
        // Save items to the Isar database
        isarRepo.addGeoFlightMissionsfromJson(flightMissionMaps);
        isarRepo.addgeoEventTypefromJson(geoEventTypeMaps);
        isarRepo.addgeoAdjustmentStatusfromJson(geoAdjustmentStatusMaps);
        isarRepo.addgeoExecutionStatusfromJson(geoExecutionStatusMaps);
        isarRepo.addgeoFlightGroupfromJson(geoFlightGroupMaps);
        isarRepo.addgeoObjectfromJson(geoObjectMaps);
        isarRepo.addgeoOperatorfromJson(geoOperatorMaps);
        isarRepo.addgeoRoutefromJson(geoRouteMaps);
        isarRepo.adddictTechnicalSupportfromJson(dictTechnicalSupportMaps);
        isarRepo.addgeoEventfromJson(geoEventMaps);
        isarRepo.addgeoFlightMissionTypefromJson(geoFlightMissionTypeMaps);
        isarRepo.addgeoLandGroupfromJson(geoLandGroupMaps);
        isarRepo.addubaUserfromJson(ubaUserMaps);
        isarRepo.addubmEnumfromJson(ubmEnumMaps);

        //load items from Isar database
        //final flightMissionIsar =  isarRepo.getAllGeoFlightMissions();
        emit(ItemConvertation());
        //Converting to Model objects
        final flightMissionConvert =  isarRepo.getGeoFlightMissionFromIsar();
        final geoEventTypeConvert =  isarRepo.getgeoEventTypeFromIsar();
        final geoAdjustmentStatusConvert =  isarRepo.getgeoAdjustmentStatusFromIsar();
        final geoExecutionStatusConvert =  isarRepo.getgeoExecutionStatusFromIsar();
        final geoFlightGroupConvert =  isarRepo.getgeoFlightGroupFromIsar();
        final geoObjectConvert =  isarRepo.getgeoObjectFromIsar();
        final geoOperatorConvert =  isarRepo.getgeoOperatorFromIsar();
        final geoRouteConvert =  isarRepo.getgeoRouteFromIsar();
        final dictTechnicalSupportConvert =  isarRepo.getdictTechnicalSupportFromIsar();
        final geoEventConvert =  isarRepo.getgeoEventFromIsar();
        final geoFlightMissionTypeConvert =  isarRepo.getgeoFlightMissionTypeFromIsar();
        final geoLandGroupConvert =  isarRepo.getgeoLandGroupFromIsar();
        final ubaUserConvert =  isarRepo.getubaUserFromIsar();
        final ubmEnumConvert =  isarRepo.getubmEnumFromIsar();



        
        
       List<GeoFlightMission> flightMissionsConverted = await flightMissionConvert;
       List<GeoEventType> geoEventTypeConverted = await geoEventTypeConvert;
       List<GeoAdjustmentStatus> geoAdjustmentStatusConverted = await geoAdjustmentStatusConvert;
       List<GeoExecutionStatus> geoExecutionStatusConverted = await geoExecutionStatusConvert;
       List<GeoFlightGroup> geoFlightGroupConverted = await geoFlightGroupConvert;
       List<GeoObject> geoObjectConverted = await geoObjectConvert;
       List<GeoOperator> geoOperatorConverted = await geoOperatorConvert;
       List<GeoRoute> geoRouteConverted = await geoRouteConvert;
       List<DictTechnicalSupport> dictTechnicalSupportConverted = await dictTechnicalSupportConvert;
       List<GeoEvent> geoEventConverted = await geoEventConvert;
       List<GeoFlightMissionType> geoFlightMissionTypeConverted = await geoFlightMissionTypeConvert;
       List<GeoLandGroup> geoLandGroupConverted = await geoLandGroupConvert;
       List<UbaUser> ubaUserConverted = await ubaUserConvert;
       List<UbmEnum> ubmEnumConverted = await ubmEnumConvert;
       // emit(ItemLoaded());
        emit(ItemLoaded(
          flightMissionsConverted, 
          geoEventTypeConverted, 
          geoAdjustmentStatusConverted, 
          geoExecutionStatusConverted, 
          geoFlightGroupConverted, 
          geoObjectConverted, 
          geoOperatorConverted, 
          geoRouteConverted,
          dictTechnicalSupportConverted,
          geoEventConverted,
          geoFlightMissionTypeConverted,
          geoLandGroupConverted,
          ubaUserConverted,
          ubmEnumConverted 
          ));
      } catch (e) {
        emit(ItemError(e.toString()));
      }
    });
    on<GetItemsFromDB> ((event, emit) async {
      emit(ItemLoading());
      try {
      
        emit(ItemConvertation());
        //load items from Isar database
        //Converting to Model objects
        final flightMissionConvert =  isarRepo.getGeoFlightMissionFromIsar();
        final geoEventTypeConvert =  isarRepo.getgeoEventTypeFromIsar();
        final geoAdjustmentStatusConvert =  isarRepo.getgeoAdjustmentStatusFromIsar();
        final geoExecutionStatusConvert =  isarRepo.getgeoExecutionStatusFromIsar();
        final geoFlightGroupConvert =  isarRepo.getgeoFlightGroupFromIsar();
        final geoObjectConvert =  isarRepo.getgeoObjectFromIsar();
        final geoOperatorConvert =  isarRepo.getgeoOperatorFromIsar();
        final geoRouteConvert =  isarRepo.getgeoRouteFromIsar();
        final dictTechnicalSupportConvert =  isarRepo.getdictTechnicalSupportFromIsar();
        final geoEventConvert =  isarRepo.getgeoEventFromIsar();
        final geoFlightMissionTypeConvert =  isarRepo.getgeoFlightMissionTypeFromIsar();
        final geoLandGroupConvert =  isarRepo.getgeoLandGroupFromIsar();
        final ubaUserConvert =  isarRepo.getubaUserFromIsar();
        final ubmEnumConvert =  isarRepo.getubmEnumFromIsar();



        
        
       List<GeoFlightMission> flightMissionsConverted = await flightMissionConvert;
       List<GeoEventType> geoEventTypeConverted = await geoEventTypeConvert;
       List<GeoAdjustmentStatus> geoAdjustmentStatusConverted = await geoAdjustmentStatusConvert;
       List<GeoExecutionStatus> geoExecutionStatusConverted = await geoExecutionStatusConvert;
       List<GeoFlightGroup> geoFlightGroupConverted = await geoFlightGroupConvert;
       List<GeoObject> geoObjectConverted = await geoObjectConvert;
       List<GeoOperator> geoOperatorConverted = await geoOperatorConvert;
       List<GeoRoute> geoRouteConverted = await geoRouteConvert;
       List<DictTechnicalSupport> dictTechnicalSupportConverted = await dictTechnicalSupportConvert;
       List<GeoEvent> geoEventConverted = await geoEventConvert;
       List<GeoFlightMissionType> geoFlightMissionTypeConverted = await geoFlightMissionTypeConvert;
       List<GeoLandGroup> geoLandGroupConverted = await geoLandGroupConvert;
       List<UbaUser> ubaUserConverted = await ubaUserConvert;
       List<UbmEnum> ubmEnumConverted = await ubmEnumConvert;
       // emit(ItemLoaded());
        emit(ItemLoaded(
          flightMissionsConverted, 
          geoEventTypeConverted, 
          geoAdjustmentStatusConverted, 
          geoExecutionStatusConverted, 
          geoFlightGroupConverted, 
          geoObjectConverted, 
          geoOperatorConverted, 
          geoRouteConverted,
          dictTechnicalSupportConverted,
          geoEventConverted,
          geoFlightMissionTypeConverted,
          geoLandGroupConverted,
          ubaUserConverted,
          ubmEnumConverted 
          ));
      } catch (e) {
        emit(ItemError(e.toString()));
      }


    });
   
   


/*
    void _onSyncData(SyncDataEvent event, Emitter<DataState> emit) async {

  emit(DataLoading());

  try {
    final apiData = await _api.getData(); 
    final localData = _isarDb.getAllData();

    await _isarDb.writeTxn(() async {
    
      // Loop through apiData and check if data exists in localData
      for(final apiItem in apiData) {
        final existing = localData.firstWhere((item) => apiItem.id == item.id);
        if(existing != null) {
          // Update existing local data
          existing.updateFromApi(apiItem); 
        } else {
          // Add new data
          await _isarDb.write(apiItem);
        }
      }

      // Loop through localData and check if data was removed from api
      for(final localItem in localData) {
        if(!apiData.contains(localItem)) {
          // Delete old data
          localItem.delete();
        }
      }

    });

    emit(DataSynced());

  } catch (error) {
    emit(DataError(error));
  }

}

*/

    
/*
    on<CreateItem>((event, emit) async {
      try {
        // Create the item through the API
        final createdItem = await apiService.createItem(event.item);

        // Save the created item to the Isar database
        await isar.writeTxn((isar) async {
          final itemsCollection = isar.itemCollection();
          await itemsCollection.put(createdItem);
        } as Future Function());

        // Fetch all items from the Isar database
        final items = await isar.itemCollection().getAll();

        emit(ItemLoaded(items));
      } catch (e) {
        emit(ItemError('Failed to create item'));
      }
    });
*/

/*
    on<UpdateItem>((event, emit) async {
      try {
        // Update the item through the API
        final updatedItem = await apiService.updateItem(event.item.id.toString(), event.item);

        // Save the updated item to the Isar database
        await isar.writeTxn((isar) async {
          final itemsCollection = isar.itemCollection();
          await itemsCollection.put(updatedItem);
        } as Future Function());

        // Fetch all items from the Isar database
        final items = await isar.itemCollection().getAll();

        emit(ItemLoaded(items));
      } catch (e) {
        emit(ItemError('Failed to update item'));
      }
    });
*/
/*
    on<DeleteItem>((event, emit) async {
      try {
        // Delete the item through the API
        await apiService.deleteItem(event.id);

        // Delete the item from the Isar database
        await isar.writeTxn((isar) async {
          final itemsCollection = isar.itemCollection();
          await itemsCollection.delete(int.parse(event.id));
        } as Future Function());

        // Fetch all items from the Isar database
        final items = await isar.itemCollection().getAll();

        emit(ItemLoaded(items));
      } catch (e) {
        emit(ItemError('Failed to delete item'));
      }
    });
  */
  
  }
}


class GeoFlightMissionDBBloc extends Bloc<GeoFlightMissionEvent, GeoFlightMissionState> {
  final IsarRepository isarRepo;

  GeoFlightMissionDBBloc( {required this.isarRepo}) : super(ItemLoading()) {
   
    
    on<GetItemsFromDB> ((event, emit) async {
      emit(ItemLoading());
      try {
      
        emit(ItemConvertation());
        //load items from Isar database
        //Converting to Model objects
        final flightMissionConvert =  isarRepo.getGeoFlightMissionFromIsar();
        final geoEventTypeConvert =  isarRepo.getgeoEventTypeFromIsar();
        final geoAdjustmentStatusConvert =  isarRepo.getgeoAdjustmentStatusFromIsar();
        final geoExecutionStatusConvert =  isarRepo.getgeoExecutionStatusFromIsar();
        final geoFlightGroupConvert =  isarRepo.getgeoFlightGroupFromIsar();
        final geoObjectConvert =  isarRepo.getgeoObjectFromIsar();
        final geoOperatorConvert =  isarRepo.getgeoOperatorFromIsar();
        final geoRouteConvert =  isarRepo.getgeoRouteFromIsar();
        final dictTechnicalSupportConvert =  isarRepo.getdictTechnicalSupportFromIsar();
        final geoEventConvert =  isarRepo.getgeoEventFromIsar();
        final geoFlightMissionTypeConvert =  isarRepo.getgeoFlightMissionTypeFromIsar();
        final geoLandGroupConvert =  isarRepo.getgeoLandGroupFromIsar();
        final ubaUserConvert =  isarRepo.getubaUserFromIsar();
        final ubmEnumConvert =  isarRepo.getubmEnumFromIsar();



        
        
       List<GeoFlightMission> flightMissionsConverted = await flightMissionConvert;
       List<GeoEventType> geoEventTypeConverted = await geoEventTypeConvert;
       List<GeoAdjustmentStatus> geoAdjustmentStatusConverted = await geoAdjustmentStatusConvert;
       List<GeoExecutionStatus> geoExecutionStatusConverted = await geoExecutionStatusConvert;
       List<GeoFlightGroup> geoFlightGroupConverted = await geoFlightGroupConvert;
       List<GeoObject> geoObjectConverted = await geoObjectConvert;
       List<GeoOperator> geoOperatorConverted = await geoOperatorConvert;
       List<GeoRoute> geoRouteConverted = await geoRouteConvert;
       List<DictTechnicalSupport> dictTechnicalSupportConverted = await dictTechnicalSupportConvert;
       List<GeoEvent> geoEventConverted = await geoEventConvert;
       List<GeoFlightMissionType> geoFlightMissionTypeConverted = await geoFlightMissionTypeConvert;
       List<GeoLandGroup> geoLandGroupConverted = await geoLandGroupConvert;
       List<UbaUser> ubaUserConverted = await ubaUserConvert;
       List<UbmEnum> ubmEnumConverted = await ubmEnumConvert;
       // emit(ItemLoaded());
        emit(ItemLoaded(
          flightMissionsConverted, 
          geoEventTypeConverted, 
          geoAdjustmentStatusConverted, 
          geoExecutionStatusConverted, 
          geoFlightGroupConverted, 
          geoObjectConverted, 
          geoOperatorConverted, 
          geoRouteConverted,
          dictTechnicalSupportConverted,
          geoEventConverted,
          geoFlightMissionTypeConverted,
          geoLandGroupConverted,
          ubaUserConverted,
          ubmEnumConverted 
          ));
      } catch (e) {
        emit(ItemError(e.toString()));
      }


    });
   
   


/*
    void _onSyncData(SyncDataEvent event, Emitter<DataState> emit) async {

  emit(DataLoading());

  try {
    final apiData = await _api.getData(); 
    final localData = _isarDb.getAllData();

    await _isarDb.writeTxn(() async {
    
      // Loop through apiData and check if data exists in localData
      for(final apiItem in apiData) {
        final existing = localData.firstWhere((item) => apiItem.id == item.id);
        if(existing != null) {
          // Update existing local data
          existing.updateFromApi(apiItem); 
        } else {
          // Add new data
          await _isarDb.write(apiItem);
        }
      }

      // Loop through localData and check if data was removed from api
      for(final localItem in localData) {
        if(!apiData.contains(localItem)) {
          // Delete old data
          localItem.delete();
        }
      }

    });

    emit(DataSynced());

  } catch (error) {
    emit(DataError(error));
  }

}

*/

    
/*
    on<CreateItem>((event, emit) async {
      try {
        // Create the item through the API
        final createdItem = await apiService.createItem(event.item);

        // Save the created item to the Isar database
        await isar.writeTxn((isar) async {
          final itemsCollection = isar.itemCollection();
          await itemsCollection.put(createdItem);
        } as Future Function());

        // Fetch all items from the Isar database
        final items = await isar.itemCollection().getAll();

        emit(ItemLoaded(items));
      } catch (e) {
        emit(ItemError('Failed to create item'));
      }
    });
*/

/*
    on<UpdateItem>((event, emit) async {
      try {
        // Update the item through the API
        final updatedItem = await apiService.updateItem(event.item.id.toString(), event.item);

        // Save the updated item to the Isar database
        await isar.writeTxn((isar) async {
          final itemsCollection = isar.itemCollection();
          await itemsCollection.put(updatedItem);
        } as Future Function());

        // Fetch all items from the Isar database
        final items = await isar.itemCollection().getAll();

        emit(ItemLoaded(items));
      } catch (e) {
        emit(ItemError('Failed to update item'));
      }
    });
*/
/*
    on<DeleteItem>((event, emit) async {
      try {
        // Delete the item through the API
        await apiService.deleteItem(event.id);

        // Delete the item from the Isar database
        await isar.writeTxn((isar) async {
          final itemsCollection = isar.itemCollection();
          await itemsCollection.delete(int.parse(event.id));
        } as Future Function());

        // Fetch all items from the Isar database
        final items = await isar.itemCollection().getAll();

        emit(ItemLoaded(items));
      } catch (e) {
        emit(ItemError('Failed to delete item'));
      }
    });
  */
  
  }
}

