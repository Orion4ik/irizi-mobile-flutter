import 'package:equatable/equatable.dart';

import '../../../db/flight_missions_isar.dart';

abstract class GeoFlightMissionEvent extends Equatable  {}

class GetItems extends GeoFlightMissionEvent {
  @override
  // TODO: implement props
  List<Object?> get props => [];
  
}

class GetItemsFromDB extends GeoFlightMissionEvent {
  @override
  // TODO: implement props
  List<Object?> get props => [];
  
}

class CreateItem extends GeoFlightMissionEvent {
  final GeoFlightMissionIsar item;

  CreateItem(this.item);
  
  @override
  // TODO: implement props
  List<Object?> get props => [item];
}

class UpdateItem extends GeoFlightMissionEvent {
  final GeoFlightMissionIsar item;

  UpdateItem(this.item);
  
  @override
  // TODO: implement props
  List<Object?> get props => [item];
}

class DeleteItem extends GeoFlightMissionEvent {
  final String id;

  DeleteItem(this.id);
  
  @override
  // TODO: implement props
  List<Object?> get props => [id];
}