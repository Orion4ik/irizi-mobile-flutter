import 'package:equatable/equatable.dart';

import '../../../model/model_all.dart';

abstract class GeoFlightMissionState   extends Equatable  {}

class ItemLoading extends GeoFlightMissionState  {
  @override
  // TODO: implement props
  List<Object?> get props => [];

}

class ItemSync extends GeoFlightMissionState  {
  @override
  // TODO: implement props
  List<Object?> get props => [];

}

class ItemSaveToDB extends GeoFlightMissionState  {
  @override
  // TODO: implement props
  List<Object?> get props => [];

}

class ItemConvertation extends GeoFlightMissionState  {
  @override
  // TODO: implement props
  List<Object?> get props => [];

}

class ItemLoaded extends GeoFlightMissionState {
  final List<GeoFlightMission>? geoFlightMission;
  final List<GeoEvent>? geoEvent;
  final List<GeoEventType>? geoEventType;
  final List<GeoObject>? geoObject;
  final List<GeoFlightMissionType>? geoFlightMissionType;
  final List<GeoRoute>? geoRoute;
  final List<GeoOperator>? geoOperator;
  final List<GeoExecutionStatus>? geoExecutionStatus;
  final List<GeoAdjustmentStatus>? geoAdjustmentStatus;
  final List<DictTechnicalSupport>? dictTechnicalSupport;
  final List<GeoFlightGroup>? geoFlightGroup;
  final List<GeoLandGroup>? geoLandGroup;
  final List<UbmEnum>? ubmEnum;
  final List<UbaUser>? ubaUser;
  



  ItemLoaded(
  this.geoFlightMission,
 this.geoEventType, 
 this.geoAdjustmentStatus, 
 this.geoExecutionStatus, 
 this.geoFlightGroup, 
 this.geoObject, 
 this.geoOperator, 
 this.geoRoute,
 this.dictTechnicalSupport,
 this.geoEvent,
 this.geoFlightMissionType,
 this.geoLandGroup,
 this.ubaUser,
 this.ubmEnum


  );
  
  @override
  // TODO: implement props
  List<Object?> get props => [
geoFlightMission,
geoEventType, 
geoAdjustmentStatus, 
geoExecutionStatus, 
geoFlightGroup, 
geoObject, 
geoOperator, 
geoRoute,
dictTechnicalSupport,
geoEvent,
geoFlightMissionType,
geoLandGroup,
ubaUser,
ubmEnum



  
     ];
}

class ItemError extends GeoFlightMissionState {
  final String error;

  ItemError(this.error);
  
   @override
  List<Object?> get props => [error];
}
