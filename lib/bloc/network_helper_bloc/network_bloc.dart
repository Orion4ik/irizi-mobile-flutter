

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_bloc/flutter_bloc.dart';



import 'network_event.dart';
import 'network_state.dart';

/*
class NetworkBloc extends Bloc<NetworkEvent, NetworkState> {



 
  
  NetworkBloc._() : super(NetworkInitial()) {
    on<NetworkObserve>(_observe);
    on<NetworkNotify>(_notifyStatus);
  }

  static final NetworkBloc _instance = NetworkBloc._();

  factory NetworkBloc() => _instance;

  void _observe(event, emit) {
    NetworkHelper.observeNetwork();
  }

  void _notifyStatus(NetworkNotify event, emit) {
    event.isConnected ? emit(NetworkSuccess()) : emit(NetworkFailure('Відсутнє підключення до мережі. Увімкнено офлайн-режим'));
  }


  
}
*/


class NetworkBloc extends Bloc<NetworkEvent, NetworkState> {
  final Connectivity connectivity;

  NetworkBloc(this.connectivity) : super(NetworkState(false)) {
    on<CheckConnectivity>((event, emit) async {
      final result = await connectivity.checkConnectivity();
      emit(NetworkState(result != ConnectivityResult.none));
    });
  }
}


