class NetworkState {
  final bool isConnected;

  NetworkState(this.isConnected);
}