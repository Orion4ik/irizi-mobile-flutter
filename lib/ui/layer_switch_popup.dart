import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/layer_switch/layer_switch_bloc.dart';

class LayerSwitch extends StatelessWidget {
  const LayerSwitch({super.key});

  @override
  Widget build(BuildContext context) {
    final layerBloc = BlocProvider.of<LayerBloc>(context);

    return PopupMenuButton<Layer>(
      icon: Container(
                              height: 40,
                              width: 40,
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.blue ),
                               child:
      const Icon(Icons.layers)),
      onSelected: (Layer selectedLayer) {
        layerBloc.switchLayer(selectedLayer);
      },
      itemBuilder: (BuildContext context) => <PopupMenuEntry<Layer>>[
        PopupMenuItem<Layer>(
          value: Layer.normal,
          child: Row(
            children: [

              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  image: const DecorationImage(
      image: AssetImage('assets/images/content/osm.png'),
      fit: BoxFit.cover,
    ),
                  borderRadius: BorderRadius.circular(8),

                )
                ,
              ),
              const SizedBox(width: 10,),
            const Text('Open Street Map'
            )
            
      ]),
        ),
        PopupMenuItem<Layer>(
          value: Layer.satellite,
          child:Row(
            children: [

              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  image: const DecorationImage(
      image: AssetImage('assets/images/content/google_satelline.jpg'),
      fit: BoxFit.cover,
    ),
                  borderRadius: BorderRadius.circular(8),

                )
                ,
              ),
              const SizedBox(width: 10,),
            const Text('Google Satellite'
            )
            
      ]),
        ),
        PopupMenuItem<Layer>(
          value: Layer.terrain,
          child: Row(
            children: [

              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  image: const DecorationImage(
      image: AssetImage('assets/images/content/google_maps.png'),
      fit: BoxFit.cover,
    ),
                  borderRadius: BorderRadius.circular(8),

                )
                ,
              ),
              const SizedBox(width: 10,),
            const Text('Google Terrain'
            )
            
      ]),
        ),
      ],
    );
  }
}
/*
class LayerSwitchPop extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    final layerBloc = BlocProvider.of<LayerBloc>(context);
    final isActive = layerBloc.state ;

    return Positioned(
                //left: 20,
                right: 20,
                top: 40,
                 child: PopupMenuButton<Layer>(
                  onSelected: (Layer selectedLayer) {
        layerBloc.switchLayer(selectedLayer);
      },
                       icon: 
                             Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.blue ),
                               child: const Icon(
                                  Icons.layers,
                                  color: Colors.white,
                                ),
                             ), 
                              itemBuilder: (BuildContext context) => <PopupMenuEntry<Layer>> [
               
                  PopupMenuItem( 
                    
                    value: Layer.normal,
                    onTap: () =>  layerBloc.switchLayer(Layer.normal),
                    
                    // row with 2 children
                    child: Row(
                      children: [
                        SvgPicture.asset('assets/images/vector/fa6-solid_map.svg', color: Colors.blue, width: 24),
                        SizedBox(
                          width: 10,
                        ),
                        Text("Open Street Map")
                      ],
                    ),
                  ),
                  PopupMenuItem( 
                    value: Layer.satellite,
                    onTap: () => layerBloc.switchLayer(Layer.satellite),
                    // row with 2 children
                    child: Row(
                      children: [
                        SvgPicture.asset('assets/images/vector/fa6-solid_map.svg', color: Colors.blue, width: 24),
                        SizedBox(
                          width: 10,
                        ),
                        Text("Google Maps Satellite")
                      ],
                    ),
                  ),
                   PopupMenuItem( 
                    value: Layer.terrain,
                    onTap: () => layerBloc.switchLayer(Layer.terrain),
                    // row with 2 children
                    child: Row(
                      children: [
                        SvgPicture.asset('assets/images/vector/fa6-solid_map.svg', color: Colors.blue, width: 24),
                        SizedBox(
                          width: 10,
                        ),
                        Text("Google Maps Terrain")
                      ],
                    ),
                  ),
                  
                  ]),
               );
  }
}
*/