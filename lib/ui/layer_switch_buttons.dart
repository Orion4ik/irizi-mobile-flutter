import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/layer_switch/layer_switch_bloc.dart';


class LayerSwitch extends StatelessWidget {
  const LayerSwitch({super.key});

  @override
  Widget build(BuildContext context) {
    final layerBloc = BlocProvider.of<LayerBloc>(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        LayerButton(
          layer: Layer.normal,
          onPressed: () {
            layerBloc.switchLayer(Layer.normal);
          },
          label: 'Normal',
        ),
        LayerButton(
          layer: Layer.satellite,
          onPressed: () {
            layerBloc.switchLayer(Layer.satellite);
          },
          label: 'Satellite',
        ),
        LayerButton(
          layer: Layer.terrain,
          onPressed: () {
            layerBloc.switchLayer(Layer.terrain);
          },
          label: 'Terrain',
        ),
      ],
    );
  }
}

class LayerButton extends StatelessWidget {
  final Layer layer;
  final VoidCallback onPressed;
  final String label;

  const LayerButton({super.key, 
    required this.layer,
    required this.onPressed,
    required this.label,
  });

  @override
  Widget build(BuildContext context) {
    final layerBloc = BlocProvider.of<LayerBloc>(context);
    final isActive = layerBloc.state == layer;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: FloatingActionButton(
        onPressed: isActive ? null : onPressed,
        backgroundColor: isActive ? Colors.grey : null,
        child: Text(label),
      ),
    );
  }
}

