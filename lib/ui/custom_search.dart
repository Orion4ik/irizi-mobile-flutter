import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_svg/svg.dart';
import 'package:irizi/repo/isar_db/isar_repo.dart';
import '../model/model_all.dart';
import '../screens/FlightGroup/flight_mission_info_screen.dart';
import '../screens/FlightGroup/flight_mission_map.dart';
import '../service/resolving_service.dart';

import '../untils/theme.dart';

class CustomSearch extends SearchDelegate<GeoFlightMission?>{
 
  
  Resolver resolver = Resolver('');
  List<GeoFlightMission> filteredMissions = [];
  List<GeoFlightMission> flightMissions;
  final List<GeoOperator>? geoOperatorL;
  final List<DictTechnicalSupport>? dictTechnicalSupport;
  final List<GeoExecutionStatus>? geoExecutionStatus;
  final List<GeoAdjustmentStatus>? geoAdjustmentStatus;
  final List<GeoRoute>? geoRoute;
   All flightMissionsAll;
   IsarRepository service;

  


CustomSearch({
  required this.flightMissions, 
  required this.flightMissionsAll,
   required this.geoOperatorL,
   required this.dictTechnicalSupport, 
   required this.geoExecutionStatus,
   required this.geoAdjustmentStatus,
   required this.geoRoute,
   required this.service});

 
 

 




  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
  }
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return SizedBox(
                          height:  MediaQuery.of(context).size.height*0.8,
                          child: ListView.builder(
                           
                                            itemCount:filteredMissions.length,
                                         
                                            itemBuilder: (context, index) {
                                           final mission = filteredMissions[index];
                                              return GestureDetector(
                                                onTap: () {
                                                  Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => FlightMissionInfo(
                                                       ID: mission.ID,
                  date: mission.date,
                  num: mission.num,
                  eventsState: mission.eventsState,
                  landGroupID: mission.landGroupID,
                  geoJson: mission.geoJson,
                  flightDuration: mission.flightDuration ?? '0',
                  flightGroupID: mission.flightGroupID,
                  flightMissionType: mission.flightMissionType,
                  latLngStartCombined:mission. latLngStartCombined ?? '-',
                  latitude: mission.latitude ?? 37.7749,
                  longitude: mission.longitude ?? -122.4194,
                  meteorologicalData: mission.meteorologicalData ?? '-',
                  object:  resolver.resolveData(mission.object, flightMissionsAll.geoObject, displayKey: 'name'),
              
                  geoOperator: 
                  resolver.resolveData(mission.geoOperator, flightMissionsAll.geoOperator, displayKey: 'name') ?? '-',
                
                  route:resolver.resolveData(mission.route, flightMissionsAll.geoRoute, displayKey: 'name') ?? '-',
                
                  routeArea: mission.routeArea ?? 0,
                  routeLength: mission.routeLength,
                  nearestLocality:mission.nearestLocality ?? '-',
                  state: resolver.resolveData(mission.state, flightMissionsAll.ubmEnum, displayKey: 'name', valueKey: 'code').toString(),
        
                  technicalSupport: resolver.resolveData(mission.technicalSupport, flightMissionsAll.dictTechnicalSupport, displayKey: 'name', valueKey: 'ID'),
                  updateInfo: mission.updateInfo,
                  responsiblePerson: mission.responsiblePerson ?? '-',
                  examinationDateFrom:mission.examinationDateFrom ,
                  examinationDateTo: mission.examinationDateTo ,
                  examinationDatesCombined: mission.examinationDatesCombined ,
                  executionDeadlineDate: mission.executionDeadlineDate ,
                  executionInfo: mission.executionInfo,
                  executionPlanDatesCombined: mission.executionPlanDatesCombined ,
                  executionPlanEndDate: mission.executionPlanEndDate,
                  executionPlanStartDate: mission.executionPlanStartDate ,
                  geoEvent: resolver.getEventsByFlightTaskID(flightMissionsAll.geoEvent, flightMissionsAll.geoFlightMission![index].ID) ,
                  geoEventType: flightMissionsAll.geoEventType,
                  geoObject:flightMissionsAll.geoObject,
                  ubmEnum: flightMissionsAll.ubmEnum,
                  geoOperatorL: geoOperatorL,
                 mi_createDate: mission.miCreateDate,
                  mi_createUser: mission.miCreateUser,
                  mi_modifyDate: mission.miModifyDate,
                  mi_modifyUser: mission.miModifyUser,
                  mi_owner: mission.miOwner,
                  service: service,
                  dictTechnicalSupport: dictTechnicalSupport,
                  geoAdjustmentStatus: geoAdjustmentStatus,
                  geoExecutionStatus: geoExecutionStatus,
                  geoRoute: geoRoute,
                                              )));
                                              },
                                                child: Container(
                                                  
                                                  child: Container(
                                                                                      margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                                                                       height:  MediaQuery.of(context).size.height/2.2,
                                                                                       //MediaQuery.sizeOf(context).height*0.3,
                                                                                       //         width: MediaQuery.sizeOf(context).width,
                                                                                      decoration: BoxDecoration(
                                                                                          color: FabColorDark,
                                                                                          borderRadius: const BorderRadius.all(Radius.circular(10)),
                                                                                          
                                                                                        ),
                                                                                        child: Column(
                                                                                          children: [
                                                                          
                                                  Padding(
                                                                          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                                                                          child: Row(
                                                                            
                                                                           crossAxisAlignment: CrossAxisAlignment.start,
                                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                          
                                                                              children: [
                                                                              
                                                                           
                                                                            Row(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [  
                                                                              Container(
                                                                          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                                                           width: 12,
                                                                           height: 12,
                                                                           decoration: const BoxDecoration(
                                                                             color: SuccessColor,
                                                                             shape: BoxShape.circle
                                                                           ),
                                                                              ),
                                                                              Padding(
                                                                                padding: const EdgeInsets.only(top: 12),
                                                                                child: Text(
                                                                                   resolver.resolveData(mission.state, flightMissionsAll.ubmEnum, displayKey: 'name', valueKey: 'code')
                                                                                  
                                                                                  
                                                                                  )),

                                                                              ]),
                                                                              


                                                                            Container(
                                                                             
                                                                              padding: const EdgeInsets.only(right: 8),
                                                                              child: FilledButton(
                                                                           
                                                                           onPressed: (){
                                                                             Navigator.of(context).push(
                                                                                     MaterialPageRoute(
                                                                                      builder: (context) => FlightMissionMapScreen(
                                                                                             ID: mission.ID,
                  date: mission.date,
                  num: mission.num,
                  eventsState: mission.eventsState,
                  landGroupID: mission.landGroupID,
                  geoJson: mission.geoJson,
                  flightDuration: mission.flightDuration ?? '0',
                  flightGroupID: mission.flightGroupID,
                  flightMissionType: mission.flightMissionType,
                  latLngStartCombined:mission.latLngStartCombined ?? '-',
                  latitude: mission.latitude ?? 50.566504,
                  longitude: mission.longitude ?? 26.261774,
                  meteorologicalData: mission.meteorologicalData ?? '-',
                  object:  resolver.resolveData(mission.object,flightMissionsAll.geoObject, displayKey: 'name'),
              
                  geoOperator: 
                  resolver.resolveData(mission.geoOperator, flightMissionsAll.geoOperator, displayKey: 'name') ?? '-',
                
                  route: resolver.resolveData(mission.route, flightMissionsAll.geoRoute, displayKey: 'name') ?? '-',
                  routeArea: mission.routeArea ?? 0,
                  routeLength: mission.routeLength,
                  nearestLocality: mission.nearestLocality ?? '-',
                  state: resolver.resolveData(mission.state,flightMissionsAll.ubmEnum, displayKey: 'name', valueKey: 'code').toString(),
        
                  technicalSupport: resolver.resolveData(mission.technicalSupport,flightMissionsAll.dictTechnicalSupport, displayKey: 'name', valueKey: 'ID'),
                  updateInfo: mission.updateInfo,
                  responsiblePerson: mission.responsiblePerson ?? '-',
                  examinationDateFrom:mission.examinationDateFrom ,
                  examinationDateTo: mission.examinationDateTo ,
                  examinationDatesCombined: mission.examinationDatesCombined ,
                  executionDeadlineDate: mission.executionDeadlineDate ,
                  executionInfo: mission.executionInfo,
                  executionPlanDatesCombined: mission.executionPlanDatesCombined ,
                  executionPlanEndDate: mission.executionPlanEndDate,
                  executionPlanStartDate: mission.executionPlanStartDate ,
                  geoEvent: resolver.getEventsByFlightTaskID(flightMissionsAll.geoEvent, flightMissionsAll.geoFlightMission![index].ID),
                  geoEventType: flightMissionsAll.geoEventType,
                  geoObject:flightMissionsAll.geoObject,
                  geoOperatorL: geoOperatorL,
                  ubmEnum: flightMissionsAll.ubmEnum,
                  mi_createDate: mission.miCreateDate,
                  mi_createUser: mission.miCreateUser,
                  mi_modifyDate: mission.miModifyDate,
                  mi_modifyUser: mission.miModifyUser,
                  mi_owner: mission.miOwner,
                  service: service,
                                                                                       
                                                                                      ),
                                                                                     ));
                                                                           },
                                                                           style: ButtonStyle(
                                                                             minimumSize: MaterialStateProperty.all(const Size(30, 40)) ,
                                                                                 backgroundColor: MaterialStateProperty.all(
                                                                                     Colors.grey.shade800),
                                                                                
                                                                                    
                                                                                     
                                                                                 shape: MaterialStateProperty.all<
                                                                                         RoundedRectangleBorder>(
                                                                                     RoundedRectangleBorder(
                                                                                         borderRadius:
                                                                                             BorderRadius.circular(16.0)))),
                                                                           child: const Center(child: Icon(IconlyLight.location)),
                                                                          
                                                                           )
                                                                           
                                                                              
                                                                            ),
                                                                          
                                                                            ]),
                                                  ),
                                                  
                                                  Container(
                                                                          
                                                                          padding: const EdgeInsets.only(left: 16),
                                                                          child: 
                                                                          Row(children: [
                                                  
                                                                            Container(
                                                                              child: Column(
                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                children: [
                                                    
                                                                                const AutoSizeText('Номер ПЗ',
                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                              ),
                                                      
                                                     // SizedBox(height: 8,),
                                                      Text(mission.num.toString(),
                                                      maxLines: 2,
                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                              ),
                                                                              ),
                                                    
                                                                              ],),
                                                                            ),
                                                                            
                                                                            Container(
                                                                          padding: const EdgeInsets.only(left: 16),
                                                                          child: Row(children: [
                                                  
                                                                            Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Дата створення',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                                          
                                                   // SizedBox(height: 8,),
                                                                          Text(
                                                                            resolver.formatDateTime(mission.executionPlanStartDate),
                                                                           
                                                                           // resolver.resolveData(all.geoFlightMission![index].geoOperator, all.geoOperator, displayKey: 'name').toString(),
                                                                           
                                                                          maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],)
                                                  
                                                                          ],), 
                                                  ),
                                                                          ],),
                                                                          
                                                  ),
                                                                          
                                                  Container(
                                                                          padding: const EdgeInsets.only(left: 16, top: 16),
                                                                          child: 
                                                                          Row(children: [
                                                  
                                                                            Container(
                                                                              child: Column(
                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                children: [
                                                    
                                                                                const AutoSizeText('Замовник',
                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                              ),
                                                      
                                                     // SizedBox(height: 8,),
                                                      AutoSizeText(
                                                        
                                                        resolver.resolveData(mission.geoOperator, flightMissionsAll.geoOperator, displayKey: 'name'),
                                                        //all.geoFlightMission[index].object.toString(),
                                                      maxLines: 2,
                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                              ),
                                                                              ),
                                                    
                                                                              ],),
                                                                            ),
                                                  
                                                                            
                                                                          ],),
                                                                          
                                                  ),
                                                  Container(
                                                                          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                                                          child: Row(children: [
                                                  
                                                                            Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText("Об'єкт",
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                                          
                                                                                         
                                                                          Text(
                                                                            
                                                                            resolver.resolveData(mission.object,flightMissionsAll.geoObject, displayKey: 'name').toString(),
                                                                          maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],)
                                                  
                                                                          ],), 
                                                  ),

                                                  Container(
                                                                          padding: const EdgeInsets.only(left: 16),
                                                                          child: Row(children: [
                                                  
                                                                            Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Відповідальна особа',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                                          
                                                                                         
                                                                          Text(
                                                                            
                                                                            mission.responsiblePerson.toString(),
                                                                          maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],)
                                                  
                                                                          ],), 
                                                  ),
                                                   Container(
                                                                          padding: const EdgeInsets.only(left: 16, top: 16),
                                                                          child: 
                                                                          Row(children: [
                                                  
                                                                            Container(
                                                                              child: Row(
                                                                                children: [
                                                                                  SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.white),
                                                                                  const SizedBox(width: 8,),
                                                                                 Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: [
                                                     
                                                                                  const AutoSizeText('Технічні засоби',
                                                                                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                                                                                ),
                                                        
                                                       // SizedBox(height: 8,),
                                                        Text(
                                                          resolver.resolveData(mission.technicalSupport, flightMissionsAll.dictTechnicalSupport, displayKey: 'name').toString(),
                                                          //all.geoFlightMission[index].technicalSupport.toString(),
                                                        maxLines: 2,
                                                                                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                                ),
                                                                                ),
                                                      
                                                                                ],),
                                                                          ]),
                                                                            ),
                                                                           
                                                                            
                                                                          ],),
                                                                          
                                                  ),
                                                                          
                                                                                          ],
                                                                                        ),
                                                                          
                                                                                    ),
                                                ),
                                              );
                            
                                            
                      },),
                        );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
     
 filteredMissions = flightMissions.where((mission) {

      return mission.num!.contains(query) ||
          mission.state.contains(query) ||
          mission.route.toString().contains(query) ||
          mission.object.toString().contains(query) ||
          mission.geoOperator.toString().contains(query) ||
          (mission.date != null && mission.date!.toString().contains(query));
    }).toList();

 return SizedBox(
                          height:  MediaQuery.of(context).size.height,
                          child: ListView.builder(
                           
                                            itemCount:filteredMissions.length,
                                         
                                            itemBuilder: (context, index) {
                                           final mission = filteredMissions[index];
                                              return GestureDetector(
                                                onTap: () {
                                                  Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => FlightMissionInfo(
                                                       ID: mission.ID,
                  date: mission.date,
                  num: mission.num,
                  eventsState: mission.eventsState,
                  landGroupID: mission.landGroupID,
                  geoJson: mission.geoJson,
                  flightDuration: mission.flightDuration ?? '0',
                  flightGroupID: mission.flightGroupID,
                  flightMissionType: mission.flightMissionType,
                  latLngStartCombined:mission. latLngStartCombined ?? '-',
                  latitude: mission.latitude ?? 37.7749,
                  longitude: mission.longitude ?? -122.4194,
                  meteorologicalData: mission.meteorologicalData ?? '-',
                  object:  resolver.resolveData(mission.object, flightMissionsAll.geoObject, displayKey: 'name'),
              
                  geoOperator: 
                  resolver.resolveData(mission.geoOperator, flightMissionsAll.geoOperator, displayKey: 'name') ?? '-',
                
                  route:resolver.resolveData(mission.route, flightMissionsAll.geoRoute, displayKey: 'name') ?? '-',
                
                  routeArea: mission.routeArea ?? 0,
                  routeLength: mission.routeLength,
                  nearestLocality:mission.nearestLocality ?? '-',
                  state: resolver.resolveData(mission.state, flightMissionsAll.ubmEnum, displayKey: 'name', valueKey: 'code').toString(),
        
                  technicalSupport: resolver.resolveData(mission.technicalSupport, flightMissionsAll.dictTechnicalSupport, displayKey: 'name', valueKey: 'ID'),
                  updateInfo: mission.updateInfo,
                  responsiblePerson: mission.responsiblePerson ?? '-',
                  examinationDateFrom:mission.examinationDateFrom ,
                  examinationDateTo: mission.examinationDateTo ,
                  examinationDatesCombined: mission.examinationDatesCombined ,
                  executionDeadlineDate: mission.executionDeadlineDate ,
                  executionInfo: mission.executionInfo,
                  executionPlanDatesCombined: mission.executionPlanDatesCombined ,
                  executionPlanEndDate: mission.executionPlanEndDate,
                  executionPlanStartDate: mission.executionPlanStartDate ,
                  geoEvent: resolver.getEventsByFlightTaskID(flightMissionsAll.geoEvent, flightMissionsAll.geoFlightMission![index].ID) ,
                  geoEventType: flightMissionsAll.geoEventType,
                  geoObject:flightMissionsAll.geoObject,
                  ubmEnum: flightMissionsAll.ubmEnum,
                  dictTechnicalSupport: dictTechnicalSupport,
                  geoOperatorL: geoOperatorL,
                  geoAdjustmentStatus: geoAdjustmentStatus,
                  geoExecutionStatus: geoExecutionStatus,
                  geoRoute: geoRoute,
                mi_createDate: mission.miCreateDate,
                  mi_createUser: mission.miCreateUser,
                  mi_modifyDate: mission.miModifyDate,
                  mi_modifyUser: mission.miModifyUser,
                  mi_owner: mission.miOwner,
                  service: service,
                                              )));
                                              },
                                                child: Container(
                                                  
                                                  child: Container(
                                                                                      margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                                                                       height:  MediaQuery.of(context).size.height/2.2,
                                                                                       //MediaQuery.sizeOf(context).height*0.3,
                                                                                       //         width: MediaQuery.sizeOf(context).width,
                                                                                      decoration: BoxDecoration(
                                                                                          color: FabColorDark,
                                                                                          borderRadius: const BorderRadius.all(Radius.circular(10)),
                                                                                          
                                                                                        ),
                                                                                        child: Column(
                                                                                          children: [
                                                                          
                                                  Padding(
                                                                          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                                                                          child: Row(
                                                                            
                                                                           crossAxisAlignment: CrossAxisAlignment.start,
                                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                          
                                                                              children: [
                                                                              
                                                                           
                                                                            Row(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [  
                                                                              Container(
                                                                          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                                                           width: 12,
                                                                           height: 12,
                                                                           decoration: const BoxDecoration(
                                                                             color: SuccessColor,
                                                                             shape: BoxShape.circle
                                                                           ),
                                                                              ),
                                                                              Padding(
                                                                                padding: const EdgeInsets.only(top: 12),
                                                                                child: Text(
                                                                                   resolver.resolveData(mission.state, flightMissionsAll.ubmEnum, displayKey: 'name', valueKey: 'code')
                                                                                  
                                                                                  
                                                                                  )),

                                                                              ]),
                                                                              


                                                                            Container(
                                                                             
                                                                              padding: const EdgeInsets.only(right: 8),
                                                                              child: FilledButton(
                                                                           
                                                                           onPressed: (){
                                                                             Navigator.of(context).push(
                                                                                     MaterialPageRoute(
                                                                                      builder: (context) => FlightMissionMapScreen(
                                                                                             ID: mission.ID,
                  date: mission.date,
                  num: mission.num,
                  eventsState: mission.eventsState,
                  landGroupID: mission.landGroupID,
                  geoJson: mission.geoJson,
                  flightDuration: mission.flightDuration ?? '0',
                  flightGroupID: mission.flightGroupID,
                  flightMissionType: mission.flightMissionType,
                  latLngStartCombined:mission.latLngStartCombined ?? '-',
                  latitude: mission.latitude ?? 50.566504,
                  longitude: mission.longitude ?? 26.261774,
                  meteorologicalData: mission.meteorologicalData ?? '-',
                  object:  resolver.resolveData(mission.object,flightMissionsAll.geoObject, displayKey: 'name'),
              
                  geoOperator: 
                  resolver.resolveData(mission.geoOperator, flightMissionsAll.geoOperator, displayKey: 'name') ?? '-',
                
                  route: resolver.resolveData(mission.route, flightMissionsAll.geoRoute, displayKey: 'name') ?? '-',
                  routeArea: mission.routeArea ?? 0,
                  routeLength: mission.routeLength,
                  nearestLocality: mission.nearestLocality ?? '-',
                  state: resolver.resolveData(mission.state,flightMissionsAll.ubmEnum, displayKey: 'name', valueKey: 'code').toString(),
        
                  technicalSupport: resolver.resolveData(mission.technicalSupport,flightMissionsAll.dictTechnicalSupport, displayKey: 'name', valueKey: 'ID'),
                  updateInfo: mission.updateInfo,
                  responsiblePerson: mission.responsiblePerson ?? '-',
                  examinationDateFrom:mission.examinationDateFrom ,
                  examinationDateTo: mission.examinationDateTo ,
                  examinationDatesCombined: mission.examinationDatesCombined ,
                  executionDeadlineDate: mission.executionDeadlineDate ,
                  executionInfo: mission.executionInfo,
                  executionPlanDatesCombined: mission.executionPlanDatesCombined ,
                  executionPlanEndDate: mission.executionPlanEndDate,
                  executionPlanStartDate: mission.executionPlanStartDate ,
                  geoEvent: resolver.getEventsByFlightTaskID(flightMissionsAll.geoEvent, flightMissionsAll.geoFlightMission![index].ID),
                  geoEventType: flightMissionsAll.geoEventType,
                  geoObject:flightMissionsAll.geoObject,
                  ubmEnum: flightMissionsAll.ubmEnum,
                  geoOperatorL: geoOperatorL,
                mi_createDate: mission.miCreateDate,
                  mi_createUser: mission.miCreateUser,
                  mi_modifyDate: mission.miModifyDate,
                  mi_modifyUser: mission.miModifyUser,
                  mi_owner: mission.miOwner,
                  service:  service,
                                                                                       
                                                                                      ),
                                                                                     ));
                                                                           },
                                                                           style: ButtonStyle(
                                                                             minimumSize: MaterialStateProperty.all(const Size(30, 40)) ,
                                                                                 backgroundColor: MaterialStateProperty.all(
                                                                                     Colors.grey.shade800),
                                                                                
                                                                                    
                                                                                     
                                                                                 shape: MaterialStateProperty.all<
                                                                                         RoundedRectangleBorder>(
                                                                                     RoundedRectangleBorder(
                                                                                         borderRadius:
                                                                                             BorderRadius.circular(16.0)))),
                                                                           child: const Center(child: Icon(IconlyLight.location)),
                                                                          
                                                                           )
                                                                           
                                                                              
                                                                            ),
                                                                          
                                                                            ]),
                                                  ),
                                                  
                                                  Container(
                                                                          
                                                                          padding: const EdgeInsets.only(left: 16),
                                                                          child: 
                                                                          Row(children: [
                                                  
                                                                            Container(
                                                                              child: Column(
                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                children: [
                                                    
                                                                                const AutoSizeText('Номер ПЗ',
                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                              ),
                                                      
                                                     // SizedBox(height: 8,),
                                                      Text(mission.num.toString(),
                                                      maxLines: 2,
                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                              ),
                                                                              ),
                                                    
                                                                              ],),
                                                                            ),
                                                                            
                                                                            Container(
                                                                          padding: const EdgeInsets.only(left: 16),
                                                                          child: Row(children: [
                                                  
                                                                            Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Дата створення',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                                          
                                                   // SizedBox(height: 8,),
                                                                          Text(
                                                                            resolver.formatDateTime(mission.executionPlanStartDate),
                                                                           
                                                                           // resolver.resolveData(all.geoFlightMission![index].geoOperator, all.geoOperator, displayKey: 'name').toString(),
                                                                           
                                                                          maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],)
                                                  
                                                                          ],), 
                                                  ),
                                                                          ],),
                                                                          
                                                  ),
                                                                          
                                                  Container(
                                                                          padding: const EdgeInsets.only(left: 16, top: 16),
                                                                          child: 
                                                                          Row(children: [
                                                  
                                                                            Container(
                                                                              child: Column(
                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                children: [
                                                    
                                                                                const AutoSizeText('Замовник',
                                                                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                              ),
                                                      
                                                     // SizedBox(height: 8,),
                                                      AutoSizeText(
                                                        
                                                        resolver.resolveData(mission.geoOperator, flightMissionsAll.geoOperator, displayKey: 'name'),
                                                        //all.geoFlightMission[index].object.toString(),
                                                      maxLines: 2,
                                                                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                              ),
                                                                              ),
                                                    
                                                                              ],),
                                                                            ),
                                                  
                                                                            
                                                                          ],),
                                                                          
                                                  ),
                                                  Container(
                                                                          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                                                          child: Row(children: [
                                                  
                                                                            Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText("Об'єкт",
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                                          
                                                                                         
                                                                          Text(
                                                                            
                                                                            resolver.resolveData(mission.object,flightMissionsAll.geoObject, displayKey: 'name').toString(),
                                                                          maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],)
                                                  
                                                                          ],), 
                                                  ),

                                                  Container(
                                                                          padding: const EdgeInsets.only(left: 16),
                                                                          child: Row(children: [
                                                  
                                                                            Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                  
                                                                              const AutoSizeText('Відповідальна особа',
                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                                                            ),
                                                                          
                                                                                         
                                                                          Text(
                                                                            
                                                                            mission.responsiblePerson.toString(),
                                                                          maxLines: 2,
                                                                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                            ),
                                                                            ),
                                                  
                                                                            ],)
                                                  
                                                                          ],), 
                                                  ),
                                                   Container(
                                                                          padding: const EdgeInsets.only(left: 16, top: 16),
                                                                          child: 
                                                                          Row(children: [
                                                  
                                                                            Container(
                                                                              child: Row(
                                                                                children: [
                                                                                  SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.white),
                                                                                  const SizedBox(width: 8,),
                                                                                 Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: [
                                                     
                                                                                  const AutoSizeText('Технічні засоби',
                                                                                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                                                                                ),
                                                        
                                                       // SizedBox(height: 8,),
                                                        Text(
                                                          resolver.resolveData(mission.technicalSupport, flightMissionsAll.dictTechnicalSupport, displayKey: 'name').toString(),
                                                          //all.geoFlightMission[index].technicalSupport.toString(),
                                                        maxLines: 2,
                                                                                style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                                                                                ),
                                                                                ),
                                                      
                                                                                ],),
                                                                          ]),
                                                                            ),
                                                                           
                                                                            
                                                                          ],),
                                                                          
                                                  ),
                                                                          
                                                                                          ],
                                                                                        ),
                                                                          
                                                                                    ),
                                                ),
                                              );
                            
                                            
                      },),
                        );

                     

    // TODO: implement buildSuggestions
    

      
    
  }
  
  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
   return [IconButton(
    icon: const Icon(Icons.clear),
    onPressed: () { 
      query = '';
      filteredMissions = [];
      }
    
   )
  ];
  }
}