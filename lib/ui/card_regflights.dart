import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_svg/svg.dart';

import '../untils/theme.dart';

class FlightsTaskCard extends StatelessWidget {
  const FlightsTaskCard({super.key,


    required this.ID,
    required this.num,
    required this.object,
    required this.date,
    required this.eventsState,
    required this.geoJson,
    required this.landGroupID,
    required this.operator,
    required this.state,
    required this.route,
    required this.routeArea,
    required this.routeLength,
    required this.flightGroupID,
    required this.flightDuration,
    required this.flightMissionType,
    required this.latLngStartCombined,
    required this.latitude,
    required this.longitude,
    required this.nearestLocality,
    required this.responsiblePerson,
    required this.technicalSupport,
    required this.updateInfo,
    required this.examinationDateFrom,
    required this.examinationDateTo,
    required this.examinationDatesCombined,
    required this.executionDeadlineDate,
    required this.executionInfo,
    required this.executionPlanDatesCombined,
    required this.executionPlanEndDate,
    required this.executionPlanStartDate,
    required this.meteorologicalData,
    required this.mi_createDate,
    required this.mi_createUser,
    required this.mi_modifyDate,
    required this.mi_modifyUser,
    required this.mi_owner
  }); 
  
  
  
  final int ID;
  final String? num;
  final int? object;
  final String? flightMissionType;
  final int? route ;
  final int? operator ;
  final double? latitude ;
  final double? longitude;
  final String? latLngStartCombined;
  final String? examinationDateFrom;
  final String? examinationDateTo ;
  final String? examinationDatesCombined;
  final String? date;
  final String? executionPlanStartDate;
  final String? executionPlanEndDate;
  final String? executionPlanDatesCombined;
  final String? executionDeadlineDate;
  final String? responsiblePerson;
  final String? geoJson;
  final int? executionInfo;
  final int? updateInfo;
  final int? technicalSupport;
  final String? meteorologicalData;
  final String? nearestLocality;
  final String? flightDuration;
  final double? routeLength;
  final double? routeArea;
  final String? state;
  final String? eventsState;
  final int? flightGroupID;
  final int? landGroupID;
  final int? mi_owner;
  final String? mi_createDate;
  final int? mi_createUser;
  final String? mi_modifyDate;
  final int? mi_modifyUser;







  
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
                margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                 height: MediaQuery.sizeOf(context).height*0.3,
                          width: MediaQuery.sizeOf(context).width,
                decoration: BoxDecoration(
                    color: FabColorDark,
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    
                  ),
                  child: Column(
                    children: [
    
                      Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: Row(
                          
                         crossAxisAlignment: CrossAxisAlignment.start,
                        
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                            
                            Container(
                        margin: const EdgeInsets.only(left: 16, top: 16),
                         width: 12,
                         height: 12,
                         decoration: const BoxDecoration(
                           color: SuccessColor,
                           shape: BoxShape.circle
                         ),
                            ),
                           
                            Padding(
                              padding: const EdgeInsets.only(top: 12),
                              child: Text('$state')),
                          Container(
                           
                            padding: const EdgeInsets.only(left: 135,right: 8),
                            child: FilledButton(
                         
                         onPressed: (){},
                         style: ButtonStyle(
                           fixedSize: MaterialStateProperty.all(const Size(25, 25)) ,
                               backgroundColor: MaterialStateProperty.all(
                                   Colors.grey.shade800),
                              
                                  
                                   
                               shape: MaterialStateProperty.all<
                                       RoundedRectangleBorder>(
                                   RoundedRectangleBorder(
                                       borderRadius:
                                           BorderRadius.circular(16.0)))),
                         child: const Center(child: Icon(IconlyLight.location)),
                        
                         )
                         
                            
                          ),
                        
                          ]),
                      ),
                      
                      Container(
                        
                        padding: const EdgeInsets.only(left: 16),
                        child: 
                        Row(children: [
                      
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                              
                              const AutoSizeText('Номер ПЗ',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                                                
                                               // SizedBox(height: 8,),
                                                Text('$num',
                                                maxLines: 2,
                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                            ),
                            ),
                                              
                            ],),
                          ),
                          
                          Container(
                        padding: const EdgeInsets.only(left: 16),
                        child: Row(children: [
                      
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                      
                            const AutoSizeText('Замовник',
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                        
                       // SizedBox(height: 8,),
                        Text('$mi_owner',
                        maxLines: 2,
                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                          ),
                          ),
                      
                          ],)
                      
                        ],), 
                      ),
                        ],),
                        
                      ),
    
                      Container(
                        padding: const EdgeInsets.only(left: 16, top: 16),
                        child: 
                        Row(children: [
                      
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                              
                              const AutoSizeText('Об’єкт',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                                                
                                               // SizedBox(height: 8,),
                                                AutoSizeText('$object',
                                                maxLines: 2,
                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                            ),
                            ),
                                              
                            ],),
                          ),
                      
                          Container(
                        padding: const EdgeInsets.only(left: 16),
                        child: Row(children: [
                      
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                      
                            const AutoSizeText('Відповідальна особа',
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                        
                   
                        Text('$responsiblePerson',
                        maxLines: 2,
                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                          ),
                          ),
                      
                          ],)
                      
                        ],), 
                      ),
                        ],),
                        
                      ),
                       Container(
                        padding: const EdgeInsets.only(left: 16, top: 16),
                        child: 
                        Row(children: [
                      
                          Container(
                            child: Row(
                              children: [
                                SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.white),
                                const SizedBox(width: 8,),
                               Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                               
                                const AutoSizeText('Технічні засоби',
                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                              ),
                                                  
                                                 // SizedBox(height: 8,),
                                                  Text('$technicalSupport',
                                                  maxLines: 2,
                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                              ),
                              ),
                                                
                              ],),
                        ]),
                          ),
                         
                          Container(
                        padding: const EdgeInsets.only(left: 10),
                        child: Row(children: [
                      
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                      
                            const AutoSizeText('Дата виконання',
                          style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                          maxLines: 2,
                          ),
                        
                       // SizedBox(height: 8,),
                        AutoSizeText('$executionDeadlineDate',
                        maxLines: 2,
                          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                          ),
                          ),
                      
                          ],)
                      
                        ],), 
                      ),
                        ],),
                        
                      ),
    
                    ],
                  ),
    
              ); 
    /*
    GestureDetector(
      onTap: () { Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => FlightMissionInfo(
                             ID: ID,
                             
      num: num,
      object: object,
      date: date,
      eventsState: eventsState,
      geoJson: geoJson,
      landGroupID: landGroupID,
      operator: operator,
      state: state,
      route: route,
      routeArea: routeArea,
      routeLength: routeLength,
      flightGroupID: flightGroupID,
      flightDuration: flightDuration,
      flightMissionType: flightMissionType,
      latLngStartCombined: latLngStartCombined,
      latitude: latitude,
      longitude: longitude,
      nearestLocality: nearestLocality,
      responsiblePerson: responsiblePerson,
      technicalSupport: technicalSupport,
      updateInfo: updateInfo,
      examinationDateFrom: examinationDateFrom,
      examinationDateTo: examinationDateTo,
      examinationDatesCombined: examinationDatesCombined,
      executionDeadlineDate: executionDeadlineDate,
      executionInfo: executionInfo,
      executionPlanDatesCombined: executionPlanDatesCombined,
      executionPlanEndDate: executionPlanEndDate,
      executionPlanStartDate: executionPlanStartDate,
      meteorologicalData: meteorologicalData,
      mi_createDate: mi_createDate,
      mi_createUser: mi_createUser,
      mi_modifyDate: mi_modifyDate,
      mi_modifyUser: mi_modifyUser,
      mi_owner: mi_owner,
                          )
      
                         ) );},
                          
                          
      child: 
      Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                 height: MediaQuery.sizeOf(context).height*0.3,
                          width: MediaQuery.sizeOf(context).width,
                decoration: BoxDecoration(
                    color: FabColorDark,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    
                  ),
                  child: Column(
                    children: [
    
                      Padding(
                        padding: EdgeInsets.only(top: 8),
                        child: Row(
                          
                         crossAxisAlignment: CrossAxisAlignment.start,
                        
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                            
                            Container(
                        margin: EdgeInsets.only(left: 16, top: 16),
                         width: 12,
                         height: 12,
                         decoration: BoxDecoration(
                           color: SuccessColor,
                           shape: BoxShape.circle
                         ),
                            ),
                           
                            Padding(
                              padding: EdgeInsets.only(top: 12),
                              child: Text('$state')),
                          Container(
                           
                            padding: EdgeInsets.only(left: 135,right: 8),
                            child: FilledButton(
                         
                         onPressed: (){},
                         style: ButtonStyle(
                           fixedSize: MaterialStateProperty.all(const Size(25, 25)) ,
                               backgroundColor: MaterialStateProperty.all(
                                   Colors.grey.shade800),
                              
                                  
                                   
                               shape: MaterialStateProperty.all<
                                       RoundedRectangleBorder>(
                                   RoundedRectangleBorder(
                                       borderRadius:
                                           BorderRadius.circular(16.0)))),
                         child: Center(child: Icon(IconlyLight.location)),
                        
                         )
                         
                            
                          ),
                        
                          ]),
                      ),
                      
                      Container(
                        
                        padding: EdgeInsets.only(left: 16),
                        child: 
                        Row(children: [
                      
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                              
                              AutoSizeText('Номер ПЗ',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                                                
                                               // SizedBox(height: 8,),
                                                Text('$num',
                                                maxLines: 2,
                            style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                            ),
                            ),
                                              
                            ],),
                          ),
                          
                          Container(
                        padding: EdgeInsets.only(left: 16),
                        child: Row(children: [
                      
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                      
                            AutoSizeText('Замовник',
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                        
                       // SizedBox(height: 8,),
                        Text('$mi_owner',
                        maxLines: 2,
                          style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                          ),
                          ),
                      
                          ],)
                      
                        ],), 
                      ),
                        ],),
                        
                      ),
    
                      Container(
                        padding: EdgeInsets.only(left: 16, top: 16),
                        child: 
                        Row(children: [
                      
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                              
                              AutoSizeText('Об’єкт',
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                                                
                                               // SizedBox(height: 8,),
                                                AutoSizeText('$object',
                                                maxLines: 2,
                            style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                            ),
                            ),
                                              
                            ],),
                          ),
                      
                          Container(
                        padding: EdgeInsets.only(left: 16),
                        child: Row(children: [
                      
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                      
                            AutoSizeText('Відповідальна особа',
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                        
                   
                        Text('$responsiblePerson',
                        maxLines: 2,
                          style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                          ),
                          ),
                      
                          ],)
                      
                        ],), 
                      ),
                        ],),
                        
                      ),
                       Container(
                        padding: EdgeInsets.only(left: 16, top: 16),
                        child: 
                        Row(children: [
                      
                          Container(
                            child: Row(
                              children: [
                                SvgPicture.asset('assets/images/vector/gis_drone.svg', color: Colors.white),
                                SizedBox(width: 8,),
                               Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                               
                                AutoSizeText('Технічні засоби',
                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                              ),
                                                  
                                                 // SizedBox(height: 8,),
                                                  Text('$technicalSupport',
                                                  maxLines: 2,
                              style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                              ),
                              ),
                                                
                              ],),
                        ]),
                          ),
                         
                          Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Row(children: [
                      
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                      
                            AutoSizeText('Дата виконання',
                          style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                          maxLines: 2,
                          ),
                        
                       // SizedBox(height: 8,),
                        AutoSizeText('$executionDeadlineDate',
                        maxLines: 2,
                          style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal,
                          ),
                          ),
                      
                          ],)
                      
                        ],), 
                      ),
                        ],),
                        
                      ),
    
                    ],
                  ),
    
              ),
    );
    */
  }
  

  
}








