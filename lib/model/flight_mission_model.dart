import 'package:json_annotation/json_annotation.dart';
part 'flight_mission_model.g.dart';




@JsonSerializable()
class flightMission {
  final int ID;
  final String? num;
  final int? object;
  final String? flightMissionType;
  final int? route ;
  final int? operator ;
  final double? latitude ;
  final double? longitude;
  final String? latLngStartCombined;
  final String? examinationDateFrom;
  final String? examinationDateTo ;
  final String? examinationDatesCombined;
  final String? date;
  final String? executionPlanStartDate;
  final String? executionPlanEndDate;
  final String? executionPlanDatesCombined;
  final String? executionDeadlineDate;
  final String? responsiblePerson;
  final String? geoJson;
  final int? executionInfo;
  final int? updateInfo;
  final int? technicalSupport;
  final String? meteorologicalData;
  final String? nearestLocality;
  final String? flightDuration;
  final double? routeLength;
  final int? routeArea;
  final String? state;
  final String? eventsState;
  final int? flightGroupID;
  final int? landGroupID;
  final int? mi_owner;
  final String? mi_createDate;
  final int? mi_createUser;
  final String? mi_modifyDate;
  final int? mi_modifyUser;

  const flightMission({
    required this.ID,
     this.num,
    this.object,
     this.date,
     this.eventsState,
    this.geoJson,
     this.landGroupID,
     this.operator,
     this.state,
    this.route,
     this.routeArea,
     this.routeLength,
     this.flightGroupID,
     this.flightDuration,
     this.flightMissionType,
     this.latLngStartCombined,
     this.latitude,
     this.longitude,
     this.nearestLocality,
     this.responsiblePerson,
     this.technicalSupport,
     this.updateInfo,
     this.examinationDateFrom,
     this.examinationDateTo,
     this.examinationDatesCombined,
     this.executionDeadlineDate,
     this.executionInfo,
     this.executionPlanDatesCombined,
     this.executionPlanEndDate,
     this.executionPlanStartDate,
    this.meteorologicalData,
     this.mi_createDate,
     this.mi_createUser,
    this.mi_modifyDate,
     this.mi_modifyUser,
     this.mi_owner
  


  });

  factory flightMission.fromJson(Map<String, dynamic> json) => _$flightMissionFromJson(json);
  Map<String, dynamic> toJson() => _$flightMissionToJson(this);
}
