// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model_all.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

All _$AllFromJson(Map<String, dynamic> json) => All(
      geoFlightMission: (json['geoFlightMission'] as List<dynamic>?)
          ?.map((e) => GeoFlightMission.fromJson(e as Map<String, dynamic>))
          .toList(),
      geoEvent: (json['geoEvent'] as List<dynamic>?)
          ?.map((e) => GeoEvent.fromJson(e as Map<String, dynamic>))
          .toList(),
      geoEventType: (json['geoEventType'] as List<dynamic>?)
          ?.map((e) => GeoEventType.fromJson(e as Map<String, dynamic>))
          .toList(),
      geoObject: (json['geoObject'] as List<dynamic>?)
          ?.map((e) => GeoObject.fromJson(e as Map<String, dynamic>))
          .toList(),
      geoFlightMissionType: (json['geoFlightMissionType'] as List<dynamic>?)
          ?.map((e) => GeoFlightMissionType.fromJson(e as Map<String, dynamic>))
          .toList(),
      geoRoute: (json['geoRoute'] as List<dynamic>?)
          ?.map((e) => GeoRoute.fromJson(e as Map<String, dynamic>))
          .toList(),
      geoOperator: (json['geoOperator'] as List<dynamic>?)
          ?.map((e) => GeoOperator.fromJson(e as Map<String, dynamic>))
          .toList(),
      geoExecutionStatus: (json['geoExecutionStatus'] as List<dynamic>?)
          ?.map((e) => GeoExecutionStatus.fromJson(e as Map<String, dynamic>))
          .toList(),
      geoAdjustmentStatus: (json['geoAdjustmentStatus'] as List<dynamic>?)
          ?.map((e) => GeoAdjustmentStatus.fromJson(e as Map<String, dynamic>))
          .toList(),
      dictTechnicalSupport: (json['dictTechnicalSupport'] as List<dynamic>?)
          ?.map((e) => DictTechnicalSupport.fromJson(e as Map<String, dynamic>))
          .toList(),
      geoFlightGroup: (json['geoFlightGroup'] as List<dynamic>?)
          ?.map((e) => GeoFlightGroup.fromJson(e as Map<String, dynamic>))
          .toList(),
      geoLandGroup: (json['geoLandGroup'] as List<dynamic>?)
          ?.map((e) => GeoLandGroup.fromJson(e as Map<String, dynamic>))
          .toList(),
      ubmEnum: (json['ubmEnum'] as List<dynamic>?)
          ?.map((e) => UbmEnum.fromJson(e as Map<String, dynamic>))
          .toList(),
      ubaUser: (json['ubaUser'] as List<dynamic>?)
          ?.map((e) => UbaUser.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$AllToJson(All instance) => <String, dynamic>{
      'geoFlightMission': instance.geoFlightMission,
      'geoEvent': instance.geoEvent,
      'geoEventType': instance.geoEventType,
      'geoObject': instance.geoObject,
      'geoFlightMissionType': instance.geoFlightMissionType,
      'geoRoute': instance.geoRoute,
      'geoOperator': instance.geoOperator,
      'geoExecutionStatus': instance.geoExecutionStatus,
      'geoAdjustmentStatus': instance.geoAdjustmentStatus,
      'dictTechnicalSupport': instance.dictTechnicalSupport,
      'geoFlightGroup': instance.geoFlightGroup,
      'geoLandGroup': instance.geoLandGroup,
      'ubmEnum': instance.ubmEnum,
      'ubaUser': instance.ubaUser,
    };

GeoFlightMission _$GeoFlightMissionFromJson(Map<String, dynamic> json) =>
    GeoFlightMission(
      ID: json['ID'] as int,
      num: json['num'] as String?,
      object: json['object'] as int?,
      flightMissionType: json['flightMissionType'] as int?,
      route: json['route'] as int?,
      geoOperator: json['operator'] as int?,
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      latLngStartCombined: json['latLngStartCombined'] as String?,
      examinationDateFrom: json['examinationDateFrom'] as String?,
      examinationDateTo: json['examinationDateTo'] as String?,
      examinationDatesCombined: json['examinationDatesCombined'] as String?,
      date: json['date'] as String?,
      executionPlanStartDate: json['executionPlanStartDate'] as String?,
      executionPlanEndDate: json['executionPlanEndDate'] as String?,
      executionPlanDatesCombined: json['executionPlanDatesCombined'] as String?,
      executionDeadlineDate: json['executionDeadlineDate'] as String?,
      responsiblePerson: json['responsiblePerson'] as String?,
      geoJson: json['geoJson'] as String?,
      executionInfo: json['executionInfo'] as int?,
      updateInfo: json['updateInfo'] as int?,
      technicalSupport: json['technicalSupport'] as int?,
      meteorologicalData: json['meteorologicalData'] as String?,
      nearestLocality: json['nearestLocality'] as String?,
      flightDuration: json['flightDuration'] as String?,
      routeLength: (json['routeLength'] as num?)?.toDouble(),
      routeArea: json['routeArea'] as int?,
      state: json['state'] as String,
      eventsState: json['eventsState'] as String?,
      flightGroupID: json['flightGroupID'] as int?,
      landGroupID: json['landGroupID'] as int?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoFlightMissionToJson(GeoFlightMission instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'num': instance.num,
      'object': instance.object,
      'flightMissionType': instance.flightMissionType,
      'route': instance.route,
      'operator': instance.geoOperator,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'latLngStartCombined': instance.latLngStartCombined,
      'examinationDateFrom': instance.examinationDateFrom,
      'examinationDateTo': instance.examinationDateTo,
      'examinationDatesCombined': instance.examinationDatesCombined,
      'date': instance.date,
      'executionPlanStartDate': instance.executionPlanStartDate,
      'executionPlanEndDate': instance.executionPlanEndDate,
      'executionPlanDatesCombined': instance.executionPlanDatesCombined,
      'executionDeadlineDate': instance.executionDeadlineDate,
      'responsiblePerson': instance.responsiblePerson,
      'geoJson': instance.geoJson,
      'executionInfo': instance.executionInfo,
      'updateInfo': instance.updateInfo,
      'technicalSupport': instance.technicalSupport,
      'meteorologicalData': instance.meteorologicalData,
      'nearestLocality': instance.nearestLocality,
      'flightDuration': instance.flightDuration,
      'routeLength': instance.routeLength,
      'routeArea': instance.routeArea,
      'state': instance.state,
      'eventsState': instance.eventsState,
      'flightGroupID': instance.flightGroupID,
      'landGroupID': instance.landGroupID,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

GeoEvent _$GeoEventFromJson(Map<String, dynamic> json) => GeoEvent(
      ID: json['ID'] as int?,
      num: json['num'] as String?,
      eventTypeID: json['eventTypeID'] as int?,
      eventDate: json['eventDate'] as String?,
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      description: json['description'] as String?,
      verifyDate: json['verifyDate'] as String?,
      verifySummary: json['verifySummary'] as String?,
      flightMissionID: json['flightMissionID'] as int?,
      state: json['state'] as String?,
      object: json['object'] as int?,
      geoOperator: json['geoOperator'] as int?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoEventToJson(GeoEvent instance) => <String, dynamic>{
      'ID': instance.ID,
      'num': instance.num,
      'eventTypeID': instance.eventTypeID,
      'eventDate': instance.eventDate,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'description': instance.description,
      'verifyDate': instance.verifyDate,
      'verifySummary': instance.verifySummary,
      'flightMissionID': instance.flightMissionID,
      'state': instance.state,
      'object': instance.object,
      'geoOperator': instance.geoOperator,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

GeoEventType _$GeoEventTypeFromJson(Map<String, dynamic> json) => GeoEventType(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      color: json['color'] as String?,
      icon: json['icon'] as String?,
      iconFile: json['iconFile'] as String?,
      iconDataUrl: json['iconDataUrl'] as String?,
      operatorID: json['operatorID'] as int?,
      objectId: json['objectId'] as int?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoEventTypeToJson(GeoEventType instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'color': instance.color,
      'icon': instance.icon,
      'iconFile': instance.iconFile,
      'iconDataUrl': instance.iconDataUrl,
      'operatorID': instance.operatorID,
      'objectId': instance.objectId,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

GeoObject _$GeoObjectFromJson(Map<String, dynamic> json) => GeoObject(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      operatorID: json['operatorID'] as int?,
      displayName: json['displayName'] as String?,
      attrValues: json['attrValues'] as String?,
      geoJson: json['geoJson'] as String?,
      color: json['color'] as String?,
      routeLength: json['routeLength'] as String?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoObjectToJson(GeoObject instance) => <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'operatorID': instance.operatorID,
      'displayName': instance.displayName,
      'attrValues': instance.attrValues,
      'geoJson': instance.geoJson,
      'color': instance.color,
      'routeLength': instance.routeLength,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

GeoFlightMissionType _$GeoFlightMissionTypeFromJson(
        Map<String, dynamic> json) =>
    GeoFlightMissionType(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      color: json['color'] as String?,
      operatorID: json['operatorID'] as int?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoFlightMissionTypeToJson(
        GeoFlightMissionType instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'color': instance.color,
      'operatorID': instance.operatorID,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

GeoRoute _$GeoRouteFromJson(Map<String, dynamic> json) => GeoRoute(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      objectID: json['objectID'] as int?,
      geoJson: json['geoJson'] as String?,
      color: json['color'] as String?,
      routeLength: json['routeLength'] as String?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoRouteToJson(GeoRoute instance) => <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'objectID': instance.objectID,
      'geoJson': instance.geoJson,
      'color': instance.color,
      'routeLength': instance.routeLength,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

GeoOperator _$GeoOperatorFromJson(Map<String, dynamic> json) => GeoOperator(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      code: json['code'] as String?,
      state: json['state'] as String?,
      phone: json['phone'] as String?,
      email: json['email'] as String?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoOperatorToJson(GeoOperator instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'code': instance.code,
      'state': instance.state,
      'phone': instance.phone,
      'email': instance.email,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

GeoExecutionStatus _$GeoExecutionStatusFromJson(Map<String, dynamic> json) =>
    GeoExecutionStatus(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoExecutionStatusToJson(GeoExecutionStatus instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

GeoAdjustmentStatus _$GeoAdjustmentStatusFromJson(Map<String, dynamic> json) =>
    GeoAdjustmentStatus(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoAdjustmentStatusToJson(
        GeoAdjustmentStatus instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

DictTechnicalSupport _$DictTechnicalSupportFromJson(
        Map<String, dynamic> json) =>
    DictTechnicalSupport(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$DictTechnicalSupportToJson(
        DictTechnicalSupport instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

GeoFlightGroup _$GeoFlightGroupFromJson(Map<String, dynamic> json) =>
    GeoFlightGroup(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoFlightGroupToJson(GeoFlightGroup instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

GeoLandGroup _$GeoLandGroupFromJson(Map<String, dynamic> json) => GeoLandGroup(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      operatorID: json['operatorID'] as int?,
      miOwner: json['miOwner'] as int?,
      miCreateDate: json['miCreateDate'] as String?,
      miCreateUser: json['miCreateUser'] as int?,
      miModifyDate: json['miModifyDate'] as String?,
      miModifyUser: json['miModifyUser'] as int?,
    );

Map<String, dynamic> _$GeoLandGroupToJson(GeoLandGroup instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'operatorID': instance.operatorID,
      'miOwner': instance.miOwner,
      'miCreateDate': instance.miCreateDate,
      'miCreateUser': instance.miCreateUser,
      'miModifyDate': instance.miModifyDate,
      'miModifyUser': instance.miModifyUser,
    };

UbmEnum _$UbmEnumFromJson(Map<String, dynamic> json) => UbmEnum(
      ID: json['ID'] as int?,
      code: json['code'] as String?,
      name: json['name'] as String?,
      eGroup: json['eGroup'] as String?,
    );

Map<String, dynamic> _$UbmEnumToJson(UbmEnum instance) => <String, dynamic>{
      'ID': instance.ID,
      'code': instance.code,
      'name': instance.name,
      'eGroup': instance.eGroup,
    };

UbaUser _$UbaUserFromJson(Map<String, dynamic> json) => UbaUser(
      ID: json['ID'] as int?,
      name: json['name'] as String?,
      fullName: json['fullName'] as String?,
    );

Map<String, dynamic> _$UbaUserToJson(UbaUser instance) => <String, dynamic>{
      'ID': instance.ID,
      'name': instance.name,
      'fullName': instance.fullName,
    };
