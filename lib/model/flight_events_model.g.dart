// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flight_events_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

flightEvent _$flightEventFromJson(Map<String, dynamic> json) => flightEvent(
      ID: json['ID'] as int,
      flightMissionID: json['flightMissionID'] as int?,
      num: json['num'] as String?,
      object: json['object'] as int?,
      eventsState: json['eventsState'] as String?,
      eventTypeID: json['eventTypeID'] as int?,
      eventDate: json['eventDate'] as String?,
      description: json['description'] as String?,
      verifyDate: json['verifyDate'] as String?,
      verifySummary: json['verifySummary'] as String?,
      operator: json['operator'] as int?,
      state: json['state'] as String?,
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      mi_createDate: json['mi_createDate'] as String?,
      mi_createUser: json['mi_createUser'] as int?,
      mi_modifyDate: json['mi_modifyDate'] as String?,
      mi_modifyUser: json['mi_modifyUser'] as int?,
      mi_owner: json['mi_owner'] as int?,
    );

Map<String, dynamic> _$flightEventToJson(flightEvent instance) =>
    <String, dynamic>{
      'ID': instance.ID,
      'num': instance.num,
      'eventDate': instance.eventDate,
      'object': instance.object,
      'flightMissionID': instance.flightMissionID,
      'eventTypeID': instance.eventTypeID,
      'operator': instance.operator,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'description': instance.description,
      'verifyDate': instance.verifyDate,
      'verifySummary': instance.verifySummary,
      'state': instance.state,
      'eventsState': instance.eventsState,
      'mi_owner': instance.mi_owner,
      'mi_createDate': instance.mi_createDate,
      'mi_createUser': instance.mi_createUser,
      'mi_modifyDate': instance.mi_modifyDate,
      'mi_modifyUser': instance.mi_modifyUser,
    };
