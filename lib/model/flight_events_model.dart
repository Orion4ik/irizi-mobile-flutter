import 'package:json_annotation/json_annotation.dart';
part 'flight_events_model.g.dart';


@JsonSerializable()
class flightEvent {
  final int ID;
  final String? num;
  final String? eventDate;
  final int? object;
  final int? flightMissionID;
  final int? eventTypeID;
  final int? operator ;
  final double? latitude ;
  final double? longitude;
  final String? description;
  final String? verifyDate;
  final String? verifySummary;
  final String? state;
  final String? eventsState;
  final int? mi_owner;
  final String? mi_createDate;
  final int? mi_createUser;
  final String? mi_modifyDate;
  final int? mi_modifyUser;

  const flightEvent({
    required this.ID,
    this.flightMissionID,
     this.num,
    this.object,
     this.eventsState,
     this.eventTypeID,
     this.eventDate,
     this.description,
     this.verifyDate,
     this.verifySummary,
     this.operator,
     this.state,
     this.latitude,
     this.longitude,
     this.mi_createDate,
     this.mi_createUser,
    this.mi_modifyDate,
     this.mi_modifyUser,
     this.mi_owner
  


  });

  factory flightEvent.fromJson(Map<String, dynamic> json) => _$flightEventFromJson(json);
  Map<String, dynamic> toJson() => _$flightEventToJson(this);
}

