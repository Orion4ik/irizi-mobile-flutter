

import 'package:json_annotation/json_annotation.dart';

import '../db/flight_missions_isar.dart';
part 'model_all.g.dart';

@JsonSerializable()
class All {
  All({
    required this.geoFlightMission,
    required this.geoEvent,
    required this.geoEventType,
    required this.geoObject,
    required this.geoFlightMissionType,
    required this.geoRoute,
    required this.geoOperator,
    required this.geoExecutionStatus,
    required this.geoAdjustmentStatus,
    required this.dictTechnicalSupport,
    required this.geoFlightGroup,
    required this.geoLandGroup,
    required this.ubmEnum,
    required this.ubaUser,
  });
  late final List<GeoFlightMission>? geoFlightMission;
  late final List<GeoEvent>? geoEvent;
  late final List<GeoEventType>? geoEventType;
  late final List<GeoObject>? geoObject;
  late final List<GeoFlightMissionType>? geoFlightMissionType;
  late final List<GeoRoute>? geoRoute;
  late final List<GeoOperator>? geoOperator;
  late final List<GeoExecutionStatus>? geoExecutionStatus;
  late final List<GeoAdjustmentStatus>? geoAdjustmentStatus;
  late final List<DictTechnicalSupport>? dictTechnicalSupport;
  late final List<GeoFlightGroup>? geoFlightGroup;
  late final List<GeoLandGroup>? geoLandGroup;
  late final List<UbmEnum>? ubmEnum;
  late final List<UbaUser>? ubaUser;
  
 
  All.fromJson(Map<String, dynamic> json){
    geoFlightMission = List.from(json['geo_flight_mission']).map((e)=>GeoFlightMission.fromJson(e)).toList();
    geoEvent = List.from(json['geo_event']).map((e)=>GeoEvent.fromJson(e)).toList();
    geoEventType = List.from(json['geo_event_type']).map((e)=>GeoEventType.fromJson(e)).toList();
    geoObject = List.from(json['geo_object']).map((e)=>GeoObject.fromJson(e)).toList();
    geoFlightMissionType = List.from(json['geo_flight_mission_type']).map((e)=>GeoFlightMissionType.fromJson(e)).toList();
    geoRoute = List.from(json['geo_route']).map((e)=>GeoRoute.fromJson(e)).toList();
    geoOperator = List.from(json['geo_operator']).map((e)=>GeoOperator.fromJson(e)).toList();
    geoExecutionStatus = List.from(json['geo_execution_status']).map((e)=>GeoExecutionStatus.fromJson(e)).toList();
    geoAdjustmentStatus = List.from(json['geo_adjustment_status']).map((e)=>GeoAdjustmentStatus.fromJson(e)).toList();
    dictTechnicalSupport = List.from(json['dict_technical_support']).map((e)=>DictTechnicalSupport.fromJson(e)).toList();
    geoFlightGroup = List.from(json['geo_flight_group']).map((e)=>GeoFlightGroup.fromJson(e)).toList();
    geoLandGroup = List.from(json['geo_land_group']).map((e)=>GeoLandGroup.fromJson(e)).toList();
    ubmEnum = List.from(json['ubm_enum']).map((e)=>UbmEnum.fromJson(e)).toList();
    ubaUser = List.from(json['uba_user']).map((e)=>UbaUser.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['geo_flight_mission'] = geoFlightMission!.map((e)=>e.toJson()).toList();
    data['geo_event'] = geoEvent!.map((e)=>e.toJson()).toList();
    data['geo_event_type'] = geoEventType!.map((e)=>e.toJson()).toList();
    data['geo_object'] = geoObject!.map((e)=>e.toJson()).toList();
    data['geo_flight_mission_type'] = geoFlightMissionType!.map((e)=>e.toJson()).toList();
    data['geo_route'] = geoRoute!.map((e)=>e.toJson()).toList();
    data['geo_operator'] = geoOperator!.map((e)=>e.toJson()).toList();
    data['geo_execution_status'] = geoExecutionStatus!.map((e)=>e.toJson()).toList();
    data['geo_adjustment_status'] = geoAdjustmentStatus!.map((e)=>e.toJson()).toList();
    data['dict_technical_support'] = dictTechnicalSupport!.map((e)=>e.toJson()).toList();
    data['geo_flight_group'] = geoFlightGroup!.map((e)=>e.toJson()).toList();
    data['geo_land_group'] = geoLandGroup!.map((e)=>e.toJson()).toList();
    data['ubm_enum'] = ubmEnum!.map((e)=>e.toJson()).toList();
    data['uba_user'] = ubaUser!.map((e)=>e.toJson()).toList();
    return data;
  }

//factory All.fromJson(Map<String, dynamic> json) => _$AllFromJson(json);
 // Map<String, dynamic> toJson() => _$AllToJson(this);
}
@JsonSerializable()
class GeoFlightMission {
  GeoFlightMission({
    required this.ID,
     this.num,
    required this.object,
     this.flightMissionType,
     this.route,
     this.geoOperator,
     this.latitude,
     this.longitude,
     this.latLngStartCombined,
     this.examinationDateFrom,
     this.examinationDateTo,
     this.examinationDatesCombined,
     this.date,
     this.executionPlanStartDate,
     this.executionPlanEndDate,
     this.executionPlanDatesCombined,
     this.executionDeadlineDate,
     this.responsiblePerson,
     this.geoJson,
     this.executionInfo,
     this.updateInfo,
     this.technicalSupport,
     this.meteorologicalData,
     this.nearestLocality,
     this.flightDuration,
    required this.routeLength,
    required this.routeArea,
    required this.state,
     this.eventsState,
     this.flightGroupID,
     this.landGroupID,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int ID;
  late final String? num;
  late final int? object;
  late final int? flightMissionType;
  late final int? route;
  late final int? geoOperator;
  late final double? latitude;
  late final double? longitude;
  late final String? latLngStartCombined;
  late final String? examinationDateFrom;
  late final String? examinationDateTo;
  late final String? examinationDatesCombined;
  late final String? date;
  late final String? executionPlanStartDate;
  late final String? executionPlanEndDate;
  late final String? executionPlanDatesCombined;
  late final String? executionDeadlineDate;
  late final String? responsiblePerson;
  late final String? geoJson;
  late final int? executionInfo;
  late final int? updateInfo;
  late final int? technicalSupport;
  late final String? meteorologicalData;
  late final String? nearestLocality;
  late final String? flightDuration;
  late final double? routeLength;
  late final int? routeArea;
  late final String state;
  late final String? eventsState;
  late final int? flightGroupID;
  late final int? landGroupID;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
  
  dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'num':
        return num;
      default:
        return null;
    }
  }


  GeoFlightMission.fromIsar(GeoFlightMissionIsar isarData) {
    
    ID = isarData.ID!;
    num = isarData.num;
    object = isarData.object;
    flightMissionType = isarData.flightMissionType;
    route = isarData.route;
    geoOperator = isarData.operator;
    latitude = isarData.latitude; 
    longitude = isarData.longitude ;
    latLngStartCombined = isarData.latLngStartCombined; 
    examinationDateFrom = isarData.examinationDateFrom ;
    examinationDateTo = isarData.examinationDateTo ;
    examinationDatesCombined = isarData.examinationDatesCombined ;
    date = isarData.date;
    executionPlanStartDate = isarData.executionPlanStartDate ;
    executionPlanEndDate = isarData.executionPlanEndDate ;
    executionPlanDatesCombined = isarData.executionPlanDatesCombined ;
    executionDeadlineDate = isarData.executionDeadlineDate ;
    responsiblePerson = isarData.responsiblePerson ;
    geoJson = isarData.geoJson;
    executionInfo = isarData.executionInfo;
    updateInfo = isarData.updateInfo;
    technicalSupport = isarData.technicalSupport;
    meteorologicalData = isarData.meteorologicalData;
    nearestLocality = isarData.nearestLocality;
    flightDuration = isarData.flightDuration;
    routeLength = isarData.routeLength ;
    routeArea = isarData.routeArea;
    state = isarData.state!;
    eventsState = isarData.eventsState;
    flightGroupID = isarData.flightGroupID;
    landGroupID = isarData.landGroupID;
    miOwner = isarData.miOwner;
    miCreateDate = isarData.miCreateDate;
    miCreateUser = isarData.miCreateUser;
    miModifyDate = isarData.miModifyDate;
    miModifyUser = isarData.miModifyUser;

    // Set other fields accordingly
  }

/*
  GeoFlightMission.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    num = json['num'];
    object = json['object'];
    flightMissionType = json['flightMissionType'];
    route = json['route'];
    geoOperator = json['operator'];
    latitude = json['latitude']; 
    longitude = json['longitude'] ;
    latLngStartCombined = json['latLngStartCombined']; 
    examinationDateFrom = json['examinationDateFrom'] ;
    examinationDateTo = json['examinationDateTo']  ;
    examinationDatesCombined = json['examinationDatesCombined'] ;
    date = json['date'];
    executionPlanStartDate = json['executionPlanStartDate'] ;
    executionPlanEndDate = json['executionPlanEndDate'] ;
    executionPlanDatesCombined = json['executionPlanDatesCombined'] ;
    executionDeadlineDate = json['executionDeadlineDate'] ;
    responsiblePerson = json['responsiblePerson']  ;
    geoJson = json['geoJson'];
    executionInfo = json['executionInfo'];
    updateInfo = json['updateInfo'];
    technicalSupport = json['technicalSupport'];
    meteorologicalData = json['meteorologicalData'];
    nearestLocality = json['nearestLocality'];
    flightDuration = json['flightDuration'];
    routeLength = json['routeLength'] ;
    routeArea = json['routeArea'];
    state = json['state'];
    eventsState = json['eventsState'];
    flightGroupID = json['flightGroupID'];
    landGroupID = json['landGroupID'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['ID'] = ID;
    _data['num'] = num;
    _data['object'] = object;
    _data['flightMissionType'] = flightMissionType;
    _data['route'] = route;
    _data['operator'] = geoOperator;
    _data['latitude'] = latitude;
    _data['longitude'] = longitude;
    _data['latLngStartCombined'] = latLngStartCombined;
    _data['examinationDateFrom'] = examinationDateFrom;
    _data['examinationDateTo'] = examinationDateTo;
    _data['examinationDatesCombined'] = examinationDatesCombined;
    _data['date'] = date;
    _data['executionPlanStartDate'] = executionPlanStartDate;
    _data['executionPlanEndDate'] = executionPlanEndDate;
    _data['executionPlanDatesCombined'] = executionPlanDatesCombined;
    _data['executionDeadlineDate'] = executionDeadlineDate;
    _data['responsiblePerson'] = responsiblePerson;
    _data['geoJson'] = geoJson;
    _data['executionInfo'] = executionInfo;
    _data['updateInfo'] = updateInfo;
    _data['technicalSupport'] = technicalSupport;
    _data['meteorologicalData'] = meteorologicalData;
    _data['nearestLocality'] = nearestLocality;
    _data['flightDuration'] = flightDuration;
    _data['routeLength'] = routeLength;
    _data['routeArea'] = routeArea;
    _data['state'] = state;
    _data['eventsState'] = eventsState;
    _data['flightGroupID'] = flightGroupID;
    _data['landGroupID'] = landGroupID;
    _data['mi_owner'] = miOwner;
    _data['mi_createDate'] = miCreateDate;
    _data['mi_createUser'] = miCreateUser;
    _data['mi_modifyDate'] = miModifyDate;
    _data['mi_modifyUser'] = miModifyUser;
    return _data;
  }
*/

factory GeoFlightMission.fromJson(Map<String, dynamic> json) => _$GeoFlightMissionFromJson(json);
  Map<String, dynamic> toJson() => _$GeoFlightMissionToJson(this);

}

@JsonSerializable()
class GeoEvent {
  GeoEvent({
    required this.ID,
    required this.num,
    required this.eventTypeID,
    required this.eventDate,
    required this.latitude,
    required this.longitude,
    required this.description,
     this.verifyDate,
     this.verifySummary,
    required this.flightMissionID,
    required this.state,
    required this.object,
     this.geoOperator,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? num;
  late final int? eventTypeID;
  late final String? eventDate;
  late final double? latitude;
  late final double? longitude;
  late final String? description;
  late final String? verifyDate;
  late final String? verifySummary;
  late final int? flightMissionID;
  late final String? state;
  late final int? object;
  late final int? geoOperator;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
  
  dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'num':
        return num;
      default:
        return null;
    }
  }
  
  /*
  GeoEvent.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    num = json['num'];
    eventTypeID = json['eventTypeID'];
    eventDate = json['eventDate'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    description = json['description'];
    verifyDate = json['verifyDate'];
    verifySummary = json['verifySummary'];
    flightMissionID = json['flightMissionID'];
    state = json['state'];
    object = json['object'];
    geoOperator = json['operator'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['num'] = num;
    data['eventTypeID'] = eventTypeID;
    data['eventDate'] = eventDate;
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['description'] = description;
    data['verifyDate'] = verifyDate;
    data['verifySummary'] = verifySummary;
    data['flightMissionID'] = flightMissionID;
    data['state'] = state;
    data['object'] = object;
    data['operator'] = geoOperator;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }
  */

factory GeoEvent.fromJson(Map<String, dynamic> json) => _$GeoEventFromJson(json);
  Map<String, dynamic> toJson() => _$GeoEventToJson(this);


}

@JsonSerializable()
class GeoEventType {
  GeoEventType({
    required this.ID,
    required this.name,
    required this.color,
     this.icon,
     this.iconFile,
     this.iconDataUrl,
    required this.operatorID,
    required this.objectId,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? name;
  late final String? color;
  late final String? icon;
  late final String? iconFile;
  late final String? iconDataUrl;
  late final int? operatorID;
  late final int? objectId;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
  
  dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'name':
        return name;
      default:
        return null;
    }
  }
  
  GeoEventType.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    color = json['color'];
    icon = json['icon'];
    iconFile = json['iconFile'];
    iconDataUrl = json['iconDataUrl'];
    operatorID = json['operatorID'];
    objectId = json['objectId'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['name'] = name;
    data['color'] = color;
    data['icon'] = icon;
    data['iconFile'] = iconFile;
    data['iconDataUrl'] = iconDataUrl;
    data['operatorID'] = operatorID;
    data['objectId'] = objectId;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }

//factory GeoEventType.fromJson(Map<String, dynamic> json) =>   _$GeoEventTypeFromJson(json);
//  Map<String, dynamic> toJson() => _$GeoEventTypeToJson(this);
}

@JsonSerializable()
class GeoObject {
  GeoObject({
    required this.ID,
    required this.name,
    required this.operatorID,
    required this.displayName,
    required this.attrValues,
    required this.geoJson,
    required this.color,
    required this.routeLength,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? name;
  late final int? operatorID;
  late final String? displayName;
  late final String? attrValues;
  late final String? geoJson;
  late final String? color;
  late final String? routeLength;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
    dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'name':
        return name;
      default:
        return null;
    }
  }
 
  GeoObject.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    operatorID = json['operatorID'];
    displayName = json['displayName'];
    attrValues = json['attrValues'];
    geoJson = json['geoJson'];
    color = json['color'];
    routeLength = json['routeLength'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['name'] = name;
    data['operatorID'] = operatorID;
    data['displayName'] = displayName;
    data['attrValues'] = attrValues;
    data['geoJson'] = geoJson;
    data['color'] = color;
    data['routeLength'] = routeLength;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }

//factory GeoObject.fromJson(Map<String, dynamic> json) => _$GeoObjectFromJson(json);
//  Map<String, dynamic> toJson() => _$GeoObjectToJson(this);

}

@JsonSerializable()
class GeoFlightMissionType {
  GeoFlightMissionType({
    required this.ID,
    required this.name,
    required this.color,
    required this.operatorID,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? name;
  late final String? color;
  late final int? operatorID;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
  
  dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'name':
        return name;
      default:
        return null;
    }
  }
  
  GeoFlightMissionType.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    color = json['color'];
    operatorID = json['operatorID'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['name'] = name;
    data['color'] = color;
    data['operatorID'] = operatorID;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }

//factory GeoFlightMissionType.fromJson(Map<String, dynamic> json) => _$GeoFlightMissionTypeFromJson(json);
//  Map<String, dynamic> toJson() => _$GeoFlightMissionTypeToJson(this);

}

@JsonSerializable()
class GeoRoute {
  GeoRoute({
    required this.ID,
    required this.name,
    required this.objectID,
     this.geoJson,
    required this.color,
    required this.routeLength,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? name;
  late final int? objectID;
  late final String? geoJson;
  late final String? color;
  late final String? routeLength;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;

  dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'name':
        return name;
      default:
        return null;
    }
  }
  
  GeoRoute.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    objectID = json['objectID'];
    geoJson = json['geoJson'];
    color = json['color'];
    routeLength = json['routeLength'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['name'] = name;
    data['objectID'] = objectID;
    data['geoJson'] = geoJson;
    data['color'] = color;
    data['routeLength'] = routeLength;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }


//factory GeoRoute.fromJson(Map<String, dynamic> json) => _$GeoRouteFromJson(json);
 // Map<String, dynamic> toJson() => _$GeoRouteToJson(this);
}

@JsonSerializable()
class GeoOperator {
  GeoOperator({
    required this.ID,
    required this.name,
    required this.code,
    required this.state,
     this.phone,
     this.email,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? name;
  late final String? code;
  late final String? state;
  late final String? phone;
  late final String? email;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
  dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'name':
        return name;
      default:
        return null;
    }
  }

  GeoOperator.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    code = json['code'];
    state = json['state'];
    phone = json['phone'];
    email = json['email'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['name'] = name;
    data['code'] = code;
    data['state'] = state;
    data['phone'] = phone;
    data['email'] = email;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }

//factory GeoOperator.fromJson(Map<String, dynamic> json) =>  _$GeoOperatorFromJson(json);
 // Map<String, dynamic> toJson() => _$GeoOperatorToJson(this);

}

@JsonSerializable()
class GeoExecutionStatus {
  GeoExecutionStatus({
    required this.ID,
    required this.name,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? name;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
  dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'name':
        return name;
      default:
        return null;
    }
  }
  
  GeoExecutionStatus.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }
  
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['name'] = name;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }

//factory GeoExecutionStatus.fromJson(Map<String, dynamic> json) => _$GeoExecutionStatusFromJson(json);
 // Map<String, dynamic> toJson() => _$GeoExecutionStatusToJson(this);

}

@JsonSerializable()
class GeoAdjustmentStatus {
  GeoAdjustmentStatus({
    required this.ID,
    required this.name,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? name;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;

  GeoAdjustmentStatus.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['name'] = name;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }

//factory GeoAdjustmentStatus.fromJson(Map<String, dynamic> json) =>  _$GeoAdjustmentStatusFromJson(json);
 // Map<String, dynamic> toJson() => _$GeoAdjustmentStatusToJson(this);

}

@JsonSerializable()
class DictTechnicalSupport {
  DictTechnicalSupport({
    required this.ID,
    required this.name,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? name;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
  dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'name':
        return name;
      default:
        return null;
    }
  }
  
  DictTechnicalSupport.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['name'] = name;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }

//factory DictTechnicalSupport.fromJson(Map<String, dynamic> json) =>  _$DictTechnicalSupportFromJson(json);
 // Map<String, dynamic> toJson() =>  _$DictTechnicalSupportToJson(this);

}
@JsonSerializable()
class GeoFlightGroup {
  GeoFlightGroup({
    required this.ID,
    required this.name,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? name;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
  dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'name':
        return name;
      default:
        return null;
    }
  }
  
  GeoFlightGroup.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['name'] = name;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }

//factory GeoFlightGroup.fromJson(Map<String, dynamic> json) => _$GeoFlightGroupFromJson(json);
//  Map<String, dynamic> toJson() => _$GeoFlightGroupToJson(this);

}
@JsonSerializable()
class GeoLandGroup {
  GeoLandGroup({
    required this.ID,
    required this.name,
    required this.operatorID,
    required this.miOwner,
    required this.miCreateDate,
    required this.miCreateUser,
    required this.miModifyDate,
    required this.miModifyUser,
  });
  late final int? ID;
  late final String? name;
  late final int? operatorID;
  late final int? miOwner;
  late final String? miCreateDate;
  late final int? miCreateUser;
  late final String? miModifyDate;
  late final int? miModifyUser;
  dynamic operator [](String key) {
    switch (key) {
      case 'ID':
        return ID;
      case 'name':
        return name;
      default:
        return null;
    }
  }
  
  GeoLandGroup.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    operatorID = json['operatorID'];
    miOwner = json['mi_owner'];
    miCreateDate = json['mi_createDate'];
    miCreateUser = json['mi_createUser'];
    miModifyDate = json['mi_modifyDate'];
    miModifyUser = json['mi_modifyUser'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = ID;
    data['name'] = name;
    data['operatorID'] = operatorID;
    data['mi_owner'] = miOwner;
    data['mi_createDate'] = miCreateDate;
    data['mi_createUser'] = miCreateUser;
    data['mi_modifyDate'] = miModifyDate;
    data['mi_modifyUser'] = miModifyUser;
    return data;
  }

//factory GeoLandGroup.fromJson(Map<String, dynamic> json) => _$GeoLandGroupFromJson(json);
 // Map<String, dynamic> toJson() => _$GeoLandGroupToJson(this);
}
@JsonSerializable()
class UbmEnum {
  UbmEnum({
    required this.ID,
    required this.code,
    required this.name,
    required this.eGroup,
  });

  late final int? ID;
  late final String? code;
  late final String? name;
  late final String? eGroup;

  dynamic operator [](String key) {
    switch (key) {
      case 'code':
        return code;
      case 'name':
        return name;
      default:
        return null;
    }
  }
  
  
  UbmEnum.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    code = json['code'];
    name = json['name'];
    eGroup = json['eGroup'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data ['ID'] = ID;
    data['code'] = code;
    data['name'] = name;
    data['eGroup'] = eGroup;
    return data;
  }
  

/*
factory UbmEnum.fromJson(Map<String, dynamic> json) => _$UbmEnumFromJson(json);
  Map<String, dynamic> toJson() => _$UbmEnumToJson(this);
*/
}
@JsonSerializable()
class UbaUser {
  UbaUser({
    required this.ID,
    required this.name,
     this.fullName,
  });
   late final int? ID;
  late final String? name;
  late final String? fullName;
  
  
  
  UbaUser.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    name = json['name'];
    fullName =json['fullName'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data ['ID'] = ID;
    data['name'] = name;
    data['fullName'] = fullName;
    return data;
  }


/*
factory UbaUser.fromJson(Map<String, dynamic> json) => _$UbaUserFromJson(json);
 Map<String, dynamic> toJson() => _$UbaUserToJson(this);
*/
}