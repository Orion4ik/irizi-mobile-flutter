

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:irizi/db/geo_adjustment_status_isar.dart';
import 'package:irizi/db/geo_execution_status_isar.dart';
import 'package:irizi/db/geo_flight_group_isar.dart';
import 'package:irizi/db/geo_flight_mission_type_isar.dart';
import 'package:irizi/db/geo_land_group_isar.dart';
import 'package:irizi/db/geo_objects_isar.dart';
import 'package:irizi/db/geo_operator_isar.dart';
import 'package:irizi/db/geo_route_isar.dart';
import 'package:irizi/db/material_flight_event_isar.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';

import '../../db/dict_technical_support_isar.dart';
import '../../db/flight_events_isar.dart';
import '../../db/flight_missions_isar.dart';
import '../../db/geo_event_type_isar.dart';
import '../../db/material_flight_mission_isar.dart';
import '../../db/uba_user_isar.dart';
import '../../db/udb_enums_isar.dart';
import '../../model/model_all.dart';
import '../../service/retrofit/api_service.dart';

class IsarRepository {

late Future<Isar> db;
//late final apiService;

  

 IsarRepository(){
  db = openDB();
 // apiService = openAPI ();
 }




Future<All> openAPI () async{
final api =  RestClient(Dio(BaseOptions(contentType: "application/json")));
final All apiItems = await api.getAll();
final List<GeoFlightMission>? flightMissions = apiItems.geoFlightMission;
final List<GeoEvent>? geoEvent = apiItems.geoEvent;
final List<GeoEventType>? geoEventType = apiItems.geoEventType;
final List<GeoObject>? geoObject = apiItems.geoObject ;
final List<GeoFlightMissionType>? geoFlightMissionType = apiItems.geoFlightMissionType;
final List<GeoRoute>? geoRoute = apiItems.geoRoute;
final List<GeoOperator>? geoOperator = apiItems.geoOperator; 
final List<GeoExecutionStatus>? geoExecutionStatus = apiItems.geoExecutionStatus;
final List<GeoAdjustmentStatus>? geoAdjustmentStatus = apiItems.geoAdjustmentStatus;
final List<DictTechnicalSupport>? dictTechnicalSupport = apiItems.dictTechnicalSupport;
final List<GeoFlightGroup>? geoFlightGroup = apiItems.geoFlightGroup;
final List<GeoLandGroup>? geoLandGroup = apiItems.geoLandGroup;
final List<UbmEnum>? ubmEnum = apiItems.ubmEnum;
final List<UbaUser>? ubaUser = apiItems.ubaUser;

 //Convert JSON to API to List
      List<Map<String, dynamic>> flightMissionMaps = flightMissions!.map((flightMission) {
      return flightMission.toJson();
      }).toList();
       List<Map<String, dynamic>> geoEventMaps = geoEvent!.map((geoEvent) {
      return geoEvent.toJson();
      }).toList();

 List<Map<String, dynamic>> geoEventTypeMaps = geoEventType!.map((geoEventType) {
      return geoEventType.toJson();
      }).toList();

 List<Map<String, dynamic>> geoObjectMaps = geoObject!.map((geoObject) {
      return geoObject.toJson();
      }).toList();

 List<Map<String, dynamic>> geoFlightMissionTypeMaps = geoFlightMissionType!.map((geoFlightMissionType) {
      return geoFlightMissionType.toJson();
      }).toList();

 List<Map<String, dynamic>> geoRouteMaps = geoRoute!.map((geoRoute) {
      return geoRoute.toJson();
      }).toList();

 List<Map<String, dynamic>> geoOperatorMaps = geoOperator!.map((geoOperator) {
      return geoOperator.toJson();
      }).toList();

 List<Map<String, dynamic>> geoAdjustmentStatusMaps = geoAdjustmentStatus!.map((geoAdjustmentStatus) {
      return geoAdjustmentStatus.toJson();
      }).toList();

       List<Map<String, dynamic>> geoExecutionStatusMaps = geoExecutionStatus!.map((geoExecutionStatus) {
      return geoExecutionStatus.toJson();
      }).toList();

 List<Map<String, dynamic>> dictTechnicalSupportMaps = dictTechnicalSupport!.map((dictTechnicalSupport) {
      return dictTechnicalSupport.toJson();
      }).toList();

 List<Map<String, dynamic>> geoFlightGroupMaps = geoFlightGroup!.map((geoFlightGroup) {
      return geoFlightGroup.toJson();
      }).toList();

 List<Map<String, dynamic>> geoLandGroupMaps = geoLandGroup!.map((geoLandGroup) {
      return geoLandGroup.toJson();
      }).toList();

 List<Map<String, dynamic>> ubmEnumMaps = ubmEnum!.map((ubmEnum) {
      return ubmEnum.toJson();
      }).toList();

 List<Map<String, dynamic>> ubaUserMaps = ubaUser!.map((ubaUser) {
      return ubaUser.toJson();
      }).toList();

 
  // Save items to the Isar database
        addGeoFlightMissionsfromJson(flightMissionMaps);
        addgeoEventTypefromJson(geoEventTypeMaps);
        addgeoAdjustmentStatusfromJson(geoAdjustmentStatusMaps);
        addgeoExecutionStatusfromJson(geoExecutionStatusMaps);
        addgeoFlightGroupfromJson(geoFlightGroupMaps);
        addgeoObjectfromJson(geoObjectMaps);
        addgeoOperatorfromJson(geoOperatorMaps);
        addgeoRoutefromJson(geoRouteMaps);
        adddictTechnicalSupportfromJson(dictTechnicalSupportMaps);
        addgeoEventfromJson(geoEventMaps);
        addgeoFlightMissionTypefromJson(geoFlightMissionTypeMaps);
        addgeoLandGroupfromJson(geoLandGroupMaps);
        addubaUserfromJson(ubaUserMaps);
        addubmEnumfromJson(ubmEnumMaps);

        final flightMissionConvert =  getGeoFlightMissionFromIsar();
        final geoEventTypeConvert =  getgeoEventTypeFromIsar();
        final geoAdjustmentStatusConvert =  getgeoAdjustmentStatusFromIsar();
        final geoExecutionStatusConvert =  getgeoExecutionStatusFromIsar();
        final geoFlightGroupConvert =  getgeoFlightGroupFromIsar();
        final geoObjectConvert =  getgeoObjectFromIsar();
        final geoOperatorConvert =  getgeoOperatorFromIsar();
        final geoRouteConvert =  getgeoRouteFromIsar();
        final dictTechnicalSupportConvert =  getdictTechnicalSupportFromIsar();
        final geoEventConvert =  getgeoEventFromIsar();
        final geoFlightMissionTypeConvert =  getgeoFlightMissionTypeFromIsar();
        final geoLandGroupConvert =  getgeoLandGroupFromIsar();
        final ubaUserConvert =  getubaUserFromIsar();
        final ubmEnumConvert =  getubmEnumFromIsar();



        
        
       List<GeoFlightMission> flightMissionsConverted = await flightMissionConvert;
       List<GeoEventType> geoEventTypeConverted = await geoEventTypeConvert;
       List<GeoAdjustmentStatus> geoAdjustmentStatusConverted = await geoAdjustmentStatusConvert;
       List<GeoExecutionStatus> geoExecutionStatusConverted = await geoExecutionStatusConvert;
       List<GeoFlightGroup> geoFlightGroupConverted = await geoFlightGroupConvert;
       List<GeoObject> geoObjectConverted = await geoObjectConvert;
       List<GeoOperator> geoOperatorConverted = await geoOperatorConvert;
       List<GeoRoute> geoRouteConverted = await geoRouteConvert;
       List<DictTechnicalSupport> dictTechnicalSupportConverted = await dictTechnicalSupportConvert;
       List<GeoEvent> geoEventConverted = await geoEventConvert;
       List<GeoFlightMissionType> geoFlightMissionTypeConverted = await geoFlightMissionTypeConvert;
       List<GeoLandGroup> geoLandGroupConverted = await geoLandGroupConvert;
       List<UbaUser> ubaUserConverted = await ubaUserConvert;
       List<UbmEnum> ubmEnumConverted = await ubmEnumConvert;

        All allMap = new All(
          geoFlightMission: flightMissionsConverted,
          dictTechnicalSupport:dictTechnicalSupportConverted,
          geoEvent:  geoEventConverted,
          geoEventType: geoEventTypeConverted,
          geoAdjustmentStatus: geoAdjustmentStatusConverted,
          geoExecutionStatus: geoExecutionStatusConverted,
          geoFlightGroup: geoFlightGroupConverted,
          geoFlightMissionType: geoFlightMissionTypeConverted,
          geoLandGroup: geoLandGroupConverted,
          geoObject: geoObjectConverted,
          geoOperator: geoOperatorConverted,
          geoRoute: geoRouteConverted,
          ubaUser: ubaUserConverted,
          ubmEnum: ubmEnumConverted

        );
      
return allMap;

}


// <<<<<< DB >>>>>>>>
 Future<Isar> openDB() async {

    if (Isar.instanceNames.isEmpty){
      final dir = await getApplicationSupportDirectory();
      
      return await Isar.open(
    [
      GeoFlightMissionIsarSchema, 
      GeoEventIsarSchema,
      DictTechnicalSupportIsarSchema,
      GeoAdjustmentStatusIsarSchema,
      GeoEventTypeIsarSchema,
      GeoExecutionStatusIsarSchema,
      GeoFlightGroupIsarSchema,
      GeoFlightMissionTypeIsarSchema,
      GeoLandGroupIsarSchema,
      GeoObjectIsarSchema,
      GeoOperatorIsarSchema,
      GeoRouteIsarSchema,
      UbaUserIsarSchema,
      UbmEnumIsarSchema,
      FlightMissionMaterialIsarSchema,
      FlightEventMaterialIsarSchema
    ], 
    inspector: true,
    directory: dir.path
);
    }

    return await Future.value(Isar.getInstance());
  }





  Future<List<FlightMissionMaterialIsar>> getGeoFlightMissionMaterial() async {
    
   final isar =  await db;

   
   

    return  isar.flightMissionMaterialIsars.where().findAll();
  }


  Future<List<FlightEventMaterialIsar>> getFlightEventMaterialIsar() async {
    
   final isar =  await db;

   
   

    return  isar.flightEventMaterialIsars.where().findAll();
  }

 

Future<void> pickMultipleFilesToDB(int ID) async {
  final isar =  await db;

  FilePickerResult? result = await FilePicker.platform.pickFiles(allowMultiple: true);
  List<PlatformFile>? dataFromIsar = result?.files;
  List<FlightMissionMaterialIsar> dataToReturn = dataFromIsar!.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return FlightMissionMaterialIsar(
      // Assign properties accordingly
      ID: ID,
      name:  isarData.name,
      size: isarData.size,
      extension: isarData.extension ,
      dateCreation: DateTime.now(),
      dateEdit: DateTime.now(),
      url: isarData.path,
      
      // Add other properties
    );
  }).toList();
  
  await isar.writeTxn(() async {
     await isar.flightMissionMaterialIsars.putAll(dataToReturn);
    });
}

Future<void> pickMultipleFilesEventToDB(int ID) async {
  final isar =  await db;

  FilePickerResult? result = await FilePicker.platform.pickFiles(allowMultiple: true);
  List<PlatformFile>? dataFromIsar = result?.files;
  List<FlightEventMaterialIsar> dataToReturn = dataFromIsar!.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return FlightEventMaterialIsar(
      // Assign properties accordingly
      ID: ID,
      name:  isarData.name,
      size: isarData.size,
      extension: isarData.extension ,
      dateCreation: DateTime.now(),
      dateEdit: DateTime.now(),
      url: isarData.path,
      
      // Add other properties
    );
  }).toList();
  
  await isar.writeTxn(() async {
     await isar.flightEventMaterialIsars.putAll(dataToReturn);
    });
}


//Materials 






// <<< Converters >>>

    
     

Future<List<GeoFlightMission>> getGeoFlightMissionFromIsar() async {
  List<GeoFlightMissionIsar> dataFromIsar = await getAllGeoFlightMissions();
  List<GeoFlightMission> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoFlightMission(
      // Assign properties accordingly
      ID: isarData.ID!,
      object: isarData.object ,
      state: isarData.state!,
      date: isarData.date,
      geoJson: isarData.geoJson,
      routeArea: isarData.routeArea,
      routeLength: isarData.routeLength,
      eventsState: isarData.eventsState,
      geoOperator: isarData.operator,
      num: isarData.num,
      latitude: isarData.latitude,
      longitude: isarData.longitude,
      flightDuration: isarData.flightDuration,
      flightGroupID: isarData.flightGroupID,
      landGroupID: isarData.landGroupID,
      route: isarData.route,
      technicalSupport: isarData.technicalSupport,
      flightMissionType: isarData.flightMissionType,
     latLngStartCombined: isarData.latLngStartCombined,
     nearestLocality: isarData.nearestLocality,
     responsiblePerson: isarData.responsiblePerson ?? '-',
     updateInfo: isarData.updateInfo,
     meteorologicalData: isarData.meteorologicalData,
     executionInfo: isarData.executionInfo,
     examinationDateFrom: isarData.examinationDateFrom,
     examinationDateTo: isarData.examinationDateTo,
     examinationDatesCombined: isarData.examinationDatesCombined,
     executionDeadlineDate: isarData.executionDeadlineDate,
     executionPlanDatesCombined: isarData.executionPlanDatesCombined,
     executionPlanEndDate: isarData.executionPlanEndDate,
     executionPlanStartDate: isarData.executionPlanStartDate,
     miCreateDate: isarData.miCreateDate,
     miCreateUser: isarData.miCreateUser,
     miModifyDate: isarData.miModifyDate,
     miModifyUser: isarData.miModifyUser,
     miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<GeoEventType>> getgeoEventTypeFromIsar() async {
  List<GeoEventTypeIsar> dataFromIsar = await getAllgeoEventType();
  List<GeoEventType> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoEventType(
      // Assign properties accordingly
      ID: isarData.ID!,
      color: isarData.color,
      name: isarData.name,
      objectId: isarData.objectId,
      operatorID: isarData.operatorID,
      icon: isarData.icon,
      iconDataUrl: isarData.iconDataUrl,
      iconFile: isarData.iconFile,
      miCreateDate: isarData.miCreateDate,
      miCreateUser: isarData.miCreateUser,
      miModifyDate: isarData.miModifyDate,
      miModifyUser: isarData.miModifyUser,
      miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<GeoAdjustmentStatus>> getgeoAdjustmentStatusFromIsar() async {
  List<GeoAdjustmentStatusIsar> dataFromIsar = await getAllgeoAdjustmentStatus();
  List<GeoAdjustmentStatus> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoAdjustmentStatus(
      // Assign properties accordingly
      ID: isarData.ID!,
      name: isarData.name,
      miCreateDate: isarData.miCreateDate,
      miCreateUser: isarData.miCreateUser,
      miModifyDate: isarData.miModifyDate,
      miModifyUser: isarData.miModifyUser,
      miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<GeoExecutionStatus>> getgeoExecutionStatusFromIsar() async {
  List<GeoExecutionStatusIsar> dataFromIsar = await getAllgeoExecutionStatus();
  List<GeoExecutionStatus> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoExecutionStatus(
      // Assign properties accordingly
      ID: isarData.ID!,
      name: isarData.name,
      miCreateDate: isarData.miCreateDate,
       miCreateUser: isarData.miCreateUser,
      miModifyDate: isarData.miModifyDate,
      miModifyUser: isarData.miModifyUser,
      miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<GeoFlightGroup>> getgeoFlightGroupFromIsar() async {
  List<GeoFlightGroupIsar> dataFromIsar = await getAllgeoFlightGroup();
  List<GeoFlightGroup> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoFlightGroup(
      // Assign properties accordingly
    ID: isarData.ID!,
    name: isarData.name,
     miCreateDate: isarData.miCreateDate,
     miCreateUser: isarData.miCreateUser,
     miModifyDate: isarData.miModifyDate,
     miModifyUser: isarData.miModifyUser,
     miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<GeoObject>> getgeoObjectFromIsar() async {
  List<GeoObjectIsar> dataFromIsar = await getAllgeoObject();
  List<GeoObject> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoObject(
      // Assign properties accordingly
      ID: isarData.ID!,
      attrValues: isarData.attrValues,
      geoJson: isarData.geoJson,
      color: isarData.color,
      routeLength: isarData.routeLength,
      displayName: isarData.displayName,
      name: isarData.name,
      operatorID: isarData.operatorID,
     miCreateDate: isarData.miCreateDate,
     miCreateUser: isarData.miCreateUser,
     miModifyDate: isarData.miModifyDate,
     miModifyUser: isarData.miModifyUser,
     miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<GeoOperator>> getgeoOperatorFromIsar() async {
  List<GeoOperatorIsar> dataFromIsar = await getAllgeoOperator();
  List<GeoOperator> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoOperator(
      // Assign properties accordingly
      ID: isarData.ID!,
      code: isarData.code,
      state: isarData.state,
      name: isarData.name,
      email: isarData.email,
      phone: isarData.phone,
      miCreateDate: isarData.miCreateDate,
      miCreateUser: isarData.miCreateUser,
      miModifyDate: isarData.miModifyDate,
      miModifyUser: isarData.miModifyUser,
      miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<GeoRoute>> getgeoRouteFromIsar() async {
  List<GeoRouteIsar> dataFromIsar = await getAllgeoRoute();
  List<GeoRoute> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoRoute(
      // Assign properties accordingly
      ID: isarData.ID!,
      color: isarData.color,
      geoJson: isarData.geoJson,
      name: isarData.name,
      routeLength: isarData.routeLength,
      objectID: isarData.objectID,
       miCreateDate: isarData.miCreateDate,
       miCreateUser: isarData.miCreateUser,
       miModifyDate: isarData.miModifyDate,
       miModifyUser: isarData.miModifyUser,
       miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<DictTechnicalSupport>> getdictTechnicalSupportFromIsar() async {
  List<DictTechnicalSupportIsar> dataFromIsar = await getAlldictTechnicalSupport();
  List<DictTechnicalSupport> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return DictTechnicalSupport(
      // Assign properties accordingly
    ID: isarData.ID!,
    name: isarData.name,
    miCreateDate: isarData.miCreateDate,
    miCreateUser: isarData.miCreateUser,
    miModifyDate: isarData.miModifyDate,
    miModifyUser: isarData.miModifyUser,
    miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<GeoEvent>> getgeoEventFromIsar() async {
  List<GeoEventIsar> dataFromIsar = await getAllgeoEvent();
  List<GeoEvent> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoEvent(
      // Assign properties accordingly
      ID: isarData.ID!,
      object: isarData.object,
      state: isarData.state!,
      description: isarData.description,
      geoOperator: isarData.operator,
      num: isarData.num,
      latitude: isarData.latitude,
      longitude: isarData.longitude,
      eventDate: isarData.eventDate,
      eventTypeID: isarData.eventTypeID,
      flightMissionID: isarData.flightMissionID,
      verifyDate: isarData.verifyDate,
      verifySummary: isarData.verifySummary,
      miCreateDate: isarData.miCreateDate,
       miCreateUser: isarData.miCreateUser,
       miModifyDate: isarData.miModifyDate,
       miModifyUser: isarData.miModifyUser,
       miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<GeoFlightMissionType>> getgeoFlightMissionTypeFromIsar() async {
  List<GeoFlightMissionTypeIsar> dataFromIsar = await getAllgeoFlightMissionType();
  List<GeoFlightMissionType> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoFlightMissionType(
      // Assign properties accordingly
    ID: isarData.ID!,
    color: isarData.color,
    name: isarData.name,
    operatorID: isarData.operatorID,
    miCreateDate: isarData.miCreateDate,
    miCreateUser: isarData.miCreateUser,
    miModifyDate: isarData.miModifyDate,
    miModifyUser: isarData.miModifyUser,
    miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<GeoLandGroup>> getgeoLandGroupFromIsar() async {
  List<GeoLandGroupIsar> dataFromIsar = await getAllgeoLandGroup();
  List<GeoLandGroup> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return GeoLandGroup(
      // Assign properties accordingly
    ID: isarData.ID!,
    name: isarData.name,
    operatorID: isarData.operatorID,
    miCreateDate: isarData.miCreateDate,
    miCreateUser: isarData.miCreateUser,
    miModifyDate: isarData.miModifyDate,
    miModifyUser: isarData.miModifyUser,
    miOwner: isarData.miOwner  
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<UbaUser>> getubaUserFromIsar() async {
  List<UbaUserIsar> dataFromIsar = await getAllubaUser();
  List<UbaUser> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return UbaUser(
      // Assign properties accordingly
      ID: isarData.ID,
      name: isarData.name,
      fullName: isarData.fullName,

      
      // Add other properties
    );
  }).toList();
  return dataToReturn;
}

Future<List<UbmEnum>> getubmEnumFromIsar() async {
  List<UbmEnumIsar> dataFromIsar = await getAllubmEnum();
  List<UbmEnum> dataToReturn = dataFromIsar.map((isarData) {
    // Convert GeoFlightMissionIsar to GeoFlightMission here
    return UbmEnum(
      // Assign properties accordingly
      ID: isarData.ID,
      code: isarData.code,
      eGroup: isarData.eGroup,
      name: isarData.name   
         // Add other properties
    );
  }).toList();
  return dataToReturn;
}

 Future<List<GeoFlightMission>> geoFlightMissionsConverterToModel(dynamic isarData) async {
 
  // Convert the Isar data to the model
  List<GeoFlightMission> geoFlightMissionsModel = isarData.map((isarData) {
    
    return GeoFlightMission.fromIsar(isarData);
    
  }).toList();

  return geoFlightMissionsModel;
}
////////////////////////////////////////////////////////////////////////

//  <<<< GET >>>> //



//Streams
Stream <List<FlightMissionMaterialIsar>> geoFlightMisiionsMaterialsStream () async*{
  final isar = await db;
 
  yield* isar.flightMissionMaterialIsars.where().watch(fireImmediately: true);
}

Stream <List<FlightMissionMaterialIsar>> geoFlightMisiionsMaterialsStreamByID (int? ID) async*{
  final isar = await db;
  yield* isar.flightMissionMaterialIsars.where().iDEqualTo(ID).watch(fireImmediately: true);
}



    // <<Flight Missions>>
Stream <List<GeoFlightMissionIsar>> geoFlightMisiionsStream () async*{
  final isar = await db;
  yield* isar.geoFlightMissionIsars.where().watch(fireImmediately: true);
}

Stream <List<FlightMissionMaterialIsar>> geoFlightMisiionsMaterialStreamByID (int ID) async*{
  final isar = await db;
  yield* isar.flightMissionMaterialIsars.where().iDEqualTo(ID).watch(fireImmediately: true);
}




Stream <List<GeoEventTypeIsar>> geoEventTypeStream () async*{
  final isar = await db;
  yield* isar.geoEventTypeIsars.where().watch(fireImmediately: true);
}
Stream <List<GeoAdjustmentStatusIsar>> geoAdjustmentStatusStream () async*{
  final isar = await db;
  yield* isar.geoAdjustmentStatusIsars.where().watch(fireImmediately: true);
}
Stream <List<GeoExecutionStatusIsar>> geoExecutionStatusStream () async*{
  final isar = await db;
  yield* isar.geoExecutionStatusIsars.where().watch(fireImmediately: true);
}
Stream <List<GeoFlightGroupIsar>> geoFlightGroupStream () async*{
  final isar = await db;
  yield* isar.geoFlightGroupIsars.where().watch(fireImmediately: true);
}
Stream <List<GeoObjectIsar>> geoObjectStream () async*{
  final isar = await db;
  yield* isar.geoObjectIsars.where().watch(fireImmediately: true);
}
Stream <List<GeoOperatorIsar>> geoOperatorStream () async*{
  final isar = await db;
  yield* isar.geoOperatorIsars.where().watch(fireImmediately: true);
}
Stream <List<GeoRouteIsar>> geoRouteStream () async*{
  final isar = await db;
  yield* isar.geoRouteIsars.where().watch(fireImmediately: true);
}
Stream <List<DictTechnicalSupportIsar>> dictTechnicalSupportStream () async*{
  final isar = await db;
  yield* isar.dictTechnicalSupportIsars.where().watch(fireImmediately: true);
}
Stream <List<GeoEventIsar>> geoEventStream () async*{
  final isar = await db;
  yield* isar.geoEventIsars.where().watch(fireImmediately: true);
}
Stream <List<GeoFlightMissionTypeIsar>> geoFlightMissionTypeStream () async*{
  final isar = await db;
  yield* isar.geoFlightMissionTypeIsars.where().watch(fireImmediately: true);
}
Stream <List<GeoLandGroupIsar>> geoLandGroupStream () async*{
  final isar = await db;
  yield* isar.geoLandGroupIsars.where().watch(fireImmediately: true);
}
Stream <List<UbaUserIsar>> ubaUserStream () async*{
  final isar = await db;
  yield* isar.ubaUserIsars.where().watch(fireImmediately: true);
}
Stream <List<UbmEnumIsar>> ubmEnumStream () async*{
  final isar = await db;
  yield* isar.ubmEnumIsars.where().watch(fireImmediately: true);
}
Stream <List <GeoFlightMission>> geoFlightMisiionsStreamModel () async*{
  final getAPI =  RestClient(Dio(BaseOptions(contentType: "application/json")));
  All all = await getAPI.getAll();
  final fm = all.geoFlightMission!;
  yield  fm;
}

//Futures

    // <<Flight Missions>>
Future<List<GeoFlightMissionIsar>> getAllGeoFlightMissions() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoFlightMissionIsars.where().findAll();
  }

  Future<List<GeoEventTypeIsar>> getAllgeoEventType() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoEventTypeIsars.where().findAll();
  }
  Future<List<GeoAdjustmentStatusIsar>> getAllgeoAdjustmentStatus() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoAdjustmentStatusIsars.where().findAll();
  }
  Future<List<GeoExecutionStatusIsar>> getAllgeoExecutionStatus() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoExecutionStatusIsars.where().findAll();
  }
  Future<List<GeoFlightGroupIsar>> getAllgeoFlightGroup() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoFlightGroupIsars.where().findAll();
  }
  Future<List<GeoObjectIsar>> getAllgeoObject() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoObjectIsars.where().findAll();
  }
  Future<List<GeoOperatorIsar>> getAllgeoOperator() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoOperatorIsars.where().findAll();
  }
  Future<List<GeoRouteIsar>> getAllgeoRoute() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoRouteIsars.where().findAll();
  }
  Future<List<DictTechnicalSupportIsar>> getAlldictTechnicalSupport() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.dictTechnicalSupportIsars.where().findAll();
  }
  Future<List<GeoEventIsar>> getAllgeoEvent() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoEventIsars.where().findAll();
  }
  Future<List<GeoFlightMissionTypeIsar>> getAllgeoFlightMissionType() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoFlightMissionTypeIsars.where().findAll();
  }
  Future<List<GeoLandGroupIsar>> getAllgeoLandGroup() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.geoLandGroupIsars.where().findAll();
  }
  Future<List<UbaUserIsar>> getAllubaUser() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.ubaUserIsars.where().findAll();
  }
  Future<List<UbmEnumIsar>> getAllubmEnum() async {
    
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    return isar.ubmEnumIsars.where().findAll();
  }

  Future<GeoFlightMissionIsar?> getGeoFlightMissionById(int ID) async {
   final isar =  await db;
  
   // final isar = await _isarDatabase.open();
    return  isar.geoFlightMissionIsars.filter().iDEqualTo(ID).findFirst();
  }


// <<<< Add >>>>> //

  // <<<< Add from JSON>>>


  Future<void> addGeoFlightMissionsfromJson(List<Map<String, dynamic>> geoFlightMission) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();
     
    await isar.writeTxn(() async {
     //await isar.geoFlightMissionIsars.clear();

     //if(){}
     await isar.geoFlightMissionIsars.importJson(geoFlightMission);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addgeoEventTypefromJson(List<Map<String, dynamic>> geoEventType) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
    // await isar.geoEventTypeIsars.clear();
     await isar.geoEventTypeIsars.importJson(geoEventType);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addgeoAdjustmentStatusfromJson(List<Map<String, dynamic>> geoAdjustmentStatus) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
    // await isar.geoAdjustmentStatusIsars.clear();
     await isar.geoAdjustmentStatusIsars.importJson(geoAdjustmentStatus);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addgeoExecutionStatusfromJson(List<Map<String, dynamic>> geoExecutionStatus) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
     //await isar.geoExecutionStatusIsars.clear();
     await isar.geoExecutionStatusIsars.importJson(geoExecutionStatus);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addgeoFlightGroupfromJson(List<Map<String, dynamic>> geoFlightGroup) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
   // await isar.geoFlightGroupIsars.clear();
     await isar.geoFlightGroupIsars.importJson(geoFlightGroup);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addgeoObjectfromJson(List<Map<String, dynamic>> geoObject) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
    // await isar.geoObjectIsars.clear();
     await isar.geoObjectIsars.importJson(geoObject);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addgeoOperatorfromJson(List<Map<String, dynamic>> geoOperator) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
   // await isar.geoOperatorIsars.clear();
     await isar.geoOperatorIsars.importJson(geoOperator);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addgeoRoutefromJson(List<Map<String, dynamic>> geoRoute) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
    // await isar.geoRouteIsars.clear();
     await isar.geoRouteIsars.importJson(geoRoute);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> adddictTechnicalSupportfromJson(List<Map<String, dynamic>> dictTechnicalSupport) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
    // await isar.dictTechnicalSupportIsars.clear();
     await isar.dictTechnicalSupportIsars.importJson(dictTechnicalSupport);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addgeoEventfromJson(List<Map<String, dynamic>> geoEvent) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
     //await isar.geoEventIsars.clear();
     await isar.geoEventIsars.importJson(geoEvent);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addgeoFlightMissionTypefromJson(List<Map<String, dynamic>> FlightMissionType) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
    /// await isar.geoFlightMissionTypeIsars.clear();
     await isar.geoFlightMissionTypeIsars.importJson(FlightMissionType);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addgeoLandGroupfromJson(List<Map<String, dynamic>> LandGroup) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
   // await isar.geoLandGroupIsars.clear();
     await isar.geoLandGroupIsars.importJson(LandGroup);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addubaUserfromJson(List<Map<String, dynamic>> ubaUser) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
   //  await isar.ubaUserIsars.clear();
     await isar.ubaUserIsars.importJson(ubaUser);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
  Future<void> addubmEnumfromJson(List<Map<String, dynamic>> ubmEnum) async {
    final isar =  await db;

   
    //final isar = await _isarDatabase.open();

    await isar.writeTxn(() async {
    // await isar.ubmEnumIsars.clear();
     await isar.ubmEnumIsars.importJson(ubmEnum);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }




// <<<< Update >>>> //
//<<<< Search >>>> //
//<<<< Delete >>>> //

Future<void> editGeoFlightMission( GeoFlightMissionIsar geoFlightMission) async {
  

   final isar =  await db;


   
  GeoFlightMissionIsar dataToReturn = 
    // Convert GeoFlightMissionIsar to GeoFlightMission here
 new GeoFlightMissionIsar(
      // Assign properties accordingly
      ID: geoFlightMission.ID!,
      object: geoFlightMission.object ,
      state: geoFlightMission.state!,
      date: geoFlightMission.date,
      geoJson: geoFlightMission.geoJson,
      routeArea: geoFlightMission.routeArea,
      routeLength: geoFlightMission.routeLength,
      eventsState: geoFlightMission.eventsState,
      operator: geoFlightMission.operator,
      num: geoFlightMission.num,
      latitude: geoFlightMission.latitude,
      longitude: geoFlightMission.longitude,
      flightDuration: geoFlightMission.flightDuration,
      flightGroupID: geoFlightMission.flightGroupID,
      landGroupID: geoFlightMission.landGroupID,
      route: geoFlightMission.route,
      technicalSupport: geoFlightMission.technicalSupport,
      flightMissionType: geoFlightMission.flightMissionType,
     latLngStartCombined: geoFlightMission.latLngStartCombined,
     nearestLocality: geoFlightMission.nearestLocality,
     responsiblePerson: geoFlightMission.responsiblePerson ?? '-',
     updateInfo: geoFlightMission.updateInfo,
     meteorologicalData: geoFlightMission.meteorologicalData,
     executionInfo: geoFlightMission.executionInfo,
     examinationDateFrom: geoFlightMission.examinationDateFrom,
     examinationDateTo: geoFlightMission.examinationDateTo,
     examinationDatesCombined: geoFlightMission.examinationDatesCombined,
     executionDeadlineDate: geoFlightMission.executionDeadlineDate,
     executionPlanDatesCombined: geoFlightMission.executionPlanDatesCombined,
     executionPlanEndDate: geoFlightMission.executionPlanEndDate,
     executionPlanStartDate: geoFlightMission.executionPlanStartDate,
     miCreateDate: geoFlightMission.miCreateDate,
     miCreateUser: geoFlightMission.miCreateUser,
     miModifyDate: geoFlightMission.miModifyDate,
     miModifyUser: geoFlightMission.miModifyUser,
     miOwner: geoFlightMission.miOwner  
      // Add other properties
    );
  
  
  

    await isar.writeTxn(() async {
     
     await isar.geoFlightMissionIsars.put(dataToReturn);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }

  Future<void> updateGeoFlightMission(int recordID , GeoFlightMission geoFlightMission) async {
  

   final isar =  await db;


   
  GeoFlightMissionIsar dataToReturn = 
    // Convert GeoFlightMissionIsar to GeoFlightMission here
 new GeoFlightMissionIsar(
      // Assign properties accordingly
      ID: geoFlightMission.ID,
      object: geoFlightMission.object ,
      state: geoFlightMission.state!,
      date: geoFlightMission.date,
      geoJson: geoFlightMission.geoJson,
      routeArea: geoFlightMission.routeArea,
      routeLength: geoFlightMission.routeLength,
      eventsState: geoFlightMission.eventsState,
      operator: geoFlightMission.geoOperator,
      num: geoFlightMission.num,
      latitude: geoFlightMission.latitude,
      longitude: geoFlightMission.longitude,
      flightDuration: geoFlightMission.flightDuration,
      flightGroupID: geoFlightMission.flightGroupID,
      landGroupID: geoFlightMission.landGroupID,
      route: geoFlightMission.route,
      technicalSupport: geoFlightMission.technicalSupport,
      flightMissionType: geoFlightMission.flightMissionType,
     latLngStartCombined: geoFlightMission.latLngStartCombined,
     nearestLocality: geoFlightMission.nearestLocality,
     responsiblePerson: geoFlightMission.responsiblePerson ?? '-',
     updateInfo: geoFlightMission.updateInfo,
     meteorologicalData: geoFlightMission.meteorologicalData,
     executionInfo: geoFlightMission.executionInfo,
     examinationDateFrom: geoFlightMission.examinationDateFrom,
     examinationDateTo: geoFlightMission.examinationDateTo,
     examinationDatesCombined: geoFlightMission.examinationDatesCombined,
     executionDeadlineDate: geoFlightMission.executionDeadlineDate,
     executionPlanDatesCombined: geoFlightMission.executionPlanDatesCombined,
     executionPlanEndDate: geoFlightMission.executionPlanEndDate,
     executionPlanStartDate: geoFlightMission.executionPlanStartDate,
     miCreateDate: geoFlightMission.miCreateDate,
     miCreateUser: geoFlightMission.miCreateUser,
     miModifyDate: geoFlightMission.miModifyDate,
     miModifyUser: geoFlightMission.miModifyUser,
     miOwner: geoFlightMission.miOwner  
      // Add other properties
    );
  
  
  

    await isar.writeTxn(() async {
     
     await isar.geoFlightMissionIsars.put(dataToReturn);
    });
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }


   Future <void> updateGeoFlightMissions(List<GeoFlightMissionIsar> geoFlightMissions) async {
    final isar =  await db;
    //final isar = await _isarDatabase.open();
    await isar.writeTxn(() async {
      await isar.geoFlightMissionIsars.putAll(geoFlightMissions);
    });

    //await isar.geoFlightMissionCollection.putAll(geoFlightMissions);
  }
/*
Future<void> getGeoFlightMissionFromID(int ID) async {
    final isar =  await db;
    await isar.writeTxn(() async {
     await isar.geoFlightMissionIsars.getByIndex('ID', ID);
    });
  
    //await isar.geoFlightMissionCollection.put(geoFlightMission);
  }
*/
  Future<void> deleteGeoFlightMission(int ID) async {
    final isar =  await db;
    await isar.writeTxn(() async {
     await isar.geoFlightMissionIsars.where().iDEqualTo(ID).deleteFirst();
    });
    //await isar.geoFlightMissionCollection.where().idEqualTo(id).remove();
  }


    


  

  




}
